import React, { useEffect, useState } from "react";
import { useData } from "./DataContext";
import localforage from  "localforage";
import { Stream } from "@mui/icons-material";
import axios from "axios";

function SkuSetter() {
    const {
        serverState, setServerState, ServerManagementEnabled, setServerManagementEnabled, DesktopPatchingEnabled, setDesktopPatchingEnabled,
        EucEnabled, setEucEnabled, BackupsEnabled, setBackupsEnabled, KeeperEnabled, setKeeperEnabled, EmailEnabled, setEmailEnabled,
        SiemEnabled, setSiemEnabled, EndpointProtectionEnabled, setEndpointProtectionEnabled, NetworkManagementEnabled, setNetworkManagementEnabled,
        DataCenterOnPremises, setDataCenterOnPremises, DataCenterChoiceCloud, setDataCenterChoiceCloud, DataCenterMicrosoftAzure, setDataCenterMicrosoftAzure,
        EucUsers, setEucUsers, HsdImages, setHsdImages, VdiImages, setVdiImages, EucType, setEucType, CitrixType, setCitrixType,
        ServerManagementQuantity, setServerManagementQuantity, DesktopPatchingQuantity, setDesktopPatchingQuantity, ServerPatchingOnly, setServerPatchingOnly,
        KeeperInstances, setKeeperInstances, EmailProtection, setEmailProtection, EndpointProtectionServers, setEndpointProtectionServers,
        EndpointProtectionDesktops, setEndpointProtectionDesktops, NetworkSwitches, setNetworkSwitches, NetworkFirewalls, setNetworkFirewalls,
        NetworkFirewallsQty, setNetworkFirewallsQty, PaloAltoLicenses, setPaloAltoLicenses, SecSeries, setSecSeries,
        NetworkLoadBalancers, setNetworkLoadBalancers, adpEnabled, setAdpEnabled, adpRubricState, setadpRubricState, rubricOptions,
        adpTier, setAdpTier, adpBand, setAdpBand, adpNodeCount, setadpNodeCount, adpStorage, setAdpStorage, CCColoSpace, setCCColoSpace,
        RackUSpace, setRackUSpace, NPlus1Required, setNPlus1Required, BackupAsAServiceUnitrends, setBackupAsAServiceUnitrends, BackupTotalStorageUnitrends, setBackupTotalStorageUnitrends,
        BackupProtectedDevicesUnitrends, setBackupProtectedDevicesUnitrends, CustBackupOnsiteDevicesUnitrends, setCustBackupOnsiteDevicesUnitrends, BusinessContinuityDisasterRelief, setBusinessContinuityDisasterRelief,
        BackupPrimaryLocationUnitrends, setBackupPrimaryLocationUnitrends, BackupAlternateLocationUnitrends, setBackupAlternateLocationUnitrends, BCDRType, setBCDRType, BCDRTarget, setBCDRTarget, AssuredDP, setAssuredDP, AssuredDPData, setAssuredDPData,
        Adc, setAdc, AdcCitrix, setAdcCitrix, AdcCitrixCount, setAdcCitrixCount, AdcCitrixCpu, setAdcCitrixCpu, AdcCitrixMem, setAdcCitrixMem,
        LoadBalancerWoCitrix, setLoadBalancerWoCitrix, LoadBalancerWoCitrixCount, setLoadBalancerWoCitrixCount, LoadBalancerWoCitrixCpu, setLoadBalancerWoCitrixCpu,
        LoadBalancerWoCitrixMem, setLoadBalancerWoCitrixMem, CcTier1Storage, setCcTier1Storage, MsWindowsServerTotalQty, setMsWindowsServerTotalQty,
        HostedVdiServerTotalQty, setHostedVdiServerTotalQty, UnixLinuxServerTotalQty, setUnixLinuxServerTotalQty,
        NetworkAppServerTotalQty, setNetworkAppServerTotalQty, Tier1StorageServerTotalQty, setTier1StorageServerTotalQty,
        Tier2StorageServerTotalQty, setTier2StorageServerTotalQty, VcpuServerTotalQty, setVcpuServerTotalQty, VmemoryServerTotalQty, setVmemoryServerTotalQty,
        CitrixSqlServerTotalQty, setCitrixSqlServerTotalQty, SqlServerLicenseServerTotalQty, setSqlServerLicenseServerTotalQty,
        SqlServerEnterpriseLicenseServerTotalQty, setSqlServerEnterpriseLicenseServerTotalQty, CitrixInfrastructureServerTotalQty, setCitrixInfrastructureServerTotalQty,
        ServerTotalsVcpu, setServerTotalsVcpu, ServerTotalsMemory, setServerTotalsMemory, ServerTotalsTier1Storage, setServerTotalsTier1Storage, ServerTotalsTier2Storage, setServerTotalsTier2Storage,
        M365Backups, setM365Backups, M365BackupsUsers, setM365BackupsUsers, M365BackupsType, setM365BackupsType, CloudBackup, setCloudBackup, CloudBackupData, setCloudBackupData,
        CloudBackupType, setCloudBackupType, SystemInfrastructure, setSystemInfrastructure, Hypervisor, setHypervisor, HypervisorTotal, setHypervisorTotal,
        HypervisorManagement, setHypervisorManagement, HypervisorManagementType, setHypervisorManagementType, AzureCost, setAzureCost, DevicesEnabled, setDevicesEnabled,
        AdditionalNetworkFirewalls, setAdditionalNetworkFirewalls, AdditionalNetworkFirewallsQty, setAdditionalNetworkFirewallsQty, ManagedNetworkSwitches, setManagedNetworkSwitches,
        ManagedNetworkSwitchesQty, setManagedNetworkSwitchesQty, NetworkWifi, setNetworkWifi, NetworkWifiQty, setNetworkWifiQty,
        AdcLicensing, setAdcLicensing, AdcLicensingQty, setAdcLicensingQty, AdcLicensingType, setAdcLicensingType, AdcLicensingVersion, setAdcLicensingVersion,
        AdcAdm, setAdcAdm, AdcAdmCount, setAdcAdmCount, AdcVserver, setAdcVserver, AdcVserverCount, setAdcVserverCount, CitrixLicense, setCitrixLicense,
        CitrixLicenseType, setCitrixLicenseType, ContentCollaberation, setContentCollaberation, ContentCollaberationCount, setContentCollaberationCount,
        EucType2, setEucType2, Nerdio, setNerdio, AvdUsers, setAvdUsers, Contract8x5or12x6, setContract8x5or12x6, Contract8x5or12x6Type, setContract8x5or12x6Type,
        ActiveDirectoryManagement, setActiveDirectoryManagement, ActiveDirectoryManagementOptions, setActiveDirectoryManagementOptions, ActiveDirectoryManagementCount, setActiveDirectoryManagementCount,
        RemoteDesktops, setRemoteDesktops, RemoteDesktopsQty, setRemoteDesktopsQty, ManagedPrintServices, setManagedPrintServices, ManagedPrintServicesLocations, setManagedPrintServicesLocations,
        ManagedPrintServicesUsers, setManagedPrintServicesUsers, EmailTraining, setEmailTraining, EmailLicense, setEmailLicense,
        RiskManagement, setRiskManagement, RiskManagementCount, setRiskManagementCount, SiemCount, setSiemCount, SiemSensorAppliance, setSiemSensorAppliance, SiemSensorApplianceCount, setSiemSensorApplianceCount,
        FirewallTP, setFirewallTP, FirewallWMA, setFirewallWMA, FirewallUrlFiltering, setFirewallUrlFiltering, FirewallDnsSecurity, setFirewallDnsSecurity,
        FirewallSdWan, setFirewallSdWan, FirewallGlobalProtect, setFirewallGlobalProtect, FirewallVPfM, setFirewallVPfM,
        Huntress, setHuntress, HuntressCount, setHuntressCount, HuntressM365Support, setHuntressM365Support, ZeroTrust, setZeroTrust, ZeroTrustCount, setZeroTrustCount,
        Microsoft365, Microsoft365BusBasic, setMicrosoft365BusBasic, Microsoft365BusBasicCount, setMicrosoft365BusBasicCount, Microsoft365BusBasicType, setMicrosoft365BusBasicType,
        Microsoft365BusPremium, setMicrosoft365BusPremium, Microsoft365BusPremiumCount, setMicrosoft365BusPremiumCount, Microsoft365BusPremiumType, setMicrosoft365BusPremiumType,
        Microsoft365BusStandard, setMicrosoft365BusStandard, Microsoft365BusStandardCount, setMicrosoft365BusStandardCount, Microsoft365BusStandardType, setMicrosoft365BusStandardType,
        Microsoft365E3, setMicrosoft365E3, Microsoft365E3Count, setMicrosoft365E3Count, Microsoft365E3Type, setMicrosoft365E3Type, Microsoft365E5, setMicrosoft365E5, Microsoft365E5Count, setMicrosoft365E5Count,
        Microsoft365E5Type, setMicrosoft365E5Type, StreamlineIT, setStreamlineIT, StreamlineITCount, setStreamlineITCount, TraditionalMspEnabled, setTraditionalMspEnabled,
        Microsoft365Enabled, setMicrosoft365Enabled, MiscellaneousEnabled, setMiscellaneousEnabled, NeedsOnboarding, setNeedsOnboarding, SowRequired, setSowRequired, SecurityEnabled, setSecurityEnabled,
        ZeroTrustStorageControl, setZeroTrustStorageControl, ZeroTrustElevation, setZeroTrustElevation, ZeroTrustThreatLockerDetect, setZeroTrustThreatLockerDetect,
        newRows, setNewRows, resetServerState, adpCustOwnedType, setAdpCustOwnedType, custOwnedTypeOptions, adpCustOwnedLocations, setAdpCustOwnedLocations, adpCustOwnedCustomPrice, setAdpCustOwnedCustomPrice, skuConfig
    } = useData()

    const [skus, setSkus] = useState([]);

    useEffect(() => {
        const fetchSkus = async () => {
            try {
                const response = await axios.get('/db/sku');
                setSkus(response.data);
            } catch (error) {
                console.error('Error fetching SKUs:', error);
            }
        };

        fetchSkus();
    }, []);


    // Setting skus from System Infrastructure section in the Excel spreadsheet
    // useEffect that sets the MS - Hypervisor sku in local forage
    useEffect(() => {
        const sku = skus.find(sku => sku.id === 140); // ID for "MS - Hypervisor"
        if (sku) {
            if (Hypervisor) {
                localforage.setItem(sku.sku, HypervisorTotal);
            } else {
                localforage.removeItem(sku.sku);
            }
        }
    }, [Hypervisor, HypervisorTotal, skus]);

    // useEffect that sets the MS - Nutanix-PC sku in local forage
    useEffect(() => {
        if (HypervisorManagement && HypervisorManagementType === "NutanixPrismCentral") {
            localforage.setItem("MS - Nutanix-PC", 1);
        } else {
            localforage.removeItem("MS - Nutanix-PC");
        }
    }, [HypervisorManagement, HypervisorManagementType])

    // useEffect that sets the MS - VMware-VC sku in local forage
    useEffect(() => {
        if (HypervisorManagement && HypervisorManagementType === "VMwareVcenter") {
            localforage.setItem("MS - VMware-VC", 1);
        } else {
            localforage.removeItem("MS - VMware-VC");
        }
    }, [HypervisorManagement, HypervisorManagementType])

    // useEffect that sets the MS - Azure Infrastructure and MS - Azure Infra Monitoring & Mgmt skus in local forage
    useEffect(() => {
        if (DataCenterMicrosoftAzure) {
            localforage.setItem("MS - Azure Infrastructure", AzureCost);
            localforage.setItem("MS - Azure Infra Monitoring & Mgmt", AzureCost);
        } else {
            localforage.removeItem("MS - Azure Infrastructure");
            localforage.removeItem("MS - Azure Infra Monitoring & Mgmt");
        }
    }, [DataCenterMicrosoftAzure, AzureCost])

    // Setting skus from Choice Cloud section in the Excel spreadsheet
    // useEffect that sets CC - Colo Space Storage sku in localForage
    useEffect(() => {
        if (CCColoSpace) {
            localforage.setItem("CC - Colo Space", RackUSpace);
        } else {
            localforage.removeItem("CC - Colo Space");
        }
    }, [CCColoSpace, RackUSpace])

    // useEffect that sets CC - Tier 1 Storage sku in localForage
    useEffect(() => {
        if (Tier1StorageServerTotalQty > 0) {
            localforage.setItem("CC - Tier 1 Storage", Tier1StorageServerTotalQty);
        } else {
            localforage.removeItem("CC - Tier 1 Storage");
        }
    }, [Tier1StorageServerTotalQty])

    // useEffect that sets CC - Tier 2 Storage sku in localForage
    useEffect(() => {
        if (DataCenterChoiceCloud && BackupAsAServiceUnitrends) {
            localforage.setItem("CC - Tier 2 Storage", Tier2StorageServerTotalQty);
        } else if (BackupAsAServiceUnitrends && BackupPrimaryLocationUnitrends != '') {
            localforage.setItem("CC - Tier 2 Storage", BackupTotalStorageUnitrends * 3 * 1024)
        } else {
            localforage.removeItem("CC - Tier 2 Storage");
        }
    }, [DataCenterChoiceCloud, BackupAsAServiceUnitrends, Tier2StorageServerTotalQty, BackupPrimaryLocationUnitrends, BackupTotalStorageUnitrends])

    // useEffect that sets CC - Tier 2 Backup sku in localForage
    useEffect(() => {
        if (adpEnabled && adpTier == 2 && DataCenterChoiceCloud) {
            localforage.setItem("CC - Tier 2 Backup", adpStorage);
        } else {
            localforage.removeItem("CC - Tier 2 Backup");
        }
    }, [adpEnabled, adpTier, DataCenterChoiceCloud, adpStorage])

    // useEffect that sets CC - Tier 3 Backup sku in localForage
    useEffect(() => {
        if (adpEnabled && adpTier == 3 && DataCenterChoiceCloud) {
            localforage.setItem("CC - Tier 3 Backup", adpStorage);
        } else {
            localforage.removeItem("CC - Tier 3 Backup");
        }
    }, [adpEnabled, adpTier, DataCenterChoiceCloud, adpStorage])

    // useEffect that sets CC - ADP - Tier 1 skus if localForage
    useEffect(() => {
        const CTierKey = `${skuConfig[162]}-162`;
        const DTierKey = `${skuConfig[164]}-164`;
        const ETierKey = `${skuConfig[166]}-166`;


        if (adpEnabled && adpTier == 1 && adpRubricState === "rent" && !DataCenterChoiceCloud) {
            if (adpStorage > 18 && adpStorage <= 28) {
                localforage.setItem(CTierKey, 1);
                localforage.removeItem(DTierKey);
                localforage.removeItem(ETierKey);
            } else if (adpStorage > 28 && adpStorage <= 38) {
                localforage.setItem(DTierKey, 1);
                localforage.removeItem(CTierKey);
                localforage.removeItem(ETierKey);
            } else if (adpStorage > 38 && adpStorage <= 48) {
                localforage.setItem(ETierKey, 1);
                localforage.removeItem(CTierKey);
                localforage.removeItem(DTierKey);
            } else {
                localforage.removeItem(CTierKey);
                localforage.removeItem(DTierKey);
                localforage.removeItem(ETierKey);
            }
        } else {
            localforage.removeItem(CTierKey);
            localforage.removeItem(DTierKey);
            localforage.removeItem(ETierKey);
        }
    }, [adpEnabled, adpTier, adpRubricState, adpStorage])

    // useEffect that sets CC - ADP - Tier 2 skus if localForage
    useEffect(() => {
        const ATierKey = `${skuConfig[168]}-168`;
        const BTierKey = `${skuConfig[170]}-170`;
        const CTierKey = `${skuConfig[172]}-172`;
        const DTierKey = `${skuConfig[174]}-174`;
        const ETierKey = `${skuConfig[176]}-176`;

        if (adpEnabled && adpTier == 2 && adpRubricState === "rent" && !DataCenterChoiceCloud) {
            if (adpStorage <= 8) {
                localforage.setItem(ATierKey, 1);
                localforage.removeItem(BTierKey);
                localforage.removeItem(CTierKey);
                localforage.removeItem(DTierKey);
                localforage.removeItem(ETierKey);
            } else if (adpStorage > 8 && adpStorage <= 18) {
                localforage.setItem(BTierKey, 1);
                localforage.removeItem(ATierKey);
                localforage.removeItem(CTierKey);
                localforage.removeItem(DTierKey);
                localforage.removeItem(ETierKey);
            } else if (adpStorage > 18 && adpStorage <= 28) {
                localforage.setItem(CTierKey, 1);
                localforage.removeItem(ATierKey);
                localforage.removeItem(BTierKey);
                localforage.removeItem(DTierKey);
                localforage.removeItem(ETierKey);
            } else if (adpStorage > 28 && adpStorage <= 38) {
                localforage.setItem(DTierKey, 1);
                localforage.removeItem(ATierKey);
                localforage.removeItem(BTierKey);
                localforage.removeItem(CTierKey);
                localforage.removeItem(ETierKey);
            } else if (adpStorage > 38 && adpStorage <= 48) {
                localforage.setItem(ETierKey, 1);
                localforage.removeItem(ATierKey);
                localforage.removeItem(BTierKey);
                localforage.removeItem(CTierKey);
                localforage.removeItem(DTierKey);
            } else {
                localforage.removeItem(ATierKey);
                localforage.removeItem(BTierKey);
                localforage.removeItem(CTierKey);
                localforage.removeItem(DTierKey);
                localforage.removeItem(ETierKey);
            }
        } else {
            localforage.removeItem(ATierKey);
            localforage.removeItem(BTierKey);
            localforage.removeItem(CTierKey);
            localforage.removeItem(DTierKey);
            localforage.removeItem(ETierKey);
        }
    }, [adpEnabled, adpTier, adpRubricState, adpStorage])

    // useEffect that sets CC - ADP - Tier 3 skus if localForage
    useEffect(() => {
        const ATierKey = `${skuConfig[178]}-178`;
        const BTierKey = `${skuConfig[180]}-180`;
        const CTierKey = `${skuConfig[182]}-182`;
        const DTierKey = `${skuConfig[184]}-184`;
        const ETierKey = `${skuConfig[186]}-186`;

        if (adpEnabled && adpTier == 3 && adpRubricState === "rent" && !DataCenterChoiceCloud) {
            if (adpStorage <= 8) {
                localforage.setItem(ATierKey, 1);
                localforage.removeItem(BTierKey);
                localforage.removeItem(CTierKey);
                localforage.removeItem(DTierKey);
                localforage.removeItem(ETierKey);
            } else if (adpStorage > 8 && adpStorage <= 18) {
                localforage.setItem(BTierKey, 1);
                localforage.removeItem(ATierKey);
                localforage.removeItem(CTierKey);
                localforage.removeItem(DTierKey);
                localforage.removeItem(ETierKey);
            } else if (adpStorage > 18 && adpStorage <= 28) {
                localforage.setItem(CTierKey, 1);
                localforage.removeItem(ATierKey);
                localforage.removeItem(BTierKey);
                localforage.removeItem(DTierKey);
                localforage.removeItem(ETierKey);
            } else if (adpStorage > 28 && adpStorage <= 38) {
                localforage.setItem(DTierKey, 1);
                localforage.removeItem(ATierKey);
                localforage.removeItem(BTierKey);
                localforage.removeItem(CTierKey);
                localforage.removeItem(ETierKey);
            } else if (adpStorage > 38 && adpStorage <= 48) {
                localforage.setItem(ETierKey, 1);
                localforage.removeItem(ATierKey);
                localforage.removeItem(BTierKey);
                localforage.removeItem(CTierKey);
                localforage.removeItem(DTierKey);
            } else {
                localforage.removeItem(ATierKey);
                localforage.removeItem(BTierKey);
                localforage.removeItem(CTierKey);
                localforage.removeItem(DTierKey);
                localforage.removeItem(ETierKey);
            }
        } else {
            localforage.removeItem(ATierKey);
            localforage.removeItem(BTierKey);
            localforage.removeItem(CTierKey);
            localforage.removeItem(DTierKey);
            localforage.removeItem(ETierKey);
        }
    }, [adpEnabled, adpTier, adpRubricState, adpStorage])

    // useEffect that sets CC - ADP - Cust Owned - Rubrik - Tier skus in localForage
    useEffect(() => {
        const tier1Key = `${skuConfig[681]}-681`;
        const tier2Key = `${skuConfig[683]}-683`;
        const tier3Key = `${skuConfig[685]}-685`;
        const tier4Key = `${skuConfig[687]}-687`;

        if (adpEnabled && adpRubricState === "own" && adpCustOwnedType === "rubrik" && !DataCenterChoiceCloud) {
            if (adpNodeCount <= 12) {
                localforage.setItem(tier1Key, adpNodeCount);
                localforage.removeItem(tier2Key);
                localforage.removeItem(tier3Key);
                localforage.removeItem(tier4Key);
            } else if (adpNodeCount > 12 && adpNodeCount <= 100) {
                localforage.setItem(tier1Key, 12);
                localforage.setItem(tier2Key, adpNodeCount - 12);
                localforage.removeItem(tier3Key);
                localforage.removeItem(tier4Key);
            } else if (adpNodeCount > 100 && adpNodeCount <= 400) {
                localforage.setItem(tier1Key, 12);
                localforage.setItem(tier2Key, 88);
                localforage.setItem(tier3Key, adpNodeCount - 100);
                localforage.removeItem(tier4Key);
            } else if (adpNodeCount > 400) {
                localforage.setItem(tier1Key, 12);
                localforage.setItem(tier2Key, 88);
                localforage.setItem(tier3Key, 300);
                localforage.setItem(tier4Key, adpNodeCount - 400);
            } else {
                localforage.removeItem(tier1Key);
                localforage.removeItem(tier2Key);
                localforage.removeItem(tier3Key);
                localforage.removeItem(tier3Key);
            }
        } else {
            localforage.removeItem(tier1Key);
            localforage.removeItem(tier2Key);
            localforage.removeItem(tier3Key);
            localforage.removeItem(tier4Key);
        }
    }, [adpEnabled, adpRubricState, adpCustOwnedType, adpNodeCount])

    // useEffect that sets CC - ADP - Cust Owned - Public Cloud - Tier skus in localForage
    useEffect(() => {
        const tier1Key = `${skuConfig[689]}-689`;
        const tier2Key = `${skuConfig[691]}-691`;
        const tier3Key = `${skuConfig[693]}-693`;
        const tier4Key = `${skuConfig[695]}-695`;

        if (adpEnabled && adpRubricState === "own" && adpCustOwnedType === "public_cloud" && !DataCenterChoiceCloud) {
            if (adpNodeCount <= 50) {
                localforage.setItem(tier1Key, adpNodeCount);
                localforage.removeItem(tier2Key);
                localforage.removeItem(tier3Key);
                localforage.removeItem(tier4Key);
            } else if (adpNodeCount > 51 && adpNodeCount <= 200) {
                localforage.setItem(tier1Key, 50);
                localforage.setItem(tier2Key, adpNodeCount - 50);
                localforage.removeItem(tier3Key);
                localforage.removeItem(tier4Key);
            } else if (adpNodeCount > 200 && adpNodeCount <= 600) {
                localforage.setItem(tier1Key, 50);
                localforage.setItem(tier2Key, 150);
                localforage.setItem(tier3Key, adpNodeCount - 200);
                localforage.removeItem(tier4Key);
            } else if (adpNodeCount > 600 && adpCustOwnedCustomPrice > 0) {
                localforage.setItem(tier4Key, adpCustOwnedCustomPrice);
                localforage.setItem(tier1Key, 50);
                localforage.setItem(tier2Key, 150);
                localforage.setItem(tier3Key, 400);
            }
            else {
                localforage.removeItem(tier1Key);
                localforage.removeItem(tier2Key);
                localforage.removeItem(tier3Key);
                localforage.removeItem(tier4Key);
            }
        } else {
            localforage.removeItem(tier1Key);
            localforage.removeItem(tier2Key);
            localforage.removeItem(tier3Key);
            localforage.removeItem(tier4Key);
        }
    }, [adpEnabled, adpRubricState, adpCustOwnedType, adpNodeCount, adpCustOwnedCustomPrice])

    // useEffect that sets CC - ADP - Cust Owned - Other License - Tier skus in localForage
    useEffect(() => {
        const tier1Key = `${skuConfig[697]}-697`;
        const tier2Key = `${skuConfig[699]}-699`;
        const tier3Key = `${skuConfig[701]}-701`;
        const tier4Key = `${skuConfig[703]}-703`;

        if (adpEnabled && adpRubricState === "own" && adpCustOwnedType === "customer_owned" && !DataCenterChoiceCloud) {
            if (adpNodeCount <= 50) {
                localforage.setItem(tier1Key, adpNodeCount);
                localforage.removeItem(tier2Key);
                localforage.removeItem(tier3Key);
                localforage.removeItem(tier4Key);
            } else if (adpNodeCount > 51 && adpNodeCount <= 200) {
                localforage.setItem(tier1Key, 50);
                localforage.setItem(tier2Key, adpNodeCount - 50);
                localforage.removeItem(tier3Key);
                localforage.removeItem(tier4Key);
            } else if (adpNodeCount > 200 && adpNodeCount <= 600) {
                localforage.setItem(tier1Key, 50);
                localforage.setItem(tier2Key, 150);
                localforage.setItem(tier3Key, adpNodeCount - 200);
                localforage.removeItem(tier4Key);
            } else if (adpNodeCount > 600 && adpCustOwnedCustomPrice > 0) {
                localforage.setItem(tier4Key, adpCustOwnedCustomPrice);
                localforage.setItem(tier1Key, 50);
                localforage.setItem(tier2Key, 150);
                localforage.setItem(tier3Key, 400);
            }
            else {
                localforage.removeItem(tier1Key);
                localforage.removeItem(tier2Key);
                localforage.removeItem(tier3Key);
                localforage.removeItem(tier4Key);
            }
        } else {
            localforage.removeItem(tier1Key);
            localforage.removeItem(tier2Key);
            localforage.removeItem(tier3Key);
            localforage.removeItem(tier4Key);
        }
    }, [adpEnabled, adpRubricState, adpCustOwnedType, adpNodeCount, adpCustOwnedCustomPrice])

    // useEffect that sets the CC - ADP - Customer Owned Rubric Base Mgmt sku in localForage
    useEffect(() => {
        if (adpEnabled && adpRubricState === 'own' && !DataCenterChoiceCloud && adpStorage > 0) {
            localforage.setItem("CC - ADP - Customer Owned Rubric Base Mgmt", 1);
        } else {
            localforage.removeItem("CC - ADP - Customer Owned Rubric Base Mgmt");
        }
    }, [adpEnabled, adpRubricState])


    // useEffect that sets the foundation consumption model skus in localForage
    useEffect(() => {
        const tier1Key = `${skuConfig[609]}-609`;
        const tier2Key = `${skuConfig[611]}-611`;
        const tier3Key = `${skuConfig[613]}-613`;
        const tier4Key = `${skuConfig[615]}-615`;
        const tier5Key = `${skuConfig[617]}-617`;
        const tier6Key = `${skuConfig[619]}-619`;
        const tier7Key = `${skuConfig[621]}-621`;

        if (adpEnabled && adpRubricState === "consumption" && adpTier == 1 && !DataCenterChoiceCloud) {
            if (adpStorage <= 200) {
                localforage.setItem(tier1Key, adpStorage);
                localforage.removeItem(tier2Key);
                localforage.removeItem(tier3Key);
                localforage.removeItem(tier4Key);
                localforage.removeItem(tier5Key);
                localforage.removeItem(tier6Key);
                localforage.removeItem(tier7Key);
            } else if (adpStorage > 200 && adpStorage <= 400) {
                localforage.setItem(tier2Key, adpStorage);
                localforage.removeItem(tier1Key);
                localforage.removeItem(tier3Key);
                localforage.removeItem(tier4Key);
                localforage.removeItem(tier5Key);
                localforage.removeItem(tier6Key);
                localforage.removeItem(tier7Key);
            } else if (adpStorage > 400 && adpStorage <= 600) {
                localforage.setItem(tier3Key, adpStorage);
                localforage.removeItem(tier1Key);
                localforage.removeItem(tier2Key);
                localforage.removeItem(tier4Key);
                localforage.removeItem(tier5Key);
                localforage.removeItem(tier6Key);
                localforage.removeItem(tier7Key);
            } else if (adpStorage > 600 && adpStorage <= 1000) {
                localforage.setItem(tier4Key, adpStorage);
                localforage.removeItem(tier1Key);
                localforage.removeItem(tier2Key);
                localforage.removeItem(tier3Key);
                localforage.removeItem(tier5Key);
                localforage.removeItem(tier6Key);
                localforage.removeItem(tier7Key);
            } else if (adpStorage > 1000 && adpStorage <= 1500) {
                localforage.setItem(tier5Key, adpStorage);
                localforage.removeItem(tier1Key);
                localforage.removeItem(tier2Key);
                localforage.removeItem(tier3Key);
                localforage.removeItem(tier4Key);
                localforage.removeItem(tier6Key);
                localforage.removeItem(tier7Key);
            } else if (adpStorage > 1500 && adpStorage <= 2000) {
                localforage.setItem(tier6Key, adpStorage);
                localforage.removeItem(tier1Key);
                localforage.removeItem(tier2Key);
                localforage.removeItem(tier3Key);
                localforage.removeItem(tier4Key);
                localforage.removeItem(tier5Key);
                localforage.removeItem(tier7Key);
            } else if (adpStorage > 2000) {
                localforage.setItem(tier7Key, adpStorage);
                localforage.removeItem(tier1Key);
                localforage.removeItem(tier2Key);
                localforage.removeItem(tier3Key);
                localforage.removeItem(tier4Key);
                localforage.removeItem(tier5Key);
                localforage.removeItem(tier6Key);
            } else {
                localforage.removeItem(tier1Key);
                localforage.removeItem(tier2Key);
                localforage.removeItem(tier3Key);
                localforage.removeItem(tier4Key);
                localforage.removeItem(tier5Key);
                localforage.removeItem(tier6Key);
                localforage.removeItem(tier7Key);
            }
        } else {
            localforage.removeItem(tier1Key);
            localforage.removeItem(tier2Key);
            localforage.removeItem(tier3Key);
            localforage.removeItem(tier4Key);
            localforage.removeItem(tier5Key);
            localforage.removeItem(tier6Key);
            localforage.removeItem(tier7Key);
        }
    }, [adpStorage, adpEnabled, adpRubricState, adpTier])

    useEffect(() => {
        const tier1Key = `${skuConfig[623]}-623`;
        const tier2Key = `${skuConfig[625]}-625`;
        const tier3Key = `${skuConfig[627]}-627`;
        const tier4Key = `${skuConfig[629]}-629`;
        const tier5Key = `${skuConfig[631]}-631`;
        const tier6Key = `${skuConfig[633]}-633`;
        const tier7Key = `${skuConfig[635]}-635`;

        if (adpEnabled && adpRubricState === "consumption" && adpTier == 2 && !DataCenterChoiceCloud) {
            if (adpStorage <= 200) {
                localforage.setItem(tier1Key, adpStorage);
                localforage.removeItem(tier2Key);
                localforage.removeItem(tier3Key);
                localforage.removeItem(tier4Key);
                localforage.removeItem(tier5Key);
                localforage.removeItem(tier6Key);
                localforage.removeItem(tier7Key);
            } else if (adpStorage > 200 && adpStorage <= 400) {
                localforage.setItem(tier2Key, adpStorage);
                localforage.removeItem(tier1Key);
                localforage.removeItem(tier3Key);
                localforage.removeItem(tier4Key);
                localforage.removeItem(tier5Key);
                localforage.removeItem(tier6Key);
                localforage.removeItem(tier7Key);
            } else if (adpStorage > 400 && adpStorage <= 600) {
                localforage.setItem(tier3Key, adpStorage);
                localforage.removeItem(tier1Key);
                localforage.removeItem(tier2Key);
                localforage.removeItem(tier4Key);
                localforage.removeItem(tier5Key);
                localforage.removeItem(tier6Key);
                localforage.removeItem(tier7Key);
            } else if (adpStorage > 600 && adpStorage <= 1000) {
                localforage.setItem(tier4Key, adpStorage);
                localforage.removeItem(tier1Key);
                localforage.removeItem(tier2Key);
                localforage.removeItem(tier3Key);
                localforage.removeItem(tier5Key);
                localforage.removeItem(tier6Key);
                localforage.removeItem(tier7Key);
            } else if (adpStorage > 1000 && adpStorage <= 1500) {
                localforage.setItem(tier5Key, adpStorage);
                localforage.removeItem(tier1Key);
                localforage.removeItem(tier2Key);
                localforage.removeItem(tier3Key);
                localforage.removeItem(tier4Key);
                localforage.removeItem(tier6Key);
                localforage.removeItem(tier7Key);
            } else if (adpStorage > 1500 && adpStorage <= 2000) {
                localforage.setItem(tier6Key, adpStorage);
                localforage.removeItem(tier1Key);
                localforage.removeItem(tier2Key);
                localforage.removeItem(tier3Key);
                localforage.removeItem(tier4Key);
                localforage.removeItem(tier5Key);
                localforage.removeItem(tier7Key);
            } else if (adpStorage > 2000) {
                localforage.setItem(tier7Key, adpStorage);
                localforage.removeItem(tier1Key);
                localforage.removeItem(tier2Key);
                localforage.removeItem(tier3Key);
                localforage.removeItem(tier4Key);
                localforage.removeItem(tier5Key);
                localforage.removeItem(tier6Key);
            } else {
                localforage.removeItem(tier1Key);
                localforage.removeItem(tier2Key);
                localforage.removeItem(tier3Key);
                localforage.removeItem(tier4Key);
                localforage.removeItem(tier5Key);
                localforage.removeItem(tier6Key);
                localforage.removeItem(tier7Key);
            }
        } else {
            localforage.removeItem(tier1Key);
            localforage.removeItem(tier2Key);
            localforage.removeItem(tier3Key);
            localforage.removeItem(tier4Key);
            localforage.removeItem(tier5Key);
            localforage.removeItem(tier6Key);
            localforage.removeItem(tier7Key);
        }
    }, [adpStorage, adpEnabled, adpRubricState, adpTier])

    useEffect(() => {
        const tier1Key = `${skuConfig[637]}-637`;
        const tier2Key = `${skuConfig[639]}-639`;
        const tier3Key = `${skuConfig[641]}-641`;
        const tier4Key = `${skuConfig[643]}-643`;
        const tier5Key = `${skuConfig[645]}-645`;
        const tier6Key = `${skuConfig[647]}-647`;
        const tier7Key = `${skuConfig[649]}-649`;

        if (adpEnabled && adpRubricState === "consumption" && adpTier == 3 && !DataCenterChoiceCloud) {
            if (adpStorage <= 200) {
                localforage.setItem(tier1Key, adpStorage);
                localforage.removeItem(tier2Key);
                localforage.removeItem(tier3Key);
                localforage.removeItem(tier4Key);
                localforage.removeItem(tier5Key);
                localforage.removeItem(tier6Key);
                localforage.removeItem(tier7Key);
            } else if (adpStorage > 200 && adpStorage <= 400) {
                localforage.setItem(tier2Key, adpStorage);
                localforage.removeItem(tier1Key);
                localforage.removeItem(tier3Key);
                localforage.removeItem(tier4Key);
                localforage.removeItem(tier5Key);
                localforage.removeItem(tier6Key);
                localforage.removeItem(tier7Key);
            } else if (adpStorage > 400 && adpStorage <= 600) {
                localforage.setItem(tier3Key, adpStorage);
                localforage.removeItem(tier1Key);
                localforage.removeItem(tier2Key);
                localforage.removeItem(tier4Key);
                localforage.removeItem(tier5Key);
                localforage.removeItem(tier6Key);
                localforage.removeItem(tier7Key);
            } else if (adpStorage > 600 && adpStorage <= 1000) {
                localforage.setItem(tier4Key, adpStorage);
                localforage.removeItem(tier1Key);
                localforage.removeItem(tier2Key);
                localforage.removeItem(tier3Key);
                localforage.removeItem(tier5Key);
                localforage.removeItem(tier6Key);
                localforage.removeItem(tier7Key);
            } else if (adpStorage > 1000 && adpStorage <= 1500) {
                localforage.setItem(tier5Key, adpStorage);
                localforage.removeItem(tier1Key);
                localforage.removeItem(tier2Key);
                localforage.removeItem(tier3Key);
                localforage.removeItem(tier4Key);
                localforage.removeItem(tier6Key);
                localforage.removeItem(tier7Key);
            } else if (adpStorage > 1500 && adpStorage <= 2000) {
                localforage.setItem(tier6Key, adpStorage);
                localforage.removeItem(tier1Key);
                localforage.removeItem(tier2Key);
                localforage.removeItem(tier3Key);
                localforage.removeItem(tier4Key);
                localforage.removeItem(tier5Key);
                localforage.removeItem(tier7Key);
            } else if (adpStorage > 2000) {
                localforage.setItem(tier7Key, adpStorage);
                localforage.removeItem(tier1Key);
                localforage.removeItem(tier2Key);
                localforage.removeItem(tier3Key);
                localforage.removeItem(tier4Key);
                localforage.removeItem(tier5Key);
                localforage.removeItem(tier6Key);
            } else {
                localforage.removeItem(tier1Key);
                localforage.removeItem(tier2Key);
                localforage.removeItem(tier3Key);
                localforage.removeItem(tier4Key);
                localforage.removeItem(tier5Key);
                localforage.removeItem(tier6Key);
                localforage.removeItem(tier7Key);
            }
        } else {
            localforage.removeItem(tier1Key);
            localforage.removeItem(tier2Key);
            localforage.removeItem(tier3Key);
            localforage.removeItem(tier4Key);
            localforage.removeItem(tier5Key);
            localforage.removeItem(tier6Key);
            localforage.removeItem(tier7Key);
        }
    }, [adpStorage, adpEnabled, adpRubricState, adpTier])

    useEffect(() => {
        const tier1Key = `${skuConfig[651]}-651`;
        const tier2Key = `${skuConfig[653]}-653`;
        const tier3Key = `${skuConfig[655]}-655`;
        const tier4Key = `${skuConfig[657]}-657`;
        const tier5Key = `${skuConfig[659]}-659`;
        const tier6Key = `${skuConfig[661]}-661`;
        const tier7Key = `${skuConfig[663]}-663`;

        if (adpEnabled && adpRubricState === "consumption_hybrid" && adpTier == 2 && !DataCenterChoiceCloud) {
            if (adpStorage <= 200) {
                localforage.setItem(tier1Key, adpStorage);
                localforage.removeItem(tier2Key);
                localforage.removeItem(tier3Key);
                localforage.removeItem(tier4Key);
                localforage.removeItem(tier5Key);
                localforage.removeItem(tier6Key);
                localforage.removeItem(tier7Key);
            } else if (adpStorage > 200 && adpStorage <= 400) {
                localforage.setItem(tier2Key, adpStorage);
                localforage.removeItem(tier1Key);
                localforage.removeItem(tier3Key);
                localforage.removeItem(tier4Key);
                localforage.removeItem(tier5Key);
                localforage.removeItem(tier6Key);
                localforage.removeItem(tier7Key);
            } else if (adpStorage > 400 && adpStorage <= 600) {
                localforage.setItem(tier3Key, adpStorage);
                localforage.removeItem(tier1Key);
                localforage.removeItem(tier2Key);
                localforage.removeItem(tier4Key);
                localforage.removeItem(tier5Key);
                localforage.removeItem(tier6Key);
                localforage.removeItem(tier7Key);
            } else if (adpStorage > 600 && adpStorage <= 1000) {
                localforage.setItem(tier4Key, adpStorage);
                localforage.removeItem(tier1Key);
                localforage.removeItem(tier2Key);
                localforage.removeItem(tier3Key);
                localforage.removeItem(tier5Key);
                localforage.removeItem(tier6Key);
                localforage.removeItem(tier7Key);
            } else if (adpStorage > 1000 && adpStorage <= 1500) {
                localforage.setItem(tier5Key, adpStorage);
                localforage.removeItem(tier1Key);
                localforage.removeItem(tier2Key);
                localforage.removeItem(tier3Key);
                localforage.removeItem(tier4Key);
                localforage.removeItem(tier6Key);
                localforage.removeItem(tier7Key);
            } else if (adpStorage > 1500 && adpStorage <= 2000) {
                localforage.setItem(tier6Key, adpStorage);
                localforage.removeItem(tier1Key);
                localforage.removeItem(tier2Key);
                localforage.removeItem(tier3Key);
                localforage.removeItem(tier4Key);
                localforage.removeItem(tier5Key);
                localforage.removeItem(tier7Key);
            } else if (adpStorage > 2000) {
                localforage.setItem(tier7Key, adpStorage);
                localforage.removeItem(tier1Key);
                localforage.removeItem(tier2Key);
                localforage.removeItem(tier3Key);
                localforage.removeItem(tier4Key);
                localforage.removeItem(tier5Key);
                localforage.removeItem(tier6Key);
            } else {
                localforage.removeItem(tier1Key);
                localforage.removeItem(tier2Key);
                localforage.removeItem(tier3Key);
                localforage.removeItem(tier4Key);
                localforage.removeItem(tier5Key);
                localforage.removeItem(tier6Key);
                localforage.removeItem(tier7Key);
            }
        } else {
            localforage.removeItem(tier1Key);
            localforage.removeItem(tier2Key);
            localforage.removeItem(tier3Key);
            localforage.removeItem(tier4Key);
            localforage.removeItem(tier5Key);
            localforage.removeItem(tier6Key);
            localforage.removeItem(tier7Key);
        }
    }, [adpStorage, adpEnabled, adpRubricState, adpTier])

    useEffect(() => {
        const tier1Key = `${skuConfig[665]}-665`;
        const tier2Key = `${skuConfig[667]}-667`;
        const tier3Key = `${skuConfig[669]}-669`;
        const tier4Key = `${skuConfig[671]}-671`;
        const tier5Key = `${skuConfig[673]}-673`;
        const tier6Key = `${skuConfig[675]}-675`;
        const tier7Key = `${skuConfig[677]}-677`;

        if (adpEnabled && adpRubricState === "consumption_hybrid" && adpTier == 3 && !DataCenterChoiceCloud) {
            if (adpStorage <= 200) {
                localforage.setItem(tier1Key, adpStorage);
                localforage.removeItem(tier2Key);
                localforage.removeItem(tier3Key);
                localforage.removeItem(tier4Key);
                localforage.removeItem(tier5Key);
                localforage.removeItem(tier6Key);
                localforage.removeItem(tier7Key);
            } else if (adpStorage > 200 && adpStorage <= 400) {
                localforage.setItem(tier2Key, adpStorage);
                localforage.removeItem(tier1Key);
                localforage.removeItem(tier3Key);
                localforage.removeItem(tier4Key);
                localforage.removeItem(tier5Key);
                localforage.removeItem(tier6Key);
                localforage.removeItem(tier7Key);
            } else if (adpStorage > 400 && adpStorage <= 600) {
                localforage.setItem(tier3Key, adpStorage);
                localforage.removeItem(tier1Key);
                localforage.removeItem(tier2Key);
                localforage.removeItem(tier4Key);
                localforage.removeItem(tier5Key);
                localforage.removeItem(tier6Key);
                localforage.removeItem(tier7Key);
            } else if (adpStorage > 600 && adpStorage <= 1000) {
                localforage.setItem(tier4Key, adpStorage);
                localforage.removeItem(tier1Key);
                localforage.removeItem(tier2Key);
                localforage.removeItem(tier3Key);
                localforage.removeItem(tier5Key);
                localforage.removeItem(tier6Key);
                localforage.removeItem(tier7Key);
            } else if (adpStorage > 1000 && adpStorage <= 1500) {
                localforage.setItem(tier5Key, adpStorage);
                localforage.removeItem(tier1Key);
                localforage.removeItem(tier2Key);
                localforage.removeItem(tier3Key);
                localforage.removeItem(tier4Key);
                localforage.removeItem(tier6Key);
                localforage.removeItem(tier7Key);
            } else if (adpStorage > 1500 && adpStorage <= 2000) {
                localforage.setItem(tier6Key, adpStorage);
                localforage.removeItem(tier1Key);
                localforage.removeItem(tier2Key);
                localforage.removeItem(tier3Key);
                localforage.removeItem(tier4Key);
                localforage.removeItem(tier5Key);
                localforage.removeItem(tier7Key);
            } else if (adpStorage > 2000) {
                localforage.setItem(tier7Key, adpStorage);
                localforage.removeItem(tier1Key);
                localforage.removeItem(tier2Key);
                localforage.removeItem(tier3Key);
                localforage.removeItem(tier4Key);
                localforage.removeItem(tier5Key);
                localforage.removeItem(tier6Key);
            } else {
                localforage.removeItem(tier1Key);
                localforage.removeItem(tier2Key);
                localforage.removeItem(tier3Key);
                localforage.removeItem(tier4Key);
                localforage.removeItem(tier5Key);
                localforage.removeItem(tier6Key);
                localforage.removeItem(tier7Key);
            }
        } else {
            localforage.removeItem(tier1Key);
            localforage.removeItem(tier2Key);
            localforage.removeItem(tier3Key);
            localforage.removeItem(tier4Key);
            localforage.removeItem(tier5Key);
            localforage.removeItem(tier6Key);
            localforage.removeItem(tier7Key);
        }
    }, [adpStorage, adpEnabled, adpRubricState, adpTier])

    // useEffect that sets the ADP Storage Reference sku in localForage
    useEffect(() => {
        if (adpStorage > 0 && adpEnabled && adpRubricState != "own") {
            localforage.setItem("ADP Storage Reference", adpStorage);
        } else {
            localforage.removeItem("ADP Storage Reference");
        }
    }, [adpEnabled, adpStorage, adpRubricState])

    // useEffect that sets CC - M365 Backup skus in localForage
    useEffect(() => {
        const sku20GBBackupKeyA = `${skuConfig[312]}-312`
        const sku20GBBackupKeyB = `${skuConfig[314]}-314`
        const sku20GBBackupKeyC = `${skuConfig[316]}-316`
        const sku20GBBackupKeyD = `${skuConfig[318]}-318`
        const sku20GBBackupKeyE = `${skuConfig[320]}-320`
        const sku20GBBackupKeyF = `${skuConfig[322]}-322`
        const sku20GBBackupKeyG = `${skuConfig[324]}-324`
        const skuUnlimitedBackupKeyA = `${skuConfig[326]}-326`
        const skuUnlimitedBackupKeyB = `${skuConfig[328]}-328`
        const skuUnlimitedBackupKeyC = `${skuConfig[330]}-330`
        const skuUnlimitedBackupKeyD = `${skuConfig[332]}-332`
        const skuUnlimitedBackupKeyE = `${skuConfig[334]}-334`
        const skuUnlimitedBackupKeyF = `${skuConfig[336]}-336`
        const skuUnlimitedBackupKeyG = `${skuConfig[338]}-338`

        if (M365Backups && M365BackupsType === "Unlimited") {
            localforage.removeItem(sku20GBBackupKeyA);
            localforage.removeItem(sku20GBBackupKeyB);
            localforage.removeItem(sku20GBBackupKeyC);
            localforage.removeItem(sku20GBBackupKeyD);
            localforage.removeItem(sku20GBBackupKeyE);
            localforage.removeItem(sku20GBBackupKeyF);
            localforage.removeItem(sku20GBBackupKeyG);
            if (M365BackupsUsers > 0 && M365BackupsUsers <= 500) {
                localforage.setItem(skuUnlimitedBackupKeyA, M365BackupsUsers);
                localforage.removeItem(skuUnlimitedBackupKeyB);
                localforage.removeItem(skuUnlimitedBackupKeyC);
                localforage.removeItem(skuUnlimitedBackupKeyD);
                localforage.removeItem(skuUnlimitedBackupKeyE);
                localforage.removeItem(skuUnlimitedBackupKeyF);
                localforage.removeItem(skuUnlimitedBackupKeyG);
            } else if (M365BackupsUsers > 500 && M365BackupsUsers <= 1000) {
                localforage.setItem(skuUnlimitedBackupKeyB, M365BackupsUsers);
                localforage.removeItem(skuUnlimitedBackupKeyA);
                localforage.removeItem(skuUnlimitedBackupKeyC);
                localforage.removeItem(skuUnlimitedBackupKeyD);
                localforage.removeItem(skuUnlimitedBackupKeyE);
                localforage.removeItem(skuUnlimitedBackupKeyF);
                localforage.removeItem(skuUnlimitedBackupKeyG);
            } else if (M365BackupsUsers > 1000 && M365BackupsUsers <= 5000) {
                localforage.setItem(skuUnlimitedBackupKeyC, M365BackupsUsers);
                localforage.removeItem(skuUnlimitedBackupKeyA);
                localforage.removeItem(skuUnlimitedBackupKeyB);
                localforage.removeItem(skuUnlimitedBackupKeyD);
                localforage.removeItem(skuUnlimitedBackupKeyE);
                localforage.removeItem(skuUnlimitedBackupKeyF);
                localforage.removeItem(skuUnlimitedBackupKeyG);
            } else if (M365BackupsUsers > 5000 && M365BackupsUsers <= 10000) {
                localforage.setItem(skuUnlimitedBackupKeyD, M365BackupsUsers);
                localforage.removeItem(skuUnlimitedBackupKeyA);
                localforage.removeItem(skuUnlimitedBackupKeyB);
                localforage.removeItem(skuUnlimitedBackupKeyC);
                localforage.removeItem(skuUnlimitedBackupKeyE);
                localforage.removeItem(skuUnlimitedBackupKeyF);
                localforage.removeItem(skuUnlimitedBackupKeyG);
            } else if (M365BackupsUsers > 10000 && M365BackupsUsers <= 15000) {
                localforage.setItem(skuUnlimitedBackupKeyE, M365BackupsUsers);
                localforage.removeItem(skuUnlimitedBackupKeyA);
                localforage.removeItem(skuUnlimitedBackupKeyB);
                localforage.removeItem(skuUnlimitedBackupKeyC);
                localforage.removeItem(skuUnlimitedBackupKeyD);
                localforage.removeItem(skuUnlimitedBackupKeyF);
                localforage.removeItem(skuUnlimitedBackupKeyG);
            } else if (M365BackupsUsers > 15000 && M365BackupsUsers <= 20000) {
                localforage.setItem(skuUnlimitedBackupKeyF, M365BackupsUsers);
                localforage.removeItem(skuUnlimitedBackupKeyA);
                localforage.removeItem(skuUnlimitedBackupKeyB);
                localforage.removeItem(skuUnlimitedBackupKeyC);
                localforage.removeItem(skuUnlimitedBackupKeyD);
                localforage.removeItem(skuUnlimitedBackupKeyE);
                localforage.removeItem(skuUnlimitedBackupKeyG);
            } else if (M365BackupsUsers > 20000) {
                localforage.setItem(skuUnlimitedBackupKeyG, M365BackupsUsers);
                localforage.removeItem(skuUnlimitedBackupKeyA);
                localforage.removeItem(skuUnlimitedBackupKeyB);
                localforage.removeItem(skuUnlimitedBackupKeyC);
                localforage.removeItem(skuUnlimitedBackupKeyD);
                localforage.removeItem(skuUnlimitedBackupKeyE);
                localforage.removeItem(skuUnlimitedBackupKeyF);
            } else {
                localforage.removeItem(skuUnlimitedBackupKeyA);
                localforage.removeItem(skuUnlimitedBackupKeyB);
                localforage.removeItem(skuUnlimitedBackupKeyC);
                localforage.removeItem(skuUnlimitedBackupKeyD);
                localforage.removeItem(skuUnlimitedBackupKeyE);
                localforage.removeItem(skuUnlimitedBackupKeyF);
                localforage.removeItem(skuUnlimitedBackupKeyG);
            }
        } else if (M365Backups && M365BackupsType === "20GB") {
            localforage.removeItem(skuUnlimitedBackupKeyA);
            localforage.removeItem(skuUnlimitedBackupKeyB);
            localforage.removeItem(skuUnlimitedBackupKeyC);
            localforage.removeItem(skuUnlimitedBackupKeyD);
            localforage.removeItem(skuUnlimitedBackupKeyE);
            localforage.removeItem(skuUnlimitedBackupKeyF);
            localforage.removeItem(skuUnlimitedBackupKeyG);
            if (M365BackupsUsers > 0 && M365BackupsUsers <= 500) {
                localforage.setItem(sku20GBBackupKeyA, M365BackupsUsers);
                localforage.removeItem(sku20GBBackupKeyB);
                localforage.removeItem(sku20GBBackupKeyC);
                localforage.removeItem(sku20GBBackupKeyD);
                localforage.removeItem(sku20GBBackupKeyE);
                localforage.removeItem(sku20GBBackupKeyF);
                localforage.removeItem(sku20GBBackupKeyG);
            } else if (M365BackupsUsers > 500 && M365BackupsUsers <= 1000) {
                localforage.setItem(sku20GBBackupKeyB, M365BackupsUsers);
                localforage.removeItem(sku20GBBackupKeyA);
                localforage.removeItem(sku20GBBackupKeyC);
                localforage.removeItem(sku20GBBackupKeyD);
                localforage.removeItem(sku20GBBackupKeyE);
                localforage.removeItem(sku20GBBackupKeyF);
                localforage.removeItem(sku20GBBackupKeyG);
            } else if (M365BackupsUsers > 1000 && M365BackupsUsers <= 5000) {
                localforage.setItem(sku20GBBackupKeyC, M365BackupsUsers);
                localforage.removeItem(sku20GBBackupKeyA);
                localforage.removeItem(sku20GBBackupKeyB);
                localforage.removeItem(sku20GBBackupKeyD);
                localforage.removeItem(sku20GBBackupKeyE);
                localforage.removeItem(sku20GBBackupKeyF);
                localforage.removeItem(sku20GBBackupKeyG);
            } else if (M365BackupsUsers > 5000 && M365BackupsUsers <= 10000) {
                localforage.setItem(sku20GBBackupKeyD, M365BackupsUsers);
                localforage.removeItem(sku20GBBackupKeyA);
                localforage.removeItem(sku20GBBackupKeyB);
                localforage.removeItem(sku20GBBackupKeyC);
                localforage.removeItem(sku20GBBackupKeyE);
                localforage.removeItem(sku20GBBackupKeyF);
                localforage.removeItem(sku20GBBackupKeyG);
            } else if (M365BackupsUsers > 10000 && M365BackupsUsers <= 15000) {
                localforage.setItem(sku20GBBackupKeyE, M365BackupsUsers);
                localforage.removeItem(sku20GBBackupKeyA);
                localforage.removeItem(sku20GBBackupKeyB);
                localforage.removeItem(sku20GBBackupKeyC);
                localforage.removeItem(sku20GBBackupKeyD);
                localforage.removeItem(sku20GBBackupKeyF);
                localforage.removeItem(sku20GBBackupKeyG);
            } else if (M365BackupsUsers > 15000 && M365BackupsUsers <= 20000) {
                localforage.setItem(sku20GBBackupKeyF, M365BackupsUsers);
                localforage.removeItem(sku20GBBackupKeyA);
                localforage.removeItem(sku20GBBackupKeyB);
                localforage.removeItem(sku20GBBackupKeyC);
                localforage.removeItem(sku20GBBackupKeyD);
                localforage.removeItem(sku20GBBackupKeyE);
                localforage.removeItem(sku20GBBackupKeyG);
            } else if (M365BackupsUsers > 20000) {
                localforage.setItem(sku20GBBackupKeyG, M365BackupsUsers);
                localforage.removeItem(sku20GBBackupKeyA);
                localforage.removeItem(sku20GBBackupKeyB);
                localforage.removeItem(sku20GBBackupKeyC);
                localforage.removeItem(sku20GBBackupKeyD);
                localforage.removeItem(sku20GBBackupKeyE);
                localforage.removeItem(sku20GBBackupKeyF);
            } else {
                localforage.removeItem(sku20GBBackupKeyA);
                localforage.removeItem(sku20GBBackupKeyB);
                localforage.removeItem(sku20GBBackupKeyC);
                localforage.removeItem(sku20GBBackupKeyD);
                localforage.removeItem(sku20GBBackupKeyE);
                localforage.removeItem(sku20GBBackupKeyF);
                localforage.removeItem(sku20GBBackupKeyG);
            }
        } else {
            localforage.removeItem(skuUnlimitedBackupKeyA);
            localforage.removeItem(skuUnlimitedBackupKeyB);
            localforage.removeItem(skuUnlimitedBackupKeyC);
            localforage.removeItem(skuUnlimitedBackupKeyD);
            localforage.removeItem(skuUnlimitedBackupKeyE);
            localforage.removeItem(skuUnlimitedBackupKeyF);
            localforage.removeItem(skuUnlimitedBackupKeyG);
            localforage.removeItem(sku20GBBackupKeyA);
            localforage.removeItem(sku20GBBackupKeyB);
            localforage.removeItem(sku20GBBackupKeyC);
            localforage.removeItem(sku20GBBackupKeyD);
            localforage.removeItem(sku20GBBackupKeyE);
            localforage.removeItem(sku20GBBackupKeyF);
            localforage.removeItem(sku20GBBackupKeyG);
        }
    }, [M365Backups, M365BackupsType, M365BackupsUsers])

    // useEffect that CC -UCL Cloud Backup skus in localForage
    useEffect(() => {
        const skuEnterpriseAKey = `${skuConfig[340]}-340`
        const skuEnterpriseBKey = `${skuConfig[342]}-342`
        const skuEnterpriseCKey = `${skuConfig[344]}-344`
        const skuEnterpriseDKey = `${skuConfig[346]}-346`
        const skuEnterpriseEKey = `${skuConfig[348]}-348`
        const skuEnterpriseFKey = `${skuConfig[350]}-350`
        const skuEnterpriseGKey = `${skuConfig[352]}-352`
        const skuFoundationAKey = `${skuConfig[354]}-354`
        const skuFoundationBKey = `${skuConfig[356]}-356`
        const skuFoundationCKey = `${skuConfig[358]}-358`
        const skuFoundationDKey = `${skuConfig[360]}-360`
        const skuFoundationEKey = `${skuConfig[362]}-362`
        const skuFoundationFKey = `${skuConfig[364]}-364`
        const skuFoundationGKey = `${skuConfig[366]}-366`

        if (CloudBackup && CloudBackupType === "Foundation") {
            localforage.removeItem(skuEnterpriseAKey);
            localforage.removeItem(skuEnterpriseBKey);
            localforage.removeItem(skuEnterpriseCKey);
            localforage.removeItem(skuEnterpriseDKey);
            localforage.removeItem(skuEnterpriseEKey);
            localforage.removeItem(skuEnterpriseFKey);
            localforage.removeItem(skuEnterpriseGKey);
            if (CloudBackupData > 0 && CloudBackupData <= 200) {
                localforage.setItem(skuFoundationAKey, CloudBackupData);
                localforage.removeItem(skuFoundationBKey);
                localforage.removeItem(skuFoundationCKey);
                localforage.removeItem(skuFoundationDKey);
                localforage.removeItem(skuFoundationEKey);
                localforage.removeItem(skuFoundationFKey);
                localforage.removeItem(skuFoundationGKey);
            } else if (CloudBackupData > 200 && CloudBackupData <= 400) {
                localforage.setItem(skuFoundationBKey, CloudBackupData);
                localforage.removeItem(skuFoundationAKey);
                localforage.removeItem(skuFoundationCKey);
                localforage.removeItem(skuFoundationDKey);
                localforage.removeItem(skuFoundationEKey);
                localforage.removeItem(skuFoundationFKey);
                localforage.removeItem(skuFoundationGKey);
            } else if (CloudBackupData > 400 && CloudBackupData <= 600) {
                localforage.setItem(skuFoundationCKey, CloudBackupData);
                localforage.removeItem(skuFoundationAKey);
                localforage.removeItem(skuFoundationBKey);
                localforage.removeItem(skuFoundationDKey);
                localforage.removeItem(skuFoundationEKey);
                localforage.removeItem(skuFoundationFKey);
                localforage.removeItem(skuFoundationGKey);
            } else if (CloudBackupData > 600 && CloudBackupData <= 1000) {
                localforage.setItem(skuFoundationDKey, CloudBackupData);
                localforage.removeItem(skuFoundationAKey);
                localforage.removeItem(skuFoundationBKey);
                localforage.removeItem(skuFoundationCKey);
                localforage.removeItem(skuFoundationEKey);
                localforage.removeItem(skuFoundationFKey);
                localforage.removeItem(skuFoundationGKey);
            } else if (CloudBackupData > 1000 && CloudBackupData <= 1500) {
                localforage.setItem(skuFoundationEKey, CloudBackupData);
                localforage.removeItem(skuFoundationAKey);
                localforage.removeItem(skuFoundationBKey);
                localforage.removeItem(skuFoundationCKey);
                localforage.removeItem(skuFoundationDKey);
                localforage.removeItem(skuFoundationFKey);
                localforage.removeItem(skuFoundationGKey);
            } else if (CloudBackupData > 1500 && CloudBackupData <= 2000) {
                localforage.setItem(skuFoundationFKey, CloudBackupData);
                localforage.removeItem(skuFoundationAKey);
                localforage.removeItem(skuFoundationBKey);
                localforage.removeItem(skuFoundationCKey);
                localforage.removeItem(skuFoundationDKey);
                localforage.removeItem(skuFoundationEKey);
                localforage.removeItem(skuFoundationGKey);
            } else if (CloudBackupData > 2000 && CloudBackupData <= 999999) {
                localforage.setItem(skuFoundationGKey, CloudBackupData);
                localforage.removeItem(skuFoundationAKey);
                localforage.removeItem(skuFoundationBKey);
                localforage.removeItem(skuFoundationCKey);
                localforage.removeItem(skuFoundationDKey);
                localforage.removeItem(skuFoundationEKey);
                localforage.removeItem(skuFoundationFKey);
            } else {
                localforage.removeItem(skuFoundationAKey);
                localforage.removeItem(skuFoundationBKey);
                localforage.removeItem(skuFoundationCKey);
                localforage.removeItem(skuFoundationDKey);
                localforage.removeItem(skuFoundationEKey);
                localforage.removeItem(skuFoundationFKey);
                localforage.removeItem(skuFoundationGKey);
            }
        } else if (CloudBackup && CloudBackupType === "Enterprise") {
            localforage.removeItem(skuFoundationAKey);
            localforage.removeItem(skuFoundationBKey);
            localforage.removeItem(skuFoundationCKey);
            localforage.removeItem(skuFoundationDKey);
            localforage.removeItem(skuFoundationEKey);
            localforage.removeItem(skuFoundationFKey);
            localforage.removeItem(skuFoundationGKey);
            if (CloudBackupData > 0 && CloudBackupData <= 200) {
                localforage.setItem(skuEnterpriseAKey, CloudBackupData);
                localforage.removeItem(skuEnterpriseBKey);
                localforage.removeItem(skuEnterpriseCKey);
                localforage.removeItem(skuEnterpriseDKey);
                localforage.removeItem(skuEnterpriseEKey);
                localforage.removeItem(skuEnterpriseFKey);
                localforage.removeItem(skuEnterpriseGKey);
            } else if (CloudBackupData > 200 && CloudBackupData <= 400) {
                localforage.setItem(skuEnterpriseBKey, CloudBackupData);
                localforage.removeItem(skuEnterpriseAKey);
                localforage.removeItem(skuEnterpriseCKey);
                localforage.removeItem(skuEnterpriseDKey);
                localforage.removeItem(skuEnterpriseEKey);
                localforage.removeItem(skuEnterpriseFKey);
                localforage.removeItem(skuEnterpriseGKey);
            } else if (CloudBackupData > 400 && CloudBackupData <= 600) {
                localforage.setItem(skuEnterpriseCKey, CloudBackupData);
                localforage.removeItem(skuEnterpriseAKey);
                localforage.removeItem(skuEnterpriseBKey);
                localforage.removeItem(skuEnterpriseDKey);
                localforage.removeItem(skuEnterpriseEKey);
                localforage.removeItem(skuEnterpriseFKey);
                localforage.removeItem(skuEnterpriseGKey);
            } else if (CloudBackupData > 600 && CloudBackupData <= 1000) {
                localforage.setItem(skuEnterpriseDKey, CloudBackupData);
                localforage.removeItem(skuEnterpriseAKey);
                localforage.removeItem(skuEnterpriseBKey);
                localforage.removeItem(skuEnterpriseCKey);
                localforage.removeItem(skuEnterpriseEKey);
                localforage.removeItem(skuEnterpriseFKey);
                localforage.removeItem(skuEnterpriseGKey);
            } else if (CloudBackupData > 1000 && CloudBackupData <= 1500) {
                localforage.setItem(skuEnterpriseEKey, CloudBackupData);
                localforage.removeItem(skuEnterpriseAKey);
                localforage.removeItem(skuEnterpriseBKey);
                localforage.removeItem(skuEnterpriseCKey);
                localforage.removeItem(skuEnterpriseDKey);
                localforage.removeItem(skuEnterpriseFKey);
                localforage.removeItem(skuEnterpriseGKey);
            } else if (CloudBackupData > 1500 && CloudBackupData <= 2000) {
                localforage.setItem(skuEnterpriseFKey, CloudBackupData);
                localforage.removeItem(skuEnterpriseAKey);
                localforage.removeItem(skuEnterpriseBKey);
                localforage.removeItem(skuEnterpriseCKey);
                localforage.removeItem(skuEnterpriseDKey);
                localforage.removeItem(skuEnterpriseEKey);
                localforage.removeItem(skuEnterpriseGKey);
            } else if (CloudBackupData > 2000 && CloudBackupData <= 999999) {
                localforage.setItem(skuEnterpriseGKey, CloudBackupData);
                localforage.removeItem(skuEnterpriseAKey);
                localforage.removeItem(skuEnterpriseBKey);
                localforage.removeItem(skuEnterpriseCKey);
                localforage.removeItem(skuEnterpriseDKey);
                localforage.removeItem(skuEnterpriseEKey);
                localforage.removeItem(skuEnterpriseFKey);
            } else {
                localforage.removeItem(skuEnterpriseAKey);
                localforage.removeItem(skuEnterpriseBKey);
                localforage.removeItem(skuEnterpriseCKey);
                localforage.removeItem(skuEnterpriseDKey);
                localforage.removeItem(skuEnterpriseEKey);
                localforage.removeItem(skuEnterpriseFKey);
                localforage.removeItem(skuEnterpriseGKey);
            }
        } else {
            localforage.removeItem(skuFoundationAKey);
            localforage.removeItem(skuFoundationBKey);
            localforage.removeItem(skuFoundationCKey);
            localforage.removeItem(skuFoundationDKey);
            localforage.removeItem(skuFoundationEKey);
            localforage.removeItem(skuFoundationFKey);
            localforage.removeItem(skuFoundationGKey);
            localforage.removeItem(skuEnterpriseAKey);
            localforage.removeItem(skuEnterpriseBKey);
            localforage.removeItem(skuEnterpriseCKey);
            localforage.removeItem(skuEnterpriseDKey);
            localforage.removeItem(skuEnterpriseEKey);
            localforage.removeItem(skuEnterpriseFKey);
            localforage.removeItem(skuEnterpriseGKey);
        }
    }, [CloudBackup, CloudBackupData, CloudBackupType])

    // useEffect that sets MS - BU Virtual App sku in localForage
    useEffect(() => {
        if (DataCenterChoiceCloud && BackupAsAServiceUnitrends) {
            localforage.setItem("MS - BU Virtual App", Math.ceil(Tier1StorageServerTotalQty / 1024));
        } else if (BackupAsAServiceUnitrends) {
            localforage.setItem("MS - BU Virtual App", Math.ceil(BackupTotalStorageUnitrends));
        } else {
            localforage.removeItem("MS - BU Virtual App");
        }
    }, [DataCenterChoiceCloud, BackupAsAServiceUnitrends, Tier1StorageServerTotalQty, BackupTotalStorageUnitrends])

    // useEffect that sets MS - BU Protected Dev sku in localForage
    useEffect(() => {
        if (BackupAsAServiceUnitrends) {
            localforage.setItem("MS - BU Protected Dev", BackupProtectedDevicesUnitrends);
        } else {
            localforage.removeItem("MS - BU Protected Dev");
        }
    }, [BackupAsAServiceUnitrends, BackupProtectedDevicesUnitrends])

    // useEffect that sets UMSP skus in localForage
    const storageValues = [0.55, 1.1, 2.4, 3.6, 4.8, 7.2, 9.4, 14.4, 19, 24, 36, 48, 60, 72];
    storageValues.forEach((value, index) => {
        useEffect(() => {
            const lowerBound = index === 0 ? 0 : storageValues[index - 1];
            const upperBound = value;
            const keys = ["UMSP-1", "UMSP-2", "UMSP-4", "UMSP-6", "UMSP-8", "UMSP-12", "UMSP-16S", "UMSP-24S", "UMSP-32S", "UMSP-40S", "UMSP-60S", "UMSP-80S", "UMSP-100S", "UMSP-120S"]
            if (
                CustBackupOnsiteDevicesUnitrends === "CustOwnsNoAppliance" &&
                BackupAsAServiceUnitrends &&
                !DataCenterChoiceCloud &&
                BackupTotalStorageUnitrends >= lowerBound &&
                BackupTotalStorageUnitrends < upperBound
            ) {
                localforage.setItem(keys[index], 1);
            } else {
                localforage.removeItem(keys[index]);
            }
        }, [
            BackupAsAServiceUnitrends,
            CustBackupOnsiteDevicesUnitrends,
            DataCenterChoiceCloud,
            BackupTotalStorageUnitrends,
        ]);
    });

    // useEffect that sets MS - Backup App Admin sku in localForage
    useEffect(() => {
        if (CustBackupOnsiteDevicesUnitrends === "CustOwnsAppliance" && BackupAsAServiceUnitrends) {
            localforage.setItem("MS - Backup App Admin", 1)
        } else {
            localforage.removeItem("MS - Backup App Admin")
        }
    }, [CustBackupOnsiteDevicesUnitrends, BackupAsAServiceUnitrends])

    // useEffects that sets Unitrends location skus in localForage
    useEffect(() => {
        if (BackupAsAServiceUnitrends && BackupPrimaryLocationUnitrends == "OnPrem") {
            localforage.setItem("Unitrends On Prem Primary Location Reference", 1)
        } else {
            localforage.removeItem("Unitrends On Prem Primary Location Reference")
        }
    }, [BackupPrimaryLocationUnitrends, BackupAsAServiceUnitrends])
    useEffect(() => {
        if (BackupAsAServiceUnitrends && BackupPrimaryLocationUnitrends == "Dallas") {
            localforage.setItem("Unitrends Dallas Primary Location Reference", 1)
        } else {
            localforage.removeItem("Unitrends Dallas Primary Location Reference")
        }
    }, [BackupPrimaryLocationUnitrends, BackupAsAServiceUnitrends])
    useEffect(() => {
        if (BackupAsAServiceUnitrends && BackupPrimaryLocationUnitrends == "Atlanta") {
            localforage.setItem("Unitrends Atlanta Primary Location Reference", 1)
        } else {
            localforage.removeItem("Unitrends Atlanta Primary Location Reference")
        }
    }, [BackupPrimaryLocationUnitrends, BackupAsAServiceUnitrends])
    useEffect(() => {
        if (BackupAsAServiceUnitrends && BackupAlternateLocationUnitrends == "OnPrem") {
            localforage.setItem("Unitrends On Prem Alternate Location Reference", 1)
        } else {
            localforage.removeItem("Unitrends On Prem Alternate Location Reference")
        }
    }, [BackupAlternateLocationUnitrends, BackupAsAServiceUnitrends])
    useEffect(() => {
        if (BackupAsAServiceUnitrends && BackupAlternateLocationUnitrends == "Dallas") {
            localforage.setItem("Unitrends Dallas Alternate Location Reference", 1)
        } else {
            localforage.removeItem("Unitrends Dallas Alternate Location Reference")
        }
    }, [BackupAlternateLocationUnitrends, BackupAsAServiceUnitrends])
    useEffect(() => {
        if (BackupAsAServiceUnitrends && BackupAlternateLocationUnitrends == "Atlanta") {
            localforage.setItem("Unitrends Atlanta Alternate Location Reference", 1)
        } else {
            localforage.removeItem("Unitrends Atlanta Alternate Location Reference")
        }
    }, [BackupAlternateLocationUnitrends, BackupAsAServiceUnitrends])


    // useEffect that sets CC - vCPU sku in localForage
    useEffect(() => {
        if (VcpuServerTotalQty > 0) {
            localforage.setItem("CC - vCPU", VcpuServerTotalQty);
        } else {
            localforage.removeItem("CC - vCPU");
        }
    }, [VcpuServerTotalQty])

    // useEffect that sets CC - vMemory sku in localForage
    useEffect(() => {
        if (VmemoryServerTotalQty > 0) {
            localforage.setItem("CC - vMemory", VmemoryServerTotalQty);
        } else {
            localforage.removeItem("CC - vMemory")
        }
    }, [VmemoryServerTotalQty])

    // useEffect that sets BC / DR - vCPU sku in localForage
    useEffect(() => {
        if (BusinessContinuityDisasterRelief && BCDRType === "ColdStorage" && (BCDRTarget === "ChoiceAtlanta" || BCDRTarget === "ChoiceDallas")) {
            localforage.setItem("BC / DR - vCPU", ServerTotalsVcpu);
        } else {
            localforage.removeItem("BC / DR - vCPU");
        }
    }, [BusinessContinuityDisasterRelief, BCDRType, BCDRTarget, ServerTotalsVcpu])

    // useEffect that sets BC / DR - vMemory sku in localForage
    useEffect(() => {
        if (BusinessContinuityDisasterRelief && BCDRType === "ColdStorage" && (BCDRTarget === "ChoiceAtlanta" || BCDRTarget === "ChoiceDallas")) {
            localforage.setItem("BC / DR - vMemory", ServerTotalsMemory);
        } else {
            localforage.removeItem("BC / DR - vMemory");
        }
    }, [BusinessContinuityDisasterRelief, BCDRType, BCDRTarget, ServerTotalsMemory])

    // useEffect that sets BC / DR - Tier 1 Storage sku in localForage
    useEffect(() => {
        if (BusinessContinuityDisasterRelief && BCDRType === "ColdStorage" && (BCDRTarget === "ChoiceAtlanta" || BCDRTarget === "ChoiceDallas")) {
            localforage.setItem("BC / DR - Tier 1 Storage", ServerTotalsTier1Storage);
        } else {
            localforage.removeItem("BC / DR - Tier 1 Storage");
        }
    }, [BusinessContinuityDisasterRelief, BCDRType, BCDRTarget, ServerTotalsTier1Storage])

    // useEffect that sets CC - Internet Bandwidth sku in localForage
    useEffect(() => {
        if (DataCenterChoiceCloud) {
            localforage.setItem("CC - Internet Bandwidth", (EucUsers + AvdUsers) * 0.2);
        } else {
            localforage.removeItem("CC - Internet Bandwidth");
        }
    }, [DataCenterChoiceCloud, EucUsers, AvdUsers])

    // useEffect that sets the BC / DR reference skus in localforage
    useEffect(() => {
        if (BusinessContinuityDisasterRelief && BCDRType === "ColdStorage") {
            localforage.setItem("BC / DR - Cold Storage Type Reference", 1);
        } else {
            localforage.removeItem("BC / DR - Cold Storage Type Reference");
        }
    }, [BusinessContinuityDisasterRelief, BCDRType])

    useEffect(() => {
        if (BusinessContinuityDisasterRelief && BCDRType === "ActivePassive") {
            localforage.setItem("BC / DR - Passive Type Reference", 1);
        } else {
            localforage.removeItem("BC / DR - Passive Type Reference");
        }
    }, [BusinessContinuityDisasterRelief, BCDRType])

    useEffect(() => {
        if (BusinessContinuityDisasterRelief && BCDRType === "ActiveActive") {
            localforage.setItem("BC / DR - Active Type Reference", 1);
        } else {
            localforage.removeItem("BC / DR - Active Type Reference");
        }
    }, [BusinessContinuityDisasterRelief, BCDRType])

    useEffect(() => {
        if (BusinessContinuityDisasterRelief && BCDRTarget === "ChoiceDallas") {
            localforage.setItem("BC / DR - Dallas Target Reference", 1);
        } else {
            localforage.removeItem("BC / DR - Dallas Target Reference");
        }
    }, [BusinessContinuityDisasterRelief, BCDRTarget])

    useEffect(() => {
        if (BusinessContinuityDisasterRelief && BCDRTarget === "ChoiceAtlanta") {
            localforage.setItem("BC / DR - Atlanta Target Reference", 1);
        } else {
            localforage.removeItem("BC / DR - Atlanta Target Reference");
        }
    }, [BusinessContinuityDisasterRelief, BCDRTarget])

    useEffect(() => {
        if (BusinessContinuityDisasterRelief && BCDRTarget === "Azure") {
            localforage.setItem("BC / DR - Azure Target Reference", 1);
        } else {
            localforage.removeItem("BC / DR - Azure Target Reference");
        }
    }, [BusinessContinuityDisasterRelief, BCDRTarget])

    useEffect(() => {
        if (BusinessContinuityDisasterRelief && BCDRTarget === "AWS") {
            localforage.setItem("BC / DR - AWS Target Reference", 1);
        } else {
            localforage.removeItem("BC / DR - AWS Target Reference");
        }
    }, [BusinessContinuityDisasterRelief, BCDRTarget])

    useEffect(() => {
        if (BusinessContinuityDisasterRelief && BCDRTarget === "Other") {
            localforage.setItem("BC / DR - Other Target Reference", 1);
        } else {
            localforage.removeItem("BC / DR - Other Target Reference");
        }
    }, [BusinessContinuityDisasterRelief, BCDRTarget])

    // Setting skus from Network section in the Excel spreadsheet
    // useEffect that sets MS - Network App sku in localForage
    useEffect(() => {
        if (NetworkAppServerTotalQty > 0) {
            localforage.setItem("MS - Network App", NetworkAppServerTotalQty);
        } else {
            localforage.removeItem("MS - Network App");
        }
    }, [NetworkAppServerTotalQty])

    // useEffect that sets MS - Network Firewalls sku in localForage
    useEffect(() => {
        if (AdditionalNetworkFirewalls) {
            localforage.setItem("MS - Network Firewalls", AdditionalNetworkFirewallsQty);
        } else {
            localforage.removeItem("MS - Network Firewalls");
        }
    }, [AdditionalNetworkFirewalls, AdditionalNetworkFirewallsQty])

    // useEffect that sets MS - Network Switches sku in localForage
    useEffect(() => {
        if (ManagedNetworkSwitches) {
            localforage.setItem("MS - Network Switches", ManagedNetworkSwitchesQty);
        } else {
            localforage.removeItem("MS - Network Switches");
        }
    }, [ManagedNetworkSwitches, ManagedNetworkSwitchesQty])

    // useEffect that sets MS - Network Wifi sku in localForage
    useEffect(() => {
        if (NetworkWifi) {
            localforage.setItem("MS - Network Wifi", NetworkWifiQty);
        } else {
            localforage.removeItem("MS - Network Wifi");
        }
    }, [NetworkWifi, NetworkWifiQty])

    // Setting skus from Network section in the Excel spreadsheet
    // useEffect that sets MS - Citrix Cloud Infra sku in localForage
    useEffect(() => {
        if (EucEnabled && EucType === "citrix" && CitrixType === "cloud") {
            localforage.setItem("MS - Citrix Cloud Infra", 1);
        } else {
            localforage.removeItem("MS - Citrix Cloud Infra");
        }
    }, [EucEnabled, EucType, CitrixType])

    // useEffect that sets MS - Citrix Infra sku in localForage
    useEffect(() => {
        if (EucEnabled && EucType === "citrix" && CitrixType === "on_prem") {
            localforage.setItem("MS - Citrix Infra", 1);
        } else {
            localforage.removeItem("MS - Citrix Infra");
        }
    }, [EucEnabled, EucType, CitrixType])

    // useEffect that sets MS - Citrix Hybrid Infra sku in localForage
    useEffect(() => {
        if (EucEnabled, EucType === "citrix" && CitrixType === "hybrid") {
            localforage.setItem("MS - Citrix Hybrid Infra", 1);
        } else {
            localforage.removeItem("MS - Citrix Hybrid Infra");
        }
    }, [EucEnabled, EucType, CitrixType])

    // useEffect that sets MS - LB-HA sku in localForage
    useEffect(() => {
        if (NPlus1Required && (AdcCitrix && LoadBalancerWoCitrix)) {
            localforage.setItem("MS - LB-HA", (AdcCitrixCount + LoadBalancerWoCitrixCount));
        } else if (NPlus1Required && (AdcCitrix && !LoadBalancerWoCitrix)) {
            localforage.setItem("MS - LB-HA", AdcCitrixCount);
        } else if (NPlus1Required && (!AdcCitrix && LoadBalancerWoCitrix)) {
            localforage.setItem("MS - LB-HA", LoadBalancerWoCitrixCount);
        } else {
            localforage.removeItem("MS - LB-HA");
        }
    }, [NPlus1Required, AdcCitrix, LoadBalancerWoCitrix, AdcCitrixCount, LoadBalancerWoCitrixCount])

    // useEffect that sets MS - LB-NHA sku in localForage
    useEffect(() => {
        if (!NPlus1Required && (AdcCitrix && LoadBalancerWoCitrix)) {
            localforage.setItem("MS - LB-NHA", (AdcCitrixCount + LoadBalancerWoCitrixCount));
        } else if (!NPlus1Required && (AdcCitrix && !LoadBalancerWoCitrix)) {
            localforage.setItem("MS - LB-NHA", AdcCitrixCount);
        } else if (!NPlus1Required && (!AdcCitrix && LoadBalancerWoCitrix)) {
            localforage.setItem("MS - LB-NHA", LoadBalancerWoCitrixCount);
        } else {
            localforage.removeItem("MS - LB-NHA");
        }
    }, [NPlus1Required, AdcCitrix, LoadBalancerWoCitrix, AdcCitrixCount, LoadBalancerWoCitrixCount])

    // useEffect that sets MS - LB-ADM sku in localForage
    useEffect(() => {
        if (NPlus1Required && AdcAdm) {
            localforage.setItem("MS - LB-ADM", AdcAdmCount * 2);
        } else if (!NPlus1Required && AdcAdm) {
            localforage.setItem("MS - LB-ADM", AdcAdmCount);
        } else {
            localforage.removeItem("MS - LB-ADM");
        }
    }, [NPlus1Required, AdcAdm, AdcAdmCount])

    // useEffect that sets MS - ADC vServer sku in localForage
    useEffect(() => {
        if (NPlus1Required && AdcVserver) {
            localforage.setItem("MS - ADC vServer", AdcVserverCount * 2);
        } else if (!NPlus1Required && AdcVserver) {
            localforage.setItem("MS - ADC vServer", AdcVserverCount);
        } else {
            localforage.removeItem("MS - ADC vServer");
        }
    }, [NPlus1Required, AdcVserver, AdcVserverCount])

    // useEffect that sets ADC licensing sku's in localForage
    useEffect(() => {
        if (AdcLicensing) {
            switch (`${AdcLicensingType}-${AdcLicensingVersion}`) {
                case '10Mbps-Standard':
                    localforage.setItem("RPTCNSVPXCSPSE10", AdcLicensingQty);
                    localforage.removeItem("4029130");
                    localforage.removeItem("RPTCNSVPXCSPSE200");
                    localforage.removeItem("RPTCNSVPXCSPSE1K");
                    localforage.removeItem("MW2H0000000");
                    localforage.removeItem("4043769");
                    localforage.removeItem("4044402");
                    localforage.removeItem("RPTCNSVPXCSPEE10");
                    localforage.removeItem("4029134");
                    localforage.removeItem("RPTCNSVPXCSPEE200");
                    localforage.removeItem("RPTCNSVPXCSPEE1K");
                    localforage.removeItem("MW2H0000001");
                    localforage.removeItem("4044400");
                    localforage.removeItem("4044403");
                    localforage.removeItem("RPTCNSVPXCSPPE10");
                    localforage.removeItem("4029138");
                    localforage.removeItem("RPTCNSVPXCSPPE200");
                    localforage.removeItem("RPTCNSVPXCSPPE1K");
                    localforage.removeItem("MW2H0000002");
                    localforage.removeItem("4044401");
                    localforage.removeItem("4044404");
                    break;
                case '50Mbps-Standard':
                    localforage.setItem("4029130", AdcLicensingQty);
                    localforage.removeItem("RPTCNSVPXCSPSE10");
                    localforage.removeItem("RPTCNSVPXCSPSE200");
                    localforage.removeItem("RPTCNSVPXCSPSE1K");
                    localforage.removeItem("MW2H0000000");
                    localforage.removeItem("4043769");
                    localforage.removeItem("4044402");
                    localforage.removeItem("RPTCNSVPXCSPEE10");
                    localforage.removeItem("4029134");
                    localforage.removeItem("RPTCNSVPXCSPEE200");
                    localforage.removeItem("RPTCNSVPXCSPEE1K");
                    localforage.removeItem("MW2H0000001");
                    localforage.removeItem("4044400");
                    localforage.removeItem("4044403");
                    localforage.removeItem("RPTCNSVPXCSPPE10");
                    localforage.removeItem("4029138");
                    localforage.removeItem("RPTCNSVPXCSPPE200");
                    localforage.removeItem("RPTCNSVPXCSPPE1K");
                    localforage.removeItem("MW2H0000002");
                    localforage.removeItem("4044401");
                    localforage.removeItem("4044404");
                    break;
                case '200Mbps-Standard':
                    localforage.setItem("RPTCNSVPXCSPSE200", AdcLicensingQty);
                    localforage.removeItem("RPTCNSVPXCSPSE10");
                    localforage.removeItem("4029130");
                    localforage.removeItem("RPTCNSVPXCSPSE1K");
                    localforage.removeItem("MW2H0000000");
                    localforage.removeItem("4043769");
                    localforage.removeItem("4044402");
                    localforage.removeItem("RPTCNSVPXCSPEE10");
                    localforage.removeItem("4029134");
                    localforage.removeItem("RPTCNSVPXCSPEE200");
                    localforage.removeItem("RPTCNSVPXCSPEE1K");
                    localforage.removeItem("MW2H0000001");
                    localforage.removeItem("4044400");
                    localforage.removeItem("4044403");
                    localforage.removeItem("RPTCNSVPXCSPPE10");
                    localforage.removeItem("4029138");
                    localforage.removeItem("RPTCNSVPXCSPPE200");
                    localforage.removeItem("RPTCNSVPXCSPPE1K");
                    localforage.removeItem("MW2H0000002");
                    localforage.removeItem("4044401");
                    localforage.removeItem("4044404");
                    break;
                case '1000Mbps-Standard':
                    localforage.setItem("RPTCNSVPXCSPSE1K", AdcLicensingQty);
                    localforage.removeItem("RPTCNSVPXCSPSE10");
                    localforage.removeItem("4029130");
                    localforage.removeItem("RPTCNSVPXCSPSE200");
                    localforage.removeItem("MW2H0000000");
                    localforage.removeItem("4043769");
                    localforage.removeItem("4044402");
                    localforage.removeItem("RPTCNSVPXCSPEE10");
                    localforage.removeItem("4029134");
                    localforage.removeItem("RPTCNSVPXCSPEE200");
                    localforage.removeItem("RPTCNSVPXCSPEE1K");
                    localforage.removeItem("MW2H0000001");
                    localforage.removeItem("4044400");
                    localforage.removeItem("4044403");
                    localforage.removeItem("RPTCNSVPXCSPPE10");
                    localforage.removeItem("4029138");
                    localforage.removeItem("RPTCNSVPXCSPPE200");
                    localforage.removeItem("RPTCNSVPXCSPPE1K");
                    localforage.removeItem("MW2H0000002");
                    localforage.removeItem("4044401");
                    localforage.removeItem("4044404");
                    break;
                case '3000Mbps-Standard':
                    localforage.setItem("MW2H0000000", AdcLicensingQty);
                    localforage.removeItem("RPTCNSVPXCSPSE10");
                    localforage.removeItem("4029130");
                    localforage.removeItem("RPTCNSVPXCSPSE200");
                    localforage.removeItem("RPTCNSVPXCSPSE1K");
                    localforage.removeItem("4043769");
                    localforage.removeItem("4044402");
                    localforage.removeItem("RPTCNSVPXCSPEE10");
                    localforage.removeItem("4029134");
                    localforage.removeItem("RPTCNSVPXCSPEE200");
                    localforage.removeItem("RPTCNSVPXCSPEE1K");
                    localforage.removeItem("MW2H0000001");
                    localforage.removeItem("4044400");
                    localforage.removeItem("4044403");
                    localforage.removeItem("RPTCNSVPXCSPPE10");
                    localforage.removeItem("4029138");
                    localforage.removeItem("RPTCNSVPXCSPPE200");
                    localforage.removeItem("RPTCNSVPXCSPPE1K");
                    localforage.removeItem("MW2H0000002");
                    localforage.removeItem("4044401");
                    localforage.removeItem("4044404");
                    break;
                case '5000Mbps-Standard':
                    localforage.setItem("4043769", AdcLicensingQty);
                    localforage.removeItem("RPTCNSVPXCSPSE10");
                    localforage.removeItem("4029130");
                    localforage.removeItem("RPTCNSVPXCSPSE200");
                    localforage.removeItem("RPTCNSVPXCSPSE1K");
                    localforage.removeItem("MW2H0000000");
                    localforage.removeItem("4044402");
                    localforage.removeItem("RPTCNSVPXCSPEE10");
                    localforage.removeItem("4029134");
                    localforage.removeItem("RPTCNSVPXCSPEE200");
                    localforage.removeItem("RPTCNSVPXCSPEE1K");
                    localforage.removeItem("MW2H0000001");
                    localforage.removeItem("4044400");
                    localforage.removeItem("4044403");
                    localforage.removeItem("RPTCNSVPXCSPPE10");
                    localforage.removeItem("4029138");
                    localforage.removeItem("RPTCNSVPXCSPPE200");
                    localforage.removeItem("RPTCNSVPXCSPPE1K");
                    localforage.removeItem("MW2H0000002");
                    localforage.removeItem("4044401");
                    localforage.removeItem("4044404");
                    break;
                case '8000Mbps-Standard':
                    localforage.setItem("4044402", AdcLicensingQty);
                    localforage.removeItem("RPTCNSVPXCSPSE10");
                    localforage.removeItem("4029130");
                    localforage.removeItem("RPTCNSVPXCSPSE200");
                    localforage.removeItem("RPTCNSVPXCSPSE1K");
                    localforage.removeItem("MW2H0000000");
                    localforage.removeItem("4043769");
                    localforage.removeItem("RPTCNSVPXCSPEE10");
                    localforage.removeItem("4029134");
                    localforage.removeItem("RPTCNSVPXCSPEE200");
                    localforage.removeItem("RPTCNSVPXCSPEE1K");
                    localforage.removeItem("MW2H0000001");
                    localforage.removeItem("4044400");
                    localforage.removeItem("4044403");
                    localforage.removeItem("RPTCNSVPXCSPPE10");
                    localforage.removeItem("4029138");
                    localforage.removeItem("RPTCNSVPXCSPPE200");
                    localforage.removeItem("RPTCNSVPXCSPPE1K");
                    localforage.removeItem("MW2H0000002");
                    localforage.removeItem("4044401");
                    localforage.removeItem("4044404");
                    break;
                case '10Mbps-Advanced':
                    localforage.setItem("RPTCNSVPXCSPEE10", AdcLicensingQty);
                    localforage.removeItem("RPTCNSVPXCSPSE10");
                    localforage.removeItem("4029130");
                    localforage.removeItem("RPTCNSVPXCSPSE200");
                    localforage.removeItem("RPTCNSVPXCSPSE1K");
                    localforage.removeItem("MW2H0000000");
                    localforage.removeItem("4043769");
                    localforage.removeItem("4044402");
                    localforage.removeItem("4029134");
                    localforage.removeItem("RPTCNSVPXCSPEE200");
                    localforage.removeItem("RPTCNSVPXCSPEE1K");
                    localforage.removeItem("MW2H0000001");
                    localforage.removeItem("4044400");
                    localforage.removeItem("4044403");
                    localforage.removeItem("RPTCNSVPXCSPPE10");
                    localforage.removeItem("4029138");
                    localforage.removeItem("RPTCNSVPXCSPPE200");
                    localforage.removeItem("RPTCNSVPXCSPPE1K");
                    localforage.removeItem("MW2H0000002");
                    localforage.removeItem("4044401");
                    localforage.removeItem("4044404");
                    break;
                case '50Mbps-Advanced':
                    localforage.setItem("4029134", AdcLicensingQty);
                    localforage.removeItem("RPTCNSVPXCSPSE10");
                    localforage.removeItem("4029130");
                    localforage.removeItem("RPTCNSVPXCSPSE200");
                    localforage.removeItem("RPTCNSVPXCSPSE1K");
                    localforage.removeItem("MW2H0000000");
                    localforage.removeItem("4043769");
                    localforage.removeItem("4044402");
                    localforage.removeItem("RPTCNSVPXCSPEE10");
                    localforage.removeItem("RPTCNSVPXCSPEE200");
                    localforage.removeItem("RPTCNSVPXCSPEE1K");
                    localforage.removeItem("MW2H0000001");
                    localforage.removeItem("4044400");
                    localforage.removeItem("4044403");
                    localforage.removeItem("RPTCNSVPXCSPPE10");
                    localforage.removeItem("4029138");
                    localforage.removeItem("RPTCNSVPXCSPPE200");
                    localforage.removeItem("RPTCNSVPXCSPPE1K");
                    localforage.removeItem("MW2H0000002");
                    localforage.removeItem("4044401");
                    localforage.removeItem("4044404");
                    break;
                case '200Mbps-Advanced':
                    localforage.setItem("RPTCNSVPXCSPEE200", AdcLicensingQty);
                    localforage.removeItem("RPTCNSVPXCSPSE10");
                    localforage.removeItem("4029130");
                    localforage.removeItem("RPTCNSVPXCSPSE200");
                    localforage.removeItem("RPTCNSVPXCSPSE1K");
                    localforage.removeItem("MW2H0000000");
                    localforage.removeItem("4043769");
                    localforage.removeItem("4044402");
                    localforage.removeItem("RPTCNSVPXCSPEE10");
                    localforage.removeItem("4029134");
                    localforage.removeItem("RPTCNSVPXCSPEE1K");
                    localforage.removeItem("MW2H0000001");
                    localforage.removeItem("4044400");
                    localforage.removeItem("4044403");
                    localforage.removeItem("RPTCNSVPXCSPPE10");
                    localforage.removeItem("4029138");
                    localforage.removeItem("RPTCNSVPXCSPPE200");
                    localforage.removeItem("RPTCNSVPXCSPPE1K");
                    localforage.removeItem("MW2H0000002");
                    localforage.removeItem("4044401");
                    localforage.removeItem("4044404");
                    break;
                case '1000Mbps-Advanced':
                    localforage.setItem("RPTCNSVPXCSPEE1K", AdcLicensingQty);
                    localforage.removeItem("RPTCNSVPXCSPSE10");
                    localforage.removeItem("4029130");
                    localforage.removeItem("RPTCNSVPXCSPSE200");
                    localforage.removeItem("RPTCNSVPXCSPSE1K");
                    localforage.removeItem("MW2H0000000");
                    localforage.removeItem("4043769");
                    localforage.removeItem("4044402");
                    localforage.removeItem("RPTCNSVPXCSPEE10");
                    localforage.removeItem("4029134");
                    localforage.removeItem("RPTCNSVPXCSPEE200");
                    localforage.removeItem("MW2H0000001");
                    localforage.removeItem("4044400");
                    localforage.removeItem("4044403");
                    localforage.removeItem("RPTCNSVPXCSPPE10");
                    localforage.removeItem("4029138");
                    localforage.removeItem("RPTCNSVPXCSPPE200");
                    localforage.removeItem("RPTCNSVPXCSPPE1K");
                    localforage.removeItem("MW2H0000002");
                    localforage.removeItem("4044401");
                    localforage.removeItem("4044404");
                    break;
                case '3000Mbps-Advanced':
                    localforage.setItem("MW2H0000001", AdcLicensingQty);
                    localforage.removeItem("RPTCNSVPXCSPSE10");
                    localforage.removeItem("4029130");
                    localforage.removeItem("RPTCNSVPXCSPSE200");
                    localforage.removeItem("RPTCNSVPXCSPSE1K");
                    localforage.removeItem("MW2H0000000");
                    localforage.removeItem("4043769");
                    localforage.removeItem("4044402");
                    localforage.removeItem("RPTCNSVPXCSPEE10");
                    localforage.removeItem("4029134");
                    localforage.removeItem("RPTCNSVPXCSPEE200");
                    localforage.removeItem("RPTCNSVPXCSPEE1K");
                    localforage.removeItem("4044400");
                    localforage.removeItem("4044403");
                    localforage.removeItem("RPTCNSVPXCSPPE10");
                    localforage.removeItem("4029138");
                    localforage.removeItem("RPTCNSVPXCSPPE200");
                    localforage.removeItem("RPTCNSVPXCSPPE1K");
                    localforage.removeItem("MW2H0000002");
                    localforage.removeItem("4044401");
                    localforage.removeItem("4044404");
                    break;
                case '5000Mbps-Advanced':
                    localforage.setItem("4044400", AdcLicensingQty);
                    localforage.removeItem("RPTCNSVPXCSPSE10");
                    localforage.removeItem("4029130");
                    localforage.removeItem("RPTCNSVPXCSPSE200");
                    localforage.removeItem("RPTCNSVPXCSPSE1K");
                    localforage.removeItem("MW2H0000000");
                    localforage.removeItem("4043769");
                    localforage.removeItem("4044402");
                    localforage.removeItem("RPTCNSVPXCSPEE10");
                    localforage.removeItem("4029134");
                    localforage.removeItem("RPTCNSVPXCSPEE200");
                    localforage.removeItem("RPTCNSVPXCSPEE1K");
                    localforage.removeItem("MW2H0000001");
                    localforage.removeItem("4044403");
                    localforage.removeItem("RPTCNSVPXCSPPE10");
                    localforage.removeItem("4029138");
                    localforage.removeItem("RPTCNSVPXCSPPE200");
                    localforage.removeItem("RPTCNSVPXCSPPE1K");
                    localforage.removeItem("MW2H0000002");
                    localforage.removeItem("4044401");
                    localforage.removeItem("4044404");
                    break;
                case '8000Mbps-Advanced':
                    localforage.setItem("4044403", AdcLicensingQty);
                    localforage.removeItem("RPTCNSVPXCSPSE10");
                    localforage.removeItem("4029130");
                    localforage.removeItem("RPTCNSVPXCSPSE200");
                    localforage.removeItem("RPTCNSVPXCSPSE1K");
                    localforage.removeItem("MW2H0000000");
                    localforage.removeItem("4043769");
                    localforage.removeItem("4044402");
                    localforage.removeItem("RPTCNSVPXCSPEE10");
                    localforage.removeItem("4029134");
                    localforage.removeItem("RPTCNSVPXCSPEE200");
                    localforage.removeItem("RPTCNSVPXCSPEE1K");
                    localforage.removeItem("MW2H0000001");
                    localforage.removeItem("4044400");
                    localforage.removeItem("RPTCNSVPXCSPPE10");
                    localforage.removeItem("4029138");
                    localforage.removeItem("RPTCNSVPXCSPPE200");
                    localforage.removeItem("RPTCNSVPXCSPPE1K");
                    localforage.removeItem("MW2H0000002");
                    localforage.removeItem("4044401");
                    localforage.removeItem("4044404");
                    break;
                case '10Mbps-Premium':
                    localforage.setItem("RPTCNSVPXCSPPE10", AdcLicensingQty);
                    localforage.removeItem("RPTCNSVPXCSPSE10");
                    localforage.removeItem("4029130");
                    localforage.removeItem("RPTCNSVPXCSPSE200");
                    localforage.removeItem("RPTCNSVPXCSPSE1K");
                    localforage.removeItem("MW2H0000000");
                    localforage.removeItem("4043769");
                    localforage.removeItem("4044402");
                    localforage.removeItem("RPTCNSVPXCSPEE10");
                    localforage.removeItem("4029134");
                    localforage.removeItem("RPTCNSVPXCSPEE200");
                    localforage.removeItem("RPTCNSVPXCSPEE1K");
                    localforage.removeItem("MW2H0000001");
                    localforage.removeItem("4044400");
                    localforage.removeItem("4044403");
                    localforage.removeItem("4029138");
                    localforage.removeItem("RPTCNSVPXCSPPE200");
                    localforage.removeItem("RPTCNSVPXCSPPE1K");
                    localforage.removeItem("MW2H0000002");
                    localforage.removeItem("4044401");
                    localforage.removeItem("4044404");
                    break;
                case '50Mbps-Premium':
                    localforage.setItem("4029138", AdcLicensingQty);
                    localforage.removeItem("RPTCNSVPXCSPSE10");
                    localforage.removeItem("4029130");
                    localforage.removeItem("RPTCNSVPXCSPSE200");
                    localforage.removeItem("RPTCNSVPXCSPSE1K");
                    localforage.removeItem("MW2H0000000");
                    localforage.removeItem("4043769");
                    localforage.removeItem("4044402");
                    localforage.removeItem("RPTCNSVPXCSPEE10");
                    localforage.removeItem("4029134");
                    localforage.removeItem("RPTCNSVPXCSPEE200");
                    localforage.removeItem("RPTCNSVPXCSPEE1K");
                    localforage.removeItem("MW2H0000001");
                    localforage.removeItem("4044400");
                    localforage.removeItem("4044403");
                    localforage.removeItem("RPTCNSVPXCSPPE10");
                    localforage.removeItem("RPTCNSVPXCSPPE200");
                    localforage.removeItem("RPTCNSVPXCSPPE1K");
                    localforage.removeItem("MW2H0000002");
                    localforage.removeItem("4044401");
                    localforage.removeItem("4044404");
                    break;
                case '200Mbps-Premium':
                    localforage.setItem("RPTCNSVPXCSPPE200", AdcLicensingQty);
                    localforage.removeItem("RPTCNSVPXCSPSE10");
                    localforage.removeItem("4029130");
                    localforage.removeItem("RPTCNSVPXCSPSE200");
                    localforage.removeItem("RPTCNSVPXCSPSE1K");
                    localforage.removeItem("MW2H0000000");
                    localforage.removeItem("4043769");
                    localforage.removeItem("4044402");
                    localforage.removeItem("RPTCNSVPXCSPEE10");
                    localforage.removeItem("4029134");
                    localforage.removeItem("RPTCNSVPXCSPEE200");
                    localforage.removeItem("RPTCNSVPXCSPEE1K");
                    localforage.removeItem("MW2H0000001");
                    localforage.removeItem("4044400");
                    localforage.removeItem("4044403");
                    localforage.removeItem("RPTCNSVPXCSPPE10");
                    localforage.removeItem("4029138");
                    localforage.removeItem("RPTCNSVPXCSPPE1K");
                    localforage.removeItem("MW2H0000002");
                    localforage.removeItem("4044401");
                    localforage.removeItem("4044404");
                    break;
                case '1000Mbps-Premium':
                    localforage.setItem("RPTCNSVPXCSPPE1K", AdcLicensingQty);
                    localforage.removeItem("RPTCNSVPXCSPSE10");
                    localforage.removeItem("4029130");
                    localforage.removeItem("RPTCNSVPXCSPSE200");
                    localforage.removeItem("RPTCNSVPXCSPSE1K");
                    localforage.removeItem("MW2H0000000");
                    localforage.removeItem("4043769");
                    localforage.removeItem("4044402");
                    localforage.removeItem("RPTCNSVPXCSPEE10");
                    localforage.removeItem("4029134");
                    localforage.removeItem("RPTCNSVPXCSPEE200");
                    localforage.removeItem("RPTCNSVPXCSPEE1K");
                    localforage.removeItem("MW2H0000001");
                    localforage.removeItem("4044400");
                    localforage.removeItem("4044403");
                    localforage.removeItem("RPTCNSVPXCSPPE10");
                    localforage.removeItem("4029138");
                    localforage.removeItem("RPTCNSVPXCSPPE200");
                    localforage.removeItem("MW2H0000002");
                    localforage.removeItem("4044401");
                    localforage.removeItem("4044404");
                    break;
                case '3000Mbps-Premium':
                    localforage.setItem("MW2H0000002", AdcLicensingQty);
                    localforage.removeItem("RPTCNSVPXCSPSE10");
                    localforage.removeItem("4029130");
                    localforage.removeItem("RPTCNSVPXCSPSE200");
                    localforage.removeItem("RPTCNSVPXCSPSE1K");
                    localforage.removeItem("MW2H0000000");
                    localforage.removeItem("4043769");
                    localforage.removeItem("4044402");
                    localforage.removeItem("RPTCNSVPXCSPEE10");
                    localforage.removeItem("4029134");
                    localforage.removeItem("RPTCNSVPXCSPEE200");
                    localforage.removeItem("RPTCNSVPXCSPEE1K");
                    localforage.removeItem("MW2H0000001");
                    localforage.removeItem("4044400");
                    localforage.removeItem("4044403");
                    localforage.removeItem("RPTCNSVPXCSPPE10");
                    localforage.removeItem("4029138");
                    localforage.removeItem("RPTCNSVPXCSPPE200");
                    localforage.removeItem("RPTCNSVPXCSPPE1K");
                    localforage.removeItem("4044401");
                    localforage.removeItem("4044404");
                    break;
                case '5000Mbps-Premium':
                    localforage.setItem("4044401", AdcLicensingQty);
                    localforage.removeItem("RPTCNSVPXCSPSE10");
                    localforage.removeItem("4029130");
                    localforage.removeItem("RPTCNSVPXCSPSE200");
                    localforage.removeItem("RPTCNSVPXCSPSE1K");
                    localforage.removeItem("MW2H0000000");
                    localforage.removeItem("4043769");
                    localforage.removeItem("4044402");
                    localforage.removeItem("RPTCNSVPXCSPEE10");
                    localforage.removeItem("4029134");
                    localforage.removeItem("RPTCNSVPXCSPEE200");
                    localforage.removeItem("RPTCNSVPXCSPEE1K");
                    localforage.removeItem("MW2H0000001");
                    localforage.removeItem("4044400");
                    localforage.removeItem("4044403");
                    localforage.removeItem("RPTCNSVPXCSPPE10");
                    localforage.removeItem("4029138");
                    localforage.removeItem("RPTCNSVPXCSPPE200");
                    localforage.removeItem("RPTCNSVPXCSPPE1K");
                    localforage.removeItem("MW2H0000002");
                    localforage.removeItem("4044404");
                    break;
                case '8000Mbps-Premium':
                    localforage.setItem("4044404", AdcLicensingQty);
                    localforage.removeItem("RPTCNSVPXCSPSE10");
                    localforage.removeItem("4029130");
                    localforage.removeItem("RPTCNSVPXCSPSE200");
                    localforage.removeItem("RPTCNSVPXCSPSE1K");
                    localforage.removeItem("MW2H0000000");
                    localforage.removeItem("4043769");
                    localforage.removeItem("4044402");
                    localforage.removeItem("RPTCNSVPXCSPEE10");
                    localforage.removeItem("4029134");
                    localforage.removeItem("RPTCNSVPXCSPEE200");
                    localforage.removeItem("RPTCNSVPXCSPEE1K");
                    localforage.removeItem("MW2H0000001");
                    localforage.removeItem("4044400");
                    localforage.removeItem("4044403");
                    localforage.removeItem("RPTCNSVPXCSPPE10");
                    localforage.removeItem("4029138");
                    localforage.removeItem("RPTCNSVPXCSPPE200");
                    localforage.removeItem("RPTCNSVPXCSPPE1K");
                    localforage.removeItem("MW2H0000002");
                    localforage.removeItem("4044401");
                    break;
                default:
                    localforage.removeItem("RPTCNSVPXCSPSE10");
                    localforage.removeItem("4029130");
                    localforage.removeItem("RPTCNSVPXCSPSE200");
                    localforage.removeItem("RPTCNSVPXCSPSE1K");
                    localforage.removeItem("MW2H0000000");
                    localforage.removeItem("4043769");
                    localforage.removeItem("4044402");
                    localforage.removeItem("RPTCNSVPXCSPEE10");
                    localforage.removeItem("4029134");
                    localforage.removeItem("RPTCNSVPXCSPEE200");
                    localforage.removeItem("RPTCNSVPXCSPEE1K");
                    localforage.removeItem("MW2H0000001");
                    localforage.removeItem("4044400");
                    localforage.removeItem("4044403");
                    localforage.removeItem("RPTCNSVPXCSPPE10");
                    localforage.removeItem("4029138");
                    localforage.removeItem("RPTCNSVPXCSPPE200");
                    localforage.removeItem("RPTCNSVPXCSPPE1K");
                    localforage.removeItem("MW2H0000002");
                    localforage.removeItem("4044401");
                    localforage.removeItem("4044404");
            }
        } else {
            localforage.removeItem("RPTCNSVPXCSPSE10");
            localforage.removeItem("4029130");
            localforage.removeItem("RPTCNSVPXCSPSE200");
            localforage.removeItem("RPTCNSVPXCSPSE1K");
            localforage.removeItem("MW2H0000000");
            localforage.removeItem("4043769");
            localforage.removeItem("4044402");
            localforage.removeItem("RPTCNSVPXCSPEE10");
            localforage.removeItem("4029134");
            localforage.removeItem("RPTCNSVPXCSPEE200");
            localforage.removeItem("RPTCNSVPXCSPEE1K");
            localforage.removeItem("MW2H0000001");
            localforage.removeItem("4044400");
            localforage.removeItem("4044403");
            localforage.removeItem("RPTCNSVPXCSPPE10");
            localforage.removeItem("4029138");
            localforage.removeItem("RPTCNSVPXCSPPE200");
            localforage.removeItem("RPTCNSVPXCSPPE1K");
            localforage.removeItem("MW2H0000002");
            localforage.removeItem("4044401");
            localforage.removeItem("4044404");
        }
    }, [AdcLicensing, AdcLicensingQty, AdcLicensingType, AdcLicensingVersion])

    // useEffect that sets the MS-Citrix Master HSD sku in localForage
    useEffect(() => {
        if (HsdImages > 0) {
            localforage.setItem("MS-Citrix Master HSD", HsdImages);
        } else {
            localforage.removeItem("MS-Citrix Master HSD");
        }
    }, [HsdImages])

    // useEffect that sets the MS-Citrix Master VDI sku in localForage
    useEffect(() => {
        if (VdiImages > 0) {
            localforage.setItem("MS-Citrix Master VDI", VdiImages);
        } else {
            localforage.removeItem("MS-Citrix Master VDI");
        }
    }, [VdiImages])

    // useEffect that sets the Citrix License skus in localForage
    useEffect(() => {
        if (EucEnabled && EucType === "citrix" && CitrixLicenseType === "Customer") {
            localforage.setItem("Citrix License", EucUsers);
            localforage.removeItem("MS - UHMC");
            localforage.removeItem("MS - 4084654");
            localforage.removeItem("MS - 4082413");
            localforage.removeItem("MS - 4084650");
            localforage.removeItem("MS - 4070577");
        } else if (EucEnabled && EucType === "citrix" && CitrixLicenseType === "UHMC") {
            localforage.setItem("MS - UHMC", EucUsers);
            localforage.removeItem("Citrix License");
            localforage.removeItem("MS - 4084654");
            localforage.removeItem("MS - 4082413");
            localforage.removeItem("MS - 4084650");
            localforage.removeItem("MS - 4070577");
        } else if (EucEnabled && EucType === "citrix" && CitrixLicenseType === "PremiumPlus") {
            localforage.setItem("MS - 4084654", EucUsers);
            localforage.removeItem("Citrix License");
            localforage.removeItem("MS - UHMC");
            localforage.removeItem("MS - 4082413");
            localforage.removeItem("MS - 4084650");
            localforage.removeItem("MS - 4070577");
        } else if (EucEnabled && EucType === "citrix" && CitrixLicenseType === "Advanced") {
            localforage.setItem("MS - 4082413", EucUsers);
            localforage.removeItem("Citrix License");
            localforage.removeItem("MS - UHMC");
            localforage.removeItem("MS - 4084654");
            localforage.removeItem("MS - 4084650");
            localforage.removeItem("MS - 4070577");
        } else if (EucEnabled && EucType === "citrix" && CitrixLicenseType === "AdvancedPlus") {
            localforage.setItem("MS - 4084650", EucUsers);
            localforage.removeItem("Citrix License");
            localforage.removeItem("MS - UHMC");
            localforage.removeItem("MS - 4084654");
            localforage.removeItem("MS - 4082413");
            localforage.removeItem("MS - 4070577");
        } else if (EucEnabled && EucType === "citrix" && CitrixLicenseType === "Azure") {
            localforage.setItem("MS - 4070577", EucUsers);
            localforage.removeItem("Citrix License");
            localforage.removeItem("MS - UHMC");
            localforage.removeItem("MS - 4084654");
            localforage.removeItem("MS - 4082413");
            localforage.removeItem("MS - 4084650");
        } else {
            localforage.removeItem("Citrix License");
            localforage.removeItem("MS - UHMC");
            localforage.removeItem("MS - 4084654");
            localforage.removeItem("MS - 4082413");
            localforage.removeItem("MS - 4084650");
            localforage.removeItem("MS - 4070577");
        }
    })

    // useEffect that sets the MS - Content Collaboration sku in localForage
    useEffect(() => {
        if (ContentCollaberation) {
            localforage.setItem("MS - Content Collaboration", ContentCollaberationCount);
        } else {
            localforage.removeItem("MS - Content Collaboration");
        }
    }, [ContentCollaberation, ContentCollaberationCount])

    // useEffect that sets the MS - Service Desk sku in localForage
    useEffect(() => {
        if (EucUsers > 0 || AvdUsers > 0) {
            localforage.setItem("MS - Service Desk", (EucUsers + AvdUsers));
        } else {
            localforage.removeItem("MS - Service Desk");
        }
    }, [EucUsers, AvdUsers])

    // useEffect that sets the MS - Service Desk Discount sku in localForage
    useEffect(() => {
        if ((EucUsers + AvdUsers) > 49) {
            localforage.setItem("MS - Service Desk Discount", (EucUsers + AvdUsers) - 49);
        } else {
            localforage.removeItem("MS - Service Desk Discount");
        }
    }, [EucUsers, AvdUsers])

    // useEffect that sets the MS - Azure VD Mgmt sku in localForage
    useEffect(() => {
        if (EucType2 === "avd") {
            localforage.setItem("MS - Azure VD Mgmt", AvdUsers);
        } else {
            localforage.removeItem("MS - Azure VD Mgmt");
        }
    }, [EucType2, AvdUsers])

    // useEffect that sets the MS - Logon Simulator sku in localForage
    useEffect(() => {
        if (EucEnabled && (EucType === "citrix" || EucType2 === "avd")) {
            localforage.setItem("MS - Logon Simulator", 1);
        } else {
            localforage.removeItem("MS - Logon Simulator");
        }
    }, [EucEnabled, EucType, EucType2])

    // useEffect that sets the MS - Nerdio sku in localForage
    useEffect(() => {
        if (EucEnabled && EucType2 === "avd" && Nerdio) {
            localforage.setItem("MS - Nerdio", AvdUsers);
        } else {
            localforage.removeItem("MS - Nerdio");
        }
    }, [EucEnabled, EucType2, Nerdio, AvdUsers])

    // useEffect that sets the MS - ControlUp sku in localForage
    useEffect(() => {
        if (EucUsers > 0 || AvdUsers > 0) {
            localforage.setItem("MS - ControlUp", (EucUsers + AvdUsers));
        } else {
            localforage.removeItem("MS - ControlUp");
        }
    }, [EucUsers, AvdUsers])

    // useEffect that sets the ADC With Citrix Reference skus in localForage
    useEffect(() => {
        if (AdcCitrix) {
            localforage.setItem("ADC Citrix Count Reference", AdcCitrixCount);
            localforage.setItem("ADC Citrix vCPU Count Reference", AdcCitrixCpu);
            localforage.setItem("ADC Citrix Mem Count Reference", AdcCitrixMem);
        } else {
            localforage.removeItem("ADC Citrix Count Reference");
            localforage.removeItem("ADC Citrix vCPU Count Reference");
            localforage.removeItem("ADC Citrix Mem Count Reference");
        }
    }, [AdcCitrix, AdcCitrixCount, AdcCitrixCpu, AdcCitrixMem])

    // useEffect that sets the Load Balancers Reference skus in localForage
    useEffect(() => {
        if (LoadBalancerWoCitrix) {
            localforage.setItem("Load Balancers Count Reference", LoadBalancerWoCitrixCount);
            localforage.setItem("Load Balancers vCPU Count Reference", LoadBalancerWoCitrixCpu);
            localforage.setItem("Load Balancers Mem Count Reference", LoadBalancerWoCitrixMem);
        } else {
            localforage.removeItem("Load Balancers Count Reference");
            localforage.removeItem("Load Balancers vCPU Count Reference");
            localforage.removeItem("Load Balancers Mem Count Reference");
        }
    }, [LoadBalancerWoCitrix, LoadBalancerWoCitrixCount, LoadBalancerWoCitrixCpu, LoadBalancerWoCitrixMem])

    // useEffect that sets the ADC Reference skus in localForage
    useEffect(() => {
        if (AdcLicensing) {
            localforage.setItem("ADC Licensing Count Reference", AdcLicensingQty);
        } else {
            localforage.removeItem("ADC Licensing Count Reference");
        }
    }, [AdcLicensing, AdcLicensingQty])

    // useEffects for the ADC Licensing reference skus
    useEffect(() => {
        if (AdcLicensing && AdcLicensingType === "10Mbps") {
            localforage.setItem("ADC Licensing 10 Mbps Type Reference", 1);
        } else {
            localforage.removeItem("ADC Licensing 10 Mbps Type Reference");
        }
    }, [AdcLicensing, AdcLicensingType])

    useEffect(() => {
        if (AdcLicensing && AdcLicensingType === "50Mbps") {
            localforage.setItem("ADC Licensing 50 Mbps Type Reference", 1);
        } else {
            localforage.removeItem("ADC Licensing 50 Mbps Type Reference");
        }
    }, [AdcLicensing, AdcLicensingType])

    useEffect(() => {
        if (AdcLicensing && AdcLicensingType === "200Mbps") {
            localforage.setItem("ADC Licensing 200 Mbps Type Reference", 1);
        } else {
            localforage.removeItem("ADC Licensing 200 Mbps Type Reference");
        }
    }, [AdcLicensing, AdcLicensingType])

    useEffect(() => {
        if (AdcLicensing && AdcLicensingType === "1000Mbps") {
            localforage.setItem("ADC Licensing 1000 Mbps Type Reference", 1);
        } else {
            localforage.removeItem("ADC Licensing 1000 Mbps Type Reference");
        }
    }, [AdcLicensing, AdcLicensingType])

    useEffect(() => {
        if (AdcLicensing && AdcLicensingType === "3000Mbps") {
            localforage.setItem("ADC Licensing 3000 Mbps Type Reference", 1);
        } else {
            localforage.removeItem("ADC Licensing 3000 Mbps Type Reference");
        }
    }, [AdcLicensing, AdcLicensingType])

    useEffect(() => {
        if (AdcLicensing && AdcLicensingType === "5000Mbps") {
            localforage.setItem("ADC Licensing 5000 Mbps Type Reference", 1);
        } else {
            localforage.removeItem("ADC Licensing 5000 Mbps Type Reference");
        }
    }, [AdcLicensing, AdcLicensingType])

    useEffect(() => {
        if (AdcLicensing && AdcLicensingType === "8000Mbps") {
            localforage.setItem("ADC Licensing 8000 Mbps Type Reference", 1);
        } else {
            localforage.removeItem("ADC Licensing 8000 Mbps Type Reference");
        }
    }, [AdcLicensing, AdcLicensingType])

    useEffect(() => {
        if (AdcLicensing && AdcLicensingVersion === "Standard") {
            localforage.setItem("ADC Licensing Standard Version Reference", 1);
        } else {
            localforage.removeItem("ADC Licensing Standard Version Reference");
        }
    }, [AdcLicensing, AdcLicensingVersion])

    useEffect(() => {
        if (AdcLicensing && AdcLicensingVersion === "Advanced") {
            localforage.setItem("ADC Licensing Advanced Version Reference", 1);
        } else {
            localforage.removeItem("ADC Licensing Advanced Version Reference");
        }
    }, [AdcLicensing, AdcLicensingVersion])

    useEffect(() => {
        if (AdcLicensing && AdcLicensingVersion === "Premium") {
            localforage.setItem("ADC Licensing Premium Version Reference", 1);
        } else {
            localforage.removeItem("ADC Licensing Premium Version Reference");
        }
    }, [AdcLicensing, AdcLicensingVersion])

    // Setting skus from MS Services section in the Excel spreadsheet
    // useEffect that sets the MS - Unix/Linux Server sku in localForage
    useEffect(() => {
        if (UnixLinuxServerTotalQty > 0) {
            localforage.setItem("MS - Unix/Linux Server", UnixLinuxServerTotalQty);
        } else {
            localforage.removeItem("MS - Unix/Linux Server");
        }
    }, [UnixLinuxServerTotalQty])

    // useEffect that sets the MS - Unix/Linux Discount sku in localForage
    useEffect(() => {
        const sku12x6Key = `${skuConfig[394]}-394`; // Unique key for SKU with ID 394
        const sku8x5Key = `${skuConfig[392]}-392`; // Unique key for SKU with ID 392

        if (UnixLinuxServerTotalQty > 0 && Contract8x5or12x6 && Contract8x5or12x6Type === "8x5") {
            localforage.removeItem(sku12x6Key);
            localforage.setItem(sku8x5Key, UnixLinuxServerTotalQty);
        } else if (UnixLinuxServerTotalQty > 0 && Contract8x5or12x6 && Contract8x5or12x6Type === "12x6") {
            localforage.removeItem(sku8x5Key);
            localforage.setItem(sku12x6Key, UnixLinuxServerTotalQty);
        } else {
            localforage.removeItem(sku8x5Key);
            localforage.removeItem(sku12x6Key);
        }
    }, [UnixLinuxServerTotalQty, Contract8x5or12x6, Contract8x5or12x6Type]);

    // useEffect that sets the MS - Windows Desktop sku in localForage
    useEffect(() => {
        if (DesktopPatchingEnabled) {
            localforage.setItem("MS - Windows Desktop", DesktopPatchingQuantity);
        } else {
            localforage.removeItem("MS - Windows Desktop");
        }
    }, [DesktopPatchingEnabled, DesktopPatchingQuantity])

    // useEffect that sets the MS - Windows Server Patching Only sku in localForage
    useEffect(() => {
        if (ServerManagementEnabled && ServerPatchingOnly) {
            localforage.setItem("MS - Windows Server Patching Only", ServerManagementQuantity);
        } else {
            localforage.removeItem("MS - Windows Server Patching Only");
        }
    }, [ServerManagementEnabled, ServerManagementQuantity, ServerPatchingOnly])

    // useEffect that sets the BU2506 sku in localForage
    useEffect(() => {
        if (RemoteDesktops) {
            localforage.setItem("BU2506", RemoteDesktopsQty);
        } else {
            localforage.removeItem("BU2506");
        }
    }, [RemoteDesktops, RemoteDesktopsQty])

    // useEffect that sets the MS - Windows Server sku in localForage
    useEffect(() => {
        if (MsWindowsServerTotalQty > 0) {
            localforage.setItem("MS - Windows Server", MsWindowsServerTotalQty);
        } else {
            localforage.removeItem("MS - Windows Server");
        }
    }, [MsWindowsServerTotalQty])

    // useEffect that sets the MS - Windows Discount sku in localForage
    useEffect(() => {
        const sku8x5Key = `${skuConfig[396]}-396`;
        const sku12x6Key = `${skuConfig[398]}-398`;
        if (MsWindowsServerTotalQty > 0 && Contract8x5or12x6 && Contract8x5or12x6Type === "8x5") {
            localforage.setItem(sku8x5Key, MsWindowsServerTotalQty);
            localforage.removeItem(sku12x6Key);
        } else if (MsWindowsServerTotalQty > 0 && Contract8x5or12x6 && Contract8x5or12x6Type === "12x6") {
            localforage.setItem(sku12x6Key, MsWindowsServerTotalQty);
            localforage.removeItem(sku8x5Key);
        }
        else {
            localforage.removeItem(sku8x5Key);
            localforage.removeItem(sku12x6Key);
        }
    }, [MsWindowsServerTotalQty, Contract8x5or12x6, Contract8x5or12x6Type])

    // useEffect that sets the MS - AD Standard/Advanced sku in localForage
    useEffect(() => {
        if (ActiveDirectoryManagement && ActiveDirectoryManagementOptions === "Standard") {
            localforage.setItem("MS - AD Standard", ActiveDirectoryManagementCount);
            localforage.removeItem("MS - AD Advanced");
        } else if (ActiveDirectoryManagement && ActiveDirectoryManagementOptions === "Advanced") {
            localforage.setItem("MS - AD Advanced", ActiveDirectoryManagementCount);
            localforage.removeItem("MS - AD Standard");
        } else {
            localforage.removeItem("MS - AD Standard");
            localforage.removeItem("MS - AD Advanced");
        }
    }, [ActiveDirectoryManagement, ActiveDirectoryManagementCount, ActiveDirectoryManagementOptions])

    // useEffect that sets the MS - AD User Discount sku in localForage
    useEffect(() => {
        const skuStandardKey = `${skuConfig[404]}-404`
        const skuAdvancedKey = `${skuConfig[406]}-406`
        if (ActiveDirectoryManagement && ActiveDirectoryManagementCount > 99 && ActiveDirectoryManagementOptions === "Standard") {
            localforage.setItem(skuStandardKey, ActiveDirectoryManagementCount - 99);
            localforage.removeItem(skuAdvancedKey);
        } else if (ActiveDirectoryManagement && ActiveDirectoryManagementCount > 99 && ActiveDirectoryManagementOptions === "Advanced") {
            localforage.setItem(skuAdvancedKey, ActiveDirectoryManagementCount - 99);
            localforage.removeItem(skuStandardKey);
        } else {
            localforage.removeItem(skuStandardKey);
            localforage.removeItem(skuAdvancedKey);
        }
    }, [ActiveDirectoryManagement, ActiveDirectoryManagementCount, ActiveDirectoryManagementOptions])

    // useEffect that sets the MS - Windows SQL Svr Lic sku in localForage
    useEffect(() => {
        if (SqlServerLicenseServerTotalQty > 0) {
            localforage.setItem("MS - Windows SQL Svr Lic", SqlServerLicenseServerTotalQty);
        } else {
            localforage.removeItem("MS - Windows SQL Svr Lic");
        }
    }, [SqlServerLicenseServerTotalQty])

    // useEffect that sets the MS - Windows SQL Svr Ent sku in localForage
    useEffect(() => {
        if (SqlServerEnterpriseLicenseServerTotalQty > 0) {
            localforage.setItem("MS - Windows SQL Svr Ent", SqlServerEnterpriseLicenseServerTotalQty);
        } else {
            localforage.removeItem("MS - Windows SQL Svr Ent");
        }
    }, [SqlServerEnterpriseLicenseServerTotalQty])

    // useEffect that sets the MS - PrintManageAAS sku in localForage
    useEffect(() => {
        if (ManagedPrintServices) {
            localforage.setItem("MS - PrintManageAAS", ManagedPrintServicesLocations);
        } else {
            localforage.removeItem("MS - PrintManageAAS");
        }
    }, [ManagedPrintServices, ManagedPrintServicesLocations])

    // useEffect that sets the MS - PrintManageAAS Users Reference sku in localForage
    useEffect(() => {
        if (ManagedPrintServices && ManagedPrintServicesUsers > 0) {
            localforage.setItem("MS - PrintManageAAS Users Reference", ManagedPrintServicesUsers);
        } else {
            localforage.removeItem("MS - PrintManageAAS Users Reference");
        }
    }, [ManagedPrintServices, ManagedPrintServicesUsers])

    // Setting skus from Security/Compliance section in the Excel spreadsheet
    // useEffect that sets the SECaaS-PP-ESS-M___-A skus in localForage
    useEffect(() => {
        if (EmailEnabled && EmailLicense === "Advanced") {
            localforage.setItem("SECaaS-PP-ESS-MADV-A", EmailProtection);
            localforage.removeItem("SECaaS-PP-ESS-MBEG-A");
            localforage.removeItem("SECaaS-PP-ESS-MBUS-A");
            localforage.removeItem("SECaaS-PP-ESS-MPRO-A");
        } else if (EmailEnabled && EmailLicense === "Beginner") {
            localforage.setItem("SECaaS-PP-ESS-MBEG-A", EmailProtection);
            localforage.removeItem("SECaaS-PP-ESS-MADV-A");
            localforage.removeItem("SECaaS-PP-ESS-MBUS-A");
            localforage.removeItem("SECaaS-PP-ESS-MPRO-A");
        } else if (EmailEnabled && EmailLicense === "Business") {
            localforage.setItem("SECaaS-PP-ESS-MBUS-A", EmailProtection);
            localforage.removeItem("SECaaS-PP-ESS-MADV-A");
            localforage.removeItem("SECaaS-PP-ESS-MBEG-A");
            localforage.removeItem("SECaaS-PP-ESS-MPRO-A");
        } else if (EmailEnabled && EmailLicense === "Professional") {
            localforage.setItem("SECaaS-PP-ESS-MPRO-A", EmailProtection);
            localforage.removeItem("SECaaS-PP-ESS-MADV-A");
            localforage.removeItem("SECaaS-PP-ESS-MBEG-A");
            localforage.removeItem("SECaaS-PP-ESS-MBUS-A");
        } else {
            localforage.removeItem("SECaaS-PP-ESS-MADV-A");
            localforage.removeItem("SECaaS-PP-ESS-MBEG-A");
            localforage.removeItem("SECaaS-PP-ESS-MBUS-A");
            localforage.removeItem("SECaaS-PP-ESS-MPRO-A");
        }
    }, [EmailEnabled, EmailLicense, EmailProtection])

    // useEffect that sets the PP-ESS-PSAT-A sku in localForage
    useEffect(() => {
        if (EmailEnabled && EmailTraining === "yes") {
            localforage.setItem("PP-ESS-PSAT-A", EmailProtection);
        } else {
            localforage.removeItem("PP-ESS-PSAT-A");
        }
    }, [EmailEnabled, EmailProtection, EmailTraining])

    // useEffect that sets the SECaaS-PP-Qty sku in localForage
    useEffect(() => {
        if (EmailEnabled && EmailProtection > 0) {
            localforage.setItem("SECaaS-PP-Qty", EmailProtection);
        } else {
            localforage.removeItem("SECaaS-PP-Qty");
        }
    }, [EmailEnabled, EmailProtection])

    // useEffect that sets the SECaaS - Risk Managment sku in localForage
    useEffect(() => {
        if (RiskManagement) {
            localforage.setItem("SECaaS - Risk Managment", RiskManagementCount);
        } else {
            localforage.removeItem("SECaaS - Risk Managment");
        }
    }, [RiskManagement, RiskManagementCount])

    // useEffect that sets the SECaaS - SIEM - Pro sku in localForage
    useEffect(() => {
        if (SiemEnabled) {
            localforage.setItem("SECaaS - SIEM - Pro", SiemCount);
        } else {
            localforage.removeItem("SECaaS - SIEM - Pro");
        }
    }, [SiemEnabled, SiemCount])

    // useEffect that sets the SECaaS - SIEM - App sku in localForage
    useEffect(() => {
        if (SiemEnabled && SiemSensorAppliance === "yes") {
            localforage.setItem("SECaaS- SIEM - App", SiemSensorApplianceCount);
        } else {
            localforage.removeItem("SECaaS- SIEM - App");
        }
    }, [SiemEnabled, SiemSensorAppliance, SiemSensorApplianceCount])

    // useEffect that sets the SECaas - NGFaaS sku in localForage
    useEffect(() => {
        if (NetworkFirewalls) {
            localforage.setItem("SECaas - NGFaaS", NetworkFirewallsQty);
        } else {
            localforage.removeItem("SECaas - NGFaaS");
        }
    }, [NetworkFirewalls, NetworkFirewallsQty])

    // useEffect that sets the SECaas - NGFaaS - Lic Reference skus in localForage
    useEffect(() => {
        if (NetworkFirewalls && SecSeries === "NoneSelected") {
            localforage.setItem("SEC None Selected Reference", 1);
            localforage.removeItem("SEC-50 Reference");
            localforage.removeItem("SEC-100 Reference");
            localforage.removeItem("SEC-200 Reference");
            localforage.removeItem("SEC-300 Reference");
            localforage.removeItem("SEC-1000-HV Reference");
            localforage.removeItem("SEC-500 Reference");
            localforage.removeItem("SEC-700 Reference");
        } else if (NetworkFirewalls && SecSeries === "SEC-50") {
            localforage.setItem("SEC-50 Reference", 1);
            localforage.removeItem("SEC None Selected Reference");
            localforage.removeItem("SEC-100 Reference");
            localforage.removeItem("SEC-200 Reference");
            localforage.removeItem("SEC-300 Reference");
            localforage.removeItem("SEC-1000-HV Reference");
            localforage.removeItem("SEC-500 Reference");
            localforage.removeItem("SEC-700 Reference");
        } else if (NetworkFirewalls && SecSeries === "SEC-100") {
            localforage.setItem("SEC-100 Reference", 1);
            localforage.removeItem("SEC None Selected Reference");
            localforage.removeItem("SEC-50 Reference");
            localforage.removeItem("SEC-200 Reference");
            localforage.removeItem("SEC-300 Reference");
            localforage.removeItem("SEC-1000-HV Reference");
            localforage.removeItem("SEC-500 Reference");
            localforage.removeItem("SEC-700 Reference");
        } else if (NetworkFirewalls && SecSeries === "SEC-200") {
            localforage.setItem("SEC-200 Reference", 1);
            localforage.removeItem("SEC None Selected Reference");
            localforage.removeItem("SEC-50 Reference");
            localforage.removeItem("SEC-100 Reference");
            localforage.removeItem("SEC-300 Reference");
            localforage.removeItem("SEC-1000-HV Reference");
            localforage.removeItem("SEC-500 Reference");
            localforage.removeItem("SEC-700 Reference");
        } else if (NetworkFirewalls && SecSeries === "SEC-300") {
            localforage.setItem("SEC-300 Reference", 1);
            localforage.removeItem("SEC None Selected Reference");
            localforage.removeItem("SEC-50 Reference");
            localforage.removeItem("SEC-100 Reference");
            localforage.removeItem("SEC-200 Reference");
            localforage.removeItem("SEC-1000-HV Reference");
            localforage.removeItem("SEC-500 Reference");
            localforage.removeItem("SEC-700 Reference");
        } else if (NetworkFirewalls && SecSeries === "SEC-500") {
            localforage.setItem("SEC-500 Reference", 1);
            localforage.removeItem("SEC None Selected Reference");
            localforage.removeItem("SEC-50 Reference");
            localforage.removeItem("SEC-100 Reference");
            localforage.removeItem("SEC-200 Reference");
            localforage.removeItem("SEC-300 Reference");
            localforage.removeItem("SEC-1000-HV Reference");
            localforage.removeItem("SEC-700 Reference");
        } else if (NetworkFirewalls && SecSeries === "SEC-700") {
            localforage.setItem("SEC-700 Reference", 1);
            localforage.removeItem("SEC None Selected Reference");
            localforage.removeItem("SEC-50 Reference");
            localforage.removeItem("SEC-100 Reference");
            localforage.removeItem("SEC-200 Reference");
            localforage.removeItem("SEC-300 Reference");
            localforage.removeItem("SEC-1000-HV Reference");
            localforage.removeItem("SEC-500 Reference");
        } else if (NetworkFirewalls && SecSeries === "SEC-1000-HV") {
            localforage.setItem("SEC-1000-HV Reference", 1);
            localforage.removeItem("SEC None Selected Reference");
            localforage.removeItem("SEC-50 Reference");
            localforage.removeItem("SEC-100 Reference");
            localforage.removeItem("SEC-200 Reference");
            localforage.removeItem("SEC-300 Reference");
            localforage.removeItem("SEC-500 Reference");
            localforage.removeItem("SEC-700 Reference");
        } else {
            localforage.removeItem("SEC None Selected Reference", 1);
            localforage.removeItem("SEC-50 Reference");
            localforage.removeItem("SEC-100 Reference");
            localforage.removeItem("SEC-200 Reference");
            localforage.removeItem("SEC-300 Reference");
            localforage.removeItem("SEC-1000-HV Reference");
            localforage.removeItem("SEC-500 Reference");
            localforage.removeItem("SEC-700 Reference");
        }
    }, [NetworkFirewalls, PaloAltoLicenses, SecSeries])

    // useEffect that sets the SECaas - NGFaaS - Lic Options skus in localForage
    useEffect(() => {
        if (NetworkFirewalls && FirewallTP) {
            localforage.setItem("SECaas - NGFaaS  - Threat Prevention Reference", PaloAltoLicenses);
        } else {
            localforage.removeItem("SECaas - NGFaaS  - Threat Prevention Reference");
        }
    }, [FirewallTP, PaloAltoLicenses, NetworkFirewalls])

    useEffect(() => {
        if (NetworkFirewalls && FirewallWMA) {
            localforage.setItem("SECaas - NGFaaS  - Wildfire Malware Analysis Reference", PaloAltoLicenses);
        } else {
            localforage.removeItem("SECaas - NGFaaS  - Wildfire Malware Analysis Reference");
        }
    }, [FirewallWMA, PaloAltoLicenses, NetworkFirewalls])

    useEffect(() => {
        if (NetworkFirewalls && FirewallUrlFiltering) {
            localforage.setItem("SECaas - NGFaaS  - URL Filtering Reference", PaloAltoLicenses);
        } else {
            localforage.removeItem("SECaas - NGFaaS  - URL Filtering Reference");
        }
    }, [FirewallUrlFiltering, PaloAltoLicenses, NetworkFirewalls])

    useEffect(() => {
        if (NetworkFirewalls && FirewallDnsSecurity) {
            localforage.setItem("SECaas - NGFaaS  - DNS Security Reference", PaloAltoLicenses);
        } else {
            localforage.removeItem("SECaas - NGFaaS  - DNS Security Reference");
        }
    }, [FirewallDnsSecurity, PaloAltoLicenses, NetworkFirewalls])

    useEffect(() => {
        if (NetworkFirewalls && FirewallSdWan) {
            localforage.setItem("SECaas - NGFaaS  - SD-Wan Reference", PaloAltoLicenses);
        } else {
            localforage.removeItem("SECaas - NGFaaS  - SD-Wan Reference");
        }
    }, [FirewallSdWan, PaloAltoLicenses, NetworkFirewalls])

    useEffect(() => {
        if (NetworkFirewalls && FirewallGlobalProtect) {
            localforage.setItem("SECaas - NGFaaS  - Global Protect Reference", PaloAltoLicenses);
        } else {
            localforage.removeItem("SECaas - NGFaaS  - Global Protect Reference");
        }
    }, [FirewallGlobalProtect, PaloAltoLicenses, NetworkFirewalls])

    useEffect(() => {
        if (NetworkFirewalls && FirewallVPfM) {
            localforage.setItem("SECaas - NGFaaS  - Virtual Panorama for Management Reference", PaloAltoLicenses);
        } else {
            localforage.removeItem("SECaas - NGFaaS  - Virtual Panorama for Management Reference");
        }
    }, [FirewallVPfM, PaloAltoLicenses, NetworkFirewalls])

    useEffect(() => {
        if (PaloAltoLicenses > 0 && NetworkFirewalls) {
            localforage.setItem("Palo Alto Reference", PaloAltoLicenses);
        } else {
            localforage.removeItem("Palo Alto Reference");
        }
    }, [PaloAltoLicenses, NetworkFirewalls])

    useEffect(() => {
        if (SecSeries > 0 && NetworkFirewalls) {
            localforage.setItem("Palo Alto Reference", PaloAltoLicenses);
        } else {
            localforage.removeItem("Palo Alto Reference");
        }
    }, [PaloAltoLicenses, NetworkFirewalls])

    // useEffect that sets the SECaaS - EPPAAS-MDR-Lic sku in localForage
    useEffect(() => {
        if (Huntress) {
            localforage.setItem("SECaaS - EPPAAS-MDR-Lic", HuntressCount);
        } else {
            localforage.removeItem("SECaaS - EPPAAS-MDR-Lic");
        }
    }, [Huntress, HuntressCount])

    // useEffect that sets the SECaaS - EPPAAS-EDR sku in localForage
    useEffect(() => {
        if (Huntress) {
            localforage.setItem("SECaaS - EPPAAS-EDR", HuntressCount);
        } else {
            localforage.removeItem("SECaaS - EPPAAS-EDR");
        }
    }, [Huntress, HuntressCount])

    // useEffect that sets the SECaaS - M365 - MDR - Lic sku in localForage
    useEffect(() => {
        if (Huntress && HuntressM365Support) {
            localforage.setItem("SECaaS - M365 - MDR - Lic", HuntressCount);
        } else {
            localforage.removeItem("SECaaS - M365 - MDR - Lic");
        }
    }, [Huntress, HuntressM365Support, HuntressCount])

    // useEffect that sets the SECaaS - ZeroTrust - Lic sku in localForage
    // I believe this is no longer in effect
    // useEffect(() => {
    //     if (ZeroTrust) {
    //         localforage.setItem("SECaaS - ZeroTrust - Lic", ZeroTrustCount);
    //     } else {
    //         localforage.removeItem("SECaaS - ZeroTrust - Lic");
    //     }
    // }, [ZeroTrust, ZeroTrustCount])

    // useEffect that sets the ZeroTrust skus in localForage
    // useEffect that sets the SECaaS - ZeroTrust - Service Fee sku in localForage
    useEffect(() => {
        if (ZeroTrust) {
            localforage.setItem("SECaaS - ZT - Service", ZeroTrustCount);
        } else {
            localforage.removeItem("SECaaS - ZT - Service");
        }
    }, [ZeroTrust, ZeroTrustCount])

    // useEffect that sets the SECaaS - ZeroTrust - Storage Control sku in localForage
    useEffect(() => {
        if (ZeroTrust && ZeroTrustStorageControl) {
            localforage.setItem("SECaaS - ZT - SC", ZeroTrustCount);
        } else {
            localforage.removeItem("SECaaS - ZT - SC");
        }
    }, [ZeroTrust, ZeroTrustCount, ZeroTrustStorageControl])

    // useEffect that sets the SECaaS - ZeroTrust - Elevation sku in localForage
    useEffect(() => {
        if (ZeroTrust && ZeroTrustElevation) {
            localforage.setItem("SECaaS - ZT - Elevation", ZeroTrustCount);
        } else {
            localforage.removeItem("SECaaS - ZT - Elevation");
        }
    }, [ZeroTrust, ZeroTrustCount, ZeroTrustElevation])

    // useEffect that sets the SECaaS - ZeroTrust - ThreatLocker Detect sku in localForage
    useEffect(() => {
        if (ZeroTrust && ZeroTrustThreatLockerDetect) {
            localforage.setItem("SECaaS - ZT - TL Detect", ZeroTrustCount);
        } else {
            localforage.removeItem("SECaaS - ZT - TL Detect");
        }
    }, [ZeroTrust, ZeroTrustCount, ZeroTrustThreatLockerDetect])

    // useEffect that sets the SECaaS - KEP sku in localForage
    useEffect(() => {
        if (KeeperEnabled) {
            localforage.setItem("SECaaS - KEP", KeeperInstances);
        } else {
            localforage.removeItem("SECaaS - KEP");
        }
    }, [KeeperEnabled, KeeperInstances])

    // Setting skus from Other section in the Excel spreadsheet
    // useEffect that sets the MS - O365 User Mgmt sku in localForage
    useEffect(() => {
        let m365TotalUsers = 0;
        let m365BasicUsers = 0;
        let m365StandardUsers = 0;
        let m365PremiumUsers = 0;
        let m365E3Users = 0;
        let m365E5Users = 0;
        if (Microsoft365BusBasic && Microsoft365BusBasicType === "UserManagement") {
            m365BasicUsers += Microsoft365BusBasicCount;
        }
        if (!Microsoft365BusBasic || Microsoft365BusBasicType !== "UserManagement") {
            m365BasicUsers = 0;
        }
        if (Microsoft365BusPremium && Microsoft365BusPremiumType === "UserManagement") {
            m365PremiumUsers += Microsoft365BusPremiumCount;
        }
        if (!Microsoft365BusPremium || Microsoft365BusPremiumType !== "UserManagement") {
            m365PremiumUsers = 0;
        }
        if (Microsoft365BusStandard && Microsoft365BusStandardType === "UserManagement") {
            m365StandardUsers += Microsoft365BusStandardCount;
        }
        if (!Microsoft365BusStandard || Microsoft365BusStandardType !== "UserManagement") {
            m365StandardUsers = 0;
        }
        if (Microsoft365E3 && Microsoft365E3Type === "UserManagement") {
            m365E3Users += Microsoft365E3Count;
        }
        if (!Microsoft365E3 || Microsoft365E3Type !== "UserManagement") {
            m365E3Users = 0;
        }
        if (Microsoft365E5 && Microsoft365E5Type === "UserManagement") {
            m365E5Users += Microsoft365E5Count;
        }
        if (!Microsoft365E5 || Microsoft365E5Type !== "UserManagement") {
            m365E5Users = 0;
        }
        m365TotalUsers = m365BasicUsers + m365StandardUsers + m365PremiumUsers + m365E3Users + m365E5Users;
        if (m365TotalUsers > 0) {
            localforage.setItem("MS - O365 User Mgmt", m365TotalUsers);
        }
        if (m365TotalUsers <= 0) {
            localforage.removeItem("MS - O365 User Mgmt");
        }
    }, [Microsoft365BusBasic, Microsoft365BusBasicType, Microsoft365BusBasicCount,
        Microsoft365BusPremium, Microsoft365BusPremiumType, Microsoft365BusPremiumCount,
        Microsoft365BusStandard, Microsoft365BusStandardType, Microsoft365BusStandardCount,
        Microsoft365E3, Microsoft365E3Type, Microsoft365E3Count,
        Microsoft365E5, Microsoft365E5Type, Microsoft365E5Count]);

    // useEffect that sets the MS - Adv O365 Support sku in localForage
    useEffect(() => {
        let m365TotalUsersAdv = 0;
        let m365BasicUsersAdv = 0;
        let m365StandardUsersAdv = 0;
        let m365PremiumUsersAdv = 0;
        let m365E3UsersAdv = 0;
        let m365E5UsersAdv = 0;
        if (Microsoft365BusBasic && Microsoft365BusBasicType === "AdvSupport") {
            m365BasicUsersAdv += Microsoft365BusBasicCount;
        }
        if (!Microsoft365BusBasic || Microsoft365BusBasicType !== "AdvSupport") {
            m365BasicUsersAdv = 0;
        }
        if (Microsoft365BusPremium && Microsoft365BusPremiumType === "AdvSupport") {
            m365PremiumUsersAdv += Microsoft365BusPremiumCount;
        }
        if (!Microsoft365BusPremium || Microsoft365BusPremiumType !== "AdvSupport") {
            m365PremiumUsersAdv = 0;
        }
        if (Microsoft365BusStandard && Microsoft365BusStandardType === "AdvSupport") {
            m365StandardUsersAdv += Microsoft365BusStandardCount;
        }
        if (!Microsoft365BusStandard || Microsoft365BusStandardType !== "AdvSupport") {
            m365StandardUsersAdv = 0;
        }
        if (Microsoft365E3 && Microsoft365E3Type === "AdvSupport") {
            m365E3UsersAdv += Microsoft365E3Count;
        }
        if (!Microsoft365E3 || Microsoft365E3Type !== "AdvSupport") {
            m365E3UsersAdv = 0;
        }
        if (Microsoft365E5 && Microsoft365E5Type === "AdvSupport") {
            m365E5UsersAdv += Microsoft365E5Count;
        }
        if (!Microsoft365E5 || Microsoft365E5Type !== "AdvSupport") {
            m365E5UsersAdv = 0;
        }
        m365TotalUsersAdv = m365BasicUsersAdv + m365StandardUsersAdv + m365PremiumUsersAdv + m365E3UsersAdv + m365E5UsersAdv;
        if (m365TotalUsersAdv > 0) {
            localforage.setItem("MS - Adv O365 Support", m365TotalUsersAdv);
        }
        if (m365TotalUsersAdv <= 0) {
            localforage.removeItem("MS - Adv O365 Support");
        }
    }, [Microsoft365BusBasic, Microsoft365BusBasicType, Microsoft365BusBasicCount,
        Microsoft365BusPremium, Microsoft365BusPremiumType, Microsoft365BusPremiumCount,
        Microsoft365BusStandard, Microsoft365BusStandardType, Microsoft365BusStandardCount,
        Microsoft365E3, Microsoft365E3Type, Microsoft365E3Count,
        Microsoft365E5, Microsoft365E5Type, Microsoft365E5Count]);

    // useEffect that sets the MS - O365 Business Basic sku in localForage
    useEffect(() => {
        if (Microsoft365BusBasic) {
            localforage.setItem("MS - O365 Business Basic", Microsoft365BusBasicCount);
        } else {
            localforage.removeItem("MS - O365 Business Basic");
        }
    }, [Microsoft365BusBasic, Microsoft365BusBasicCount])

    // useEffect that sets the MS - O365 Business Premium sku in localForage
    useEffect(() => {
        if (Microsoft365BusPremium) {
            localforage.setItem("MS - O365 Business Premium", Microsoft365BusPremiumCount);
        } else {
            localforage.removeItem("MS - O365 Business Premium");
        }
    }, [Microsoft365BusPremium, Microsoft365BusPremiumCount])

    // useEffect that sets the MS - O365 Business Standard sku in localForage
    useEffect(() => {
        if (Microsoft365BusStandard) {
            localforage.setItem("MS - O365 Business Standard", Microsoft365BusStandardCount);
        } else {
            localforage.removeItem("MS - O365 Business Standard");
        }
    }, [Microsoft365BusStandard, Microsoft365BusStandardCount])

    // useEffect that sets the MS - O365 E3 sku in localForage
    useEffect(() => {
        if (Microsoft365E3) {
            localforage.setItem("MS - O365 E3", Microsoft365E3Count);
        } else {
            localforage.removeItem("MS - O365 E3");
        }
    }, [Microsoft365E3, Microsoft365E3Count])

    // useEffect that sets the MS - O365 E5 sku in localForage
    useEffect(() => {
        if (Microsoft365E5) {
            localforage.setItem("MS - O365 E5", Microsoft365E5Count);
        } else {
            localforage.removeItem("MS - O365 E5");
        }
    }, [Microsoft365E5, Microsoft365E5Count])

    // useEffect that sets the MS - StreamlineIT sku in localForage
    useEffect(() => {
        if (StreamlineIT) {
            localforage.setItem("MS - StreamlineIT", StreamlineITCount);
        } else {
            localforage.removeItem("MS - StreamlineIT");
        }
    }, [StreamlineIT, StreamlineITCount])

    // useEffects that set the M365 sku types in localForage
    // These skus are simply for reference to bring back the state of saved configurations
    useEffect(() => {
        if (Microsoft365BusBasic && Microsoft365BusBasicType == "UserManagement") {
            localforage.setItem("MS - O365 Business Basic User Mgmt Reference", Microsoft365BusBasicCount);
        } else {
            localforage.removeItem("MS - O365 Business Basic User Mgmt Reference");
        }
    }, [Microsoft365BusBasic, Microsoft365BusBasicCount, Microsoft365BusBasicType])

    useEffect(() => {
        if (Microsoft365BusStandard && Microsoft365BusStandardType == "UserManagement") {
            localforage.setItem("MS - O365 Business Standard User Mgmt Reference", Microsoft365BusStandardCount);
        } else {
            localforage.removeItem("MS - O365 Business Standard User Mgmt Reference");
        }
    }, [Microsoft365BusStandard, Microsoft365BusStandardCount, Microsoft365BusStandardType])

    useEffect(() => {
        if (Microsoft365BusPremium && Microsoft365BusPremiumType == "UserManagement") {
            localforage.setItem("MS - O365 Business Premium User Mgmt Reference", Microsoft365BusPremiumCount);
        } else {
            localforage.removeItem("MS - O365 Business Premium User Mgmt Reference");
        }
    }, [Microsoft365BusPremium, Microsoft365BusPremiumCount, Microsoft365BusPremiumType])

    useEffect(() => {
        if (Microsoft365E3 && Microsoft365E3Type == "UserManagement") {
            localforage.setItem("MS - O365 E3 User Mgmt Reference", Microsoft365E3Count);
        } else {
            localforage.removeItem("MS - O365 E3 User Mgmt Reference");
        }
    }, [Microsoft365E3, Microsoft365E3Count, Microsoft365E3Type])

    useEffect(() => {
        if (Microsoft365E5 && Microsoft365E5Type == "UserManagement") {
            localforage.setItem("MS - O365 E5 User Mgmt Reference", Microsoft365E5Count);
        } else {
            localforage.removeItem("MS - O365 E5 User Mgmt Reference");
        }
    }, [Microsoft365E5, Microsoft365E5Count, Microsoft365E5Type])

    useEffect(() => {
        if (Microsoft365BusBasic && Microsoft365BusBasicType == "AdvSupport") {
            localforage.setItem("MS - O365 Business Basic Adv Support Reference", Microsoft365BusBasicCount);
        } else {
            localforage.removeItem("MS - O365 Business Basic Adv Support Reference");
        }
    }, [Microsoft365BusBasic, Microsoft365BusBasicCount, Microsoft365BusBasicType])

    useEffect(() => {
        if (Microsoft365BusStandard && Microsoft365BusStandardType == "AdvSupport") {
            localforage.setItem("MS - O365 Business Standard Adv Support Reference", Microsoft365BusStandardCount);
        } else {
            localforage.removeItem("MS - O365 Business Standard Adv Support Reference");
        }
    }, [Microsoft365BusStandard, Microsoft365BusStandardCount, Microsoft365BusStandardType])

    useEffect(() => {
        if (Microsoft365BusPremium && Microsoft365BusPremiumType == "AdvSupport") {
            localforage.setItem("MS - O365 Business Premium Adv Support Reference", Microsoft365BusPremiumCount);
        } else {
            localforage.removeItem("MS - O365 Business Premium Adv Support Reference");
        }
    }, [Microsoft365BusPremium, Microsoft365BusPremiumCount, Microsoft365BusPremiumType])

    useEffect(() => {
        if (Microsoft365E3 && Microsoft365E3Type == "AdvSupport") {
            localforage.setItem("MS - O365 E3 Adv Support Reference", Microsoft365E3Count);
        } else {
            localforage.removeItem("MS - O365 E3 Adv Support Reference");
        }
    }, [Microsoft365E3, Microsoft365E3Count, Microsoft365E3Type])

    useEffect(() => {
        if (Microsoft365E5 && Microsoft365E5Type == "AdvSupport") {
            localforage.setItem("MS - O365 E5 Adv Support Reference", Microsoft365E5Count);
        } else {
            localforage.removeItem("MS - O365 E5 Adv Support Reference");
        }
    }, [Microsoft365E5, Microsoft365E5Count, Microsoft365E5Type])



    // useEffects for calculating onboarding costs
    useEffect(() => {
        if (NeedsOnboarding && !SowRequired) {
            localforage.setItem("")
        }
    })

    // useEffects for setting reference skus
    useEffect(() => {
        if (EucType === "citrix") {
            localforage.setItem("Citrix Reference", 1);
        } else {
            localforage.removeItem("Citrix Reference");
        }
    }, [EucType])

    useEffect(() => {
        if (EucType2 === "avd") {
            localforage.setItem("AVD Reference", 1);
        } else {
            localforage.removeItem("AVD Reference");
        }
    }, [EucType2])

    useEffect(() => {
        if (EucType === "citrix_on_avd") {
            localforage.setItem("Citrix on AVD Reference", 1);
        } else {
            localforage.removeItem("Citrix on AVD Reference");
        }
    }, [EucType])

    useEffect(() => {
        if (EucUsers > 0) {
            localforage.setItem("Citrix Users Reference", EucUsers);
        } else {
            localforage.removeItem("Citrix Users Reference");
        }
    }, [EucUsers])

    useEffect(() => {
        if (AvdUsers > 0) {
            localforage.setItem("AVD Users Reference", AvdUsers);
        } else {
            localforage.removeItem("AVD Users Reference");
        }
    }, [AvdUsers])

    useEffect(() => {
        if (Nerdio) {
            localforage.setItem("Nerdio Reference", 1);
        } else {
            localforage.removeItem("Nerdio Reference");
        }
    }, [Nerdio])

    useEffect(() => {
        if (CitrixLicenseType === "Customer") {
            localforage.setItem("Citrix Customer Provided License Reference", 1);
        } else {
            localforage.removeItem("Citrix Customer Provided License Reference");
        }
    }, [CitrixLicenseType])

    useEffect(() => {
        if (CitrixLicenseType === "Premium") {
            localforage.setItem("Citrix Premium License Reference", 1);
        } else {
            localforage.removeItem("Citrix Premium License Reference");
        }
    }, [CitrixLicenseType])

    useEffect(() => {
        if (CitrixLicenseType === "PremiumPlus") {
            localforage.setItem("Citrix Premium + License Reference", 1);
        } else {
            localforage.removeItem("Citrix Premium + License Reference");
        }
    }, [CitrixLicenseType])

    useEffect(() => {
        if (CitrixLicenseType === "UHMC") {
            localforage.setItem("Citrix UHMC User License Reference", 1);
        } else {
            localforage.removeItem("Citrix UHMC User License Reference");
        }
    }, [CitrixLicenseType])

    useEffect(() => {
        if (CitrixLicenseType === "AdvancedPlus") {
            localforage.setItem("Citrix Advanced + License Reference", 1);
        } else {
            localforage.removeItem("Citrix Advanced + License Reference");
        }
    }, [CitrixLicenseType])

    useEffect(() => {
        if (CitrixLicenseType === "Azure") {
            localforage.setItem("Citrix Standard License Reference", 1);
        } else {
            localforage.removeItem("Citrix Standard License Reference");
        }
    }, [CitrixLicenseType])

    useEffect(() => {
        if (NPlus1Required) {
            localforage.setItem("N+1 Reference", 1);
        } else {
            localforage.removeItem("N+1 Reference");
        }
    }, [NPlus1Required])

    useEffect(() => {
        if (NeedsOnboarding) {
            localforage.setItem("Needs Onboarding Reference", 1);
        } else {
            localforage.removeItem("Needs Onboarding Reference");
        }
    }, [NeedsOnboarding])

    useEffect(() => {
        if (SowRequired) {
            localforage.setItem("SOW Required Reference", 1);
        } else {
            localforage.removeItem("SOW Required Reference");
        }
    }, [SowRequired])

    useEffect(() => {
        if (Contract8x5or12x6 && Contract8x5or12x6Type === "8x5") {
            localforage.setItem("8x5 Contract Reference", 1);
        } else {
            localforage.removeItem("8x5 Contract Reference");
        }
    }, [Contract8x5or12x6, Contract8x5or12x6Type])

    useEffect(() => {
        if (Contract8x5or12x6 && Contract8x5or12x6Type === "12x6") {
            localforage.setItem("12x6 Contract Reference", 1);
        } else {
            localforage.removeItem("12x6 Contract Reference");
        }
    }, [Contract8x5or12x6, Contract8x5or12x6Type])

    useEffect(() => {
        if (DataCenterOnPremises) {
            localforage.setItem("On Prem Data Center Reference", 1);
        } else {
            localforage.removeItem("On Prem Data Center Reference");
        }
    }, [DataCenterOnPremises])

    useEffect(() => {
        if (DataCenterChoiceCloud) {
            localforage.setItem("Choice Cloud Data Center Reference", 1);
        } else {
            localforage.removeItem("Choice Cloud Data Center Reference");
        }
    }, [DataCenterChoiceCloud])

    useEffect(() => {
        if (DataCenterMicrosoftAzure) {
            localforage.setItem("Azure Data Center Reference", 1);
        } else {
            localforage.removeItem("Azure Cloud Data Center Reference");
        }
    }, [DataCenterMicrosoftAzure])

    useEffect(() => {
        if (AdcVserver) {
            localforage.setItem("ADC vServer Reference", AdcVserverCount);
        } else {
            localforage.removeItem("ADC vServer Reference");
        }
    }, [AdcVserver, AdcVserverCount])

    useEffect(() => {
        if (AdcAdm) {
            localforage.setItem("ADC ADM Reference", AdcAdmCount);
        } else {
            localforage.removeItem("ADC ADM Reference");
        }
    }, [AdcAdm, AdcAdmCount])

    useEffect(() => {
        if (adpEnabled) {
            localforage.setItem("ADP Tier Reference", adpTier);
        } else {
            localforage.removeItem("ADP Tier Reference");
        }
    }, [adpEnabled, adpTier])

    useEffect(() => {
        if (adpEnabled && adpRubricState === "own") {
            localforage.setItem("ADP Node Count Reference", adpNodeCount);
            localforage.setItem("ADP Location Type Reference", adpCustOwnedLocations);
            localforage.setItem("ADP Custom Price Reference", adpCustOwnedCustomPrice);
        } else {
            localforage.removeItem("ADP Node Count Reference");
            localforage.removeItem("ADP Location Type Reference");
            localforage.removeItem("ADP Custom Price Reference");
        }
    }, [adpEnabled, adpRubricState, adpNodeCount, adpCustOwnedLocations, adpCustOwnedCustomPrice])

    useEffect(() => {
        if (adpRubricState === "rent" && adpStorage > 0) {
            localforage.setItem("ADP Rubric State Rent Reference", 1);
            localforage.removeItem("ADP Rubric State Own Reference");
            localforage.removeItem("ADP Rubric State Consumption Reference");
            localforage.removeItem("ADP Rubric State Consumption Hybrid Reference");
        } else if (adpRubricState === "own") {
            localforage.setItem("ADP Rubric State Own Reference", 1);
            localforage.removeItem("ADP Rubric State Rent Reference");
            localforage.removeItem("ADP Rubric State Consumption Reference");
            localforage.removeItem("ADP Rubric State Consumption Hybrid Reference");
        } else if (adpRubricState === "consumption") {
            localforage.setItem("ADP Rubric State Consumption Reference", 1);
            localforage.removeItem("ADP Rubric State Rent Reference");
            localforage.removeItem("ADP Rubric State Own Reference");
            localforage.removeItem("ADP Rubric State Consumption Hybrid Reference");
        } else if (adpRubricState === "consumption_hybrid") {
            localforage.setItem("ADP Rubric State Consumption Hybrid Reference", 1);
            localforage.removeItem("ADP Rubric State Rent Reference");
            localforage.removeItem("ADP Rubric State Own Reference");
            localforage.removeItem("ADP Rubric State Consumption Reference");
        } else {
            localforage.removeItem("ADP Rubric State Rent Reference");
            localforage.removeItem("ADP Rubric State Own Reference");
            localforage.removeItem("ADP Rubric State Consumption Reference");
            localforage.removeItem("ADP Rubric State Consumption Hybrid Reference");
        }
    }, [adpRubricState, adpStorage])

    useEffect(() => {
        if (adpRubricState === "own" && adpCustOwnedType === "rubrik") {
            localforage.setItem("ADP Cust Owned Type Rubrik Reference", 1);
            localforage.removeItem("ADP Cust Owned Type Cloud Reference");
            localforage.removeItem("ADP Cust Owned Type Owned Reference");
        } else if (adpRubricState === "own" && adpCustOwnedType === "public_cloud") {
            localforage.setItem("ADP Cust Owned Type Cloud Reference", 1);
            localforage.removeItem("ADP Cust Owned Type Rubrik Reference");
            localforage.removeItem("ADP Cust Owned Type Owned Reference");
        } else if (adpRubricState === "own" && adpCustOwnedType === "customer_owned") {
            localforage.setItem("ADP Cust Owned Type Owned Reference", 1);
            localforage.removeItem("ADP Cust Owned Type Rubrik Reference");
            localforage.removeItem("ADP Cust Owned Type Cloud Reference");
        }
    }, [adpRubricState, adpCustOwnedType])

    useEffect(() => {
        if (M365Backups) {
            localforage.setItem("M365 Backups Enabled Reference", 1);
        } else {
            localforage.removeItem("M365 Backups Enabled Reference");
        }
    }, [M365Backups])

    useEffect(() => {
        if (M365Backups && M365BackupsUsers > 0) {
            localforage.setItem("M365 Backups Users Reference", M365BackupsUsers);
        } else {
            localforage.removeItem("M365 Backups Users Reference");
        }
    }, [M365Backups, M365BackupsUsers])

    useEffect(() => {
        if (M365Backups && M365BackupsType === "20GB") {
            localforage.setItem("M365 Backups 20GB Reference", 1);
            localforage.removeItem("M365 Backups Unlimited Reference");
        } else if (M365Backups && M365BackupsType === "Unlimited") {
            localforage.setItem("M365 Backups Unlimited Reference", 1);
            localforage.removeItem("M365 Backups 20GB Reference");
        } else {
            localforage.removeItem("M365 Backups 20GB Reference");
            localforage.removeItem("M365 Backups Unlimited Reference");
        }
    }, [M365Backups, M365BackupsType])

    useEffect(() => {
        if (CloudBackup) {
            localforage.setItem("UCL Cloud Backups Enabled Reference", 1);
        } else {
            localforage.removeItem("UCL Cloud Backups Enabled Reference");
        }
    }, [CloudBackup])

    useEffect(() => {
        if (CloudBackup && CloudBackupData > 0) {
            localforage.setItem("UCL Cloud Backups Data Reference", CloudBackupData);
        } else {
            localforage.removeItem("UCL Cloud Backups Data Reference");
        }
    }, [CloudBackup, CloudBackupData])

    useEffect(() => {
        if (CloudBackup && CloudBackupType === "Foundation") {
            localforage.setItem("UCL Cloud Backups Foundation Reference", 1);
            localforage.removeItem("UCL Cloud Backups Enterprise Reference");
        } else if (CloudBackup && CloudBackupType === "Enterprise") {
            localforage.setItem("UCL Cloud Backups Enterprise Reference", 1);
            localforage.removeItem("UCL Cloud Backups Foundation Reference");
        } else {
            localforage.removeItem("UCL Cloud Backups Foundation Reference");
            localforage.removeItem("UCL Cloud Backups Enterprise Reference");
        }
    }, [CloudBackup, CloudBackupType])

    useEffect(() => {
        let totalCost = 0;
        let totalPrice = 0;

        // Map of option SKUs with their respective costs and prices
        const optionSkus = {
            "Threat Prevention": { cost: 37, price: 52.85 },
            "Wildfire Malware Analysis": { cost: 37, price: 52.85 },
            "URL Filtering": { cost: 37, price: 52.85 },
            "DNS Security": { cost: 37, price: 52.85 },
            "SD-WAN": { cost: 37, price: 52.85 },
            "Global Protect": { cost: 37, price: 52.85 },
            "Virtual Panorama for Management": { cost: 18.5, price: 26.43 },
        };

        // Series SKUs with their respective costs and prices
        const seriesSkus = {
            "NoneSelected": { cost: 0, price: 0 },
            "SEC-50": { cost: 185, price: 240.5 },
            "SEC-100": { cost: 185, price: 240.5 },
            "SEC-200": { cost: 185, price: 240.5 },
            "SEC-300": { cost: 555, price: 721.5 },
            "SEC-1000-HV": { cost: 555, price: 721.5 },
            "SEC-500": { cost: 1110, price: 1443 },
            "SEC-700": { cost: 2220, price: 2886 },
        };

        // Aggregate option costs and prices
        if (FirewallTP) {
            totalCost += optionSkus["Threat Prevention"].cost;
            totalPrice += optionSkus["Threat Prevention"].price;
            console.log('Added Threat Prevention:', optionSkus["Threat Prevention"]);
        }
        if (FirewallWMA) {
            totalCost += optionSkus["Wildfire Malware Analysis"].cost;
            totalPrice += optionSkus["Wildfire Malware Analysis"].price;
            console.log('Added Wildfire Malware Analysis:', optionSkus["Wildfire Malware Analysis"]);
        }
        if (FirewallUrlFiltering) {
            totalCost += optionSkus["URL Filtering"].cost;
            totalPrice += optionSkus["URL Filtering"].price;
            console.log('Added URL Filtering:', optionSkus["URL Filtering"]);
        }
        if (FirewallDnsSecurity) {
            totalCost += optionSkus["DNS Security"].cost;
            totalPrice += optionSkus["DNS Security"].price;
            console.log('Added DNS Security:', optionSkus["DNS Security"]);
        }
        if (FirewallSdWan) {
            totalCost += optionSkus["SD-WAN"].cost;
            totalPrice += optionSkus["SD-WAN"].price;
            console.log('Added SD-WAN:', optionSkus["SD-WAN"]);
        }
        if (FirewallGlobalProtect) {
            totalCost += optionSkus["Global Protect"].cost;
            totalPrice += optionSkus["Global Protect"].price;
            console.log('Added Global Protect:', optionSkus["Global Protect"]);
        }
        if (FirewallVPfM) {
            totalCost += optionSkus["Virtual Panorama for Management"].cost;
            totalPrice += optionSkus["Virtual Panorama for Management"].price;
            console.log('Added Virtual Panorama for Management:', optionSkus["Virtual Panorama for Management"]);
        }

        // Aggregate series cost and price
        if (seriesSkus[SecSeries]) {
            totalCost += seriesSkus[SecSeries].cost;
            totalPrice += seriesSkus[SecSeries].price;
            console.log('Added SEC Series:', seriesSkus[SecSeries]);
        }

        // Log the final total cost and price
        console.log('Total Cost:', totalCost);
        console.log('Total Price:', totalPrice);

        // Store the combined SKU in localforage
        if (NetworkFirewalls === true) {
            localforage.setItem("SECaas - NGFaaS - Lic", PaloAltoLicenses);
        } else {
            localforage.removeItem("SECaas - NGFaaS - Lic");
        }

        // Make API call to update backend
        axios.patch(`/db/sku/update?id=853`, {
            unitCost: totalCost,
            unitPrice: totalCost / 0.7
            // unitPrice: totalPrice
        })
            .then(response => {
                console.log('Combined SKU updated in backend:', response.data);
            })
            .catch(error => {
                console.error('Error updating combined SKU in backend:', error);
            });

    }, [SecSeries, FirewallTP, FirewallWMA, FirewallUrlFiltering, FirewallDnsSecurity, FirewallSdWan, FirewallGlobalProtect, FirewallVPfM, PaloAltoLicenses, NetworkFirewalls]);

}

export default SkuSetter;