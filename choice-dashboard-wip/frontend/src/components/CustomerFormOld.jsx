import {
    Box, Card, Typography, TextField, FormLabel, FormControl,
    FormControlLabel, FormHelperText, FormGroup, Checkbox, Grid,
    RadioGroup, Radio, Slider, Stepper, Step, StepLabel
} from "@mui/material";
import { useEffect, useState } from "react";

const skus = Object.fromEntries((await (await fetch("/db/sku")).json()).map((sku_) => {
    let { sku, ...rest } = sku_;
    return [sku, rest];
}));

const AssuredStorageBands = [
    { value: 0, max: 8, label: "A" },
    { value: 9, max: 18, label: "B" },
    { value: 19, max: 28, label: "C" },
    { value: 29, max: 38, label: "D" },
    { value: 39, max: 48, label: "E" },
];

const GridItem = ({ width, children }) => (
    <Grid item xs={12}>
        <Card variant="outlined" sx={{ height: "100%" }}>
            <Box p={3}>{children}</Box>
        </Card>
    </Grid>
);

const GridCard = ({ width, title, children }) => {
    return (
        <GridItem width={12}>
            <Typography variant="h1" mb={3}><Checkbox sx={{ ml: "-10px", mr: "-5px", verticalAlign: "0" }} /> {title}</Typography>
            <Box display="flex" gap={3}>{children}</Box>
        </GridItem>
    );
};

export default function CustomerForm() {
    const [StreamlineITUsers, setStreamlineITUsers] = useState(0);
    const [NetworkFirewalls, setNetworkFirewalls] = useState(0);
    const [NetworkWifi, setNetworkWifi] = useState(false);
    const [NetworkSwitches, setNetworksSwitches] = useState(false);
    const [SIEMUsers, setSIEMUsers] = useState(0);
    const [SIEMSensorApplicances, setSIEMSensorApplicances] = useState(0);
    const [ADCADM, setADCADM] = useState(0);
    const [ADCvServer, setADCvServer] = useState(0);
    const [customerCost, setCustomerCost] = useState(0);
    const [choiceCost, setChoiceCost] = useState(0);
    const [CitrixUsers, setCitrixUsers] = useState(0);
    const [VDInstances, setVDInstances] = useState(0);
    const [AssuredTier, setAssuredTier] = useState("1");
    const [AssuredStorage, setAssuredStorage] = useState(0);
    const [HypervisorNodes, setHypervisorNodes] = useState(0);
    const [HypervisorManagement, setHypervisorManagement] = useState("None");
    const [VDHostingSharedDesktops, setVDHostingSharedDesktops] = useState(0);
    const [VDVirtualSharedDesktops, setVDVirtualSharedDesktops] = useState(0);
    const [CitrixInfrastructureCloud, setCitrixInfrastructureCloud] = useState(true);
    const [CitrixInfrastructureOnPremises, setCitrixInfrastructureOnPremises] = useState(true);
    const [Microsoft365BusinessBasic, setMicrosoft365BusinessBasic] = useState(0);
    const [Microsoft365BusinessPremium, setMicrosoft365BusinessPremium] = useState(0);
    const [Microsoft365BusinessStandard, setMicrosoft365BusinessStandard] = useState(0);
    const [Microsoft365E3, setMicrosoft365E3] = useState(0);
    const [Microsoft365E5, setMicrosoft365E5] = useState(0);
    const [KeeperInstances, setKeeperInstances] = useState(0);
    useEffect(() => {
        let AssuredStorageSku = `CC - ADP - TIER ${AssuredTier} - BAND ${AssuredStorageBands.find((band) => AssuredStorage >= band.value && AssuredStorage <= band.max).label}`;
        setCustomerCost(
            1 * (skus[AssuredStorageSku]?.unitCost || 0) +
            VDHostingSharedDesktops * skus["MS-Citrix Master HSD"].unitCost +
            VDVirtualSharedDesktops * skus["MS-Citrix Master VDI"].unitCost +
            Microsoft365BusinessBasic * skus["MS - O365 Business Basic"].unitCost +
            Microsoft365BusinessPremium * skus["MS - O365 Business Premium"].unitCost +
            Microsoft365BusinessStandard * skus["MS - O365 Business Standard"].unitCost +
            Microsoft365E3 * skus["MS - O365 E3"].unitCost +
            Microsoft365E5 * skus["MS - O365 E5"].unitCost +
            KeeperInstances * skus["SECaaS - KEP"].unitCost +
            HypervisorNodes * skus["MS - Hypervisor"].unitCost +
            1 * {
                "NutanixPrismCentral": skus["MS - Nutanix-PC"].unitCost,
                "VMWareVCenter": skus["MS - VMware-VC"].unitCost,
                "None": 0,
            }[HypervisorManagement]
        );
        setChoiceCost(
            1 * (skus[AssuredStorageSku]?.unitCost || 0) * (skus[AssuredStorageSku]?.unitPrice || 0) +
            VDHostingSharedDesktops * skus["MS-Citrix Master HSD"].unitCost * skus["MS-Citrix Master HSD"].unitPrice +
            VDVirtualSharedDesktops * skus["MS-Citrix Master VDI"].unitCost * skus["MS-Citrix Master VDI"].unitPrice +
            Microsoft365BusinessBasic * skus["MS - O365 Business Basic"].unitCost * skus["MS - O365 Business Basic"].unitPrice +
            Microsoft365BusinessPremium * skus["MS - O365 Business Premium"].unitCost * skus["MS - O365 Business Premium"].unitPrice +
            Microsoft365BusinessStandard * skus["MS - O365 Business Standard"].unitCost * skus["MS - O365 Business Standard"].unitPrice +
            Microsoft365E3 * skus["MS - O365 E3"].unitCost * skus["MS - O365 E3"].unitPrice +
            Microsoft365E5 * skus["MS - O365 E5"].unitCost * skus["MS - O365 E5"].unitPrice +
            KeeperInstances * skus["SECaaS - KEP"].unitCost * skus["SECaaS - KEP"].unitPrice +
            HypervisorNodes * skus["MS - Hypervisor"].unitCost * skus["MS - Hypervisor"].unitPrice +
            1 * {
                "NutanixPrismCentral": skus["MS - Nutanix-PC"].unitCost * skus["MS - Nutanix-PC"].unitPrice,
                "VMWareVCenter": skus["MS - VMware-VC"].unitCost * skus["MS - VMware-VC"].unitPrice,
                "None": 0,
            }[HypervisorManagement]
        );
    }, [CitrixUsers,
        VDInstances,
        AssuredTier,
        AssuredStorage,
        HypervisorNodes,
        HypervisorManagement,
        VDHostingSharedDesktops,
        VDVirtualSharedDesktops,
        CitrixInfrastructureCloud,
        CitrixInfrastructureOnPremises,
        Microsoft365BusinessBasic,
        Microsoft365BusinessPremium,
        Microsoft365BusinessStandard,
        Microsoft365E3,
        Microsoft365E5,
        KeeperInstances,]);
    return (
        <Box>
            <Box mb={4}>
                <Stepper orientation="horizontal">
                    <Step>
                        <StepLabel>
                            <Typography>Step One</Typography>
                            <Typography>Sales Wizard</Typography>
                        </StepLabel>
                    </Step>
                    <Step>
                        <StepLabel>
                            <Typography>Step Two</Typography>
                            <Typography>SKU & Server Tables</Typography>
                        </StepLabel>
                    </Step>
                    <Step>
                        <StepLabel>
                            <Typography>Step Three</Typography>
                            <Typography>Summary</Typography>
                        </StepLabel>
                    </Step>
                </Stepper>
            </Box>
            <Grid container spacing={3}>
                <GridCard width={6} title="Citrix">
                    <TextField label="Users" helperText="Number of users" variant="standard" type="number"
                        defaultValue={CitrixUsers} onChange={(event) => setCitrixUsers(event.target.value)} />
                    <FormControl component="fieldset" variant="standard">
                        <FormLabel component="legend">Infrastructure</FormLabel>
                        <FormGroup>
                            <FormControlLabel control={
                                <Checkbox defaultChecked={CitrixInfrastructureCloud} name="CitrixInfrastructureCloud"
                                    onChange={(event) => setCitrixInfrastructureCloud(event.target.value)} />
                            } label="Cloud" />
                            <FormControlLabel control={
                                <Checkbox defaultChecked={CitrixInfrastructureOnPremises} name="CitrixInfrastructureOnPremises"
                                    onChange={(event) => setCitrixInfrastructureOnPremises(event.target.value)} />
                            } label="On-Premises" />
                        </FormGroup>
                        <FormHelperText>Select both for <b>Hybrid</b></FormHelperText>
                    </FormControl>
                </GridCard>
                <GridCard width={6} title="Azure Virtual Desktop Management (VD)">
                    <TextField label="Instances" helperText="Number of instances" variant="standard" type="number"
                        defaultValue={VDInstances} onChange={(event) => setVDInstances(event.target.value)} />
                    <TextField label="Hosting Shared Desktops" helperText="Number of users" variant="standard" type="number"
                        defaultValue={VDHostingSharedDesktops} onChange={(event) => setVDHostingSharedDesktops(event.target.value)} />
                    <TextField label="Virtual Desktop Infrastructure" helperText="Number of users" variant="standard" type="number"
                        defaultValue={VDVirtualSharedDesktops} onChange={(event) => setVDVirtualSharedDesktops(event.target.value)} />
                </GridCard>
                <GridCard width={6} title="Application Delivery Controller (ADC)">
                    <TextField label="ADC ADM" helperText="Count" variant="standard" type="number"
                        defaultValue={ADCADM} onChange={(event) => setADCADM(event.target.value)} />
                    <TextField label="ADC vServer" helperText="Count" variant="standard" type="number"
                        defaultValue={ADCvServer} onChange={(event) => setADCvServer(event.target.value)} />
                </GridCard>
                <GridCard width={3} title="Keeper">
                    <TextField label="Password Security" helperText="Number of instances" variant="standard" type="number"
                        defaultValue={KeeperInstances} onChange={(event) => setKeeperInstances(event.target.value)} />
                </GridCard>
                <GridCard width={3} title="Streamline IT">
                    <TextField label="Users" helperText="Number of users with Choice Ticketing access" variant="standard" type="number"
                        defaultValue={StreamlineITUsers} onChange={(event) => setStreamlineITUsers(event.target.value)} />
                </GridCard>
                <GridCard width={6} title="Assured DP Tables">
                    <FormControl component="fieldset" variant="standard">
                        <FormLabel component="legend">Tier</FormLabel>
                        <RadioGroup name="AssuredTier"
                            onChange={(event) => setAssuredTier(event.target.value)}>
                            <FormControlLabel value="1" control={<Radio />} label="Tier 1" />
                            <FormControlLabel value="2" control={<Radio />} label="Tier 2" />
                            <FormControlLabel value="3" control={<Radio />} label="Tier 3" />
                        </RadioGroup>
                    </FormControl>
                    <FormControl component="fieldset" variant="standard">
                        <FormLabel component="legend">Storage</FormLabel>
                        <Slider
                            sx={{ width: "400px", mx: "6px" }}
                            onChange={(event, data) => setAssuredStorage(data)}
                            min={0} max={48}
                            valueLabelFormat={(value) => `${value}TB`}
                            valueLabelDisplay="auto"
                            marks={AssuredStorageBands}
                        />
                    </FormControl>
                </GridCard>
                <GridCard width={6} title="Security Information & Event Management (SIEM)">
                    <TextField label="Users" helperText="Number of users" variant="standard" type="number"
                        defaultValue={SIEMUsers} onChange={(event) => setSIEMUsers(event.target.value)} />
                    <TextField label="Sensor Applicances" helperText="Number of locations with public internet portal" variant="standard" type="number"
                        defaultValue={SIEMSensorApplicances} onChange={(event) => setSIEMSensorApplicances(event.target.value)} />
                </GridCard>
                <GridCard width={6} title="Hypervisor">
                    <TextField label="Nodes" helperText="Number of hosts" variant="standard" type="number"
                        defaultValue={HypervisorNodes} onChange={(event) => setHypervisorNodes(event.target.value)} />
                    <FormControl component="fieldset" variant="standard">
                        <FormLabel component="legend">Management</FormLabel>
                        <RadioGroup name="HypervisorManagement"
                            onChange={(event) => setHypervisorManagement(event.target.value)}>
                            <FormControlLabel value="None" control={<Radio />} label="None" />
                            <FormControlLabel value="NutanixPrismCentral" control={<Radio />} label="Nutanix Prism Central" />
                            <FormControlLabel value="VMWareVCenter" control={<Radio />} label="VMWare VCenter" />
                        </RadioGroup>
                        <FormHelperText>Type of Hypervisor</FormHelperText>
                    </FormControl>
                </GridCard>
                <GridCard width={6} title="Network">
                    <TextField label="Firewalls" helperText="Number of physical and virtual firewalls monitored by Choice." variant="standard" type="number"
                        defaultValue={NetworkFirewalls} onChange={(event) => setNetworkFirewalls(event.target.value)} />
                    <FormControl>
                        <FormControlLabel control={
                            <Checkbox defaultChecked={NetworkSwitches} name="Switches"
                                onChange={(event) => setNetworksSwitches(event.target.value)} />
                        } label="Switches" />
                        <FormControlLabel control={
                            <Checkbox defaultChecked={NetworkWifi} name="Wifi"
                                onChange={(event) => setNetworkWifi(event.target.value)} />
                        } label="Wifi" />
                    </FormControl>
                </GridCard>
                <GridCard width={12} title="Microsoft 365">
                    <TextField label="Business Basic" helperText="Number of users" variant="standard" type="number"
                        defaultValue={Microsoft365BusinessBasic} onChange={(event) => setMicrosoft365BusinessBasic(event.target.value)} />
                    <TextField label="Business Premium" helperText="Number of users" variant="standard" type="number"
                        defaultValue={Microsoft365BusinessPremium} onChange={(event) => setMicrosoft365BusinessPremium(event.target.value)} />
                    <TextField label="Business Standard" helperText="Number of users" variant="standard" type="number"
                        defaultValue={Microsoft365BusinessStandard} onChange={(event) => setMicrosoft365BusinessStandard(event.target.value)} />
                    <TextField label="E3" helperText="Number of users" variant="standard" type="number"
                        defaultValue={Microsoft365E3} onChange={(event) => setMicrosoft365E3(event.target.value)} />
                    <TextField label="E5" helperText="Number of users" variant="standard" type="number"
                        defaultValue={Microsoft365E5} onChange={(event) => setMicrosoft365E5(event.target.value)} />
                </GridCard>
                <GridItem width={12}>
                    <Typography variant="h1" mb={3}><em>Customer Cost</em>: ${customerCost.toFixed(2)}</Typography>
                    <Typography mb={1}><em>Choice Cost</em>: ${choiceCost.toFixed(2)}</Typography>
                    <Typography><em>Choice Profit</em>: ${(customerCost - choiceCost).toFixed(2)}</Typography>
                </GridItem>
            </Grid>
        </Box>
    );
}