import { defineConfig } from "vite";
import react from "@vitejs/plugin-react";

// https://vitejs.dev/config/
export default defineConfig({
  optimizeDeps: {
    exclude: ["localforage"]
  },
  plugins: [react()],
  server: {
    port: 3000,
    open: true,
    proxy: {
      "/db": "http://10.100.10.152:3001"
      // "/db": "http://localhost:3001"
    },
  }
})