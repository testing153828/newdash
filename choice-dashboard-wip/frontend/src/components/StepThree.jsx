import { useState, useEffect } from "react";
import { Box, TextField, Button, MenuItem } from "@mui/material";
import localforage from "localforage";
import { useData } from "./DataContext";

/**
 * This is abstracted out in case I want to change how it is implemented.
 * for instance using `<table>`. Currently this uses a simple div block layout.
 */
function Rows({ children, ...rest }) {
    return <Box mb={6} {...rest}>{children}</Box>;
}

/**
 * This is abstracted out in case I want to change how it is implemented, for
 * instance using `<tr>`. Currently this uses a horizontal flexbox.
 * @param {Object} parameters Any unknown parameters will be added directly to
 *      the root component.
 * @param {Array} parameters.children The cells. Auto-populated by JSX.
 */
function Row({ children, ...rest }) {
    return <Box display="flex" width="100%" py={0.75} borderBottom="1px solid grey" borderColor="grey.900" {...rest}>{children}</Box>;
}

/**
 * This subclasses `Row`, and it is abstracted out in case I want to change how
 * `Row` is implemented, for instance using `<tr>`. Make sure to keep all
 * children of `HeaderRow`s consistent with other `Row`s.
 */
function HeaderRow({ children, ...rest }) {
    return <Row backgroundColor="#f5f5f5" fontSize={12} {...rest}>{children}</Row>;
}

/**
 * This is abstracted out in case I want to change how it is implemented, for
 * instance using `<td>`. Currently this is a flex child.
 * @param {Object} parameters Any unknown parameters will be added directly to
 *      the root component.
 * @param {number} parameters.size The relative width / column span of this cell.
 * @param {Array} parameters.children The cell contents. Auto-populated by JSX.
 */
function Cell({ size, children, ...rest }) {
    return <Box flex={size} px={1} {...rest}>{children}</Box>;
}

/**
 * This is only referenced once, so the only reason this is its own component
 * is because it contains a state variable and an effect that reflect the IndexedDB.
 * @param {Object} parameters 
 * @param {Object} parameters.sku A Sku row returned by the API.
 */
function SkuRow({ sku }) {
    const [quantity, setQuantity] = useState(0);
    const { skuConfig } = useData();
    useEffect(() => {
        if (skuConfig[sku.id]) { 
        const skuKey = `${sku.sku}-${sku.id}`; // Generate a unique key
        localforage.getItem(skuKey).then((value) => {
            setQuantity(parseFloat(value || 0));
        });
        } else {
            const skuKey = `${sku.sku}`
            localforage.getItem(skuKey).then((value) => {
            setQuantity(parseFloat(value || 0));
        });
        }
    }, [sku.id, sku.sku]);

    return (
        <Row>
            <Cell size={2}>{sku.sku}</Cell>
            <Cell size={3}>{sku.description} / <em>{sku.hardCost ? "Hard" : "Soft"}</em></Cell>
            <Cell size={1}>
                ${sku.unitPrice || <TextField variant="standard" size="small" defaultValue={0} sx={{ width: "50px" }}
                    type="number" InputProps={{ inputProps: { min: 0 } }} />} /
                ${sku.unitCost || <TextField variant="standard" size="small" defaultValue={0} sx={{ width: "50px" }}
                    type="number" InputProps={{ inputProps: { min: 0 } }} />}
            </Cell>
            <Cell size={1} color="steelblue">${(sku.unitPrice * quantity).toFixed(2)} / <em>${(sku.unitCost * quantity).toFixed(2)}</em></Cell>
            <Cell size={"0 0 75px"}>
                <TextField variant="standard" size="small" sx={{ width: "100%" }}
                    type="number" InputProps={{ inputProps: { min: 0 } }}
                    value={quantity} onChange={(event) => {
                        const newQuantity = parseFloat(event.target.value);
                        setQuantity(newQuantity);
                        localforage.setItem(skuKey, newQuantity);
                    }} />
            </Cell>
        </Row>
    );
}

function NewSkuRow({ handleSubmitNewSku, setSkuName, setDescription, setHardCost, setUnitPrice, setUnitCost }) {
    return (
        <Row style={{ marginTop: "-47px" }}>
            <Cell size={1.8}>
                <TextField
                    id="newSkuName"
                    onChange={(e) => setSkuName(e.target.value)}
                    fullWidth
                    size="small"
                    label="Sku Name"
                />
            </Cell>
            <Cell size={2.25}>
                <TextField
                    id="newDescription"
                    onChange={(e) => setDescription(e.target.value)}
                    fullWidth
                    size="small"
                    label="Description"
                />
            </Cell>
            <Cell size={0.55}>
                <TextField
                    select
                    id="newHardCost"
                    onChange={(e) => setHardCost(e.target.value)}
                    fullWidth
                    size="small"
                    label="Cost Type"
                >
                    <MenuItem value={1}>Hard</MenuItem>
                    <MenuItem value={0}>Soft</MenuItem>
                </TextField>
            </Cell>
            <Cell size={0.75}>
                <TextField
                    id="newUnitPrice"
                    onChange={(e) => setUnitPrice(parseFloat(e.target.value))}
                    type="number"
                    InputProps={{ inputProps: { min: 0 } }}
                    fullWidth
                    size="small"
                    label="Customer Price"
                />
            </Cell>
            <Cell size={0.75}>
                <TextField
                    id="newUnitCost"
                    onChange={(e) => setUnitCost(parseFloat(e.target.value))}
                    type="number"
                    InputProps={{ inputProps: { min: 0 } }}
                    fullWidth
                    size="small"
                    label="Choice Cost"
                />
            </Cell>
            <Cell>
                <Button onClick={handleSubmitNewSku} variant="contained" style={{ marginLeft: "10px" }}>Submit</Button>
            </Cell>
        </Row>
    );
}

export default function StepThree() {
    const [groups, setGroups] = useState({});
    const [newSku, setNewSku] = useState(null);
    const [newQuantity, setNewQuantity] = useState(0);
    const [skuName, setSkuName] = useState("");
    const [description, setDescription] = useState("");
    const [hardCost, setHardCost] = useState(0);
    const [unitPrice, setUnitPrice] = useState(0);
    const [unitCost, setUnitCost] = useState(0);

    const fetchGroups = async () => {
        const response = await fetch(`/db/sku_grouped`);
        const data = await response.json();
        setGroups(data);
    };

    useEffect(() => {
        fetchGroups();
    }, []);

    const handleSubmitNewSku = async () => {
        const newSkuData = {
            sku: skuName,
            description,
            customerDescription: description,
            hardCost,
            unitPrice,
            unitCost,
            groupid: 16
        };

        const response = await fetch(`/db/sku/create`, {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify(newSkuData)
        });
        const id = await response.json();
        setNewSku(null);
        setNewQuantity(0);
        fetchGroups();
    };

    return (
        <Rows>
            {Object.entries(groups).map(([name, skus]) => (
                <Rows key={name}>
                    <HeaderRow>
                        <Cell size={2}><b>{name}</b></Cell>
                        <Cell size={3}>Description / <em>Hard or Soft</em></Cell>
                        <Cell size={1}>Customer Price / <em>Choice Cost</em></Cell>
                        <Cell size={1}>Customer Total / <em>Choice Total</em></Cell>
                        <Cell size={"0 0 75px"}><em>Quantity</em></Cell>
                    </HeaderRow>
                    {skus
                        .filter(sku => !sku.sku.includes("Reference"))
                        .map(sku => (
                            <SkuRow key={sku.sku} sku={sku} quantity={newQuantity} setQuantity={setNewQuantity} />
                    ))}
                </Rows>
            ))}
            {newSku !== null && (
                <NewSkuRow 
                    handleSubmitNewSku={handleSubmitNewSku}
                    setSkuName={setSkuName}
                    setDescription={setDescription}
                    setHardCost={setHardCost}
                    setUnitPrice={setUnitPrice}
                    setUnitCost={setUnitCost}
                />
            )}
            <Button onClick={() => setNewSku({})} variant="contained" style={{ marginTop: "10px" }}>Create SKU</Button>
        </Rows>
    );
}