import React, { useEffect, useState } from "react";
import { useData } from "./DataContext";
import "localforage";
import { Stream } from "@mui/icons-material";

function SkuSetter() {
    const {
        serverState, setServerState, ServerManagementEnabled, setServerManagementEnabled, DesktopPatchingEnabled, setDesktopPatchingEnabled,
        EucEnabled, setEucEnabled, BackupsEnabled, setBackupsEnabled, KeeperEnabled, setKeeperEnabled, EmailEnabled, setEmailEnabled,
        SiemEnabled, setSiemEnabled, EndpointProtectionEnabled, setEndpointProtectionEnabled, NetworkManagementEnabled, setNetworkManagementEnabled,
        DataCenterOnPremises, setDataCenterOnPremises, DataCenterChoiceCloud, setDataCenterChoiceCloud, DataCenterMicrosoftAzure, setDataCenterMicrosoftAzure,
        EucUsers, setEucUsers, HsdImages, setHsdImages, VdiImages, setVdiImages, EucType, setEucType, CitrixType, setCitrixType,
        ServerManagementQuantity, setServerManagementQuantity, DesktopPatchingQuantity, setDesktopPatchingQuantity, ServerPatchingOnly, setServerPatchingOnly,
        KeeperInstances, setKeeperInstances, EmailProtection, setEmailProtection, EndpointProtectionServers, setEndpointProtectionServers,
        EndpointProtectionDesktops, setEndpointProtectionDesktops, NetworkSwitches, setNetworkSwitches, NetworkFirewalls, setNetworkFirewalls,
        NetworkFirewallsQty, setNetworkFirewallsQty, PaloAltoLicenses, setPaloAltoLicenses, SecSeries, setSecSeries,
        NetworkLoadBalancers, setNetworkLoadBalancers, adpEnabled, setAdpEnabled, adpRubricState, setadpRubricState, rubricOptions,
        adpTier, setAdpTier, adpBand, setAdpBand, adpNodeCount, setadpNodeCount, adpStorage, setAdpStorage, CCColoSpace, setCCColoSpace,
        RackUSpace, setRackUSpace, NPlus1Required, setNPlus1Required, BackupAsAServiceUnitrends, setBackupAsAServiceUnitrends, BackupTotalStorageUnitrends, setBackupTotalStorageUnitrends, 
        BackupProtectedDevicesUnitrends, setBackupProtectedDevicesUnitrends, CustBackupOnsiteDevicesUnitrends, setCustBackupOnsiteDevicesUnitrends, BusinessContinuityDisasterRelief,setBusinessContinuityDisasterRelief,
        BackupPrimaryLocationUnitrends, setBackupPrimaryLocationUnitrends, BackupAlternateLocationUnitrends, setBackupAlternateLocationUnitrends, BCDRType, setBCDRType, BCDRTarget, setBCDRTarget, AssuredDP, setAssuredDP, AssuredDPData, setAssuredDPData,
        Adc, setAdc, AdcCitrix, setAdcCitrix, AdcCitrixCount, setAdcCitrixCount, AdcCitrixCpu, setAdcCitrixCpu, AdcCitrixMem, setAdcCitrixMem,
        LoadBalancerWoCitrix, setLoadBalancerWoCitrix, LoadBalancerWoCitrixCount, setLoadBalancerWoCitrixCount, LoadBalancerWoCitrixCpu, setLoadBalancerWoCitrixCpu,
        LoadBalancerWoCitrixMem, setLoadBalancerWoCitrixMem, CcTier1Storage, setCcTier1Storage, MsWindowsServerTotalQty, setMsWindowsServerTotalQty,
        HostedVdiServerTotalQty, setHostedVdiServerTotalQty, UnixLinuxServerTotalQty, setUnixLinuxServerTotalQty,
        NetworkAppServerTotalQty, setNetworkAppServerTotalQty, Tier1StorageServerTotalQty, setTier1StorageServerTotalQty,
        Tier2StorageServerTotalQty, setTier2StorageServerTotalQty, VcpuServerTotalQty, setVcpuServerTotalQty, VmemoryServerTotalQty, setVmemoryServerTotalQty,
        CitrixSqlServerTotalQty, setCitrixSqlServerTotalQty, SqlServerLicenseServerTotalQty, setSqlServerLicenseServerTotalQty,
        SqlServerEnterpriseLicenseServerTotalQty, setSqlServerEnterpriseLicenseServerTotalQty, CitrixInfrastructureServerTotalQty, setCitrixInfrastructureServerTotalQty,
        ServerTotalsVcpu, setServerTotalsVcpu, ServerTotalsMemory, setServerTotalsMemory, ServerTotalsTier1Storage, setServerTotalsTier1Storage, ServerTotalsTier2Storage, setServerTotalsTier2Storage, 
        M365Backups, setM365Backups, M365BackupsUsers, setM365BackupsUsers, M365BackupsType, setM365BackupsType, CloudBackup, setCloudBackup, CloudBackupData, setCloudBackupData,
        CloudBackupType, setCloudBackupType, SystemInfrastructure, setSystemInfrastructure, Hypervisor, setHypervisor, HypervisorTotal, setHypervisorTotal,
        HypervisorManagement, setHypervisorManagement, HypervisorManagementType, setHypervisorManagementType, AzureCost, setAzureCost, DevicesEnabled, setDevicesEnabled,
        AdditionalNetworkFirewalls, setAdditionalNetworkFirewalls, AdditionalNetworkFirewallsQty, setAdditionalNetworkFirewallsQty, ManagedNetworkSwitches, setManagedNetworkSwitches,
        ManagedNetworkSwitchesQty, setManagedNetworkSwitchesQty, NetworkWifi, setNetworkWifi, NetworkWifiQty, setNetworkWifiQty,
        AdcLicensing, setAdcLicensing, AdcLicensingQty, setAdcLicensingQty, AdcLicensingType, setAdcLicensingType, AdcLicensingVersion, setAdcLicensingVersion,
        AdcAdm, setAdcAdm, AdcAdmCount, setAdcAdmCount, AdcVserver, setAdcVserver, AdcVserverCount, setAdcVserverCount, CitrixLicense, setCitrixLicense,
        CitrixLicenseType, setCitrixLicenseType, ContentCollaberation, setContentCollaberation, ContentCollaberationCount, setContentCollaberationCount,
        EucType2, setEucType2, Nerdio, setNerdio, AvdUsers, setAvdUsers, Contract8x5or12x6, setContract8x5or12x6, Contract8x5or12x6Type, setContract8x5or12x6Type,
        ActiveDirectoryManagement, setActiveDirectoryManagement, ActiveDirectoryManagementOptions, setActiveDirectoryManagementOptions, ActiveDirectoryManagementCount, setActiveDirectoryManagementCount,
        RemoteDesktops, setRemoteDesktops, RemoteDesktopsQty, setRemoteDesktopsQty, ManagedPrintServices, setManagedPrintServices, ManagedPrintServicesLocations, setManagedPrintServicesLocations,
        ManagedPrintServicesUsers, setManagedPrintServicesUsers, EmailTraining, setEmailTraining, EmailLicense, setEmailLicense,
        RiskManagement, setRiskManagement, RiskManagementCount, setRiskManagementCount, SiemCount, setSiemCount, SiemSensorAppliance, setSiemSensorAppliance, SiemSensorApplianceCount, setSiemSensorApplianceCount,
        FirewallTP, setFirewallTP, FirewallWMA, setFirewallWMA, FirewallUrlFiltering, setFirewallUrlFiltering, FirewallDnsSecurity, setFirewallDnsSecurity,
        FirewallSdWan, setFirewallSdWan, FirewallGlobalProtect, setFirewallGlobalProtect, FirewallVPfM, setFirewallVPfM,
        Huntress, setHuntress, HuntressCount, setHuntressCount, HuntressM365Support, setHuntressM365Support, ZeroTrust, setZeroTrust, ZeroTrustCount, setZeroTrustCount,
        Microsoft365, Microsoft365BusBasic, setMicrosoft365BusBasic, Microsoft365BusBasicCount, setMicrosoft365BusBasicCount, Microsoft365BusBasicType, setMicrosoft365BusBasicType, 
        Microsoft365BusPremium, setMicrosoft365BusPremium, Microsoft365BusPremiumCount, setMicrosoft365BusPremiumCount, Microsoft365BusPremiumType, setMicrosoft365BusPremiumType, 
        Microsoft365BusStandard, setMicrosoft365BusStandard, Microsoft365BusStandardCount, setMicrosoft365BusStandardCount, Microsoft365BusStandardType, setMicrosoft365BusStandardType, 
        Microsoft365E3, setMicrosoft365E3, Microsoft365E3Count, setMicrosoft365E3Count, Microsoft365E3Type, setMicrosoft365E3Type, Microsoft365E5, setMicrosoft365E5, Microsoft365E5Count, setMicrosoft365E5Count, 
        Microsoft365E5Type, setMicrosoft365E5Type, StreamlineIT, setStreamlineIT, StreamlineITCount, setStreamlineITCount, TraditionalMspEnabled, setTraditionalMspEnabled,
        Microsoft365Enabled, setMicrosoft365Enabled, MiscellaneousEnabled, setMiscellaneousEnabled, NeedsOnboarding, setNeedsOnboarding, SowRequired, setSowRequired,
        newRows, setNewRows, resetServerState,
    } = useData()

    // Setting skus from System Infrastructure section in the Excel spreadsheet
    // useEffect that sets the MS - Hypervisor sku in local forage
    useEffect(() => {
        if (Hypervisor) {
            localforage.setItem("MS - Hypervisor", HypervisorTotal);
        } else {
            localforage.removeItem("MS - Hypervisor");
        }
    }, [Hypervisor, HypervisorTotal])

    // useEffect that sets the MS - Nutanix-PC sku in local forage
    useEffect(() => {
        if (HypervisorManagement && HypervisorManagementType === "NutanixPrismCentral") {
            localforage.setItem("MS - Nutanix-PC", 1);
        } else {
            localforage.removeItem("MS - Nutanix-PC");
        }
    }, [HypervisorManagement, HypervisorManagementType])

    // useEffect that sets the MS - VMware-VC sku in local forage
    useEffect(() => {
        if (HypervisorManagement && HypervisorManagementType === "VMwareVcenter") {
            localforage.setItem("MS - VMware-VC", 1);
        } else {
            localforage.removeItem("MS - VMware-VC");
        }
    }, [HypervisorManagement, HypervisorManagementType])

    // useEffect that sets the MS - Azure Infrastructure and MS - Azure Infra Monitoring & Mgmt skus in local forage
    useEffect(() => {
        if (DataCenterMicrosoftAzure) {
            localforage.setItem("MS - Azure Infrastructure", AzureCost);
            localforage.setItem("MS - Azure Infra Monitoring & Mgmt", AzureCost);
        } else {
            localforage.removeItem("MS - Azure Infrastructure");
            localforage.removeItem("MS - Azure Infra Monitoring & Mgmt");
        }
    }, [DataCenterMicrosoftAzure, AzureCost])

    // Setting skus from Choice Cloud section in the Excel spreadsheet
    // useEffect that sets CC - Colo Space Storage sku in localForage
    useEffect(() => {
        if (CCColoSpace) {
            localforage.setItem("CC - Colo Space", RackUSpace);
        } else {
            localforage.removeItem("CC - Colo Space");
        }
    }, [CCColoSpace, RackUSpace])

    // useEffect that sets CC - Tier 1 Storage sku in localForage
    useEffect(() => {
        if (Tier1StorageServerTotalQty > 0) {
            localforage.setItem("CC - Tier 1 Storage", Tier1StorageServerTotalQty);
        } else {
            localforage.removeItem("CC - Tier 1 Storage");
        }
    }, [Tier1StorageServerTotalQty])

    // useEffect that sets CC - Tier 2 Storage sku in localForage
    useEffect(() => {
        if (DataCenterChoiceCloud && BackupAsAServiceUnitrends) {
            localforage.setItem("CC - Tier 2 Storage", Tier2StorageServerTotalQty);
        } else if (BackupAsAServiceUnitrends && BackupPrimaryLocationUnitrends != '') {
            localforage.setItem("CC - Tier 2 Storage", BackupTotalStorageUnitrends * 3 * 1024)
        } else {
            localforage.removeItem("CC - Tier 2 Storage");
        }
    }, [DataCenterChoiceCloud, BackupAsAServiceUnitrends, Tier2StorageServerTotalQty, BackupPrimaryLocationUnitrends, BackupTotalStorageUnitrends])

    // useEffect that sets CC - Tier 2 Backup sku in localForage
    useEffect(() => {
        if (adpEnabled && adpTier == 2 && DataCenterChoiceCloud) {
            localforage.setItem("CC - Tier 2 Backup", adpStorage);
        } else {
            localforage.removeItem("CC - Tier 2 Backup");
        }
    }, [adpEnabled, adpTier, DataCenterChoiceCloud, adpStorage])

    // useEffect that sets CC - Tier 3 Backup sku in localForage
    useEffect(() => {
        if (adpEnabled && adpTier == 3 && DataCenterChoiceCloud) {
            localforage.setItem("CC - Tier 3 Backup", adpStorage);
        } else {
            localforage.removeItem("CC - Tier 3 Backup");
        }
    }, [adpEnabled, adpTier, DataCenterChoiceCloud, adpStorage])

    // useEffect that sets ADP - Service Tier 2 skus in localForage
    useEffect(() => {
        if (adpEnabled && adpTier == 2 && !DataCenterChoiceCloud) {
            if (adpStorage <= 8) {
                localforage.setItem("ADP - Service Tier 2 - Band A", 1);
                localforage.removeItem("ADP - Service Tier 2 - Band B");
                localforage.removeItem("ADP - Service Tier 2 - Band C");
                localforage.removeItem("ADP - Service Tier 2 - Band D");
                localforage.removeItem("ADP - Service Tier 2 - Band E");
            } else if (adpStorage <= 18) {
                localforage.setItem("ADP - Service Tier 2 - Band B", 1);
                localforage.removeItem("ADP - Service Tier 2 - Band A");
                localforage.removeItem("ADP - Service Tier 2 - Band C");
                localforage.removeItem("ADP - Service Tier 2 - Band D");
                localforage.removeItem("ADP - Service Tier 2 - Band E");
            } else if (adpStorage <= 28) {
                localforage.setItem("ADP - Service Tier 2 - Band C", 1);
                localforage.removeItem("ADP - Service Tier 2 - Band A");
                localforage.removeItem("ADP - Service Tier 2 - Band B");
                localforage.removeItem("ADP - Service Tier 2 - Band D");
                localforage.removeItem("ADP - Service Tier 2 - Band E");
            } else if (adpStorage <= 38) {
                localforage.setItem("ADP - Service Tier 2 - Band D", 1);
                localforage.removeItem("ADP - Service Tier 2 - Band A");
                localforage.removeItem("ADP - Service Tier 2 - Band B");
                localforage.removeItem("ADP - Service Tier 2 - Band C");
                localforage.removeItem("ADP - Service Tier 2 - Band E");
            } else if (adpStorage <= 48) {
                localforage.setItem("ADP - Service Tier 2 - Band E", 1);
                localforage.removeItem("ADP - Service Tier 2 - Band A");
                localforage.removeItem("ADP - Service Tier 2 - Band B");
                localforage.removeItem("ADP - Service Tier 2 - Band C");
                localforage.removeItem("ADP - Service Tier 2 - Band D");
            } else {
                localforage.removeItem("ADP - Service Tier 2 - Band A");
                localforage.removeItem("ADP - Service Tier 2 - Band B");
                localforage.removeItem("ADP - Service Tier 2 - Band C");
                localforage.removeItem("ADP - Service Tier 2 - Band D");
                localforage.removeItem("ADP - Service Tier 2 - Band E");
            }
        } else {
            localforage.removeItem("ADP - Service Tier 2 - Band A");
            localforage.removeItem("ADP - Service Tier 2 - Band B");
            localforage.removeItem("ADP - Service Tier 2 - Band C");
            localforage.removeItem("ADP - Service Tier 2 - Band D");
            localforage.removeItem("ADP - Service Tier 2 - Band E");
        }
    }, [adpEnabled, adpTier, DataCenterChoiceCloud, adpStorage])

    // useEffect that sets ADP - Service Tier 3 skus in localForage
    useEffect(() => {
        if (adpEnabled && adpTier == 3 && !DataCenterChoiceCloud) {
            if (adpStorage <= 8) {
                localforage.setItem("ADP - Service Tier 3 - Band A", 1);
                localforage.removeItem("ADP - Service Tier 3 - Band B");
                localforage.removeItem("ADP - Service Tier 3 - Band C");
                localforage.removeItem("ADP - Service Tier 3 - Band D");
                localforage.removeItem("ADP - Service Tier 3 - Band E");
            } else if (adpStorage <= 18) {
                localforage.setItem("ADP - Service Tier 3 - Band B", 1);
                localforage.removeItem("ADP - Service Tier 3 - Band A");
                localforage.removeItem("ADP - Service Tier 3 - Band C");
                localforage.removeItem("ADP - Service Tier 3 - Band D");
                localforage.removeItem("ADP - Service Tier 3 - Band E");
            } else if (adpStorage <= 28) {
                localforage.setItem("ADP - Service Tier 3 - Band C", 1);
                localforage.removeItem("ADP - Service Tier 3 - Band A");
                localforage.removeItem("ADP - Service Tier 3 - Band B");
                localforage.removeItem("ADP - Service Tier 3 - Band D");
                localforage.removeItem("ADP - Service Tier 3 - Band E");
            } else if (adpStorage <= 38) {
                localforage.setItem("ADP - Service Tier 3 - Band D", 1);
                localforage.removeItem("ADP - Service Tier 3 - Band A");
                localforage.removeItem("ADP - Service Tier 3 - Band B");
                localforage.removeItem("ADP - Service Tier 3 - Band C");
                localforage.removeItem("ADP - Service Tier 3 - Band E");
            } else if (adpStorage <= 48) {
                localforage.setItem("ADP - Service Tier 3 - Band E", 1);
                localforage.removeItem("ADP - Service Tier 3 - Band A");
                localforage.removeItem("ADP - Service Tier 3 - Band B");
                localforage.removeItem("ADP - Service Tier 3 - Band C");
                localforage.removeItem("ADP - Service Tier 3 - Band D");
            } else {
                localforage.removeItem("ADP - Service Tier 3 - Band A");
                localforage.removeItem("ADP - Service Tier 3 - Band B");
                localforage.removeItem("ADP - Service Tier 3 - Band C");
                localforage.removeItem("ADP - Service Tier 3 - Band D");
                localforage.removeItem("ADP - Service Tier 3 - Band E");
            }
        } else {
            localforage.removeItem("ADP - Service Tier 3 - Band A");
            localforage.removeItem("ADP - Service Tier 3 - Band B");
            localforage.removeItem("ADP - Service Tier 3 - Band C");
            localforage.removeItem("ADP - Service Tier 3 - Band D");
            localforage.removeItem("ADP - Service Tier 3 - Band E");
        }
    }, [adpEnabled, adpTier, DataCenterChoiceCloud, adpStorage])

    // useEffect that sets CC - M365 Backup skus in localForage
    useEffect(() => {
        if (M365Backups && M365BackupsType === "Unlimited") {
            localforage.removeItem("CC - M365 Backup 20GB Tier A");
            localforage.removeItem("CC - M365 Backup 20GB Tier B");
            localforage.removeItem("CC - M365 Backup 20GB Tier C");
            localforage.removeItem("CC - M365 Backup 20GB Tier D");
            localforage.removeItem("CC - M365 Backup 20GB Tier E");
            localforage.removeItem("CC - M365 Backup 20GB Tier F");
            localforage.removeItem("CC - M365 Backup 20GB Tier G");
            if (M365BackupsUsers > 0 && M365BackupsUsers <= 500) {
                localforage.setItem("CC - M365 Backup Unlimited Tier A", M365BackupsUsers);
                localforage.removeItem("CC - M365 Backup Unlimited Tier B");
                localforage.removeItem("CC - M365 Backup Unlimited Tier C");
                localforage.removeItem("CC - M365 Backup Unlimited Tier D");
                localforage.removeItem("CC - M365 Backup Unlimited Tier E");
                localforage.removeItem("CC - M365 Backup Unlimited Tier F");
                localforage.removeItem("CC - M365 Backup Unlimited Tier G");
            } else if (M365BackupsUsers > 500 && M365BackupsUsers <= 1000) {
                localforage.setItem("CC - M365 Backup Unlimited Tier B", M365BackupsUsers);
                localforage.removeItem("CC - M365 Backup Unlimited Tier A");
                localforage.removeItem("CC - M365 Backup Unlimited Tier C");
                localforage.removeItem("CC - M365 Backup Unlimited Tier D");
                localforage.removeItem("CC - M365 Backup Unlimited Tier E");
                localforage.removeItem("CC - M365 Backup Unlimited Tier F");
                localforage.removeItem("CC - M365 Backup Unlimited Tier G");
            } else if (M365BackupsUsers > 1000 && M365BackupsUsers <= 5000) {
                localforage.setItem("CC - M365 Backup Unlimited Tier C", M365BackupsUsers);
                localforage.removeItem("CC - M365 Backup Unlimited Tier A");
                localforage.removeItem("CC - M365 Backup Unlimited Tier B");
                localforage.removeItem("CC - M365 Backup Unlimited Tier D");
                localforage.removeItem("CC - M365 Backup Unlimited Tier E");
                localforage.removeItem("CC - M365 Backup Unlimited Tier F");
                localforage.removeItem("CC - M365 Backup Unlimited Tier G");
            } else if (M365BackupsUsers > 5000 && M365BackupsUsers <= 10000) {
                localforage.setItem("CC - M365 Backup Unlimited Tier D", M365BackupsUsers);
                localforage.removeItem("CC - M365 Backup Unlimited Tier A");
                localforage.removeItem("CC - M365 Backup Unlimited Tier B");
                localforage.removeItem("CC - M365 Backup Unlimited Tier C");
                localforage.removeItem("CC - M365 Backup Unlimited Tier E");
                localforage.removeItem("CC - M365 Backup Unlimited Tier F");
                localforage.removeItem("CC - M365 Backup Unlimited Tier G");
            } else if (M365BackupsUsers > 10000 && M365BackupsUsers <= 15000) {
                localforage.setItem("CC - M365 Backup Unlimited Tier E", M365BackupsUsers);
                localforage.removeItem("CC - M365 Backup Unlimited Tier A");
                localforage.removeItem("CC - M365 Backup Unlimited Tier B");
                localforage.removeItem("CC - M365 Backup Unlimited Tier C");
                localforage.removeItem("CC - M365 Backup Unlimited Tier D");
                localforage.removeItem("CC - M365 Backup Unlimited Tier F");
                localforage.removeItem("CC - M365 Backup Unlimited Tier G");
            } else if (M365BackupsUsers > 15000 && M365BackupsUsers <= 20000) {
                localforage.setItem("CC - M365 Backup Unlimited Tier F", M365BackupsUsers);
                localforage.removeItem("CC - M365 Backup Unlimited Tier A");
                localforage.removeItem("CC - M365 Backup Unlimited Tier B");
                localforage.removeItem("CC - M365 Backup Unlimited Tier C");
                localforage.removeItem("CC - M365 Backup Unlimited Tier D");
                localforage.removeItem("CC - M365 Backup Unlimited Tier E");
                localforage.removeItem("CC - M365 Backup Unlimited Tier G");
            } else if (M365BackupsUsers > 20000) {
                localforage.setItem("CC - M365 Backup Unlimited Tier G", M365BackupsUsers);
                localforage.removeItem("CC - M365 Backup Unlimited Tier A");
                localforage.removeItem("CC - M365 Backup Unlimited Tier B");
                localforage.removeItem("CC - M365 Backup Unlimited Tier C");
                localforage.removeItem("CC - M365 Backup Unlimited Tier D");
                localforage.removeItem("CC - M365 Backup Unlimited Tier E");
                localforage.removeItem("CC - M365 Backup Unlimited Tier F");
            } else {
                localforage.removeItem("CC - M365 Backup Unlimited Tier A");
                localforage.removeItem("CC - M365 Backup Unlimited Tier B");
                localforage.removeItem("CC - M365 Backup Unlimited Tier C");
                localforage.removeItem("CC - M365 Backup Unlimited Tier D");
                localforage.removeItem("CC - M365 Backup Unlimited Tier E");
                localforage.removeItem("CC - M365 Backup Unlimited Tier F");
                localforage.removeItem("CC - M365 Backup Unlimited Tier G");
            }
        } else if (M365Backups && M365BackupsType === "20GB") {
            localforage.removeItem("CC - M365 Backup Unlimited Tier A");
            localforage.removeItem("CC - M365 Backup Unlimited Tier B");
            localforage.removeItem("CC - M365 Backup Unlimited Tier C");
            localforage.removeItem("CC - M365 Backup Unlimited Tier D");
            localforage.removeItem("CC - M365 Backup Unlimited Tier E");
            localforage.removeItem("CC - M365 Backup Unlimited Tier F");
            localforage.removeItem("CC - M365 Backup Unlimited Tier G");
            if (M365BackupsUsers > 0 && M365BackupsUsers <= 500) {
                localforage.setItem("CC - M365 Backup 20GB Tier A", M365BackupsUsers);
                localforage.removeItem("CC - M365 Backup 20GB Tier B");
                localforage.removeItem("CC - M365 Backup 20GB Tier C");
                localforage.removeItem("CC - M365 Backup 20GB Tier D");
                localforage.removeItem("CC - M365 Backup 20GB Tier E");
                localforage.removeItem("CC - M365 Backup 20GB Tier F");
                localforage.removeItem("CC - M365 Backup 20GB Tier G");
            } else if (M365BackupsUsers > 500 && M365BackupsUsers <= 1000) {
                localforage.setItem("CC - M365 Backup 20GB Tier B", M365BackupsUsers);
                localforage.removeItem("CC - M365 Backup 20GB Tier A");
                localforage.removeItem("CC - M365 Backup 20GB Tier C");
                localforage.removeItem("CC - M365 Backup 20GB Tier D");
                localforage.removeItem("CC - M365 Backup 20GB Tier E");
                localforage.removeItem("CC - M365 Backup 20GB Tier F");
                localforage.removeItem("CC - M365 Backup 20GB Tier G");
            } else if (M365BackupsUsers > 1000 && M365BackupsUsers <= 5000) {
                localforage.setItem("CC - M365 Backup 20GB Tier C", M365BackupsUsers);
                localforage.removeItem("CC - M365 Backup 20GB Tier A");
                localforage.removeItem("CC - M365 Backup 20GB Tier B");
                localforage.removeItem("CC - M365 Backup 20GB Tier D");
                localforage.removeItem("CC - M365 Backup 20GB Tier E");
                localforage.removeItem("CC - M365 Backup 20GB Tier F");
                localforage.removeItem("CC - M365 Backup 20GB Tier G");
            } else if (M365BackupsUsers > 5000 && M365BackupsUsers <= 10000) {
                localforage.setItem("CC - M365 Backup 20GB Tier D", M365BackupsUsers);
                localforage.removeItem("CC - M365 Backup 20GB Tier A");
                localforage.removeItem("CC - M365 Backup 20GB Tier B");
                localforage.removeItem("CC - M365 Backup 20GB Tier C");
                localforage.removeItem("CC - M365 Backup 20GB Tier E");
                localforage.removeItem("CC - M365 Backup 20GB Tier F");
                localforage.removeItem("CC - M365 Backup 20GB Tier G");
            } else if (M365BackupsUsers > 10000 && M365BackupsUsers <= 15000) {
                localforage.setItem("CC - M365 Backup 20GB Tier E", M365BackupsUsers);
                localforage.removeItem("CC - M365 Backup 20GB Tier A");
                localforage.removeItem("CC - M365 Backup 20GB Tier B");
                localforage.removeItem("CC - M365 Backup 20GB Tier C");
                localforage.removeItem("CC - M365 Backup 20GB Tier D");
                localforage.removeItem("CC - M365 Backup 20GB Tier F");
                localforage.removeItem("CC - M365 Backup 20GB Tier G");
            } else if (M365BackupsUsers > 15000 && M365BackupsUsers <= 20000) {
                localforage.setItem("CC - M365 Backup 20GB Tier F", M365BackupsUsers);
                localforage.removeItem("CC - M365 Backup 20GB Tier A");
                localforage.removeItem("CC - M365 Backup 20GB Tier B");
                localforage.removeItem("CC - M365 Backup 20GB Tier C");
                localforage.removeItem("CC - M365 Backup 20GB Tier D");
                localforage.removeItem("CC - M365 Backup 20GB Tier E");
                localforage.removeItem("CC - M365 Backup 20GB Tier G");
            } else if (M365BackupsUsers > 20000) {
                localforage.setItem("CC - M365 Backup 20GB Tier G", M365BackupsUsers);
                localforage.removeItem("CC - M365 Backup 20GB Tier A");
                localforage.removeItem("CC - M365 Backup 20GB Tier B");
                localforage.removeItem("CC - M365 Backup 20GB Tier C");
                localforage.removeItem("CC - M365 Backup 20GB Tier D");
                localforage.removeItem("CC - M365 Backup 20GB Tier E");
                localforage.removeItem("CC - M365 Backup 20GB Tier F");
            } else {
                localforage.removeItem("CC - M365 Backup 20GB Tier A");
                localforage.removeItem("CC - M365 Backup 20GB Tier B");
                localforage.removeItem("CC - M365 Backup 20GB Tier C");
                localforage.removeItem("CC - M365 Backup 20GB Tier D");
                localforage.removeItem("CC - M365 Backup 20GB Tier E");
                localforage.removeItem("CC - M365 Backup 20GB Tier F");
                localforage.removeItem("CC - M365 Backup 20GB Tier G");
            }
        } else {
            localforage.removeItem("CC - M365 Backup Unlimited Tier A");
            localforage.removeItem("CC - M365 Backup Unlimited Tier B");
            localforage.removeItem("CC - M365 Backup Unlimited Tier C");
            localforage.removeItem("CC - M365 Backup Unlimited Tier D");
            localforage.removeItem("CC - M365 Backup Unlimited Tier E");
            localforage.removeItem("CC - M365 Backup Unlimited Tier F");
            localforage.removeItem("CC - M365 Backup Unlimited Tier G");
            localforage.removeItem("CC - M365 Backup 20GB Tier A");
            localforage.removeItem("CC - M365 Backup 20GB Tier B");
            localforage.removeItem("CC - M365 Backup 20GB Tier C");
            localforage.removeItem("CC - M365 Backup 20GB Tier D");
            localforage.removeItem("CC - M365 Backup 20GB Tier E");
            localforage.removeItem("CC - M365 Backup 20GB Tier F");
            localforage.removeItem("CC - M365 Backup 20GB Tier G");
        }
    }, [M365Backups, M365BackupsType, M365BackupsUsers])

    // useEffect that CC -UCL Cloud Backup skus in localForage
    useEffect(() => {
        if (CloudBackup && CloudBackupType === "Foundation") {
            localforage.removeItem("CC - UCL Cloud Backup Enterprise Tier A");
            localforage.removeItem("CC - UCL Cloud Backup Enterprise Tier B");
            localforage.removeItem("CC - UCL Cloud Backup Enterprise Tier C");
            localforage.removeItem("CC - UCL Cloud Backup Enterprise Tier D");
            localforage.removeItem("CC - UCL Cloud Backup Enterprise Tier E");
            localforage.removeItem("CC - UCL Cloud Backup Enterprise Tier F");
            localforage.removeItem("CC - UCL Cloud Backup Enterprise Tier G");
            if (CloudBackupData > 0 && CloudBackupData <= 200) {
                localforage.setItem("CC - UCL Cloud Backup Foundation Tier A", CloudBackupData);
                localforage.removeItem("CC - UCL Cloud Backup Foundation Tier B");
                localforage.removeItem("CC - UCL Cloud Backup Foundation Tier C");
                localforage.removeItem("CC - UCL Cloud Backup Foundation Tier D");
                localforage.removeItem("CC - UCL Cloud Backup Foundation Tier E");
                localforage.removeItem("CC - UCL Cloud Backup Foundation Tier F");
                localforage.removeItem("CC - UCL Cloud Backup Foundation Tier G");
            } else if (CloudBackupData > 200 && CloudBackupData <= 400) {
                localforage.setItem("CC - UCL Cloud Backup Foundation Tier B", CloudBackupData);
                localforage.removeItem("CC - UCL Cloud Backup Foundation Tier A");
                localforage.removeItem("CC - UCL Cloud Backup Foundation Tier C");
                localforage.removeItem("CC - UCL Cloud Backup Foundation Tier D");
                localforage.removeItem("CC - UCL Cloud Backup Foundation Tier E");
                localforage.removeItem("CC - UCL Cloud Backup Foundation Tier F");
                localforage.removeItem("CC - UCL Cloud Backup Foundation Tier G");
            } else if (CloudBackupData > 400 && CloudBackupData <= 600) {
                localforage.setItem("CC - UCL Cloud Backup Foundation Tier C", CloudBackupData);
                localforage.removeItem("CC - UCL Cloud Backup Foundation Tier A");
                localforage.removeItem("CC - UCL Cloud Backup Foundation Tier B");
                localforage.removeItem("CC - UCL Cloud Backup Foundation Tier D");
                localforage.removeItem("CC - UCL Cloud Backup Foundation Tier E");
                localforage.removeItem("CC - UCL Cloud Backup Foundation Tier F");
                localforage.removeItem("CC - UCL Cloud Backup Foundation Tier G");
            } else if (CloudBackupData > 600 && CloudBackupData <= 1000) {
                localforage.setItem("CC - UCL Cloud Backup Foundation Tier D", CloudBackupData);
                localforage.removeItem("CC - UCL Cloud Backup Foundation Tier A");
                localforage.removeItem("CC - UCL Cloud Backup Foundation Tier B");
                localforage.removeItem("CC - UCL Cloud Backup Foundation Tier C");
                localforage.removeItem("CC - UCL Cloud Backup Foundation Tier E");
                localforage.removeItem("CC - UCL Cloud Backup Foundation Tier F");
                localforage.removeItem("CC - UCL Cloud Backup Foundation Tier G");
            } else if (CloudBackupData > 1000 && CloudBackupData <= 1500) {
                localforage.setItem("CC - UCL Cloud Backup Foundation Tier E", CloudBackupData);
                localforage.removeItem("CC - UCL Cloud Backup Foundation Tier A");
                localforage.removeItem("CC - UCL Cloud Backup Foundation Tier B");
                localforage.removeItem("CC - UCL Cloud Backup Foundation Tier C");
                localforage.removeItem("CC - UCL Cloud Backup Foundation Tier D");
                localforage.removeItem("CC - UCL Cloud Backup Foundation Tier F");
                localforage.removeItem("CC - UCL Cloud Backup Foundation Tier G");
            } else if (CloudBackupData > 1500 && CloudBackupData <= 2000) {
                localforage.setItem("CC - UCL Cloud Backup Foundation Tier F", CloudBackupData);
                localforage.removeItem("CC - UCL Cloud Backup Foundation Tier A");
                localforage.removeItem("CC - UCL Cloud Backup Foundation Tier B");
                localforage.removeItem("CC - UCL Cloud Backup Foundation Tier C");
                localforage.removeItem("CC - UCL Cloud Backup Foundation Tier D");
                localforage.removeItem("CC - UCL Cloud Backup Foundation Tier E");
                localforage.removeItem("CC - UCL Cloud Backup Foundation Tier G");
            } else if (CloudBackupData > 2000 && CloudBackupData <= 999999) {
                localforage.setItem("CC - UCL Cloud Backup Foundation Tier G", CloudBackupData);
                localforage.removeItem("CC - UCL Cloud Backup Foundation Tier A");
                localforage.removeItem("CC - UCL Cloud Backup Foundation Tier B");
                localforage.removeItem("CC - UCL Cloud Backup Foundation Tier C");
                localforage.removeItem("CC - UCL Cloud Backup Foundation Tier D");
                localforage.removeItem("CC - UCL Cloud Backup Foundation Tier E");
                localforage.removeItem("CC - UCL Cloud Backup Foundation Tier F");
            } else {
                localforage.removeItem("CC - UCL Cloud Backup Foundation Tier A");
                localforage.removeItem("CC - UCL Cloud Backup Foundation Tier B");
                localforage.removeItem("CC - UCL Cloud Backup Foundation Tier C");
                localforage.removeItem("CC - UCL Cloud Backup Foundation Tier D");
                localforage.removeItem("CC - UCL Cloud Backup Foundation Tier E");
                localforage.removeItem("CC - UCL Cloud Backup Foundation Tier F");
                localforage.removeItem("CC - UCL Cloud Backup Foundation Tier G");
            }
        } else if (CloudBackup && CloudBackupType === "Enterprise") {
            localforage.removeItem("CC - UCL Cloud Backup Foundation Tier A");
            localforage.removeItem("CC - UCL Cloud Backup Foundation Tier B");
            localforage.removeItem("CC - UCL Cloud Backup Foundation Tier C");
            localforage.removeItem("CC - UCL Cloud Backup Foundation Tier D");
            localforage.removeItem("CC - UCL Cloud Backup Foundation Tier E");
            localforage.removeItem("CC - UCL Cloud Backup Foundation Tier F");
            localforage.removeItem("CC - UCL Cloud Backup Foundation Tier G");
            if (CloudBackupData > 0 && CloudBackupData <= 200) {
                localforage.setItem("CC - UCL Cloud Backup Enterprise Tier A", CloudBackupData);
                localforage.removeItem("CC - UCL Cloud Backup Enterprise Tier B");
                localforage.removeItem("CC - UCL Cloud Backup Enterprise Tier C");
                localforage.removeItem("CC - UCL Cloud Backup Enterprise Tier D");
                localforage.removeItem("CC - UCL Cloud Backup Enterprise Tier E");
                localforage.removeItem("CC - UCL Cloud Backup Enterprise Tier F");
                localforage.removeItem("CC - UCL Cloud Backup Enterprise Tier G");
            } else if (CloudBackupData > 200 && CloudBackupData <= 400) {
                localforage.setItem("CC - UCL Cloud Backup Enterprise Tier B", CloudBackupData);
                localforage.removeItem("CC - UCL Cloud Backup Enterprise Tier A");
                localforage.removeItem("CC - UCL Cloud Backup Enterprise Tier C");
                localforage.removeItem("CC - UCL Cloud Backup Enterprise Tier D");
                localforage.removeItem("CC - UCL Cloud Backup Enterprise Tier E");
                localforage.removeItem("CC - UCL Cloud Backup Enterprise Tier F");
                localforage.removeItem("CC - UCL Cloud Backup Enterprise Tier G");
            } else if (CloudBackupData > 400 && CloudBackupData <= 600) {
                localforage.setItem("CC - UCL Cloud Backup Enterprise Tier C", CloudBackupData);
                localforage.removeItem("CC - UCL Cloud Backup Enterprise Tier A");
                localforage.removeItem("CC - UCL Cloud Backup Enterprise Tier B");
                localforage.removeItem("CC - UCL Cloud Backup Enterprise Tier D");
                localforage.removeItem("CC - UCL Cloud Backup Enterprise Tier E");
                localforage.removeItem("CC - UCL Cloud Backup Enterprise Tier F");
                localforage.removeItem("CC - UCL Cloud Backup Enterprise Tier G");
            } else if (CloudBackupData > 600 && CloudBackupData <= 1000) {
                localforage.setItem("CC - UCL Cloud Backup Enterprise Tier D", CloudBackupData);
                localforage.removeItem("CC - UCL Cloud Backup Enterprise Tier A");
                localforage.removeItem("CC - UCL Cloud Backup Enterprise Tier B");
                localforage.removeItem("CC - UCL Cloud Backup Enterprise Tier C");
                localforage.removeItem("CC - UCL Cloud Backup Enterprise Tier E");
                localforage.removeItem("CC - UCL Cloud Backup Enterprise Tier F");
                localforage.removeItem("CC - UCL Cloud Backup Enterprise Tier G");
            } else if (CloudBackupData > 1000 && CloudBackupData <= 1500) {
                localforage.setItem("CC - UCL Cloud Backup Enterprise Tier E", CloudBackupData);
                localforage.removeItem("CC - UCL Cloud Backup Enterprise Tier A");
                localforage.removeItem("CC - UCL Cloud Backup Enterprise Tier B");
                localforage.removeItem("CC - UCL Cloud Backup Enterprise Tier C");
                localforage.removeItem("CC - UCL Cloud Backup Enterprise Tier D");
                localforage.removeItem("CC - UCL Cloud Backup Enterprise Tier F");
                localforage.removeItem("CC - UCL Cloud Backup Enterprise Tier G");
            } else if (CloudBackupData > 1500 && CloudBackupData <= 2000) {
                localforage.setItem("CC - UCL Cloud Backup Enterprise Tier F", CloudBackupData);
                localforage.removeItem("CC - UCL Cloud Backup Enterprise Tier A");
                localforage.removeItem("CC - UCL Cloud Backup Enterprise Tier B");
                localforage.removeItem("CC - UCL Cloud Backup Enterprise Tier C");
                localforage.removeItem("CC - UCL Cloud Backup Enterprise Tier D");
                localforage.removeItem("CC - UCL Cloud Backup Enterprise Tier E");
                localforage.removeItem("CC - UCL Cloud Backup Enterprise Tier G");
            } else if (CloudBackupData > 2000 && CloudBackupData <= 999999) {
                localforage.setItem("CC - UCL Cloud Backup Enterprise Tier G", CloudBackupData);
                localforage.removeItem("CC - UCL Cloud Backup Enterprise Tier A");
                localforage.removeItem("CC - UCL Cloud Backup Enterprise Tier B");
                localforage.removeItem("CC - UCL Cloud Backup Enterprise Tier C");
                localforage.removeItem("CC - UCL Cloud Backup Enterprise Tier D");
                localforage.removeItem("CC - UCL Cloud Backup Enterprise Tier E");
                localforage.removeItem("CC - UCL Cloud Backup Enterprise Tier F");
            } else {
                localforage.removeItem("CC - UCL Cloud Backup Enterprise Tier A");
                localforage.removeItem("CC - UCL Cloud Backup Enterprise Tier B");
                localforage.removeItem("CC - UCL Cloud Backup Enterprise Tier C");
                localforage.removeItem("CC - UCL Cloud Backup Enterprise Tier D");
                localforage.removeItem("CC - UCL Cloud Backup Enterprise Tier E");
                localforage.removeItem("CC - UCL Cloud Backup Enterprise Tier F");
                localforage.removeItem("CC - UCL Cloud Backup Enterprise Tier G");
            }
        } else {
            localforage.removeItem("CC - UCL Cloud Backup Foundation Tier A");
            localforage.removeItem("CC - UCL Cloud Backup Foundation Tier B");
            localforage.removeItem("CC - UCL Cloud Backup Foundation Tier C");
            localforage.removeItem("CC - UCL Cloud Backup Foundation Tier D");
            localforage.removeItem("CC - UCL Cloud Backup Foundation Tier E");
            localforage.removeItem("CC - UCL Cloud Backup Foundation Tier F");
            localforage.removeItem("CC - UCL Cloud Backup Foundation Tier G");
            localforage.removeItem("CC - UCL Cloud Backup Enterprise Tier A");
            localforage.removeItem("CC - UCL Cloud Backup Enterprise Tier B");
            localforage.removeItem("CC - UCL Cloud Backup Enterprise Tier C");
            localforage.removeItem("CC - UCL Cloud Backup Enterprise Tier D");
            localforage.removeItem("CC - UCL Cloud Backup Enterprise Tier E");
            localforage.removeItem("CC - UCL Cloud Backup Enterprise Tier F");
            localforage.removeItem("CC - UCL Cloud Backup Enterprise Tier G");
        }
    }, [CloudBackup, CloudBackupData, CloudBackupType])

    // useEffect that sets MS - BU Virtual App sku in localForage
    useEffect(() => {
        if (DataCenterChoiceCloud && BackupAsAServiceUnitrends) {
            localforage.setItem("MS - BU Virtual App", Math.ceil(Tier1StorageServerTotalQty / 1024));
        } else if (BackupAsAServiceUnitrends) {
            localforage.setItem("MS - BU Virtual App", Math.ceil(BackupTotalStorageUnitrends));
        } else {
            localforage.removeItem("MS - BU Virtual App");
        }
    }, [DataCenterChoiceCloud, BackupAsAServiceUnitrends, Tier1StorageServerTotalQty, BackupTotalStorageUnitrends])

    // useEffect that sets MS - BU Protected Dev sku in localForage
    useEffect(() => {
        if (BackupAsAServiceUnitrends) {
            localforage.setItem("MS - BU Protected Dev", BackupProtectedDevicesUnitrends);
        } else {
            localforage.removeItem("MS - BU Protected Dev");
        }
    }, [BackupAsAServiceUnitrends, BackupProtectedDevicesUnitrends])

    // useEffect that sets UMSP skus in localForage
    const storageValues = [0.55, 1.1, 2.4, 3.6, 4.8, 7.2, 9.4, 14.4, 19, 24, 36, 48, 60, 72];
    storageValues.forEach((value, index) => {
        useEffect(() => {
            const lowerBound = index === 0 ? 0 : storageValues[index - 1];
            const upperBound = value;
            const keys = ["UMSP-1", "UMSP-2", "UMSP-4", "UMSP-6", "UMSP-8", "UMSP-12", "UMSP-16S", "UMSP-24S", "UMSP-32S", "UMSP-40S", "UMSP-60S", "UMSP-80S", "UMSP-100S", "UMSP-120S"]
            if (
                CustBackupOnsiteDevicesUnitrends === "CustOwnsNoAppliance" &&
                BackupAsAServiceUnitrends &&
                !DataCenterChoiceCloud &&
                BackupTotalStorageUnitrends >= lowerBound &&
                BackupTotalStorageUnitrends < upperBound
            ) {
                localforage.setItem(keys[index], 1);
            } else {
                localforage.removeItem(keys[index]);
            }
        }, [
            BackupAsAServiceUnitrends,
            CustBackupOnsiteDevicesUnitrends,
            DataCenterChoiceCloud,
            BackupTotalStorageUnitrends,
        ]);
    });

    // useEffect that sets MS - Backup App Admin sku in localForage
    useEffect(() => {
        if (CustBackupOnsiteDevicesUnitrends === "CustOwnsAppliance" && BackupAsAServiceUnitrends) {
            localforage.setItem("MS - Backup App Admin", 1)
        } else {
            localforage.removeItem("MS - Backup App Admin")
        }
    }, [CustBackupOnsiteDevicesUnitrends, BackupAsAServiceUnitrends])

    // useEffect that sets CC - vCPU sku in localForage
    useEffect(() => {
        if (VcpuServerTotalQty > 0) {
            localforage.setItem("CC - vCPU", VcpuServerTotalQty);
        } else {
            localforage.removeItem("CC - vCPU");
        }
    }, [VcpuServerTotalQty])

    // useEffect that sets CC - vMemory sku in localForage
    useEffect(() => {
        if (VmemoryServerTotalQty > 0) {
            localforage.setItem("CC - vMemory", VmemoryServerTotalQty);
        } else {
            localforage.removeItem("CC - vMemory")
        }
    }, [VmemoryServerTotalQty])

    // useEffect that sets BC / DR - vCPU sku in localForage
    useEffect(() => {
        if (BusinessContinuityDisasterRelief && BCDRType === "ColdStorage" && (BCDRTarget === "ChoiceAtlanta" || BCDRTarget === "ChoiceDallas")) {
            localforage.setItem("BC / DR - vCPU", ServerTotalsVcpu);
        } else {
            localforage.removeItem("BC / DR - vCPU");
        }
    }, [BusinessContinuityDisasterRelief, BCDRType, BCDRTarget, ServerTotalsVcpu])

    // useEffect that sets BC / DR - vMemory sku in localForage
    useEffect(() => {
        if (BusinessContinuityDisasterRelief && BCDRType === "ColdStorage" && (BCDRTarget === "ChoiceAtlanta" || BCDRTarget === "ChoiceDallas")) {
            localforage.setItem("BC / DR - vMemory", ServerTotalsMemory);
        } else {
            localforage.removeItem("BC / DR - vMemory");
        }
    }, [BusinessContinuityDisasterRelief, BCDRType, BCDRTarget, ServerTotalsMemory])

    // useEffect that sets BC / DR - Tier 1 Storage sku in localForage
    useEffect(() => {
        if (BusinessContinuityDisasterRelief && BCDRType === "ColdStorage" && (BCDRTarget === "ChoiceAtlanta" || BCDRTarget === "ChoiceDallas")) {
            localforage.setItem("BC / DR - Tier 1 Storage", ServerTotalsTier1Storage);
        } else {
            localforage.removeItem("BC / DR - Tier 1 Storage");
        }
    }, [BusinessContinuityDisasterRelief, BCDRType, BCDRTarget, ServerTotalsTier1Storage])

    // useEffect that sets CC - Internet Bandwidth sku in localForage
    useEffect(() => {
        if (DataCenterChoiceCloud) {
            localforage.setItem("CC - Internet Bandwidth", (EucUsers + AvdUsers) * 0.2);
        } else {
            localforage.removeItem("CC - Internet Bandwidth");
        }
    }, [DataCenterChoiceCloud, EucUsers, AvdUsers])

    // Setting skus from Network section in the Excel spreadsheet
    // useEffect that sets MS - Network App sku in localForage
    useEffect(() => {
        if (NetworkAppServerTotalQty > 0) {
            localforage.setItem("MS - Network App", NetworkAppServerTotalQty);
        } else {
            localforage.removeItem("MS - Network App");
        }
    }, [NetworkAppServerTotalQty])

    // useEffect that sets MS - Network Firewalls sku in localForage
    useEffect(() => {
        if (AdditionalNetworkFirewalls) {
            localforage.setItem("MS - Network Firewalls", AdditionalNetworkFirewallsQty);
        } else {
            localforage.removeItem("MS - Network Firewalls");
        }
    }, [AdditionalNetworkFirewalls, AdditionalNetworkFirewallsQty])

    // useEffect that sets MS - Network Switches sku in localForage
    useEffect(() => {
        if (ManagedNetworkSwitches) {
            localforage.setItem("MS - Network Switches", ManagedNetworkSwitchesQty);
        } else {
            localforage.removeItem("MS - Network Switches");
        }
    }, [ManagedNetworkSwitches, ManagedNetworkSwitchesQty])

    // useEffect that sets MS - Network Wifi sku in localForage
    useEffect(() => {
        if (NetworkWifi) {
            localforage.setItem("MS - Network Wifi", NetworkWifiQty);
        } else {
            localforage.removeItem("MS - Network Wifi");
        }
    }, [NetworkWifi, NetworkWifiQty])

    // Setting skus from Network section in the Excel spreadsheet
    // useEffect that sets MS - Citrix Cloud Infra sku in localForage
    useEffect(() => {
        if (EucEnabled && EucType === "citrix" && CitrixType === "cloud") {
            localforage.setItem("MS - Citrix Cloud Infra", 1);
        } else {
            localforage.removeItem("MS - Citrix Cloud Infra");
        }
    }, [EucEnabled, EucType, CitrixType])

    // useEffect that sets MS - Citrix Infra sku in localForage
    useEffect(() => {
        if (EucEnabled && EucType === "citrix" && CitrixType === "on_prem") {
            localforage.setItem("MS - Citrix Infra", 1);
        } else {
            localforage.removeItem("MS - Citrix Infra");
        }
    }, [EucEnabled, EucType, CitrixType])

    // useEffect that sets MS - Citrix Hybrid Infra sku in localForage
    useEffect(() => {
        if (EucEnabled, EucType === "citrix" && CitrixType === "hybrid") {
            localforage.setItem("MS - Citrix Hybrid Infra", 1);
        } else {
            localforage.removeItem("MS - Citrix Hybrid Infra");
        }
    }, [EucEnabled, EucType, CitrixType])

    // useEffect that sets MS - LB-HA sku in localForage
    useEffect(() => {
        if (NPlus1Required && (AdcCitrix && LoadBalancerWoCitrix)) {
            localforage.setItem("MS - LB-HA", (AdcCitrixCount + LoadBalancerWoCitrixCount));
        } else if (NPlus1Required && (AdcCitrix && !LoadBalancerWoCitrix)) {
            localforage.setItem("MS - LB-HA", AdcCitrixCount);
        } else if (NPlus1Required && (!AdcCitrix && LoadBalancerWoCitrix)) {
            localforage.setItem("MS - LB-HA", LoadBalancerWoCitrixCount);
        } else {
            localforage.removeItem("MS - LB-HA");
        }
    }, [NPlus1Required, AdcCitrix, LoadBalancerWoCitrix, AdcCitrixCount, LoadBalancerWoCitrixCount])

    // useEffect that sets MS - LB-NHA sku in localForage
    useEffect(() => {
        if (!NPlus1Required && (AdcCitrix && LoadBalancerWoCitrix)) {
            localforage.setItem("MS - LB-NHA", (AdcCitrixCount + LoadBalancerWoCitrixCount));
        } else if (!NPlus1Required && (AdcCitrix && !LoadBalancerWoCitrix)) {
            localforage.setItem("MS - LB-NHA", AdcCitrixCount);
        } else if (!NPlus1Required && (!AdcCitrix && LoadBalancerWoCitrix)) {
            localforage.setItem("MS - LB-NHA", LoadBalancerWoCitrixCount);
        } else {
            localforage.removeItem("MS - LB-NHA");
        }
    }, [NPlus1Required, AdcCitrix, LoadBalancerWoCitrix, AdcCitrixCount, LoadBalancerWoCitrixCount])

    // useEffect that sets MS - LB-ADM sku in localForage
    useEffect(() => {
        if (NPlus1Required && AdcAdm) {
            localforage.setItem("MS - LB-ADM", AdcAdmCount * 2);
        } else if (!NPlus1Required && AdcAdm) {
            localforage.setItem("MS - LB-ADM", AdcAdmCount);
        } else {
            localforage.removeItem("MS - LB-ADM");
        }
    }, [NPlus1Required, AdcAdm, AdcAdmCount])

    // useEffect that sets MS - ADC vServer sku in localForage
    useEffect(() => {
        if (NPlus1Required && AdcVserver) {
            localforage.setItem("MS - ADC vServer", AdcVserverCount * 2);
        } else if (!NPlus1Required && AdcVserver) {
            localforage.setItem("MS - ADC vServer", AdcVserverCount);
        } else {
            localforage.removeItem("MS - ADC vServer");
        }
    }, [NPlus1Required, AdcVserver, AdcVserverCount])

    // useEffect that sets ADC licensing sku's in localForage
    useEffect(() => {
        if (AdcLicensing) {
            switch (`${AdcLicensingType}-${AdcLicensingVersion}`) {
                case '10Mbps-Standard':
                    localforage.setItem("RPTCNSVPXCSPSE10", AdcLicensingQty);
                    localforage.removeItem("4029130");
                    localforage.removeItem("RPTCNSVPXCSPSE200");
                    localforage.removeItem("RPTCNSVPXCSPSE1K");
                    localforage.removeItem("MW2H0000000");
                    localforage.removeItem("4043769");
                    localforage.removeItem("4044402");
                    localforage.removeItem("RPTCNSVPXCSPEE10");
                    localforage.removeItem("4029134");
                    localforage.removeItem("RPTCNSVPXCSPEE200");
                    localforage.removeItem("RPTCNSVPXCSPEE1K");
                    localforage.removeItem("MW2H0000001");
                    localforage.removeItem("4044400");
                    localforage.removeItem("4044403");
                    localforage.removeItem("RPTCNSVPXCSPPE10");
                    localforage.removeItem("4029138");
                    localforage.removeItem("RPTCNSVPXCSPPE200");
                    localforage.removeItem("RPTCNSVPXCSPPE1K");
                    localforage.removeItem("MW2H0000002");
                    localforage.removeItem("4044401");
                    localforage.removeItem("4044404");
                    break;
                case '50Mbps-Standard':
                    localforage.setItem("4029130", AdcLicensingQty);
                    localforage.removeItem("RPTCNSVPXCSPSE10");
                    localforage.removeItem("RPTCNSVPXCSPSE200");
                    localforage.removeItem("RPTCNSVPXCSPSE1K");
                    localforage.removeItem("MW2H0000000");
                    localforage.removeItem("4043769");
                    localforage.removeItem("4044402");
                    localforage.removeItem("RPTCNSVPXCSPEE10");
                    localforage.removeItem("4029134");
                    localforage.removeItem("RPTCNSVPXCSPEE200");
                    localforage.removeItem("RPTCNSVPXCSPEE1K");
                    localforage.removeItem("MW2H0000001");
                    localforage.removeItem("4044400");
                    localforage.removeItem("4044403");
                    localforage.removeItem("RPTCNSVPXCSPPE10");
                    localforage.removeItem("4029138");
                    localforage.removeItem("RPTCNSVPXCSPPE200");
                    localforage.removeItem("RPTCNSVPXCSPPE1K");
                    localforage.removeItem("MW2H0000002");
                    localforage.removeItem("4044401");
                    localforage.removeItem("4044404");
                    break;
                case '200Mbps-Standard':
                    localforage.setItem("RPTCNSVPXCSPSE200", AdcLicensingQty);
                    localforage.removeItem("RPTCNSVPXCSPSE10");
                    localforage.removeItem("4029130");
                    localforage.removeItem("RPTCNSVPXCSPSE1K");
                    localforage.removeItem("MW2H0000000");
                    localforage.removeItem("4043769");
                    localforage.removeItem("4044402");
                    localforage.removeItem("RPTCNSVPXCSPEE10");
                    localforage.removeItem("4029134");
                    localforage.removeItem("RPTCNSVPXCSPEE200");
                    localforage.removeItem("RPTCNSVPXCSPEE1K");
                    localforage.removeItem("MW2H0000001");
                    localforage.removeItem("4044400");
                    localforage.removeItem("4044403");
                    localforage.removeItem("RPTCNSVPXCSPPE10");
                    localforage.removeItem("4029138");
                    localforage.removeItem("RPTCNSVPXCSPPE200");
                    localforage.removeItem("RPTCNSVPXCSPPE1K");
                    localforage.removeItem("MW2H0000002");
                    localforage.removeItem("4044401");
                    localforage.removeItem("4044404");
                    break;
                case '1000Mbps-Standard':
                    localforage.setItem("RPTCNSVPXCSPSE1K", AdcLicensingQty);
                    localforage.removeItem("RPTCNSVPXCSPSE10");
                    localforage.removeItem("4029130");
                    localforage.removeItem("RPTCNSVPXCSPSE200");
                    localforage.removeItem("MW2H0000000");
                    localforage.removeItem("4043769");
                    localforage.removeItem("4044402");
                    localforage.removeItem("RPTCNSVPXCSPEE10");
                    localforage.removeItem("4029134");
                    localforage.removeItem("RPTCNSVPXCSPEE200");
                    localforage.removeItem("RPTCNSVPXCSPEE1K");
                    localforage.removeItem("MW2H0000001");
                    localforage.removeItem("4044400");
                    localforage.removeItem("4044403");
                    localforage.removeItem("RPTCNSVPXCSPPE10");
                    localforage.removeItem("4029138");
                    localforage.removeItem("RPTCNSVPXCSPPE200");
                    localforage.removeItem("RPTCNSVPXCSPPE1K");
                    localforage.removeItem("MW2H0000002");
                    localforage.removeItem("4044401");
                    localforage.removeItem("4044404");
                    break;
                case '3000Mbps-Standard':
                    localforage.setItem("MW2H0000000", AdcLicensingQty);
                    localforage.removeItem("RPTCNSVPXCSPSE10");
                    localforage.removeItem("4029130");
                    localforage.removeItem("RPTCNSVPXCSPSE200");
                    localforage.removeItem("RPTCNSVPXCSPSE1K");
                    localforage.removeItem("4043769");
                    localforage.removeItem("4044402");
                    localforage.removeItem("RPTCNSVPXCSPEE10");
                    localforage.removeItem("4029134");
                    localforage.removeItem("RPTCNSVPXCSPEE200");
                    localforage.removeItem("RPTCNSVPXCSPEE1K");
                    localforage.removeItem("MW2H0000001");
                    localforage.removeItem("4044400");
                    localforage.removeItem("4044403");
                    localforage.removeItem("RPTCNSVPXCSPPE10");
                    localforage.removeItem("4029138");
                    localforage.removeItem("RPTCNSVPXCSPPE200");
                    localforage.removeItem("RPTCNSVPXCSPPE1K");
                    localforage.removeItem("MW2H0000002");
                    localforage.removeItem("4044401");
                    localforage.removeItem("4044404");
                    break;
                case '5000Mbps-Standard':
                    localforage.setItem("4043769", AdcLicensingQty);
                    localforage.removeItem("RPTCNSVPXCSPSE10");
                    localforage.removeItem("4029130");
                    localforage.removeItem("RPTCNSVPXCSPSE200");
                    localforage.removeItem("RPTCNSVPXCSPSE1K");
                    localforage.removeItem("MW2H0000000");
                    localforage.removeItem("4044402");
                    localforage.removeItem("RPTCNSVPXCSPEE10");
                    localforage.removeItem("4029134");
                    localforage.removeItem("RPTCNSVPXCSPEE200");
                    localforage.removeItem("RPTCNSVPXCSPEE1K");
                    localforage.removeItem("MW2H0000001");
                    localforage.removeItem("4044400");
                    localforage.removeItem("4044403");
                    localforage.removeItem("RPTCNSVPXCSPPE10");
                    localforage.removeItem("4029138");
                    localforage.removeItem("RPTCNSVPXCSPPE200");
                    localforage.removeItem("RPTCNSVPXCSPPE1K");
                    localforage.removeItem("MW2H0000002");
                    localforage.removeItem("4044401");
                    localforage.removeItem("4044404");
                    break;
                case '8000Mbps-Standard':
                    localforage.setItem("4044402", AdcLicensingQty);
                    localforage.removeItem("RPTCNSVPXCSPSE10");
                    localforage.removeItem("4029130");
                    localforage.removeItem("RPTCNSVPXCSPSE200");
                    localforage.removeItem("RPTCNSVPXCSPSE1K");
                    localforage.removeItem("MW2H0000000");
                    localforage.removeItem("4043769");
                    localforage.removeItem("RPTCNSVPXCSPEE10");
                    localforage.removeItem("4029134");
                    localforage.removeItem("RPTCNSVPXCSPEE200");
                    localforage.removeItem("RPTCNSVPXCSPEE1K");
                    localforage.removeItem("MW2H0000001");
                    localforage.removeItem("4044400");
                    localforage.removeItem("4044403");
                    localforage.removeItem("RPTCNSVPXCSPPE10");
                    localforage.removeItem("4029138");
                    localforage.removeItem("RPTCNSVPXCSPPE200");
                    localforage.removeItem("RPTCNSVPXCSPPE1K");
                    localforage.removeItem("MW2H0000002");
                    localforage.removeItem("4044401");
                    localforage.removeItem("4044404");
                    break;
                case '10Mbps-Advanced':
                    localforage.setItem("RPTCNSVPXCSPEE10", AdcLicensingQty);
                    localforage.removeItem("RPTCNSVPXCSPSE10");
                    localforage.removeItem("4029130");
                    localforage.removeItem("RPTCNSVPXCSPSE200");
                    localforage.removeItem("RPTCNSVPXCSPSE1K");
                    localforage.removeItem("MW2H0000000");
                    localforage.removeItem("4043769");
                    localforage.removeItem("4044402");
                    localforage.removeItem("4029134");
                    localforage.removeItem("RPTCNSVPXCSPEE200");
                    localforage.removeItem("RPTCNSVPXCSPEE1K");
                    localforage.removeItem("MW2H0000001");
                    localforage.removeItem("4044400");
                    localforage.removeItem("4044403");
                    localforage.removeItem("RPTCNSVPXCSPPE10");
                    localforage.removeItem("4029138");
                    localforage.removeItem("RPTCNSVPXCSPPE200");
                    localforage.removeItem("RPTCNSVPXCSPPE1K");
                    localforage.removeItem("MW2H0000002");
                    localforage.removeItem("4044401");
                    localforage.removeItem("4044404");
                    break;
                case '50Mbps-Advanced':
                    localforage.setItem("4029134", AdcLicensingQty);
                    localforage.removeItem("RPTCNSVPXCSPSE10");
                    localforage.removeItem("4029130");
                    localforage.removeItem("RPTCNSVPXCSPSE200");
                    localforage.removeItem("RPTCNSVPXCSPSE1K");
                    localforage.removeItem("MW2H0000000");
                    localforage.removeItem("4043769");
                    localforage.removeItem("4044402");
                    localforage.removeItem("RPTCNSVPXCSPEE10");
                    localforage.removeItem("RPTCNSVPXCSPEE200");
                    localforage.removeItem("RPTCNSVPXCSPEE1K");
                    localforage.removeItem("MW2H0000001");
                    localforage.removeItem("4044400");
                    localforage.removeItem("4044403");
                    localforage.removeItem("RPTCNSVPXCSPPE10");
                    localforage.removeItem("4029138");
                    localforage.removeItem("RPTCNSVPXCSPPE200");
                    localforage.removeItem("RPTCNSVPXCSPPE1K");
                    localforage.removeItem("MW2H0000002");
                    localforage.removeItem("4044401");
                    localforage.removeItem("4044404");
                    break;
                case '200Mbps-Advanced':
                    localforage.setItem("RPTCNSVPXCSPEE200", AdcLicensingQty);
                    localforage.removeItem("RPTCNSVPXCSPSE10");
                    localforage.removeItem("4029130");
                    localforage.removeItem("RPTCNSVPXCSPSE200");
                    localforage.removeItem("RPTCNSVPXCSPSE1K");
                    localforage.removeItem("MW2H0000000");
                    localforage.removeItem("4043769");
                    localforage.removeItem("4044402");
                    localforage.removeItem("RPTCNSVPXCSPEE10");
                    localforage.removeItem("4029134");
                    localforage.removeItem("RPTCNSVPXCSPEE1K");
                    localforage.removeItem("MW2H0000001");
                    localforage.removeItem("4044400");
                    localforage.removeItem("4044403");
                    localforage.removeItem("RPTCNSVPXCSPPE10");
                    localforage.removeItem("4029138");
                    localforage.removeItem("RPTCNSVPXCSPPE200");
                    localforage.removeItem("RPTCNSVPXCSPPE1K");
                    localforage.removeItem("MW2H0000002");
                    localforage.removeItem("4044401");
                    localforage.removeItem("4044404");
                    break;
                case '1000Mbps-Advanced':
                    localforage.setItem("RPTCNSVPXCSPEE1K", AdcLicensingQty);
                    localforage.removeItem("RPTCNSVPXCSPSE10");
                    localforage.removeItem("4029130");
                    localforage.removeItem("RPTCNSVPXCSPSE200");
                    localforage.removeItem("RPTCNSVPXCSPSE1K");
                    localforage.removeItem("MW2H0000000");
                    localforage.removeItem("4043769");
                    localforage.removeItem("4044402");
                    localforage.removeItem("RPTCNSVPXCSPEE10");
                    localforage.removeItem("4029134");
                    localforage.removeItem("RPTCNSVPXCSPEE200");
                    localforage.removeItem("MW2H0000001");
                    localforage.removeItem("4044400");
                    localforage.removeItem("4044403");
                    localforage.removeItem("RPTCNSVPXCSPPE10");
                    localforage.removeItem("4029138");
                    localforage.removeItem("RPTCNSVPXCSPPE200");
                    localforage.removeItem("RPTCNSVPXCSPPE1K");
                    localforage.removeItem("MW2H0000002");
                    localforage.removeItem("4044401");
                    localforage.removeItem("4044404");
                    break;
                case '3000Mbps-Advanced':
                    localforage.setItem("MW2H0000001", AdcLicensingQty);
                    localforage.removeItem("RPTCNSVPXCSPSE10");
                    localforage.removeItem("4029130");
                    localforage.removeItem("RPTCNSVPXCSPSE200");
                    localforage.removeItem("RPTCNSVPXCSPSE1K");
                    localforage.removeItem("MW2H0000000");
                    localforage.removeItem("4043769");
                    localforage.removeItem("4044402");
                    localforage.removeItem("RPTCNSVPXCSPEE10");
                    localforage.removeItem("4029134");
                    localforage.removeItem("RPTCNSVPXCSPEE200");
                    localforage.removeItem("RPTCNSVPXCSPEE1K");
                    localforage.removeItem("4044400");
                    localforage.removeItem("4044403");
                    localforage.removeItem("RPTCNSVPXCSPPE10");
                    localforage.removeItem("4029138");
                    localforage.removeItem("RPTCNSVPXCSPPE200");
                    localforage.removeItem("RPTCNSVPXCSPPE1K");
                    localforage.removeItem("MW2H0000002");
                    localforage.removeItem("4044401");
                    localforage.removeItem("4044404");
                    break;
                case '5000Mbps-Advanced':
                    localforage.setItem("4044400", AdcLicensingQty);
                    localforage.removeItem("RPTCNSVPXCSPSE10");
                    localforage.removeItem("4029130");
                    localforage.removeItem("RPTCNSVPXCSPSE200");
                    localforage.removeItem("RPTCNSVPXCSPSE1K");
                    localforage.removeItem("MW2H0000000");
                    localforage.removeItem("4043769");
                    localforage.removeItem("4044402");
                    localforage.removeItem("RPTCNSVPXCSPEE10");
                    localforage.removeItem("4029134");
                    localforage.removeItem("RPTCNSVPXCSPEE200");
                    localforage.removeItem("RPTCNSVPXCSPEE1K");
                    localforage.removeItem("MW2H0000001");
                    localforage.removeItem("4044403");
                    localforage.removeItem("RPTCNSVPXCSPPE10");
                    localforage.removeItem("4029138");
                    localforage.removeItem("RPTCNSVPXCSPPE200");
                    localforage.removeItem("RPTCNSVPXCSPPE1K");
                    localforage.removeItem("MW2H0000002");
                    localforage.removeItem("4044401");
                    localforage.removeItem("4044404");
                    break;
                case '8000Mbps-Advanced':
                    localforage.setItem("4044403", AdcLicensingQty);
                    localforage.removeItem("RPTCNSVPXCSPSE10");
                    localforage.removeItem("4029130");
                    localforage.removeItem("RPTCNSVPXCSPSE200");
                    localforage.removeItem("RPTCNSVPXCSPSE1K");
                    localforage.removeItem("MW2H0000000");
                    localforage.removeItem("4043769");
                    localforage.removeItem("4044402");
                    localforage.removeItem("RPTCNSVPXCSPEE10");
                    localforage.removeItem("4029134");
                    localforage.removeItem("RPTCNSVPXCSPEE200");
                    localforage.removeItem("RPTCNSVPXCSPEE1K");
                    localforage.removeItem("MW2H0000001");
                    localforage.removeItem("4044400");
                    localforage.removeItem("RPTCNSVPXCSPPE10");
                    localforage.removeItem("4029138");
                    localforage.removeItem("RPTCNSVPXCSPPE200");
                    localforage.removeItem("RPTCNSVPXCSPPE1K");
                    localforage.removeItem("MW2H0000002");
                    localforage.removeItem("4044401");
                    localforage.removeItem("4044404");
                    break;
                case '10Mbps-Premium':
                    localforage.setItem("RPTCNSVPXCSPPE10", AdcLicensingQty);
                    localforage.removeItem("RPTCNSVPXCSPSE10");
                    localforage.removeItem("4029130");
                    localforage.removeItem("RPTCNSVPXCSPSE200");
                    localforage.removeItem("RPTCNSVPXCSPSE1K");
                    localforage.removeItem("MW2H0000000");
                    localforage.removeItem("4043769");
                    localforage.removeItem("4044402");
                    localforage.removeItem("RPTCNSVPXCSPEE10");
                    localforage.removeItem("4029134");
                    localforage.removeItem("RPTCNSVPXCSPEE200");
                    localforage.removeItem("RPTCNSVPXCSPEE1K");
                    localforage.removeItem("MW2H0000001");
                    localforage.removeItem("4044400");
                    localforage.removeItem("4044403");
                    localforage.removeItem("4029138");
                    localforage.removeItem("RPTCNSVPXCSPPE200");
                    localforage.removeItem("RPTCNSVPXCSPPE1K");
                    localforage.removeItem("MW2H0000002");
                    localforage.removeItem("4044401");
                    localforage.removeItem("4044404");
                    break;
                case '50Mbps-Premium':
                    localforage.setItem("4029138", AdcLicensingQty);
                    localforage.removeItem("RPTCNSVPXCSPSE10");
                    localforage.removeItem("4029130");
                    localforage.removeItem("RPTCNSVPXCSPSE200");
                    localforage.removeItem("RPTCNSVPXCSPSE1K");
                    localforage.removeItem("MW2H0000000");
                    localforage.removeItem("4043769");
                    localforage.removeItem("4044402");
                    localforage.removeItem("RPTCNSVPXCSPEE10");
                    localforage.removeItem("4029134");
                    localforage.removeItem("RPTCNSVPXCSPEE200");
                    localforage.removeItem("RPTCNSVPXCSPEE1K");
                    localforage.removeItem("MW2H0000001");
                    localforage.removeItem("4044400");
                    localforage.removeItem("4044403");
                    localforage.removeItem("RPTCNSVPXCSPPE10");
                    localforage.removeItem("RPTCNSVPXCSPPE200");
                    localforage.removeItem("RPTCNSVPXCSPPE1K");
                    localforage.removeItem("MW2H0000002");
                    localforage.removeItem("4044401");
                    localforage.removeItem("4044404");
                    break;
                case '200Mbps-Premium':
                    localforage.setItem("RPTCNSVPXCSPPE200", AdcLicensingQty);
                    localforage.removeItem("RPTCNSVPXCSPSE10");
                    localforage.removeItem("4029130");
                    localforage.removeItem("RPTCNSVPXCSPSE200");
                    localforage.removeItem("RPTCNSVPXCSPSE1K");
                    localforage.removeItem("MW2H0000000");
                    localforage.removeItem("4043769");
                    localforage.removeItem("4044402");
                    localforage.removeItem("RPTCNSVPXCSPEE10");
                    localforage.removeItem("4029134");
                    localforage.removeItem("RPTCNSVPXCSPEE200");
                    localforage.removeItem("RPTCNSVPXCSPEE1K");
                    localforage.removeItem("MW2H0000001");
                    localforage.removeItem("4044400");
                    localforage.removeItem("4044403");
                    localforage.removeItem("RPTCNSVPXCSPPE10");
                    localforage.removeItem("4029138");
                    localforage.removeItem("RPTCNSVPXCSPPE1K");
                    localforage.removeItem("MW2H0000002");
                    localforage.removeItem("4044401");
                    localforage.removeItem("4044404");
                    break;
                case '1000Mbps-Premium':
                    localforage.setItem("RPTCNSVPXCSPPE1K", AdcLicensingQty);
                    localforage.removeItem("RPTCNSVPXCSPSE10");
                    localforage.removeItem("4029130");
                    localforage.removeItem("RPTCNSVPXCSPSE200");
                    localforage.removeItem("RPTCNSVPXCSPSE1K");
                    localforage.removeItem("MW2H0000000");
                    localforage.removeItem("4043769");
                    localforage.removeItem("4044402");
                    localforage.removeItem("RPTCNSVPXCSPEE10");
                    localforage.removeItem("4029134");
                    localforage.removeItem("RPTCNSVPXCSPEE200");
                    localforage.removeItem("RPTCNSVPXCSPEE1K");
                    localforage.removeItem("MW2H0000001");
                    localforage.removeItem("4044400");
                    localforage.removeItem("4044403");
                    localforage.removeItem("RPTCNSVPXCSPPE10");
                    localforage.removeItem("4029138");
                    localforage.removeItem("RPTCNSVPXCSPPE200");
                    localforage.removeItem("MW2H0000002");
                    localforage.removeItem("4044401");
                    localforage.removeItem("4044404");
                    break;
                case '3000Mbps-Premium':
                    localforage.setItem("MW2H0000002", AdcLicensingQty);
                    localforage.removeItem("RPTCNSVPXCSPSE10");
                    localforage.removeItem("4029130");
                    localforage.removeItem("RPTCNSVPXCSPSE200");
                    localforage.removeItem("RPTCNSVPXCSPSE1K");
                    localforage.removeItem("MW2H0000000");
                    localforage.removeItem("4043769");
                    localforage.removeItem("4044402");
                    localforage.removeItem("RPTCNSVPXCSPEE10");
                    localforage.removeItem("4029134");
                    localforage.removeItem("RPTCNSVPXCSPEE200");
                    localforage.removeItem("RPTCNSVPXCSPEE1K");
                    localforage.removeItem("MW2H0000001");
                    localforage.removeItem("4044400");
                    localforage.removeItem("4044403");
                    localforage.removeItem("RPTCNSVPXCSPPE10");
                    localforage.removeItem("4029138");
                    localforage.removeItem("RPTCNSVPXCSPPE200");
                    localforage.removeItem("RPTCNSVPXCSPPE1K");
                    localforage.removeItem("4044401");
                    localforage.removeItem("4044404");
                    break;
                case '5000Mbps-Premium':
                    localforage.setItem("4044401", AdcLicensingQty);
                    localforage.removeItem("RPTCNSVPXCSPSE10");
                    localforage.removeItem("4029130");
                    localforage.removeItem("RPTCNSVPXCSPSE200");
                    localforage.removeItem("RPTCNSVPXCSPSE1K");
                    localforage.removeItem("MW2H0000000");
                    localforage.removeItem("4043769");
                    localforage.removeItem("4044402");
                    localforage.removeItem("RPTCNSVPXCSPEE10");
                    localforage.removeItem("4029134");
                    localforage.removeItem("RPTCNSVPXCSPEE200");
                    localforage.removeItem("RPTCNSVPXCSPEE1K");
                    localforage.removeItem("MW2H0000001");
                    localforage.removeItem("4044400");
                    localforage.removeItem("4044403");
                    localforage.removeItem("RPTCNSVPXCSPPE10");
                    localforage.removeItem("4029138");
                    localforage.removeItem("RPTCNSVPXCSPPE200");
                    localforage.removeItem("RPTCNSVPXCSPPE1K");
                    localforage.removeItem("MW2H0000002");
                    localforage.removeItem("4044404");
                    break;
                case '8000Mbps-Premium':
                    localforage.setItem("4044404", AdcLicensingQty);
                    localforage.removeItem("RPTCNSVPXCSPSE10");
                    localforage.removeItem("4029130");
                    localforage.removeItem("RPTCNSVPXCSPSE200");
                    localforage.removeItem("RPTCNSVPXCSPSE1K");
                    localforage.removeItem("MW2H0000000");
                    localforage.removeItem("4043769");
                    localforage.removeItem("4044402");
                    localforage.removeItem("RPTCNSVPXCSPEE10");
                    localforage.removeItem("4029134");
                    localforage.removeItem("RPTCNSVPXCSPEE200");
                    localforage.removeItem("RPTCNSVPXCSPEE1K");
                    localforage.removeItem("MW2H0000001");
                    localforage.removeItem("4044400");
                    localforage.removeItem("4044403");
                    localforage.removeItem("RPTCNSVPXCSPPE10");
                    localforage.removeItem("4029138");
                    localforage.removeItem("RPTCNSVPXCSPPE200");
                    localforage.removeItem("RPTCNSVPXCSPPE1K");
                    localforage.removeItem("MW2H0000002");
                    localforage.removeItem("4044401");
                    break;
                default:
                    localforage.removeItem("RPTCNSVPXCSPSE10");
                    localforage.removeItem("4029130");
                    localforage.removeItem("RPTCNSVPXCSPSE200");
                    localforage.removeItem("RPTCNSVPXCSPSE1K");
                    localforage.removeItem("MW2H0000000");
                    localforage.removeItem("4043769");
                    localforage.removeItem("4044402");
                    localforage.removeItem("RPTCNSVPXCSPEE10");
                    localforage.removeItem("4029134");
                    localforage.removeItem("RPTCNSVPXCSPEE200");
                    localforage.removeItem("RPTCNSVPXCSPEE1K");
                    localforage.removeItem("MW2H0000001");
                    localforage.removeItem("4044400");
                    localforage.removeItem("4044403");
                    localforage.removeItem("RPTCNSVPXCSPPE10");
                    localforage.removeItem("4029138");
                    localforage.removeItem("RPTCNSVPXCSPPE200");
                    localforage.removeItem("RPTCNSVPXCSPPE1K");
                    localforage.removeItem("MW2H0000002");
                    localforage.removeItem("4044401");
                    localforage.removeItem("4044404");
            }
        } else {
            localforage.removeItem("RPTCNSVPXCSPSE10");
            localforage.removeItem("4029130");
            localforage.removeItem("RPTCNSVPXCSPSE200");
            localforage.removeItem("RPTCNSVPXCSPSE1K");
            localforage.removeItem("MW2H0000000");
            localforage.removeItem("4043769");
            localforage.removeItem("4044402");
            localforage.removeItem("RPTCNSVPXCSPEE10");
            localforage.removeItem("4029134");
            localforage.removeItem("RPTCNSVPXCSPEE200");
            localforage.removeItem("RPTCNSVPXCSPEE1K");
            localforage.removeItem("MW2H0000001");
            localforage.removeItem("4044400");
            localforage.removeItem("4044403");
            localforage.removeItem("RPTCNSVPXCSPPE10");
            localforage.removeItem("4029138");
            localforage.removeItem("RPTCNSVPXCSPPE200");
            localforage.removeItem("RPTCNSVPXCSPPE1K");
            localforage.removeItem("MW2H0000002");
            localforage.removeItem("4044401");
            localforage.removeItem("4044404");
        }
    }, [AdcLicensing, AdcLicensingQty, AdcLicensingType, AdcLicensingVersion])

    // useEffect that sets the MS-Citrix Master HSD sku in localForage
    useEffect(() => {
        if (HsdImages > 0) {
            localforage.setItem("MS-Citrix Master HSD", HsdImages);
        } else {
            localforage.removeItem("MS-Citrix Master HSD");
        }
    }, [HsdImages])

    // useEffect that sets the MS-Citrix Master VDI sku in localForage
    useEffect(() => {
        if (VdiImages > 0) {
            localforage.setItem("MS-Citrix Master VDI", VdiImages);
        } else {
            localforage.removeItem("MS-Citrix Master VDI");
        }
    }, [VdiImages])

    // useEffect that sets the Citrix License skus in localForage
    useEffect(() => {
        if (EucEnabled && EucType === "citrix" && CitrixLicenseType === "Customer") {
            localforage.setItem("Citrix License", EucUsers);
            localforage.removeItem("MS - 4067160");
            localforage.removeItem("MS - 4084654");
            localforage.removeItem("MS - 4082413");
            localforage.removeItem("MS - 4084650");
            localforage.removeItem("MS - 4070577");
        } else if (EucEnabled && EucType === "citrix" && CitrixLicenseType === "Premium") {
            localforage.setItem("MS - 4067160", EucUsers);
            localforage.removeItem("Citrix License");
            localforage.removeItem("MS - 4084654");
            localforage.removeItem("MS - 4082413");
            localforage.removeItem("MS - 4084650");
            localforage.removeItem("MS - 4070577");
        } else if (EucEnabled && EucType === "citrix" && CitrixLicenseType === "PremiumPlus") {
            localforage.setItem("MS - 4084654", EucUsers);
            localforage.removeItem("Citrix License");
            localforage.removeItem("MS - 4067160");
            localforage.removeItem("MS - 4082413");
            localforage.removeItem("MS - 4084650");
            localforage.removeItem("MS - 4070577");
        } else if (EucEnabled && EucType === "citrix" && CitrixLicenseType === "Advanced") {
            localforage.setItem("MS - 4082413", EucUsers);
            localforage.removeItem("Citrix License");
            localforage.removeItem("MS - 4067160");
            localforage.removeItem("MS - 4084654");
            localforage.removeItem("MS - 4084650");
            localforage.removeItem("MS - 4070577");
        } else if (EucEnabled && EucType === "citrix" && CitrixLicenseType === "AdvancedPlus") {
            localforage.setItem("MS - 4084650", EucUsers);
            localforage.removeItem("Citrix License");
            localforage.removeItem("MS - 4067160");
            localforage.removeItem("MS - 4084654");
            localforage.removeItem("MS - 4082413");
            localforage.removeItem("MS - 4070577");
        } else if (EucEnabled && EucType === "citrix" && CitrixLicenseType === "Azure") {
            localforage.setItem("MS - 4070577", EucUsers);
            localforage.removeItem("Citrix License");
            localforage.removeItem("MS - 4067160");
            localforage.removeItem("MS - 4084654");
            localforage.removeItem("MS - 4082413");
            localforage.removeItem("MS - 4084650");
        } else {
            localforage.removeItem("Citrix License");
            localforage.removeItem("MS - 4067160");
            localforage.removeItem("MS - 4084654");
            localforage.removeItem("MS - 4082413");
            localforage.removeItem("MS - 4084650");
            localforage.removeItem("MS - 4070577");
        }
    })

    // useEffect that sets the MS - Content Collaboration sku in localForage
    useEffect(() => {
        if (ContentCollaberation) {
            localforage.setItem("MS - Content Collaboration", ContentCollaberationCount);
        } else {
            localforage.removeItem("MS - Content Collaboration");
        }
    }, [ContentCollaberation, ContentCollaberationCount])

    // useEffect that sets the MS - Service Desk sku in localForage
    useEffect(() => {
        if (EucUsers > 0 || AvdUsers > 0) {
            localforage.setItem("MS - Service Desk", (EucUsers + AvdUsers));
        } else {
            localforage.removeItem("MS - Service Desk");
        }
    }, [EucUsers, AvdUsers])

    // useEffect that sets the MS - Service Desk Discount sku in localForage
    useEffect(() => {
        if ((EucUsers + AvdUsers) > 49) {
            localforage.setItem("MS - Service Desk Discount", (EucUsers + AvdUsers) - 49);
        } else {
            localforage.removeItem("MS - Service Desk Discount");
        }
    }, [EucUsers, AvdUsers])

    // useEffect that sets the MS - Azure VD Mgmt sku in localForage
    useEffect(() => {
        if (EucType2 === "avd") {
            localforage.setItem("MS - Azure VD Mgmt", AvdUsers);
        } else {
            localforage.removeItem("MS - Azure VD Mgmt");
        }
    }, [EucType2, AvdUsers])

    // useEffect that sets the MS - Logon Simulator sku in localForage
    useEffect(() => {
        if (EucEnabled && (EucType === "citrix" || EucType2 === "avd")) {
            localforage.setItem("MS - Logon Simulator", 1);
        } else {
            localforage.removeItem("MS - Logon Simulator");
        }
    }, [EucEnabled, EucType, EucType2])

    // useEffect that sets the MS - Nerdio sku in localForage
    useEffect(() => {
        if (EucEnabled && EucType2 === "avd" && Nerdio) {
            localforage.setItem("MS - Nerdio", AvdUsers);
        } else {
            localforage.removeItem("MS - Nerdio");
        }
    }, [EucEnabled, EucType2, Nerdio, AvdUsers])

    // useEffect that sets the MS - ControlUp sku in localForage
    useEffect(() => {
        if (EucUsers > 0 || AvdUsers > 0) {
            localforage.setItem("MS - ControlUp", (EucUsers + AvdUsers));
        } else {
            localforage.removeItem("MS - ControlUp");
        }
    }, [EucUsers, AvdUsers])

    // Setting skus from MS Services section in the Excel spreadsheet
    // useEffect that sets the MS - Unix/Linux Server sku in localForage
    useEffect(() => {
        if (UnixLinuxServerTotalQty > 0) {
            localforage.setItem("MS - Unix/Linux Server", UnixLinuxServerTotalQty);
        } else {
            localforage.removeItem("MS - Unix/Linux Server");
        }
    }, [UnixLinuxServerTotalQty])

    // useEffect that sets the MS - Unix/Linux Discount sku in localForage
    useEffect(() => {
        if (UnixLinuxServerTotalQty > 0 && Contract8x5or12x6 && Contract8x5or12x6Type === "8x5") {
            localforage.removeItem("MS - Unix/Linux Discount 12x6");
            localforage.setItem("MS - Unix/Linux Discount 8x5", UnixLinuxServerTotalQty);
        } else if (UnixLinuxServerTotalQty > 0 && Contract8x5or12x6 && Contract8x5or12x6Type === "12x6") {
            localforage.removeItem("MS - Unix/Linux Discount 8x5");
            localforage.setItem("MS - Unix/Linux Discount 12x6", UnixLinuxServerTotalQty);
        }
        else {
            localforage.removeItem("MS - Unix/Linux Discount 8x5");
            localforage.removeItem("MS - Unix/Linux Discount 12x6");
        }
    }, [UnixLinuxServerTotalQty, Contract8x5or12x6, Contract8x5or12x6Type])

    // useEffect that sets the MS - Windows Desktop sku in localForage
    useEffect(() => {
        if (DesktopPatchingEnabled) {
            localforage.setItem("MS - Windows Desktop", DesktopPatchingQuantity);
        } else {
            localforage.removeItem("MS - Windows Desktop");
        }
    }, [DesktopPatchingEnabled, DesktopPatchingQuantity])

    // useEffect that sets the MS - Windows Server Patching Only sku in localForage
    useEffect(() => {
        if (ServerManagementEnabled && ServerPatchingOnly) {
            localforage.setItem("MS - Windows Server Patching Only", ServerManagementQuantity);
        } else {
            localforage.removeItem("MS - Windows Server Patching Only");
        }
    }, [ServerManagementEnabled, ServerManagementQuantity, ServerPatchingOnly])

    // useEffect that sets the BU2506 sku in localForage
    useEffect(() => {
        if (RemoteDesktops) {
            localforage.setItem("BU2506", RemoteDesktopsQty);
        } else {
            localforage.removeItem("BU2506");
        }
    }, [RemoteDesktops, RemoteDesktopsQty])

    // useEffect that sets the MS - Windows Server sku in localForage
    useEffect(() => {
        if (MsWindowsServerTotalQty > 0) {
            localforage.setItem("MS - Windows Server", MsWindowsServerTotalQty);
        } else {
            localforage.removeItem("MS - Windows Server");
        }
    }, [MsWindowsServerTotalQty])

    // useEffect that sets the MS - Windows Discount sku in localForage
    useEffect(() => {
        if (MsWindowsServerTotalQty > 0 && Contract8x5or12x6 && Contract8x5or12x6Type === "8x5") {
            localforage.setItem("MS - Windows Discount 8x5", MsWindowsServerTotalQty);
            localforage.removeItem("MS - Windows Discount 12x6");
        } else if (MsWindowsServerTotalQty > 0 && Contract8x5or12x6 && Contract8x5or12x6Type === "12x6") {
            localforage.setItem("MS - Windows Discount 12x6", MsWindowsServerTotalQty);
            localforage.removeItem("MS - Windows Discount 8x5");
        }
        else {
            localforage.removeItem("MS - Windows Discount 8x5");
            localforage.removeItem("MS - Windows Discount 12x6");
        }
    }, [MsWindowsServerTotalQty, Contract8x5or12x6, Contract8x5or12x6Type])

    // useEffect that sets the MS - AD Standard/Advanced sku in localForage
    useEffect(() => {
        if (ActiveDirectoryManagement && ActiveDirectoryManagementOptions === "Standard") {
            localforage.setItem("MS - AD Standard", ActiveDirectoryManagementCount);
            localforage.removeItem("MS - AD Advanced");
        } else if (ActiveDirectoryManagement && ActiveDirectoryManagementOptions === "Advanced") {
            localforage.setItem("MS - AD Advanced", ActiveDirectoryManagementCount);
            localforage.removeItem("MS - AD Standard");
        } else {
            localforage.removeItem("MS - AD Standard");
            localforage.removeItem("MS - AD Advanced");
        }
    }, [ActiveDirectoryManagement, ActiveDirectoryManagementCount, ActiveDirectoryManagementOptions])

    // useEffect that sets the MS - AD User Discount sku in localForage
    useEffect(() => {
        if (ActiveDirectoryManagement && ActiveDirectoryManagementCount > 99 && ActiveDirectoryManagementOptions === "Standard") {
            localforage.setItem("MS - AD User Discount Standard", ActiveDirectoryManagementCount - 99);
            localforage.removeItem("MS - AD User Discount Advanced");
        } else if (ActiveDirectoryManagement && ActiveDirectoryManagementCount > 99 && ActiveDirectoryManagementOptions === "Advanced") {
            localforage.setItem("MS - AD User Discount Advanced", ActiveDirectoryManagementCount - 99);
            localforage.removeItem("MS - AD User Discount Standard");
        } else {
            localforage.removeItem("MS - AD User Discount Standard");
            localforage.removeItem("MS - AD User Discount Advanced");
        }
    }, [ActiveDirectoryManagement, ActiveDirectoryManagementCount, ActiveDirectoryManagementOptions])

    // useEffect that sets the MS - Windows SQL Svr Lic sku in localForage
    useEffect(() => {
        if (SqlServerLicenseServerTotalQty > 0) {
            localforage.setItem("MS - Windows SQL Svr Lic", SqlServerLicenseServerTotalQty);
        } else {
            localforage.removeItem("MS - Windows SQL Svr Lic");
        }
    }, [SqlServerLicenseServerTotalQty])

    // useEffect that sets the MS - Windows SQL Svr Ent sku in localForage
    useEffect(() => {
        if (SqlServerEnterpriseLicenseServerTotalQty > 0) {
            localforage.setItem("MS - Windows SQL Svr Ent", SqlServerEnterpriseLicenseServerTotalQty);
        } else {
            localforage.removeItem("MS - Windows SQL Svr Ent");
        }
    }, [SqlServerEnterpriseLicenseServerTotalQty])

    // useEffect that sets the MS - PrintManageAAS sku in localForage
    useEffect(() => {
        if (ManagedPrintServices) {
            localforage.setItem("MS - PrintManageAAS", ManagedPrintServicesLocations);
        } else {
            localforage.removeItem("MS - PrintManageAAS");
        }
    }, [ManagedPrintServices, ManagedPrintServicesLocations])

    // Setting skus from Security/Compliance section in the Excel spreadsheet
    // useEffect that sets the SECaaS-PP-ESS-M___-A skus in localForage
    useEffect(() => {
        if (EmailEnabled && EmailLicense === "Advanced") {
            localforage.setItem("SECaaS-PP-ESS-MADV-A", EmailProtection);
            localforage.removeItem("SECaaS-PP-ESS-MBEG-A");
            localforage.removeItem("SECaaS-PP-ESS-MBUS-A");
            localforage.removeItem("SECaaS-PP-ESS-MPRO-A");
        } else if (EmailEnabled && EmailLicense === "Beginner") {
            localforage.setItem("SECaaS-PP-ESS-MBEG-A", EmailProtection);
            localforage.removeItem("SECaaS-PP-ESS-MADV-A");
            localforage.removeItem("SECaaS-PP-ESS-MBUS-A");
            localforage.removeItem("SECaaS-PP-ESS-MPRO-A");
        } else if (EmailEnabled && EmailLicense === "Business") {
            localforage.setItem("SECaaS-PP-ESS-MBUS-A", EmailProtection);
            localforage.removeItem("SECaaS-PP-ESS-MADV-A");
            localforage.removeItem("SECaaS-PP-ESS-MBEG-A");
            localforage.removeItem("SECaaS-PP-ESS-MPRO-A");
        } else if (EmailEnabled && EmailLicense === "Professional") {
            localforage.setItem("SECaaS-PP-ESS-MPRO-A", EmailProtection);
            localforage.removeItem("SECaaS-PP-ESS-MADV-A");
            localforage.removeItem("SECaaS-PP-ESS-MBEG-A");
            localforage.removeItem("SECaaS-PP-ESS-MBUS-A");
        } else {
            localforage.removeItem("SECaaS-PP-ESS-MADV-A");
            localforage.removeItem("SECaaS-PP-ESS-MBEG-A");
            localforage.removeItem("SECaaS-PP-ESS-MBUS-A");
            localforage.removeItem("SECaaS-PP-ESS-MPRO-A");
        }
    }, [EmailEnabled, EmailLicense, EmailProtection])

    // useEffect that sets the PP-ESS-PSAT-A sku in localForage
    useEffect(() => {
        if (EmailEnabled && EmailTraining === "yes") {
            localforage.setItem("PP-ESS-PSAT-A", EmailProtection);
        } else {
            localforage.removeItem("PP-ESS-PSAT-A");
        }
    }, [EmailEnabled, EmailProtection, EmailTraining])

    // useEffect that sets the SECaaS - Risk Managment sku in localForage
    useEffect(() => {
        if (RiskManagement) {
            localforage.setItem("SECaaS - Risk Managment", RiskManagementCount);
        } else {
            localforage.removeItem("SECaaS - Risk Managment");
        }
    }, [RiskManagement, RiskManagementCount])

    // useEffect that sets the SECaaS - SIEM - Pro sku in localForage
    useEffect(() => {
        if (SiemEnabled) {
            localforage.setItem("SECaaS - SIEM - Pro", SiemCount);
        } else {
            localforage.removeItem("SECaaS - SIEM - Pro");
        }
    }, [SiemEnabled, SiemCount])

    // useEffect that sets the SECaaS - SIEM - App sku in localForage
    useEffect(() => {
        if (SiemEnabled && SiemSensorAppliance === "yes") {
            localforage.setItem("SECaaS- SIEM - App", SiemSensorApplianceCount);
        } else {
            localforage.removeItem("SECaaS- SIEM - App");
        }
    }, [SiemEnabled, SiemSensorAppliance, SiemSensorApplianceCount])

    // useEffect that sets the SECaas - NGFaaS sku in localForage
    useEffect(() => {
        if (NetworkFirewalls) {
            localforage.setItem("SECaas - NGFaaS", NetworkFirewallsQty);
        } else {
            localforage.removeItem("SECaas - NGFaaS");
        }
    }, [NetworkFirewalls, NetworkFirewallsQty])

    // useEffect that sets the SECaas - NGFaaS - Lic skus in localForage
    useEffect(() => {
        if (NetworkFirewalls && SecSeries === "SEC-50") {
            localforage.setItem("SECaas - NGFaaS - Lic SEC-50", PaloAltoLicenses);
            localforage.removeItem("SECaas - NGFaaS - Lic SEC-100");
            localforage.removeItem("SECaas - NGFaaS - Lic SEC-200");
            localforage.removeItem("SECaas - NGFaaS - Lic SEC-300");
            localforage.removeItem("SECaas - NGFaaS - Lic SEC-500");
            localforage.removeItem("SECaas - NGFaaS - Lic SEC-700");
            localforage.removeItem("SECaas - NGFaaS - Lic SEC-1000-HV");
        } else if (NetworkFirewalls && SecSeries === "SEC-100") {
            localforage.setItem("SECaas - NGFaaS - Lic SEC-100", PaloAltoLicenses);
            localforage.removeItem("SECaas - NGFaaS - Lic SEC-50");
            localforage.removeItem("SECaas - NGFaaS - Lic SEC-200");
            localforage.removeItem("SECaas - NGFaaS - Lic SEC-300");
            localforage.removeItem("SECaas - NGFaaS - Lic SEC-500");
            localforage.removeItem("SECaas - NGFaaS - Lic SEC-700");
            localforage.removeItem("SECaas - NGFaaS - Lic SEC-1000-HV");
        } else if (NetworkFirewalls && SecSeries === "SEC-200") {
            localforage.setItem("SECaas - NGFaaS - Lic SEC-200", PaloAltoLicenses);
            localforage.removeItem("SECaas - NGFaaS - Lic SEC-50");
            localforage.removeItem("SECaas - NGFaaS - Lic SEC-100");
            localforage.removeItem("SECaas - NGFaaS - Lic SEC-300");
            localforage.removeItem("SECaas - NGFaaS - Lic SEC-500");
            localforage.removeItem("SECaas - NGFaaS - Lic SEC-700");
            localforage.removeItem("SECaas - NGFaaS - Lic SEC-1000-HV");
        } else if (NetworkFirewalls && SecSeries === "SEC-300") {
            localforage.setItem("SECaas - NGFaaS - Lic SEC-300", PaloAltoLicenses);
            localforage.removeItem("SECaas - NGFaaS - Lic SEC-50");
            localforage.removeItem("SECaas - NGFaaS - Lic SEC-100");
            localforage.removeItem("SECaas - NGFaaS - Lic SEC-200");
            localforage.removeItem("SECaas - NGFaaS - Lic SEC-500");
            localforage.removeItem("SECaas - NGFaaS - Lic SEC-700");
            localforage.removeItem("SECaas - NGFaaS - Lic SEC-1000-HV");
        } else if (NetworkFirewalls && SecSeries === "SEC-500") {
            localforage.setItem("SECaas - NGFaaS - Lic SEC-500", PaloAltoLicenses);
            localforage.removeItem("SECaas - NGFaaS - Lic SEC-50");
            localforage.removeItem("SECaas - NGFaaS - Lic SEC-100");
            localforage.removeItem("SECaas - NGFaaS - Lic SEC-200");
            localforage.removeItem("SECaas - NGFaaS - Lic SEC-300");
            localforage.removeItem("SECaas - NGFaaS - Lic SEC-700");
            localforage.removeItem("SECaas - NGFaaS - Lic SEC-1000-HV");
        } else if (NetworkFirewalls && SecSeries === "SEC-700") {
            localforage.setItem("SECaas - NGFaaS - Lic SEC-700", PaloAltoLicenses);
            localforage.removeItem("SECaas - NGFaaS - Lic SEC-50");
            localforage.removeItem("SECaas - NGFaaS - Lic SEC-100");
            localforage.removeItem("SECaas - NGFaaS - Lic SEC-200");
            localforage.removeItem("SECaas - NGFaaS - Lic SEC-300");
            localforage.removeItem("SECaas - NGFaaS - Lic SEC-500");
            localforage.removeItem("SECaas - NGFaaS - Lic SEC-1000-HV");
        } else if (NetworkFirewalls && SecSeries === "SEC-1000-HV") {
            localforage.setItem("SECaas - NGFaaS - Lic SEC-1000-HV", PaloAltoLicenses);
            localforage.removeItem("SECaas - NGFaaS - Lic SEC-50");
            localforage.removeItem("SECaas - NGFaaS - Lic SEC-100");
            localforage.removeItem("SECaas - NGFaaS - Lic SEC-200");
            localforage.removeItem("SECaas - NGFaaS - Lic SEC-300");
            localforage.removeItem("SECaas - NGFaaS - Lic SEC-500");
            localforage.removeItem("SECaas - NGFaaS - Lic SEC-700");
        } else {
            localforage.removeItem("SECaas - NGFaaS - Lic SEC-50");
            localforage.removeItem("SECaas - NGFaaS - Lic SEC-100");
            localforage.removeItem("SECaas - NGFaaS - Lic SEC-200");
            localforage.removeItem("SECaas - NGFaaS - Lic SEC-300");
            localforage.removeItem("SECaas - NGFaaS - Lic SEC-500");
            localforage.removeItem("SECaas - NGFaaS - Lic SEC-700");
            localforage.removeItem("SECaas - NGFaaS - Lic SEC-1000-HV");
        }
    }, [NetworkFirewalls, PaloAltoLicenses, SecSeries])

    // useEffect that sets the SECaas - NGFaaS - Lic Options skus in localForage
    useEffect(() => {
        if (NetworkFirewalls && FirewallTP) {
            localforage.setItem("SECaas - NGFaaS  - Threat Prevention", PaloAltoLicenses);
        } else {
            localforage.removeItem("SECaas - NGFaaS  - Threat Prevention");
        }
    }, [FirewallTP, PaloAltoLicenses, NetworkFirewalls])

    useEffect(() => {
        if (NetworkFirewalls && FirewallWMA) {
            localforage.setItem("SECaas - NGFaaS  - Wildfire Malware Analysis", PaloAltoLicenses);
        } else {
            localforage.removeItem("SECaas - NGFaaS  - Wildfire Malware Analysis");
        }
    }, [FirewallWMA, PaloAltoLicenses, NetworkFirewalls])

    useEffect(() => {
        if (NetworkFirewalls && FirewallUrlFiltering) {
            localforage.setItem("SECaas - NGFaaS  - URL Filtering", PaloAltoLicenses);
        } else {
            localforage.removeItem("SECaas - NGFaaS  - URL Filtering");
        }
    }, [FirewallUrlFiltering, PaloAltoLicenses, NetworkFirewalls])

    useEffect(() => {
        if (NetworkFirewalls && FirewallDnsSecurity) {
            localforage.setItem("SECaas - NGFaaS  - DNS Security", PaloAltoLicenses);
        } else {
            localforage.removeItem("SECaas - NGFaaS  - DNS Security");
        }
    }, [FirewallDnsSecurity, PaloAltoLicenses, NetworkFirewalls])

    useEffect(() => {
        if (NetworkFirewalls && FirewallSdWan) {
            localforage.setItem("SECaas - NGFaaS  - SD-Wan", PaloAltoLicenses);
        } else {
            localforage.removeItem("SECaas - NGFaaS  - SD-Wan");
        }
    }, [FirewallSdWan, PaloAltoLicenses, NetworkFirewalls])

    useEffect(() => {
        if (NetworkFirewalls && FirewallGlobalProtect) {
            localforage.setItem("SECaas - NGFaaS  - Global Project", PaloAltoLicenses);
        } else {
            localforage.removeItem("SECaas - NGFaaS  - Global Project");
        }
    }, [FirewallGlobalProtect, PaloAltoLicenses, NetworkFirewalls])

    useEffect(() => {
        if (NetworkFirewalls && FirewallVPfM) {
            localforage.setItem("SECaas - NGFaaS  - Virtual Panorama for Management", PaloAltoLicenses);
        } else {
            localforage.removeItem("SECaas - NGFaaS  - Virtual Panorama for Management");
        }
    }, [FirewallVPfM, PaloAltoLicenses, NetworkFirewalls])

    // useEffect that sets the SECaaS - EPPAAS-MDR-Lic sku in localForage
    useEffect(() => {
        if (Huntress) {
            localforage.setItem("SECaaS - EPPAAS-MDR-Lic", HuntressCount);
        } else {
            localforage.removeItem("SECaaS - EPPAAS-MDR-Lic");
        }
    }, [Huntress, HuntressCount])

    // useEffect that sets the SECaaS - EPPAAS-EDR sku in localForage
    useEffect(() => {
        if (Huntress) {
            localforage.setItem("SECaaS - EPPAAS-EDR", HuntressCount);
        } else {
            localforage.removeItem("SECaaS - EPPAAS-EDR");
        }
    }, [Huntress, HuntressCount])

    // useEffect that sets the SECaaS - M365 - MDR - Lic sku in localForage
    useEffect(() => {
        if (Huntress && HuntressM365Support) {
            localforage.setItem("SECaaS - M365 - MDR - Lic", HuntressCount);
        } else {
            localforage.removeItem("SECaaS - M365 - MDR - Lic");
        }
    }, [Huntress, HuntressM365Support, HuntressCount])

    // useEffect that sets the SECaaS - ZeroTrust - Lic sku in localForage
    useEffect(() => {
        if (ZeroTrust) {
            localforage.setItem("SECaaS - ZeroTrust - Lic", ZeroTrustCount);
        } else {
            localforage.removeItem("SECaaS - ZeroTrust - Lic");
        }
    }, [ZeroTrust, ZeroTrustCount])

    // useEffect that sets the SECaaS - KEP sku in localForage
    useEffect(() => {
        if (KeeperEnabled) {
            localforage.setItem("SECaaS - KEP", KeeperInstances);
        } else {
            localforage.removeItem("SECaaS - KEP");
        }
    }, [KeeperEnabled, KeeperInstances])

    // Setting skus from Other section in the Excel spreadsheet
    // useEffect that sets the MS - O365 User Mgmt sku in localForage
    useEffect(() => {
        let m365TotalUsers = 0;
        if (Microsoft365BusBasic && Microsoft365BusBasicType === "UserManagement") {
            m365TotalUsers += Microsoft365BusBasicCount;
        }
        if (Microsoft365BusPremium && Microsoft365BusPremiumType === "UserManagement") {
            m365TotalUsers += Microsoft365BusPremiumCount;
        }
        if (Microsoft365BusStandard && Microsoft365BusStandardType === "UserManagement") {
            m365TotalUsers += Microsoft365BusStandardCount;
        }
        if (Microsoft365E3 && Microsoft365E3Type === "UserManagement") {
            m365TotalUsers += Microsoft365E3Count;
        }
        if (Microsoft365E5 && Microsoft365E5Type === "UserManagement") {
            m365TotalUsers += Microsoft365E5Count;
        }
        if (m365TotalUsers > 0) {
            localforage.setItem("MS - O365 User Mgmt", m365TotalUsers);
        }
    }, [Microsoft365BusBasic, Microsoft365BusBasicType, Microsoft365BusBasicCount,
        Microsoft365BusPremium, Microsoft365BusPremiumType, Microsoft365BusPremiumCount,
        Microsoft365BusStandard, Microsoft365BusStandardType, Microsoft365BusStandardCount,
        Microsoft365E3, Microsoft365E3Type, Microsoft365E3Count,
        Microsoft365E5, Microsoft365E5Type, Microsoft365E5Count]);  
        
    // useEffect that sets the MS - O365 Business Basic sku in localForage
    useEffect(() => {
        if (Microsoft365BusBasic) {
            localforage.setItem("MS - O365 Business Basic", Microsoft365BusBasicCount);
        } else {
            localforage.removeItem("MS - O365 Business Basic");
        }
    }, [Microsoft365BusBasic, Microsoft365BusBasicCount])

    // useEffect that sets the MS - O365 Business Premium sku in localForage
    useEffect(() => {
        if (Microsoft365BusPremium) {
            localforage.setItem("MS - O365 Business Premium", Microsoft365BusPremiumCount);
        } else {
            localforage.removeItem("MS - O365 Business Premium");
        }
    }, [Microsoft365BusPremium, Microsoft365BusPremiumCount])

    // useEffect that sets the MS - O365 Business Standard sku in localForage
    useEffect(() => {
        if (Microsoft365BusStandard) {
            localforage.setItem("MS - O365 Business Standard", Microsoft365BusStandardCount);
        } else {
            localforage.removeItem("MS - O365 Business Standard");
        }
    }, [Microsoft365BusStandard, Microsoft365BusStandardCount])

    // useEffect that sets the MS - O365 E3 sku in localForage
    useEffect(() => {
        if (Microsoft365E3) {
            localforage.setItem("MS - O365 E3", Microsoft365E3Count);
        } else {
            localforage.removeItem("MS - O365 E3");
        }
    }, [Microsoft365E3, Microsoft365E3Count])

    // useEffect that sets the MS - O365 E5 sku in localForage
    useEffect(() => {
        if (Microsoft365E5) {
            localforage.setItem("MS - O365 E5", Microsoft365E5Count);
        } else {
            localforage.removeItem("MS - O365 E5");
        }
    }, [Microsoft365E5, Microsoft365E5Count])

    // useEffect that sets the MS - StreamlineIT sku in localForage
    useEffect(() => {
        if (StreamlineIT) {
            localforage.setItem("MS - StreamlineIT", StreamlineITCount);
        } else {
            localforage.removeItem("MS - StreamlineIT");
        }
    }, [StreamlineIT, StreamlineITCount])



    // useEffects for calculating onboarding costs
    useEffect(() => {
        if(NeedsOnboarding && !SowRequired) {
            localforage.setItem("")
        }
    })
}

export default SkuSetter;