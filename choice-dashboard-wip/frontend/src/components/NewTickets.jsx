import { useState, useEffect } from "react";
import { DataGrid } from "@mui/x-data-grid";
import { Box, Typography } from "@mui/material";
import { Routes, Route, Link, useParams } from "react-router-dom";
import ContextMenu from "./ContextMenu";
import TicketDetails from "./TicketDetails";
import "../css/component/core.css";

/**
 * Fetches new ConnectWise tickets and displays them in a DataGrid.
 * Supports a context (right-click) menu.
 * @param {Object} parameters
 * @param {boolean} parameters.isDashboard Boolean to specify if the DataGrid
 *    is being rendered on the home page of the dashboard. When this is truthy,
 *    the table will have shorter pagination.
 */

export default function NewTickets({ isDashboard, isNewTickets, isTicketFollowUp, isCustomerResponded}) {
  const [rows, setRows] = useState([]);
  const [selectedTableData, setSelectedTableData] = useState([]);
  const [selectedRowIds, setSelectedRowIds] = useState([]);
  const [contextMenuPosition, setContextMenuPosition] = useState(null);

  const handleOptionSelected = (option) => {
    // Handle the selected option (e.g., edit, delete)
    console.log("Selected option:", option);
    setContextMenuPosition(null); // Close the context menu
  };

  const fetchData = async () => {
    try {
      let combinedData = [];

      if (isDashboard) {
        const response1 = await fetch("http://10.100.10.152:5000/cwapi/queries/cwNewTicket");
        const data1 = await response1.json();
        const response2 = await fetch("http://10.100.10.152:5000/cwapi/queries/cwCheckTicketFollowUp");
        const data2 = await response2.json();
        const response3 = await fetch("http://10.100.10.152:5000/cwapi/queries/cwCustomerResponded");
        const data3 = await response3.json();

        combinedData = [...data1, ...data2, ...data3];
      } else if (isTicketFollowUp) {
        const response = await fetch("http://10.100.10.152:5000/cwapi/queries/cwCheckTicketFollowUp");
        combinedData = await response.json();
      } else if (isNewTickets) {
        const response = await fetch("http://10.100.10.152:5000/cwapi/queries/cwNewTicket");
        combinedData = await response.json();
      } else if (isCustomerResponded) {
        const response = await fetch("http://10.100.10.152:5000/cwapi/queries/cwCustomerResponded");
        combinedData = await response.json();
      }

      setRows(combinedData);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  useEffect(() => {
    fetchData();
  }, [isDashboard, isNewTickets, isTicketFollowUp, isCustomerResponded]);


  // These reflect the fields returned by the API. Make sure
  // to keep matched with the API, such as if a field is renamed.
  const columns = [
    {
      field: "Rec_ID",
      headerName: "ID",
      width: 90,
      renderCell: (params) => (
        <Link to={`/${params.row.Rec_ID}`}>{params.row.Rec_ID}</Link>
      ),
    },
    {
      field: "Summary",
      headerName: "Summary",
      width: 700,
    },
    {
      field: "Board Name",
      headerName: "Board",
      width: 200,
    },
    {
      field: "Type",
      headerName: "Type",
      width: 100,
    },
    {
      field: "Status Description",
      headerName: "Status",
      width: 150,
    },
    {
      field: "Company Name",
      headerName: "Company",
      width: 290,
    },
  ];

  // We have to manually implement multi-selection functionality with a custom
  // selection model because the proper implementation is only available in
  // MUI Data Grid Pro.
   const handleSelectionModelChange = (newSelection) => {
    setSelectedRowIds(newSelection.selectionModel || []); // Handle undefined selectionModel
    setSelectedTableData(
      (newSelection.selectionModel || []).map((selectedId) =>
        rows.find((row) => row.id === selectedId)
      )
    );
  };

  const titleForPage = <Typography variant="h1" gutterBottom>New Tickets</Typography>

  return (
    <Box>
      <Typography variant="h1" gutterBottom>
        {isDashboard
          ? "New Tickets"
          : isNewTickets
            ? "New Tickets"
            : isTicketFollowUp
              ? "Ticket Follow-Up"
              : isCustomerResponded
                ? "Customer Responded"
                  : 
                  "Default Title"} </Typography>
      <Box
        onContextMenu={(event) => {
          event.preventDefault();
          setContextMenuPosition({ x: event.clientX, y: event.clientY });
        }}
      >
        <Routes>
          {/* Ticket Details Route */}
          <Route path="/" element={<DataGrid
            autoHeight
            rows={rows}
            columns={columns}
            classes={"ticketlist"}
            initialState={{
              pagination: {
                paginationModel: {
                  pageSize: isDashboard ? 4 : 15,
                },
              },
            }}
            pageSizeOptions={isDashboard ? [4, 8, 16, 25] : [15, 30, 50, 100]}
            getRowId={(row) => row.Rec_ID}
            checkboxSelection
            onSelectionModelChange={handleSelectionModelChange}
          />} />
          <Route path="/:ticketId" element={<TicketDetails rows={rows} />} />
        </Routes>
        {contextMenuPosition && (
          <ContextMenu
            position={contextMenuPosition}
            onOptionSelected={handleOptionSelected}
            selectedTableData={selectedTableData}
          />
        )}
      </Box>
    </Box>
  );
}