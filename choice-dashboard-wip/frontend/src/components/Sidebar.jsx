import { useState } from "react";
import { Link } from "react-router-dom";
import { Sidebar as ProSidebar, Menu, MenuItem, sidebarClasses } from "react-pro-sidebar";
import AlarmIcon from "@mui/icons-material/Alarm";
import FiberNewSharpIcon from "@mui/icons-material/FiberNewSharp";
import FormatListBulletedIcon from "@mui/icons-material/FormatListBulleted";
import HouseOutlinedIcon from "@mui/icons-material/HouseOutlined";
import LocalActivityIcon from "@mui/icons-material/LocalActivityOutlined";
import MenuIcon from "@mui/icons-material/MenuOutlined";
import ReportProblemIcon from "@mui/icons-material/ReportProblemOutlined";
import TagIcon from "@mui/icons-material/Tag";
import ChatOutlinedIcon from '@mui/icons-material/ChatOutlined';
import AssignmentIndOutlinedIcon from '@mui/icons-material/AssignmentIndOutlined';

/**
 * The collapsable sidebar, powered by react-pro-sidebar. Note that earlier
 * versions of react-pro-sidebar work completely differently than this version.
 * There is no need to import any styles.css file, for example.
 */
export default function Sidebar() {
  const [isCollapsed, setIsCollapsed] = useState(false);
  return (
    <ProSidebar collapsed={isCollapsed} rootStyles={{
      [`.${sidebarClasses.container}`]: {
        backgroundColor: "#f5f5f5"
      },
    }}>
      <Menu>
        <MenuItem onClick={() => setIsCollapsed(!isCollapsed)}
          icon={<MenuIcon />}></MenuItem>
        <MenuItem icon={<HouseOutlinedIcon color="primary" />}
          component={<Link to="/" />}> Home</MenuItem>
        <MenuItem icon={<TagIcon color="primary" />}
          component={<Link to="/skuTable" />}> SKU Table</MenuItem>
        <MenuItem icon={<FiberNewSharpIcon color="primary" />}
          component={<Link to="/newTickets" />}> New Tickets</MenuItem>
        <MenuItem icon={<ChatOutlinedIcon color="primary" />}
          component={<Link to="/ticketFollowUp" />}> Ticket Follow-Up</MenuItem>
        <MenuItem icon={<AssignmentIndOutlinedIcon color="primary" />}
          component={<Link to="/customerResponded" />}> Customer Responded</MenuItem>
        <MenuItem icon={<LocalActivityIcon color="primary" />}
          component={<Link to="/openTickets" />}> Open Tickets</MenuItem>
        <MenuItem icon={<AlarmIcon color="primary" />}
          component={<Link to="/slaTickets" />}> Sla Violated</MenuItem>
        <MenuItem icon={<ReportProblemIcon color="primary" />}
          component={<Link to="/zabbixProblems" />}> Zabbix Alerts</MenuItem>
        <MenuItem icon={<FormatListBulletedIcon color="primary" />}
          component={<Link to="/customerForm" />}> Customer Form</MenuItem>
      </Menu>
    </ProSidebar>
  );
}
