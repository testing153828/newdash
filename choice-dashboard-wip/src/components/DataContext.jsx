import { createContext, useContext, useState } from 'react';

const DataContext = createContext();

// Server configurations for various components, including quantity, CPU, memory, and storage settings.
// Each key represents a server type, and the associated values contain initial settings.
const serverConfigurations = {
    CitrixWeb: { Qty: 0, Vcpu: 2, TotalVcpu: 0, Mem: 8, TotalMem: 0, Tier1Storage: 100, TotalTier1Storage: 0, Tier2StorageTF: true, Tier2Storage: 0, Description: "Citrix Web", OS: "MS - Ctx - Infrastructure" },
    CitrixDdc: { Qty: 0, Vcpu: 2, TotalVcpu: 0, Mem: 8, TotalMem: 0, Tier1Storage: 100, TotalTier1Storage: 0, Tier2StorageTF: true, Tier2Storage: 0, Description: "Citrix DDC", OS: "MS - Ctx - Infrastructure" },
    CitrixCloudConnect: { Qty: 0, Vcpu: 2, TotalVcpu: 0, Mem: 8, TotalMem: 0, Tier1Storage: 100, TotalTier1Storage: 0, Tier2StorageTF: true, Tier2Storage: 0, Description: "Citrix Cloud Connect", OS: "MS - Ctx - Infrastructure" },
    MsActiveDirectory: { Qty: 0, Vcpu: 2, TotalVcpu: 0, Mem: 8, TotalMem: 0, Tier1Storage: 100, TotalTier1Storage: 0, Tier2StorageTF: true, Tier2Storage: 0, Description: "MS Active Directory", OS: "MS - Windows Server" },
    FileServerProfiles: { Qty: 0, Vcpu: 2, TotalVcpu: 0, Mem: 8, TotalMem: 0, Tier1Storage: 100, TotalTier1Storage: 0, Tier2StorageTF: true, Tier2Storage: 0, Description: "File Server (Profiles)", OS: "MS - Ctx - Infrastructure" },
    SqlExpress: { Qty: 0, Vcpu: 2, TotalVcpu: 0, Mem: 12, TotalMem: 0, Tier1Storage: 500, TotalTier1Storage: 0, Tier2StorageTF: true, Tier2Storage: 0, Description: "SQL Express", OS: "MS - Ctx - Infrastructure" },
    SqlServer: { Qty: 0, Vcpu: 4, TotalVcpu: 0, Mem: 24, TotalMem: 0, Tier1Storage: 500, TotalTier1Storage: 0, Tier2StorageTF: true, Tier2Storage: 0, Description: "SQL Server", OS: "MS - Ctx - Infrastructure" },
    MasterHsdImages: { Qty: 0, Vcpu: 2, TotalVcpu: 0, Mem: 8, TotalMem: 0, Tier1Storage: 300, TotalTier1Storage: 0, Tier2StorageTF: true, Tier2Storage: 0, Description: "Master HSD Image", OS: "MS - Ctx - Master Image" },
    WorkerServers: { Qty: 0, Vcpu: 4, TotalVcpu: 0, Mem: 24, TotalMem: 0, Tier1Storage: 300, TotalTier1Storage: 0, Tier2StorageTF: true, Tier2Storage: 0, Description: "Worker Servers", OS: "MS - Ctx - App Servers" },
    MasterVdiImages: { Qty: 0, Vcpu: 4, TotalVcpu: 0, Mem: 8, TotalMem: 0, Tier1Storage: 150, TotalTier1Storage: 0, Tier2StorageTF: true, Tier2Storage: 0, Description: "Master VDI Images", OS: "MS - Ctx - Master VDI" },
    VdiImages: { Qty: 0, Vcpu: 4, TotalVcpu: 0, Mem: 8, TotalMem: 0, Tier1Storage: 150, TotalTier1Storage: 0, Tier2StorageTF: false, Tier2Storage: 0, Description: "VDI Images", OS: "MS - Ctx - VDI" },
    Adc: { Qty: 0, Vcpu: 0, TotalVcpu: 0, Mem: 0, TotalMem: 0, Tier1Storage: 20, TotalTier1Storage: 0, Tier2StorageTF: false, Tier2Storage: 0, Description: "ADC", OS: "MS - Ctx - ADC" },
    LicenseServer: { Qty: 0, Vcpu: 2, TotalVcpu: 0, Mem: 8, TotalMem: 0, Tier1Storage: 100, TotalTier1Storage: 0, Tier2StorageTF: true, Tier2Storage: 0, Description: "License Server", OS: "MS - Ctx - Infrastrucure" },
    CitrixDirector: { Qty: 0, Vcpu: 2, TotalVcpu: 0, Mem: 8, TotalMem: 0, Tier1Storage: 100, TotalTier1Storage: 0, Tier2StorageTF: true, Tier2Storage: 0, Description: "Citrix Director", OS: "MS - Ctx - Infrastrucure" },
    SqlServerWoCitrix: { Qty: 0, Vcpu: 4, TotalVcpu: 0, Mem: 24, TotalMem: 0, Tier1Storage: 500, TotalTier1Storage: 0, Tier2StorageTF: true, Tier2Storage: 0, Description: "SQL Server w/o Citrix", OS: "MS - Windows Server" },
    SqlServerEnterpriseWoCitrix: { Qty: 0, Vcpu: 4, TotalVcpu: 0, Mem: 0, TotalMem: 0, Tier1Storage: 500, TotalTier1Storage: 0, Tier2StorageTF: true, Tier2Storage: 0, Description: "SQL Server Enterprise", OS: "MS - Windows Server" },
    AdcWoCitrix: { Qty: 0, Vcpu: 0, TotalVcpu: 0, Mem: 0, TotalMem: 0, Tier1Storage: 20, Tier2StorageTF: true, TotalTier1Storage: 0, Tier2Storage: 0, Description: "ADC w/o Citrix", OS: "MS - Ctx - ADC" },
    Firewall: { Qty: 0, Vcpu: 0, TotalVcpu: 0, Mem: 0, TotalMem: 0, Tier1Storage: 0, TotalTier1Storage: 0, Tier2StorageTF: false, Tier2Storage: 0, Description: "Firewall", OS: "MS - Firewall" },
};

/**
 * Generate the initial state for server configurations based on the defined serverConfigurations structure.
 * This code uses the reduce function to iterate over each server type,
 * extracting the initial values for quantity, CPU, memory, storage, etc.,
 * and combines them into a single object with keys formatted as `${server}${property}`,
 * creating a flat structure suitable for use with the useState hook in React.
 * @type {Object} InitialServerState
 */
const initialServerState = {};
Object.keys(serverConfigurations).forEach((server) => {
    const serverValues = serverConfigurations[server];
    Object.keys(serverValues).forEach((key) => {
        initialServerState[`${server}${key}`] = serverValues[key];
    });
});


export function DataProvider({ children }) {
    const [serverState, setServerState] = useState(initialServerState);
    const [ServerManagementEnabled, setServerManagementEnabled] = useState(false);
    const [DesktopPatchingEnabled, setDesktopPatchingEnabled] = useState(false);
    const [EucEnabled, setEucEnabled] = useState(true);
    const [BackupsEnabled, setBackupsEnabled] = useState(true);
    const [KeeperEnabled, setKeeperEnabled] = useState(false);
    const [EmailEnabled, setEmailEnabled] = useState(false);
    const [SiemEnabled, setSiemEnabled] = useState(false);
    const [EndpointProtectionEnabled, setEndpointProtectionEnabled] = useState(false);
    const [NetworkManagementEnabled, setNetworkManagementEnabled] = useState(false);
    const [DataCenterOnPremises, setDataCenterOnPremises] = useState(false);
    const [DataCenterChoiceCloud, setDataCenterChoiceCloud] = useState(false);
    const [DataCenterMicrosoftAzure, setDataCenterMicrosoftAzure] = useState(false);
    const [EucUsers, setEucUsers] = useState(0);
    const [HsdImages, setHsdImages] = useState(0);
    const [VdiImages, setVdiImages] = useState(0);
    const [EucType, setEucType] = useState(0);
    const [CitrixType, setCitrixType] = useState(0);
    const [ServerManagementQuantity, setServerManagementQuantity] = useState(0);
    const [DesktopPatchingQuantity, setDesktopPatchingQuantity] = useState(0);
    const [ServerPatchingOnly, setServerPatchingOnly] = useState(true);
    const [KeeperInstances, setKeeperInstances] = useState(0);
    const [EmailProtection, setEmailProtection] = useState(0);
    const [EmailTraining, setEmailTraining] = useState('');
    const [EmailLicense, setEmailLicense] = useState('');
    const [EndpointProtectionServers, setEndpointProtectionServers] = useState(0);
    const [EndpointProtectionDesktops, setEndpointProtectionDesktops] = useState(0);
    const [NetworkSwitches, setNetworkSwitches] = useState(0);
    const [NetworkFirewalls, setNetworkFirewalls] = useState(0);
    const [NetworkFirewallsQty, setNetworkFirewallsQty] = useState(0);
    const [PaloAltoLicenses, setPaloAltoLicenses] = useState(0);
    const [SecSeries, setSecSeries] = useState("NoneSelected");
    const [NetworkLoadBalancers, setNetworkLoadBalancers] = useState(0);
    const [adpEnabled, setAdpEnabled] = useState(true);
    const [adpRubricState, setadpRubricState] = useState('rent');
    const rubricOptions = [
        { label: 'Customer will rent the rubric', value: 'rent' },
        { label: 'Customer owns rubric', value: 'own' },
    ];
    const [adpTier, setAdpTier] = useState(1);
    const [adpBand, setAdpBand] = useState(0);
    const [adpNodeCount, setadpNodeCount] = useState(0);
    const [adpStorage, setAdpStorage] = useState(0);
    const [CCColoSpace, setCCColoSpace] = useState(false);
    const [RackUSpace, setRackUSpace] = useState(0);
    const [NPlus1Required, setNPlus1Required] = useState(false);
    const [SowRequired, setSowRequired] = useState(false);
    const [NeedsOnboarding, setNeedsOnboarding] = useState(false);
    const [BackupAsAServiceUnitrends, setBackupAsAServiceUnitrends] = useState(false);
    const [BackupTotalStorageUnitrends, setBackupTotalStorageUnitrends] = useState('');
    const [BackupProtectedDevicesUnitrends, setBackupProtectedDevicesUnitrends] = useState(0);
    const [CustBackupOnsiteDevicesUnitrends, setCustBackupOnsiteDevicesUnitrends] = useState('');
    const [BackupPrimaryLocationUnitrends, setBackupPrimaryLocationUnitrends] = useState('');
    const [BackupAlternateLocationUnitrends, setBackupAlternateLocationUnitrends] = useState('');
    const [M365Backups, setM365Backups] = useState(false);
    const [M365BackupsUsers, setM365BackupsUsers] = useState(0);
    const [M365BackupsType, setM365BackupsType] = useState('');
    const [CloudBackup, setCloudBackup] = useState(false);
    const [CloudBackupData, setCloudBackupData] = useState(0);
    const [CloudBackupType, setCloudBackupType] = useState('');
    const [BusinessContinuityDisasterRelief, setBusinessContinuityDisasterRelief] = useState(false);
    const [BCDRType, setBCDRType] = useState('');
    const [BCDRTarget, setBCDRTarget] = useState('');
    const [AssuredDP, setAssuredDP] = useState(false);
    const [AssuredDPData, setAssuredDPData] = useState(0);
    const [Adc, setAdc] = useState(true);
    const [AdcCitrix, setAdcCitrix] = useState(0);
    const [AdcCitrixCount, setAdcCitrixCount] = useState(0);
    const [AdcCitrixCpu, setAdcCitrixCpu] = useState(0);
    const [AdcCitrixMem, setAdcCitrixMem] = useState(0);
    const [LoadBalancerWoCitrix, setLoadBalancerWoCitrix] = useState(0);
    const [LoadBalancerWoCitrixCount, setLoadBalancerWoCitrixCount] = useState(0);
    const [LoadBalancerWoCitrixCpu, setLoadBalancerWoCitrixCpu] = useState(0);
    const [LoadBalancerWoCitrixMem, setLoadBalancerWoCitrixMem] = useState(0);
    const [CcTier1Storage, setCcTier1Storage] = useState(0);
    const [MsWindowsServerTotalQty, setMsWindowsServerTotalQty] = useState(0);
    const [HostedVdiServerTotalQty, setHostedVdiServerTotalQty] = useState(0);
    const [UnixLinuxServerTotalQty, setUnixLinuxServerTotalQty] = useState(0);
    const [NetworkAppServerTotalQty, setNetworkAppServerTotalQty] = useState(0);
    const [Tier1StorageServerTotalQty, setTier1StorageServerTotalQty] = useState(0);
    const [Tier2StorageServerTotalQty, setTier2StorageServerTotalQty] = useState(0);
    const [VcpuServerTotalQty, setVcpuServerTotalQty] = useState(0);
    const [VmemoryServerTotalQty, setVmemoryServerTotalQty] = useState(0);
    const [CitrixSqlServerTotalQty, setCitrixSqlServerTotalQty] = useState(0);
    const [SqlServerLicenseServerTotalQty, setSqlServerLicenseServerTotalQty] = useState(0);
    const [SqlServerEnterpriseLicenseServerTotalQty, setSqlServerEnterpriseLicenseServerTotalQty] = useState(0);
    const [CitrixInfrastructureServerTotalQty, setCitrixInfrastructureServerTotalQty] = useState(0);
    const [ServerTotalsVcpu, setServerTotalsVcpu] = useState(0);
    const [ServerTotalsMemory, setServerTotalsMemory] = useState(0);
    const [ServerTotalsTier1Storage, setServerTotalsTier1Storage] = useState(0);
    const [ServerTotalsTier2Storage, setServerTotalsTier2Storage] = useState(0);
    const [SystemInfrastructure, setSystemInfrastructure] = useState(true);
    const [Hypervisor, setHypervisor] = useState(0);
    const [HypervisorTotal, setHypervisorTotal] = useState(0);
    const [HypervisorManagement, setHypervisorManagement] = useState(0);
    const [HypervisorManagementType, setHypervisorManagementType] = useState('');
    const [AzureCost, setAzureCost] = useState(0);
    const [DevicesEnabled, setDevicesEnabled] = useState(true);
    const [AdditionalNetworkFirewalls, setAdditionalNetworkFirewalls] = useState(0);
    const [AdditionalNetworkFirewallsQty, setAdditionalNetworkFirewallsQty] = useState(0);
    const [ManagedNetworkSwitches, setManagedNetworkSwitches] = useState(0);
    const [ManagedNetworkSwitchesQty, setManagedNetworkSwitchesQty] = useState(0);
    const [NetworkWifi, setNetworkWifi] = useState(0);
    const [NetworkWifiQty, setNetworkWifiQty] = useState(0);
    const [AdcLicensing, setAdcLicensing] = useState(false);
    const [AdcLicensingQty, setAdcLicensingQty] = useState(0);
    const [AdcLicensingType, setAdcLicensingType] = useState('');
    const [AdcLicensingVersion, setAdcLicensingVersion] = useState('');
    const [AdcAdm, setAdcAdm] = useState(0);
    const [AdcAdmCount, setAdcAdmCount] = useState(0);
    const [AdcVserver, setAdcVserver] = useState(0);
    const [AdcVserverCount, setAdcVserverCount] = useState(0);
    const [CitrixLicense, setCitrixLicense] = useState(0);
    const [CitrixLicenseType, setCitrixLicenseType] = useState('');
    const [ContentCollaberation, setContentCollaberation] = useState(0);
    const [ContentCollaberationCount, setContentCollaberationCount] = useState(0);
    const [EucType2, setEucType2] = useState(0);
    const [Nerdio, setNerdio] = useState(0);
    const [AvdUsers, setAvdUsers] = useState(0);
    const [Contract8x5or12x6, setContract8x5or12x6] = useState(0);
    const [Contract8x5or12x6Type, setContract8x5or12x6Type] = useState('');
    const [ActiveDirectoryManagement, setActiveDirectoryManagement] = useState(0);
    const [ActiveDirectoryManagementOptions, setActiveDirectoryManagementOptions] = useState('');
    const [ActiveDirectoryManagementCount, setActiveDirectoryManagementCount] = useState(0);
    const [RemoteDesktops, setRemoteDesktops] = useState(0);
    const [RemoteDesktopsQty, setRemoteDesktopsQty] = useState(0);
    const [ManagedPrintServices, setManagedPrintServices] = useState(0);
    const [ManagedPrintServicesLocations, setManagedPrintServicesLocations] = useState(0);
    const [ManagedPrintServicesUsers, setManagedPrintServicesUsers] = useState(0);
    const [RiskManagement, setRiskManagement] = useState(0);
    const [RiskManagementCount, setRiskManagementCount] = useState(0);
    const [SiemCount, setSiemCount] = useState(0);
    const [SiemSensorAppliance, setSiemSensorAppliance] = useState('');
    const [SiemSensorApplianceCount, setSiemSensorApplianceCount] = useState(0);
    const [FirewallTP, setFirewallTP] = useState(0);
    const [FirewallWMA, setFirewallWMA] = useState(0);
    const [FirewallUrlFiltering, setFirewallUrlFiltering] = useState(0);
    const [FirewallDnsSecurity, setFirewallDnsSecurity] = useState(0);
    const [FirewallSdWan, setFirewallSdWan] = useState(0);
    const [FirewallGlobalProtect, setFirewallGlobalProtect] = useState(0);
    const [FirewallVPfM, setFirewallVPfM] = useState(0);
    const [Huntress, setHuntress] = useState(0);
    const [HuntressCount, setHuntressCount] = useState(0);
    const [HuntressM365Support, setHuntressM365Support] = useState(0);
    const [ZeroTrust, setZeroTrust] = useState(0);
    const [ZeroTrustCount, setZeroTrustCount] = useState(0);
    const [Microsoft365, setMicrosoft365] = useState(0);
    const [Microsoft365BusBasic, setMicrosoft365BusBasic] = useState(0);
    const [Microsoft365BusBasicCount, setMicrosoft365BusBasicCount] = useState(0);
    const [Microsoft365BusBasicType, setMicrosoft365BusBasicType] = useState('');
    const [Microsoft365BusPremium, setMicrosoft365BusPremium] = useState(0);
    const [Microsoft365BusPremiumCount, setMicrosoft365BusPremiumCount] = useState(0);
    const [Microsoft365BusPremiumType, setMicrosoft365BusPremiumType] = useState('');
    const [Microsoft365BusStandard, setMicrosoft365BusStandard] = useState(0);
    const [Microsoft365BusStandardCount, setMicrosoft365BusStandardCount] = useState(0);
    const [Microsoft365BusStandardType, setMicrosoft365BusStandardType] = useState('');
    const [Microsoft365E3, setMicrosoft365E3] = useState(0);
    const [Microsoft365E3Count, setMicrosoft365E3Count] = useState(0);
    const [Microsoft365E3Type, setMicrosoft365E3Type] = useState('');
    const [Microsoft365E5, setMicrosoft365E5] = useState(0);
    const [Microsoft365E5Count, setMicrosoft365E5Count] = useState(0);
    const [Microsoft365E5Type, setMicrosoft365E5Type] = useState('');
    const [StreamlineIT, setStreamlineIT] = useState(0);
    const [StreamlineITCount, setStreamlineITCount] = useState(0);
    const [TraditionalMspEnabled, setTraditionalMspEnabled] = useState(true);
    const [Microsoft365Enabled, setMicrosoft365Enabled] = useState(true);
    const [MiscellaneousEnabled, setMiscellaneousEnabled] = useState(true);
    const [newRows, setNewRows] = useState([]);

    const resetServerState = () => {
        setServerState(initialServerState);
        setServerState(initialServerState);
        setServerManagementEnabled(false);
        setDesktopPatchingEnabled(false);
        setEucEnabled(true);
        setBackupsEnabled(true);
        setKeeperEnabled(false);
        setEmailEnabled(false);
        setSiemEnabled(false);
        setEndpointProtectionEnabled(false);
        setNetworkManagementEnabled(false);
        setDataCenterOnPremises(false);
        setDataCenterChoiceCloud(false);
        setDataCenterMicrosoftAzure(false);
        setEucUsers(0);
        setHsdImages(0);
        setVdiImages(0);
        setEucType(0);
        setEucType2(0);
        setCitrixType(0);
        setServerManagementQuantity(0);
        setDesktopPatchingQuantity(0);
        setServerPatchingOnly(true);
        setKeeperInstances(0);
        setEmailProtection(0);
        setEmailTraining('');
        setEmailLicense('');
        setEndpointProtectionServers(0);
        setEndpointProtectionDesktops(0);
        setNetworkSwitches(0);
        setNetworkFirewalls(0);
        setNetworkFirewallsQty(2);
        setPaloAltoLicenses(2);
        setSecSeries("SEC-100");
        setNetworkLoadBalancers(0);
        setAdpEnabled(true);
        setadpRubricState('rent');
        setAdpTier(1);
        setAdpBand(0);
        setadpNodeCount(0);
        setAdpStorage(0);
        setCCColoSpace(false);
        setRackUSpace(0);
        setNPlus1Required(false);
        setBackupAsAServiceUnitrends(false);
        setBackupTotalStorageUnitrends('');
        setBackupProtectedDevicesUnitrends(0);
        setCustBackupOnsiteDevicesUnitrends('');
        setBackupPrimaryLocationUnitrends('');
        setBackupAlternateLocationUnitrends('');
        setM365Backups(0);
        setM365BackupsUsers(0);
        setM365BackupsType('');
        setCloudBackup(0);
        setCloudBackupData(0);
        setCloudBackupType('');
        setBusinessContinuityDisasterRelief(false);
        setBCDRType('');
        setBCDRTarget('');
        setAssuredDP(false);
        setAssuredDPData(0);
        setAdc(true);
        setAdcCitrix(0);
        setAdcCitrixCount(0);
        setAdcCitrixCpu(0);
        setAdcCitrixMem(0);
        setLoadBalancerWoCitrix(0);
        setLoadBalancerWoCitrixCount(0);
        setLoadBalancerWoCitrixCpu(0);
        setLoadBalancerWoCitrixMem(0);
        setCcTier1Storage(0);
        setMsWindowsServerTotalQty(0);
        setHostedVdiServerTotalQty(0);
        setUnixLinuxServerTotalQty(0);
        setNetworkAppServerTotalQty(0);
        setTier1StorageServerTotalQty(0);
        setTier2StorageServerTotalQty(0);
        setVcpuServerTotalQty(0);
        setVmemoryServerTotalQty(0);
        setCitrixSqlServerTotalQty(0);
        setSqlServerLicenseServerTotalQty(0);
        setSqlServerEnterpriseLicenseServerTotalQty(0);
        setCitrixInfrastructureServerTotalQty(0);
        setServerTotalsVcpu(0);
        setServerTotalsMemory(0);
        setServerTotalsTier1Storage(0);
        setServerTotalsTier2Storage(0);
        setSystemInfrastructure(true);
        setHypervisor(0);
        setHypervisorTotal(0);
        setHypervisorManagement(0);
        setHypervisorManagementType('');
        setAzureCost(0);
        setDevicesEnabled(true);
        setAdditionalNetworkFirewalls(0);
        setAdditionalNetworkFirewallsQty(0);
        setManagedNetworkSwitches(0);
        setManagedNetworkSwitchesQty(0);
        setNetworkWifi(0);
        setNetworkWifiQty(0);
        setAdcLicensing(false);
        setAdcLicensingQty(0);
        setAdcLicensingType('');
        setAdcLicensingVersion('');
        setAdcAdm(0);
        setAdcAdmCount(0);
        setAdcVserver(0);
        setAdcVserverCount(0);
        setCitrixLicense(0);
        setCitrixLicenseType('');
        setContentCollaberation(0);
        setContentCollaberationCount(0);
        setNerdio(0);
        setAvdUsers(0);
        setContract8x5or12x6(0);
        setContract8x5or12x6Type('');
        setActiveDirectoryManagement(0);
        setActiveDirectoryManagementOptions('');
        setActiveDirectoryManagementCount(0);
        setRemoteDesktops(0);
        setRemoteDesktopsQty(0);
        setManagedPrintServices(0);
        setManagedPrintServicesLocations(0);
        setManagedPrintServicesUsers(0);
        setRiskManagement(0);
        setRiskManagementCount(0);
        setSiemCount(0);
        setSiemSensorAppliance('');
        setSiemSensorApplianceCount(0);
        setFirewallTP(1);
        setFirewallWMA(1);
        setFirewallUrlFiltering(1);
        setFirewallDnsSecurity(0);
        setFirewallSdWan(0);
        setFirewallGlobalProtect(0);
        setFirewallVPfM(1);
        setHuntress(0);
        setHuntressCount(0);
        setHuntressM365Support(0);
        setZeroTrust(0);
        setZeroTrustCount(0);
        setMicrosoft365(0);
        setMicrosoft365BusBasic(0);
        setMicrosoft365BusBasicCount(0);
        setMicrosoft365BusBasicType('');
        setMicrosoft365BusPremium(0);
        setMicrosoft365BusPremiumCount(0);
        setMicrosoft365BusPremiumType('');
        setMicrosoft365BusStandard(0);
        setMicrosoft365BusStandardCount(0);
        setMicrosoft365BusStandardType('');
        setMicrosoft365E3(0);
        setMicrosoft365E3Count(0);
        setMicrosoft365E3Type('');
        setMicrosoft365E5(0);
        setMicrosoft365E3Count(0);
        setMicrosoft365E5Type('');
        setStreamlineIT(0);
        setStreamlineITCount(0);
        setTraditionalMspEnabled(true);
        setMicrosoft365Enabled(true);
        setMiscellaneousEnabled(true);
        setNeedsOnboarding(false);
        setSowRequired(false);
        setNewRows([]);
    };


    return (
        <DataContext.Provider value={{
            serverState, setServerState, ServerManagementEnabled, setServerManagementEnabled, DesktopPatchingEnabled, setDesktopPatchingEnabled,
            EucEnabled, setEucEnabled, BackupsEnabled, setBackupsEnabled, KeeperEnabled, setKeeperEnabled, EmailEnabled, setEmailEnabled,
            SiemEnabled, setSiemEnabled, EndpointProtectionEnabled, setEndpointProtectionEnabled, NetworkManagementEnabled, setNetworkManagementEnabled,
            DataCenterOnPremises, setDataCenterOnPremises, DataCenterChoiceCloud, setDataCenterChoiceCloud, DataCenterMicrosoftAzure, setDataCenterMicrosoftAzure,
            EucUsers, setEucUsers, HsdImages, setHsdImages, VdiImages, setVdiImages, EucType, setEucType, CitrixType, setCitrixType,
            ServerManagementQuantity, setServerManagementQuantity, DesktopPatchingQuantity, setDesktopPatchingQuantity, ServerPatchingOnly, setServerPatchingOnly,
            KeeperInstances, setKeeperInstances, EmailProtection, setEmailProtection, EndpointProtectionServers, setEndpointProtectionServers,
            EndpointProtectionDesktops, setEndpointProtectionDesktops, NetworkSwitches, setNetworkSwitches, NetworkFirewalls, setNetworkFirewalls,
            NetworkFirewallsQty, setNetworkFirewallsQty, PaloAltoLicenses, setPaloAltoLicenses, SecSeries, setSecSeries,
            NetworkLoadBalancers, setNetworkLoadBalancers, adpEnabled, setAdpEnabled, adpRubricState, setadpRubricState, rubricOptions,
            adpTier, setAdpTier, adpBand, setAdpBand, adpNodeCount, setadpNodeCount, adpStorage, setAdpStorage, CCColoSpace, setCCColoSpace,
            RackUSpace, setRackUSpace, NPlus1Required, setNPlus1Required, BackupAsAServiceUnitrends, setBackupAsAServiceUnitrends, BackupTotalStorageUnitrends, setBackupTotalStorageUnitrends, 
            BackupProtectedDevicesUnitrends, setBackupProtectedDevicesUnitrends, CustBackupOnsiteDevicesUnitrends, setCustBackupOnsiteDevicesUnitrends, BusinessContinuityDisasterRelief,setBusinessContinuityDisasterRelief,
            BackupPrimaryLocationUnitrends, setBackupPrimaryLocationUnitrends, BackupAlternateLocationUnitrends, setBackupAlternateLocationUnitrends, BCDRType, setBCDRType, BCDRTarget, setBCDRTarget, AssuredDP, setAssuredDP, AssuredDPData, setAssuredDPData,
            Adc, setAdc, AdcCitrix, setAdcCitrix, AdcCitrixCount, setAdcCitrixCount, AdcCitrixCpu, setAdcCitrixCpu, AdcCitrixMem, setAdcCitrixMem,
            LoadBalancerWoCitrix, setLoadBalancerWoCitrix, LoadBalancerWoCitrixCount, setLoadBalancerWoCitrixCount, LoadBalancerWoCitrixCpu, setLoadBalancerWoCitrixCpu,
            LoadBalancerWoCitrixMem, setLoadBalancerWoCitrixMem, CcTier1Storage, setCcTier1Storage, MsWindowsServerTotalQty, setMsWindowsServerTotalQty,
            HostedVdiServerTotalQty, setHostedVdiServerTotalQty, UnixLinuxServerTotalQty, setUnixLinuxServerTotalQty,
            NetworkAppServerTotalQty, setNetworkAppServerTotalQty, Tier1StorageServerTotalQty, setTier1StorageServerTotalQty,
            Tier2StorageServerTotalQty, setTier2StorageServerTotalQty, VcpuServerTotalQty, setVcpuServerTotalQty, VmemoryServerTotalQty, setVmemoryServerTotalQty,
            CitrixSqlServerTotalQty, setCitrixSqlServerTotalQty, SqlServerLicenseServerTotalQty, setSqlServerLicenseServerTotalQty,
            SqlServerEnterpriseLicenseServerTotalQty, setSqlServerEnterpriseLicenseServerTotalQty, CitrixInfrastructureServerTotalQty, setCitrixInfrastructureServerTotalQty,
            ServerTotalsVcpu, setServerTotalsVcpu, ServerTotalsMemory, setServerTotalsMemory, ServerTotalsTier1Storage, setServerTotalsTier1Storage, ServerTotalsTier2Storage, setServerTotalsTier2Storage, 
            M365Backups, setM365Backups, M365BackupsUsers, setM365BackupsUsers, M365BackupsType, setM365BackupsType, CloudBackup, setCloudBackup, CloudBackupData, setCloudBackupData,
            CloudBackupType, setCloudBackupType, SystemInfrastructure, setSystemInfrastructure, Hypervisor, setHypervisor, HypervisorTotal, setHypervisorTotal,
            HypervisorManagement, setHypervisorManagement, HypervisorManagementType, setHypervisorManagementType, AzureCost, setAzureCost, DevicesEnabled, setDevicesEnabled,
            AdditionalNetworkFirewalls, setAdditionalNetworkFirewalls, AdditionalNetworkFirewallsQty, setAdditionalNetworkFirewallsQty, ManagedNetworkSwitches, setManagedNetworkSwitches,
            ManagedNetworkSwitchesQty, setManagedNetworkSwitchesQty, NetworkWifi, setNetworkWifi, NetworkWifiQty, setNetworkWifiQty,
            AdcLicensing, setAdcLicensing, AdcLicensingQty, setAdcLicensingQty, AdcLicensingType, setAdcLicensingType, AdcLicensingVersion, setAdcLicensingVersion,
            AdcAdm, setAdcAdm, AdcAdmCount, setAdcAdmCount, AdcVserver, setAdcVserver, AdcVserverCount, setAdcVserverCount, CitrixLicense, setCitrixLicense,
            CitrixLicenseType, setCitrixLicenseType, ContentCollaberation, setContentCollaberation, ContentCollaberationCount, setContentCollaberationCount,
            EucType2, setEucType2, Nerdio, setNerdio, AvdUsers, setAvdUsers, Contract8x5or12x6, setContract8x5or12x6, Contract8x5or12x6Type, setContract8x5or12x6Type,
            ActiveDirectoryManagement, setActiveDirectoryManagement, ActiveDirectoryManagementOptions, setActiveDirectoryManagementOptions, ActiveDirectoryManagementCount, setActiveDirectoryManagementCount,
            RemoteDesktops, setRemoteDesktops, RemoteDesktopsQty, setRemoteDesktopsQty, ManagedPrintServices, setManagedPrintServices, ManagedPrintServicesLocations, setManagedPrintServicesLocations,
            ManagedPrintServicesUsers, setManagedPrintServicesUsers, EmailTraining, setEmailTraining, EmailLicense, setEmailLicense,
            RiskManagement, setRiskManagement, RiskManagementCount, setRiskManagementCount, SiemCount, setSiemCount, SiemSensorAppliance, setSiemSensorAppliance, SiemSensorApplianceCount, setSiemSensorApplianceCount,
            FirewallTP, setFirewallTP, FirewallWMA, setFirewallWMA, FirewallUrlFiltering, setFirewallUrlFiltering, FirewallDnsSecurity, setFirewallDnsSecurity,
            FirewallSdWan, setFirewallSdWan, FirewallGlobalProtect, setFirewallGlobalProtect, FirewallVPfM, setFirewallVPfM,
            Huntress, setHuntress, HuntressCount, setHuntressCount, HuntressM365Support, setHuntressM365Support, ZeroTrust, setZeroTrust, ZeroTrustCount, setZeroTrustCount,
            Microsoft365, Microsoft365BusBasic, setMicrosoft365BusBasic, Microsoft365BusBasicCount, setMicrosoft365BusBasicCount, Microsoft365BusBasicType, setMicrosoft365BusBasicType, 
            Microsoft365BusPremium, setMicrosoft365BusPremium, Microsoft365BusPremiumCount, setMicrosoft365BusPremiumCount, Microsoft365BusPremiumType, setMicrosoft365BusPremiumType, 
            Microsoft365BusStandard, setMicrosoft365BusStandard, Microsoft365BusStandardCount, setMicrosoft365BusStandardCount, Microsoft365BusStandardType, setMicrosoft365BusStandardType, 
            Microsoft365E3, setMicrosoft365E3, Microsoft365E3Count, setMicrosoft365E3Count, Microsoft365E3Type, setMicrosoft365E3Type, Microsoft365E5, setMicrosoft365E5, Microsoft365E5Count, setMicrosoft365E5Count, 
            Microsoft365E5Type, setMicrosoft365E5Type, StreamlineIT, setStreamlineIT, StreamlineITCount, setStreamlineITCount, TraditionalMspEnabled, setTraditionalMspEnabled,
            Microsoft365Enabled, setMicrosoft365Enabled, MiscellaneousEnabled, setMiscellaneousEnabled, NeedsOnboarding, setNeedsOnboarding, SowRequired, setSowRequired,
            newRows, setNewRows, resetServerState,
        }
        }>
            {children}
        </DataContext.Provider>
    );
}

export function useData() {
    return useContext(DataContext);
}