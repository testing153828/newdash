import React, { useEffect, useState } from "react";
import { Box, TextField, MenuItem, Button } from "@mui/material";
import { useData } from "./DataContext";
import "localforage";

function Rows({ children, ...rest }) {
    return <Box mb={6} {...rest}>{children}</Box>;
}

function Row({ children, ...rest }) {
    return (
        <Box
            display="flex"
            width="100%"
            py={0.75}
            borderBottom="1px solid grey"
            borderColor="grey.900"
            {...rest}
        >
            {children}
        </Box>
    );
}

function HeaderRow({ children, style, ...rest }) {
    return (
        <Row
            backgroundColor="#f5f5f5"
            fontSize={14}
            fontWeight="bold" // Add fontWeight property for bold text
            style={{ minWidth: "100px", ...style }}
            {...rest}
        >
            {children}
        </Row>
    );
}

function TotalRow({ children, style, ...rest }) {
    return (
        <Row
            backgroundColor="#f5f5f5"
            fontSize={14}
            fontWeight="bold" // Add fontWeight property for bold text
            style={{ minWidth: "100px", ...style }}
            {...rest}
        >
            {children}
        </Row>
    );
}

function Cell({ children, style, ...rest }) {
    return (
        <Box flex={1} px={1} style={{ minWidth: "100px", ...style }} {...rest}>
            {children}
        </Box>
    );
}

function StepTwo() {
    const {
        serverState, setServerState, ServerManagementEnabled, setServerManagementEnabled, DesktopPatchingEnabled, setDesktopPatchingEnabled,
        EucEnabled, setEucEnabled, BackupsEnabled, setBackupsEnabled, KeeperEnabled, setKeeperEnabled, EmailEnabled, setEmailEnabled,
        SiemEnabled, setSiemEnabled, EndpointProtectionEnabled, setEndpointProtectionEnabled, NetworkManagementEnabled, setNetworkManagementEnabled,
        DataCenterOnPremises, setDataCenterOnPremises, DataCenterChoiceCloud, setDataCenterChoiceCloud, DataCenterMicrosoftAzure, setDataCenterMicrosoftAzure,
        EucUsers, setEucUsers, HsdImages, setHsdImages, VdiImages, setVdiImages, EucType, setEucType, CitrixType, setCitrixType,
        ServerManagementQuantity, setServerManagementQuantity, DesktopPatchingQuantity, setDesktopPatchingQuantity, ServerPatchingOnly, setServerPatchingOnly,
        KeeperInstances, setKeeperInstances, EmailProtection, setEmailProtection, EndpointProtectionServers, setEndpointProtectionServers,
        EndpointProtectionDesktops, setEndpointProtectionDesktops, NetworkSwitches, setNetworkSwitches, NetworkFirewalls, setNetworkFirewalls,
        NetworkFirewallsQty, setNetworkFirewallsQty, PaloAltoLicenses, setPaloAltoLicenses, SecSeries, setSecSeries,
        NetworkLoadBalancers, setNetworkLoadBalancers, adpEnabled, setAdpEnabled, adpRubricState, setadpRubricState, rubricOptions,
        adpTier, setAdpTier, adpBand, setAdpBand, adpNodeCount, setadpNodeCount, adpStorage, setAdpStorage, CCColoSpace, setCCColoSpace,
        RackUSpace, setRackUSpace, NPlus1Required, setNPlus1Required, BackupAsAServiceUnitrends, setBackupAsAServiceUnitrends, BackupTotalStorageUnitrends, setBackupTotalStorageUnitrends,
        BackupProtectedDevicesUnitrends, setBackupProtectedDevicesUnitrends, CustBackupOnsiteDevicesUnitrends, setCustBackupOnsiteDevicesUnitrends, BusinessContinuityDisasterRelief, setBusinessContinuityDisasterRelief,
        BackupPrimaryLocationUnitrends, setBackupPrimaryLocationUnitrends, BackupAlternateLocationUnitrends, setBackupAlternateLocationUnitrends, BCDRType, setBCDRType, BCDRTarget, setBCDRTarget, AssuredDP, setAssuredDP, AssuredDPData, setAssuredDPData,
        Adc, setAdc, AdcCitrix, setAdcCitrix, AdcCitrixCount, setAdcCitrixCount, AdcCitrixCpu, setAdcCitrixCpu, AdcCitrixMem, setAdcCitrixMem,
        LoadBalancerWoCitrix, setLoadBalancerWoCitrix, LoadBalancerWoCitrixCount, setLoadBalancerWoCitrixCount, LoadBalancerWoCitrixCpu, setLoadBalancerWoCitrixCpu,
        LoadBalancerWoCitrixMem, setLoadBalancerWoCitrixMem, CcTier1Storage, setCcTier1Storage, MsWindowsServerTotalQty, setMsWindowsServerTotalQty,
        HostedVdiServerTotalQty, setHostedVdiServerTotalQty, UnixLinuxServerTotalQty, setUnixLinuxServerTotalQty,
        NetworkAppServerTotalQty, setNetworkAppServerTotalQty, Tier1StorageServerTotalQty, setTier1StorageServerTotalQty,
        Tier2StorageServerTotalQty, setTier2StorageServerTotalQty, VcpuServerTotalQty, setVcpuServerTotalQty, VmemoryServerTotalQty, setVmemoryServerTotalQty,
        CitrixSqlServerTotalQty, setCitrixSqlServerTotalQty, SqlServerLicenseServerTotalQty, setSqlServerLicenseServerTotalQty,
        SqlServerEnterpriseLicenseServerTotalQty, setSqlServerEnterpriseLicenseServerTotalQty, CitrixInfrastructureServerTotalQty, setCitrixInfrastructureServerTotalQty,
        ServerTotalsVcpu, setServerTotalsVcpu, ServerTotalsMemory, setServerTotalsMemory, ServerTotalsTier1Storage, setServerTotalsTier1Storage, ServerTotalsTier2Storage, setServerTotalsTier2Storage,
        M365Backups, setM365Backups, M365BackupsUsers, setM365BackupsUsers, M365BackupsType, setM365BackupsType, CloudBackup, setCloudBackup, CloudBackupData, setCloudBackupData,
        CloudBackupType, setCloudBackupType, SystemInfrastructure, setSystemInfrastructure, Hypervisor, setHypervisor, HypervisorTotal, setHypervisorTotal,
        HypervisorManagement, setHypervisorManagement, HypervisorManagementType, setHypervisorManagementType, AzureCost, setAzureCost, DevicesEnabled, setDevicesEnabled,
        AdditionalNetworkFirewalls, setAdditionalNetworkFirewalls, AdditionalNetworkFirewallsQty, setAdditionalNetworkFirewallsQty, ManagedNetworkSwitches, setManagedNetworkSwitches,
        ManagedNetworkSwitchesQty, setManagedNetworkSwitchesQty, NetworkWifi, setNetworkWifi, NetworkWifiQty, setNetworkWifiQty, newRows, setNewRows, resetServerState } = useData()
    const [editableQuantities, setEditableQuantities] = useState({});

    useEffect(() => {
        setServerState((prevState) => {
            const newState = { ...prevState };
            let updated = false;

            const configurationsOrder = [
                "CitrixWeb",
                "CitrixDdc",
                "CitrixCloudConnect",
                "MsActiveDirectory",
                "FileServerProfiles",
                "SqlExpress",
                "SqlServer",
                "MasterHsdImages",
                "WorkerServers",
                "MasterVdiImages",
                "VdiImages",
                "Adc",
                "LicenseServer",
                "CitrixDirector",
                "SqlServerWoCitrix",
                "SqlServerEnterpriseWoCitrix",
                "AdcWoCitrix",
                "Firewall",
            ];

            configurationsOrder.forEach((config) => {
                const qty = Number(editableQuantities[`${config}Qty`] || prevState[`${config}Qty`] || 0);
                const qtyKey = `${config}Qty`;
                const vcpu = prevState[`${config}Vcpu`] || 0;
                const totalVcpu = qty * vcpu;
                const totalVcpuKey = `${config}TotalVcpu`;
                const mem = prevState[`${config}Mem`] || 0;
                const totalMem = qty * mem;
                const totalMemKey = `${config}TotalMem`;
                const tier1Storage = prevState[`${config}Tier1Storage`] || 0;
                const totalTier1Storage = qty * tier1Storage;
                const totalTier1StorageKey = `${config}TotalTier1Storage`;

                const tier2StorageTF = prevState[`${config}Tier2StorageTF`] || false;
                const tier2Storage = tier2StorageTF ? totalTier1Storage * 3 : 0;
                const tier2StorageKey = `${config}Tier2Storage`;

                if (
                    newState[totalTier1StorageKey] !== totalTier1Storage ||
                    newState[totalVcpuKey] !== totalVcpu ||
                    newState[totalMemKey] !== totalMem ||
                    newState[tier2StorageKey] !== tier2Storage
                ) {
                    updated = true;
                }

                newState[totalTier1StorageKey] = totalTier1Storage;
                newState[totalVcpuKey] = totalVcpu;
                newState[totalMemKey] = totalMem;
                newState[tier2StorageKey] = tier2Storage;
                newState[qtyKey] = qty;
            });

            return updated ? newState : prevState;
        });
    }, [editableQuantities, setServerState]);

    const [newRowData, setNewRowData] = useState({
        CountsTowardsT2Storage: false,
        Qty: 0,
        Description: "Description",
        OS: "OS",
        Vcpu: 0,
        TotalVcpu: 0,
        Mem: 0,
        TotalMem: 0,
        Tier1Storage: 0,
        TotalTier1Storage: 0,
        Tier2StorageTF: false,
        Tier2Storage: 0,
    });

    const renderNewRows = () => (
        newRows.map((rowData, index) => (
            <Row key={index}>
                <Cell style={{ minWidth: "185px" }}>
                    <TextField
                        select
                        size="small"
                        value={rowData.CountsTowardsT2Storage ? "true" : "false"}
                        onChange={(event) => handleNewRowChange(index, 'CountsTowardsT2Storage', event.target.value)}
                    >
                        <MenuItem value="false">No</MenuItem>
                        <MenuItem value="true">Yes</MenuItem>
                    </TextField>
                </Cell>
                <Cell>
                    <TextField
                        variant="standard"
                        size="small"
                        sx={{ width: "60px" }}
                        type="number"
                        InputProps={{ inputProps: { min: 0 } }}
                        value={rowData.Qty}
                        onChange={(event) => handleNewRowChange(index, 'Qty', event.target.value)}
                    />
                </Cell>
                <Cell style={{ minWidth: "200px" }}>
                    <TextField
                        variant="standard"
                        size="small"
                        value={rowData.Description}
                        onChange={(event) => handleNewRowChange(index, 'Description', event.target.value)}
                    />
                </Cell>
                <Cell style={{ minWidth: "215px" }}>
                    <TextField
                        variant="standard"
                        size="small"
                        value={rowData.OS}
                        onChange={(event) => handleNewRowChange(index, 'OS', event.target.value)}
                    />
                </Cell>
                <Cell>
                    <TextField
                        variant="standard"
                        size="small"
                        type="number"
                        InputProps={{ inputProps: { min: 0 } }}
                        value={rowData.Vcpu}
                        onChange={(event) => handleNewRowChange(index, 'Vcpu', event.target.value)}
                    />
                </Cell>
                <Cell>{rowData.TotalVcpu}</Cell>
                <Cell>
                    <TextField
                        variant="standard"
                        size="small"
                        type="number"
                        InputProps={{ inputProps: { min: 0 } }}
                        value={rowData.Mem}
                        onChange={(event) => handleNewRowChange(index, 'Mem', event.target.value)}
                    />
                </Cell>
                <Cell>{rowData.TotalMem}</Cell>
                <Cell>
                    <TextField
                        variant="standard"
                        size="small"
                        type="number"
                        InputProps={{ inputProps: { min: 0 } }}
                        value={rowData.Tier1Storage}
                        onChange={(event) => handleNewRowChange(index, 'Tier1Storage', event.target.value)}
                    />
                </Cell>
                <Cell style={{ minWidth: "135px" }}>{rowData.TotalTier1Storage}</Cell>
                <Cell>{rowData.Tier2Storage}</Cell>
            </Row>
        ))
    );

    const handleAddRow = () => {
        // Add the current newRowData to the newRows array
        setNewRows((prevRows) => [...prevRows, newRowData]);

        // Reset newRowData for the next new row
        setNewRowData({
            CountsTowardsT2Storage: false,
            Qty: 0,
            Description: "Description",
            OS: "OS",
            Vcpu: 0,
            TotalVcpu: 0,
            Mem: 0,
            TotalMem: 0,
            Tier1Storage: 0,
            TotalTier1Storage: 0,
            Tier2StorageTF: false,
            Tier2Storage: 0,
        });
    };

    const handleNewRowChange = (index, field, newValue) => {
        setNewRows((prevRows) => {
            const updatedRows = [...prevRows];
            updatedRows[index][field] = field === 'CountsTowardsT2Storage' ? newValue === "true" : newValue;

            // Recalculate derived values when Qty, Vcpu, Mem, or Tier1Storage changes
            if (field === 'Qty' || field === 'Vcpu' || field === 'Mem' || field === 'Tier1Storage') {
                const qty = updatedRows[index]['Qty'];
                const vcpu = updatedRows[index]['Vcpu'];
                const mem = updatedRows[index]['Mem'];
                const tier1Storage = updatedRows[index]['Tier1Storage'];

                updatedRows[index]['TotalVcpu'] = qty * vcpu;
                updatedRows[index]['TotalMem'] = qty * mem;
                updatedRows[index]['TotalTier1Storage'] = qty * tier1Storage;
            }

            // Update Tier2Storage based on CountsTowardsT2Storage
            if (updatedRows[index]['CountsTowardsT2Storage']) {
                // Calculate Tier2Storage when CountsTowardsT2Storage is true
                const totalTier1Storage = updatedRows[index]['TotalTier1Storage'];
                updatedRows[index]['Tier2Storage'] = totalTier1Storage * 3;
            } else {
                // Set Tier2Storage to 0 when CountsTowardsT2Storage is false
                updatedRows[index]['Tier2Storage'] = 0;
            }

            return updatedRows;
        });
    };



    const configurationsOrder = [
        "CitrixWeb",
        "CitrixDdc",
        "CitrixCloudConnect",
        "MsActiveDirectory",
        "FileServerProfiles",
        "SqlExpress",
        "SqlServer",
        "MasterHsdImages",
        "WorkerServers",
        "MasterVdiImages",
        "VdiImages",
        "Adc",
        "LicenseServer",
        "CitrixDirector",
        "SqlServerWoCitrix",
        "SqlServerEnterpriseWoCitrix",
        "AdcWoCitrix",
        "Firewall",
    ];

    const headers = [
        "Counts Towards T2 Storage",
        "Qty",
        "Description",
        "OS",
        "vCPU",
        "Total vCPU",
        "Memory",
        "Total Memory",
        "Tier 1 Storage",
        "Total Tier 1 Storage",
        "Tier 2 Storage",
    ];

    const renderCell = (config, header) => {
        const headerMappings = {
            "Counts Towards T2 Storage": "Tier2StorageTF",
            Qty: "Qty",
            Description: "Description",
            OS: "OS",
            vCPU: "Vcpu",
            "Total vCPU": "TotalVcpu",
            Memory: "Mem",
            "Total Memory": "TotalMem",
            "Tier 1 Storage": "Tier1Storage",
            "Total Tier 1 Storage": "TotalTier1Storage",
            "Tier 2 Storage": "Tier2Storage",
        };

        const key = `${config}${headerMappings[header]}`;
        const value = serverState[key];

        // Handle boolean value for "Counts Towards T2 Storage"
        if (header === "Counts Towards T2 Storage") {
            return (
                <Cell key={`${config}-${header}`}
                    style={{
                        // Adjust the width for "Counts Towards T2 Storage" column
                        minWidth: "185px",
                    }}>
                    {value ? "Yes" : "No"}
                </Cell>
            );
        }

        if (header === "Qty") {
            return (
                <Cell key={`${config}-${header}`}>
                    <TextField
                        variant="standard"
                        size="small"
                        sx={{ width: "60%" }}
                        type="number"
                        InputProps={{ inputProps: { min: 0 } }}
                        value={editableQuantities[key] || value}
                        onChange={(event) => handleQtyChange(key, event.target.value)}
                    />
                </Cell>
            );
        }

        return (
            <Cell
                key={`${config}-${header}`}
                style={{
                    // Adjust the width for specific columns
                    ...(header === "Description" && { minWidth: "200px" }),
                    ...(header === "OS" && { minWidth: "215px" }),
                    ...(header === "Total Tier 1 Storage" && { minWidth: "135px" }),
                }}
            >
                {value}
            </Cell>
        );
    };

    const handleQtyChange = (key, newValue) => {
        setEditableQuantities((prevQuantities) => ({
            ...prevQuantities,
            [key]: newValue,
        }));
    };

    const calculateTotals = () => {
        let totalVcpu = 0;
        let totalMem = 0;
        let totalTier1Storage = 0;
        let totalTier2Storage = 0;

        configurationsOrder.forEach((config) => {
            totalVcpu += (serverState[`${config}TotalVcpu`] || 0);
            totalMem += (serverState[`${config}TotalMem`] || 0);
            totalTier1Storage += (serverState[`${config}TotalTier1Storage`] || 0);
            totalTier2Storage += (serverState[`${config}Tier2Storage`] || 0);
        });

        // Include values from new rows
        newRows.forEach((rowData) => {
            totalVcpu += (rowData['TotalVcpu'] || 0);
            totalMem += (rowData['TotalMem'] || 0);
            totalTier1Storage += (rowData['TotalTier1Storage'] || 0);
            totalTier2Storage += (rowData['Tier2Storage'] || 0);
        });

        setServerTotalsVcpu(totalVcpu);
        setServerTotalsMemory(totalMem);
        setServerTotalsTier1Storage(totalTier1Storage);
        setServerTotalsTier2Storage(totalTier2Storage);

        return {
            totalVcpu,
            totalMem,
            totalTier1Storage,
            totalTier2Storage,
        };
    };

    const totals = calculateTotals();

    // Calculating the SerTabForms values
    useEffect(() => {
        let msWindowsServerTotalQty = 0;
        let msUnixLinuxServerQty = 0;
        let ccHostedVdiServerTotalQty = 0;
        let msNetworkAppQty = 0;
        let msCitrixSqlQty = 0;
        let msSqlServerLicQty = 0;
        let msSqlServerEnterpriseLicQty = 0;
        let msCitrixInfrastructureQty = 0;

        configurationsOrder.forEach((config) => {
            const os = serverState[`${config}OS`] || "";
            const description = serverState[`${config}Description`] || "";
            const isWindowsServer = os === "MS - Windows Server";
            const isUnixLinuxServer = os === "MS - Unix/Linux Server";
            const isHostedVdi = os === "CC - Hosted VDI";
            const isNetworkApp = os === "MS - Network App";
            const isCitrixSql = os === "MS - Ctx - SQL";
            const isSqlServerLic = (description === "SQL Server" || description === "SQL Server w/o Citrix");
            const isSqlServerEnterpriseLic = description === "SQL Server Enterprise";
            const isCitrixInfrastructure = os === "MS - Ctx - Infrastructure";

            if (isWindowsServer) {
                const windowsServerQty = Number(serverState[`${config}Qty`]) || 0;
                msWindowsServerTotalQty += windowsServerQty;
            }
            if (isUnixLinuxServer) {
                const unixLinuxServerQty = Number(serverState[`${config}Qty`]) || 0;
                msUnixLinuxServerQty += unixLinuxServerQty;
            }
            if (isHostedVdi) {
                const vdiServerQty = Number(serverState[`${config}Qty`]) || 0;
                ccHostedVdiServerTotalQty += vdiServerQty;
            }
            if (isNetworkApp) {
                const networkAppQty = Number(serverState[`${config}Qty`]) || 0;
                msNetworkAppQty += networkAppQty;
            }
            if (isCitrixSql) {
                const citrixSqlQty = Number(serverState[`${config}TotalVcpu`]) || 0;
                msCitrixSqlQty += citrixSqlQty;
            }
            if (isSqlServerLic) {
                const sqlServerLicQty = Number(serverState[`${config}TotalVcpu`] / 2) || 0;
                msSqlServerLicQty += sqlServerLicQty;
            }
            if (isSqlServerEnterpriseLic) {
                const sqlServerEnterpriseLicQty = Number(serverState[`${config}TotalVcpu`] / 2) || 0;
                msSqlServerEnterpriseLicQty += sqlServerEnterpriseLicQty;
            }
            if (isCitrixInfrastructure) {
                const citrixInfrastructure = Number(serverState[`${config}Qty`]) || 0;
                msCitrixInfrastructureQty += citrixInfrastructure;
            }
        });

        newRows.forEach((rowData) => {
            const os = rowData['OS'] || "";
            const description = rowData['Description'] || "";
            const isWindowsServer = os === "MS - Windows Server";
            const isUnixLinuxServer = os === "MS - Unix/Linux Server";
            const isHostedVdi = os === "CC - Hosted VDI";
            const isNetworkApp = os === "MS - Network App";
            const isCitrixSql = os === "MS - Ctx - SQL";
            const isSqlServerLic = (description === "SQL Server" || description === "SQL Server w/o Citrix");
            const isSqlServerEnterpriseLic = description === "SQL Server Enterprise";
            const isCitrixInfrastructure = os === "MS - Ctx - Infrastructure";

            if (isWindowsServer) {
                const windowsServerQty = Number(rowData['Qty']) || 0;
                msWindowsServerTotalQty += windowsServerQty;
            }
            if (isUnixLinuxServer) {
                const unixLinuxServerQty = Number(rowData['Qty']) || 0;
                msUnixLinuxServerQty += unixLinuxServerQty;
            }
            if (isHostedVdi) {
                const vdiServerQty = Number(rowData['Qty']) || 0;
                ccHostedVdiServerTotalQty += vdiServerQty;
            }
            if (isNetworkApp) {
                const networkAppQty = Number(rowData['Qty']) || 0;
                msNetworkAppQty += networkAppQty;
            }
            if (isCitrixSql) {
                const citrixSqlQty = Number(rowData['TotalVcpu']) || 0;
                msCitrixSqlQty += citrixSqlQty;
            }
            if (isSqlServerLic) {
                const sqlServerLicQty = Number(Math.ceil(rowData['TotalVcpu'] / 2)) || 0;
                msSqlServerLicQty += sqlServerLicQty;
            }
            if (isSqlServerEnterpriseLic) {
                const sqlServerEnterpriseLicQty = Number(Math.ceil(rowData['TotalVcpu'] / 2)) || 0;
                msSqlServerEnterpriseLicQty += sqlServerEnterpriseLicQty;
            }
            if (isCitrixInfrastructure) {
                const citrixInfrastructure = Number(rowData['Qty']) || 0;
                msCitrixInfrastructureQty += citrixInfrastructure;
            }
        });

        setMsWindowsServerTotalQty(msWindowsServerTotalQty);
        setUnixLinuxServerTotalQty(msUnixLinuxServerQty)
        setHostedVdiServerTotalQty(ccHostedVdiServerTotalQty);
        setNetworkAppServerTotalQty(msNetworkAppQty);
        setCitrixSqlServerTotalQty(msCitrixSqlQty);
        setSqlServerLicenseServerTotalQty(msSqlServerLicQty);
        setSqlServerEnterpriseLicenseServerTotalQty(msSqlServerEnterpriseLicQty);
        setCitrixInfrastructureServerTotalQty(msCitrixInfrastructureQty);
    }, [serverState, setMsWindowsServerTotalQty, setUnixLinuxServerTotalQty, setHostedVdiServerTotalQty,
        setNetworkAppServerTotalQty, setCitrixSqlServerTotalQty, setSqlServerLicenseServerTotalQty,
        setSqlServerEnterpriseLicenseServerTotalQty, setCitrixInfrastructureServerTotalQty, configurationsOrder, newRows]);

    useEffect(() => {
        if (DataCenterChoiceCloud && VdiImages <= 0) {
            setTier1StorageServerTotalQty(totals.totalTier1Storage);
            setVcpuServerTotalQty(totals.totalVcpu);
            setVmemoryServerTotalQty(totals.totalMem);
        } else {
            setTier1StorageServerTotalQty(0);
            setVcpuServerTotalQty(0);
            setVmemoryServerTotalQty(0);
        }
    }, [DataCenterChoiceCloud, VdiImages, totals.totalTier1Storage, totals.totalVcpu, totals.totalMem])

    useEffect(() => {
        if (DataCenterChoiceCloud || BackupAsAServiceUnitrends) {
            setTier2StorageServerTotalQty(totals.totalTier2Storage);
        } else {
            setTier2StorageServerTotalQty(0);
        }
    }, [DataCenterChoiceCloud, BackupAsAServiceUnitrends, totals.totalTier2Storage])

    return (
        <Rows>
            <HeaderRow>
                {headers.map((header, index) => (
                    <Cell
                        key={index}
                        style={{
                            // Apply the same styles as body cells to header cells
                            ...(header === "Counts Towards T2 Storage" && { minWidth: "185px" }),
                            ...(header === "Description" && { minWidth: "200px" }),
                            ...(header === "OS" && { minWidth: "215px" }),
                            ...(header === "Total Tier 1 Storage" && { minWidth: "135px" }),
                        }}
                    >
                        {header}
                    </Cell>
                ))}
            </HeaderRow>
            {configurationsOrder.map((config, index) => (
                <Row key={index}>
                    {headers.map((header) => renderCell(config, header))}
                </Row>
            ))}
            {renderNewRows()}
            <TotalRow>
                <Cell style={{ minWidth: "185px" }}>Total</Cell>
                <Cell></Cell>
                <Cell style={{ minWidth: "200px" }}></Cell>
                <Cell style={{ minWidth: "215px" }}></Cell>
                <Cell></Cell>
                <Cell>{totals.totalVcpu}</Cell>
                <Cell></Cell>
                <Cell>{totals.totalMem}</Cell>
                <Cell></Cell>
                <Cell style={{ minWidth: "135px" }}>{totals.totalTier1Storage}</Cell>
                <Cell>{totals.totalTier2Storage}</Cell>
            </TotalRow>
            <Box mt={2}>
                <Button variant="outlined" onClick={handleAddRow}>
                    Add New Row
                </Button>
            </Box>
        </Rows>

    );
}

export default StepTwo;