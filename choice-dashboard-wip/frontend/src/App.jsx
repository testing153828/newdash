import { Routes, Route } from "react-router-dom";
import { Box } from "@mui/material";
import Home from "./components/Home";
import Sidebar from "./components/Sidebar";
import Topbar from "./components/Topbar";
import OpenTickets from "./components/OpenTickets";
import ZabbixProblems from "./components/ZabbixProblems";
import SlaTicket from "./components/SlaTicket";
import NewTickets from "./components/NewTickets";
import SkuTable from "./components/SkuTable";
import CustomerForm from "./components/CustomerForm";
import TicketDetails from "./components/TicketDetails";
import { DataProvider } from "./components/DataContext";


export default function App() {
  return (
    <DataProvider>
      <Box display="flex" className="content">
        <Sidebar />
        <Box flexGrow="1">
          <Topbar />
          <Box p={4}>
            <Routes>
              <Route path="/" element={<Home />} />
              <Route path="/skuTable" element={<SkuTable />} />
              <Route path="/openTickets" element={<OpenTickets />} />
              <Route path="/zabbixProblems" element={<ZabbixProblems isDashboard={false} />} />
              <Route path="/slaTickets" element={<SlaTicket isDashboard={false} />} />
              <Route path="/newTickets" element={<NewTickets isDashboard={false} isNewTickets={true} />} />
              <Route path="/ticketFollowUp" element={<NewTickets isDashboard={false} isTicketFollowUp={true} />} />
              <Route path="/customerResponded" element={<NewTickets isDashboard={false} isCustomerResponded={true} />} />
              <Route path="/:ticketId" element={<TicketDetails />} />
              <Route path="/customerForm" element={<CustomerForm />} />
            </Routes>
          </Box>
        </Box>
      </Box>
    </DataProvider>
  );
}
