import { useState, useEffect } from "react";
import { Box, Divider, Stack, Typography } from "@mui/material";
import { useData } from "./DataContext";
import localforage from "localforage";

/**
 * This is abstracted out in case I want to change how it is implemented, for
 * instance using `<tr>`. Currently this uses a horizontal flexbox.
 * @param {Object} parameters Any unknown parameters will be added directly to
 *      the root component.
 * @param {Array} parameters.children The cells. Auto-populated by JSX.
 * @param {boolean} parameters.hidden An optional boolean. If true, the row is
 *      invisible. Defaults to false.
 */
function Row({ children, hidden = false, ...rest }) {
    return <Box display={hidden ? "none" : "flex"} width="100%" py={0.75}
        borderBottom="1px solid grey" borderColor="grey.900" {...rest}>{children}</Box>;
}

/**
 * This subclasses `Row`, and it is abstracted out in case I want to change how
 * `Row` is implemented, for instance using `<tr>`. Make sure to keep all
 * children of `HeaderRow`s consistent with other `Row`s.
 */
function HeaderRow({ children, ...rest }) {
    return <Row backgroundColor="#f5f5f5" fontSize={12} {...rest}>{children}</Row>;
}

/**
 * This is abstracted out in case I want to change how it is implemented, for
 * instance using `<td>`. Currently this is a flex child.
 * @param {Object} parameters Any unknown parameters will be added directly to
 *      the root component.
 * @param {number} parameters.size The relative width / column span of this cell.
 * @param {Array} parameters.children The cell contents. Auto-populated by JSX.
 */
function Cell({ size, children, ...rest }) {
    return <Box flex={size} px={1} {...rest}>{children}</Box>;
}

/**
 * This is only referenced once, so the only reason this is its own component
 * is because it contains a state variable and an effect that reflect the IndexedDB.
 * @param {Object} parameters 
 * @param {Object} parameters.sku A Sku row returned by the API.
 */
function SkuRow({ sku, quantity }) {
    return (
        <Row>
            <Cell size={2}>{sku.sku}</Cell>
            <Cell size={3}>
                {sku.description} /
                <em>{sku.hardCost ? "Hard" : "Soft"}</em>
            </Cell>
            <Cell size={1}>
                ${sku.unitPrice} /
                ${sku.unitCost}</Cell>
            <Cell size={1} color="steelblue">
                ${(sku.unitPrice * quantity).toFixed(2)} /
                <em>${(sku.unitCost * quantity).toFixed(2)}</em>
            </Cell>
            <Cell size={"0 0 75px"}>{quantity}</Cell>
        </Row>
    );
}

/**
 * Step Four is similar to Step Three, but it lists every Sku row that has a
 * nonzero value in the IndexedDB. This is done by iterating again through the
 * fetched list of Sku Groups and querying the IndexedDB for each one.
 * 
 * Note that it makes no distinction between the cache key being missing and
 * being present but set to 0. My assumption is that this is no problem because
 * a summary view should never need to display a product included 0 times. This
 * step is designed to be read-only; any changes to the included products should
 * be done by returning to Step Three. This gives the steps distinct purposes and
 * makes this step easier to implement.
 * @param {Object} parameters 
 * @param {Object} parameters.groups An object, with keys containing the names
 *      of the groups, and values containing arrays of every Sku row.
 */
export default function StepFour() {
    const [customerCost, setCustomerCost] = useState(0);
    const [choiceCost, setChoiceCost] = useState(0);
    const [skuGroups, setSkuGroups] = useState([]);
    const { EmailEnabled, NeedsOnboarding, SowRequired, adpEnabled, adpStorage, skuConfig } = useData();
    const [onBoardingCustomerCost, setOnBoardingCustomerCost] = useState(0);
    const [onBoardingChoiceCost, setOnBoardingChoiceCost] = useState(0);

    const fetchGroups = async () => {
        const response = await fetch(`/db/sku_grouped`);
        const data = await response.json();
        return data;
    };

    useEffect(() => {
        (async () => {
            const groups = await fetchGroups();
            const newGroups = {};

            for (const [name, skus] of Object.entries(groups)) {
                const skusWithQuantities = [];

                for (const sku of skus) {
                    let skuKey = sku.sku; // Default to using the SKU name

                    // Use the unique key format for specific SKUs
                    if (skuConfig[sku.id]) {
                        skuKey = `${skuConfig[sku.id]}-${sku.id}`;
                    }

                    const quantity = await localforage.getItem(skuKey);
                    if (quantity && !sku.sku.includes("Reference")) {
                        skusWithQuantities.push([sku, quantity]);
                    }
                }

                if (skusWithQuantities.length) {
                    newGroups[name] = skusWithQuantities;
                }
            }

            console.log("New Groups:", newGroups);
            setSkuGroups(newGroups);

            const totalCustomerCost = Object.values(newGroups)
                .flat()
                .reduce((acc, [sku, quantity]) => acc + quantity * sku.unitPrice, 0);
            setCustomerCost(totalCustomerCost);

            const totalChoiceCost = Object.values(newGroups)
                .flat()
                .reduce((acc, [sku, quantity]) => acc + quantity * sku.unitCost, 0);
            setChoiceCost(totalChoiceCost);
        })();
    }, []);

    // Nested useEffect that will calculate the total on boarding price/cost
    useEffect(() => {
        let totalChoiceCost = 0;
        let totalCustomerCost = 0;
        if (NeedsOnboarding && !SowRequired) {
            totalChoiceCost += choiceCost;
            totalCustomerCost += customerCost;
        }
        if (NeedsOnboarding && adpEnabled && adpStorage > 0) {
            totalChoiceCost += 1500;
            totalCustomerCost += 2500;
        }
        if (EmailEnabled) {
            totalChoiceCost += 200;
            totalCustomerCost += 250;
        }
        setOnBoardingChoiceCost(totalChoiceCost);
        setOnBoardingCustomerCost(totalCustomerCost);
    }, [NeedsOnboarding, SowRequired, adpEnabled, EmailEnabled, adpStorage, choiceCost, customerCost]);

    return (
        <Stack>
            <Box sx={{ mb: 1 }}>
                <Typography variant="h1"><em>Monthly Summary</em></Typography>
            </Box>
            <Box sx={{ mb: 1 }}>
                {Object.entries(skuGroups).map(([name, skusQuantities]) => (
                    <Box key={name}>
                        <HeaderRow>
                            <Cell size={2}><b>{name}</b></Cell>
                            <Cell size={3}>Description / <em>Hard or Soft</em></Cell>
                            <Cell size={1}>Customer Price / <em>Choice Cost</em></Cell>
                            <Cell size={1}>Customer Total / <em>Choice Total</em></Cell>
                            <Cell size={"0 0 75px"}><em>Quantity</em></Cell>
                        </HeaderRow>
                        {skusQuantities.map(([sku, quantity]) => (
                            <SkuRow key={`${sku.sku}-${sku.id}`} sku={sku} quantity={quantity % 1 === 0 ? quantity : quantity.toFixed(1)} />
                        ))}
                    </Box>
                ))}
            </Box>
            <Box sx={{ mb: 2 }}>
                <Typography variant="h1"><em>Customer Cost:</em> ${customerCost.toFixed(2)} &mdash; <em>Choice Cost: </em>${choiceCost.toFixed(2)}</Typography>
            </Box>
            <Divider sx={{ mb: 1 }} />

            {/* Display the Onboarding Summary if onboarding conditions are met */}
            {((NeedsOnboarding) || (EmailEnabled) || (NeedsOnboarding && adpEnabled)) ? (
                <Box sx={{ mb: 1 }}>
                    <Typography variant="h1"><em>Onboarding Summary</em></Typography>
                    <HeaderRow>
                        <Cell size={2}>SKU</Cell>
                        <Cell size={3}>Description / <em>Hard or Soft</em></Cell>
                        <Cell size={1}>Customer Price / <em>Choice Cost</em></Cell>
                        <Cell size={1}>Customer Total / <em>Choice Total</em></Cell>
                        <Cell size={"0 0 75px"}><em>Quantity</em></Cell>
                    </HeaderRow>
                    {NeedsOnboarding ? (
                    <Row>
                        <Cell size={2}>MS - Onboarding Fee</Cell>
                        <Cell size={3}>Choice Solutions Onboarding Fee</Cell>
                        <Cell size={1}>${customerCost.toFixed(2)} / <em>${choiceCost.toFixed(2)}</em> </Cell>
                        <Cell size={1} color="steelblue">${customerCost.toFixed(2)} / <em>${choiceCost.toFixed(2)}</em> </Cell>
                        <Cell size={"0 0 75px"}>1</Cell>
                    </Row>
                    ) : null}
                    {EmailEnabled ? (
                    <Row>
                        <Cell size={2}>MS - Proofpoint Setup Fee</Cell>
                        <Cell size={3}>Choice Solutions Proofpoint Setup Fee</Cell>
                        <Cell size={1}>$250 / <em>$200</em> </Cell>
                        <Cell size={1} color="steelblue">$250 / <em>$200</em> </Cell>
                        <Cell size={"0 0 75px"}>1</Cell>
                    </Row>
                    ) : null}
                    {adpEnabled && adpStorage > 0 ? (
                    <Row>
                        <Cell size={2}>MS - Proofpoint Setup Fee</Cell>
                        <Cell size={3}>Rubrik Onboarding Fee</Cell>
                        <Cell size={1}>$2500 / <em>$1500</em> </Cell>
                        <Cell size={1} color="steelblue">$2500 / <em>$1500</em> </Cell>
                        <Cell size={"0 0 75px"}>1</Cell>
                    </Row>
                    ) : null}
                    <Box sx={{ mb: 2 }}>
                        <Typography variant="h1"><em>Customer Cost:</em> ${onBoardingCustomerCost.toFixed(2)} &mdash; <em>Choice Cost: </em>${onBoardingChoiceCost.toFixed(2)}</Typography>
                    </Box>
                </Box> 
            ) : null}
        </Stack>
    );
}