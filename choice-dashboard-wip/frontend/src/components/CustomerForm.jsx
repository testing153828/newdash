import React, { useState, useEffect, useRef } from "react";
import {
    Box, Button, Card, Chip, Divider, TextField, Typography, Stack,
    Dialog, DialogActions, DialogContent, DialogContentText, LinearProgress
} from "@mui/material";
import AddOutlinedIcon from "@mui/icons-material/AddOutlined";
import NavigateBeforeOutlinedIcon from "@mui/icons-material/NavigateBeforeOutlined";
import NavigateNextOutlinedIcon from "@mui/icons-material/NavigateNextOutlined";
import UndoOutlinedIcon from "@mui/icons-material/UndoOutlined";
import CheckIcon from "@mui/icons-material/Check";
//import * as user-friendly-date from "user-friendly-date";
import userFriendlyDate from "user-friendly-date";
import localforage from "localforage";
import StepOne from "./StepOne";
import StepTwo from "./StepTwo";
import StepThree from "./StepThree";
import StepFour from "./StepFour";
import { useData } from "./DataContext";
import SkuSetter from "./SkuSetter";

const length = await localforage.length();

function encapsulate(object_or_value) {
    return typeof object_or_value === "object" ? object_or_value : [object_or_value];
}

function StepSection({ step, number, heading = "", description = "", buttons = [], children, ...rest }) {
    return <>{step === number && <Box {...rest}>
        <Typography sx={{ mb: 1 }} variant="h1">{heading || `Step ${number}`}</Typography>
        <Typography sx={{ mb: 2 }}>{description}</Typography>
        <Stack sx={{ mb: 3 }} direction="row" spacing={1}>{buttons}</Stack>
        <Divider sx={{ mb: 3 }} />
        {children}
    </Box>}</>;
}

export default function CustomerForm() {
    const [id, setId] = useState(0);
    const [name, setName] = useState("");
    const [nameDialogStatus, setNameDialogStatus] = useState("closed");
    const [step, setStep] = useState(length ? 0 : 0);
    const [configurations, setConfigurations] = useState([]);
    const [groups, setGroups] = useState([]);
    const { resetServerState, serverState, fetchServerConfig, initialServerState, setConfigID, setConfigIndex, addNewRowToServerState, defaultState
    } = useData();
    const containerRef = useRef(null);

    async function fetchGroups() {
        const response = await fetch("/db/sku_grouped");
        const data = await response.json();
        setGroups(data);
        console.log(groups);
    }

    async function upload() {
        await fetch(`/db/configuration?id=${id}`, {
            method: id ? "PATCH" : "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify({
                title: name,
                quantities: {
                    ...Object.fromEntries(
                        await Promise.all(
                            Object.values(groups).flat().map(async (sku) =>
                                [sku.sku, await localforage.getItem(sku.sku) || 0]
                            )
                        )
                    ), _updated: new Date().getTime()
                }
            })
        });
    }

    const handleUpload = async () => {
        console.log("Server State:", serverState);
        const configurationData = {
            title: name,
            quantities: {
                ...Object.fromEntries(
                    await Promise.all(
                        Object.values(groups).flat().map(async (sku) =>
                            [sku.sku, await localforage.getItem(sku.sku) || 0]
                        )
                    )
                ), _updated: new Date().getTime()
            },
            serverState: serverState
        };

        console.log("Configuration Data:", configurationData); // Log the entire configuration data

        await fetch(`/db/configuration?id=${id}`, {
            method: id ? "PATCH" : "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify(configurationData)
        });

        await localforage.clear();
    };

    const clearState = async () => {
        await localforage.clear();
        resetServerState();
        setConfigID(null);
        setConfigIndex(null);
        setId(0);
    }

    useEffect(() => {
        const fetchConfigurations = async () => {
            const response = await fetch("/db/configuration");
            const data = await response.json();
            setConfigurations(data);
        };

        fetchConfigurations();
    }, []);

    useEffect(() => {
        const mainContent = document.getElementById("mainContent");
        if (mainContent) {
            mainContent.scrollTop = 0;
        }
    }, [step]);

    return (
        <Box>
            <StepSection step={step} number={0} description={`Click a version tag of a configuration below to view and edit the configuration at that point in time,\n
                 or click the Create New button to start a new configuration.`}
                buttons={[
                    <Button variant="outlined" startIcon={<AddOutlinedIcon />} key="create"
                        onClick={async () => {
                            defaultState();
                            setStep(1);
                        }}>Create</Button>
                ]}>
                <Box sx={{ display: "grid", gridTemplateColumns: "1fr 1fr", gap: 2 }}>
                    {configurations.map((configuration) => (
                        <Card sx={{ p: 3 }} key={configuration.id} variant="outlined">
                            <Typography sx={{ mb: 2 }} variant="h2"><em>#{configuration.id}</em> {configuration.title}</Typography>
                            <Box sx={{ display: "flex", flexWrap: "wrap", gap: 1 }}>
                                {(Array.from(encapsulate(configuration.quantities?._updated))).reverse().map((time, index) => (
                                    <Chip label={userFriendlyDate(time)} clickable key={index}
                                    onClick={async () => {
                                        try {
                                            await clearState();
                                            for (const [key, value] of Object.entries(configuration.quantities)) {
                                                if (!key.startsWith("_")) {
                                                    if (Array.isArray(value)) {
                                                        const value_reversed = value.slice().reverse();
                                                        if (value_reversed[index] > 0) {
                                                            await localforage.setItem(key, value_reversed[index]);
                                                        }
                                                    } else {
                                                        if (value > 0) {
                                                            await localforage.setItem(key, value);
                                                        }
                                                    }
                                                }
                                            }
                                            await fetchServerConfig(configuration.id, index);
                                            setId(configuration.id);
                                            setConfigID(configuration.id);
                                            setConfigIndex(index);
                                            setStep(1);
                                            console.log("Configuration state set successfully.");
                                        } catch (error) {
                                            console.error("Error loading configuration state:", error);
                                        }
                                    }}
                                     variant={index ? "filled" : "outlined"} />
                                ))}
                            </Box>
                        </Card>
                    ))}
                </Box>
            </StepSection>
            {/* Commented out code:
                This was a start of the Questionnaire for the sales engineers but will not be implemented for version 1
            */}
            {/* <StepSection step={step} number={1} description={`Architect Questionnaire that will help with pricing options`}
                buttons={[
                    <Button variant="outlined" startIcon={<UndoOutlinedIcon />} key="startOver"
                        onClick={async () => { await localforage.clear(); resetServerState(); setStep(0); }}>Start Over</Button>,
                    <Button variant="outlined" startIcon={<NavigateNextOutlinedIcon />} key="nextStep"
                        onClick={() => setStep(2)}>Next Step</Button>
                ]}>
                <Questionnaire />
            </StepSection> */}
            <StepSection step={step} number={1} description={`Click the icons of different products and features to add or remove them from this configuration`}
                buttons={[
                    <Button variant="outlined" startIcon={<UndoOutlinedIcon />} key="startOver"
                        onClick={async () => {
                            await clearState();
                            setStep(0);
                        }}>Start Over</Button>,
                    // <Button variant="outlined" startIcon={<NavigateBeforeOutlinedIcon />} key="previousStep"
                    // onClick={() => setStep(0)}>Previous Step</Button>,
                    <Button variant="outlined" startIcon={<NavigateNextOutlinedIcon />} key="nextStep"
                        onClick={() => setStep(2)}>Next Step</Button>
                ]}>
                <StepOne groups={groups} />
            </StepSection>
            <StepSection step={step} number={2} description={`Server Table: Quantities for the pre-loaded configurations are formula driven. You can also add a new row if needed.`}
                buttons={[
                    <Button variant="outlined" startIcon={<UndoOutlinedIcon />} key="startOver"
                        onClick={async () => {
                            await clearState();
                            setStep(0);
                        }}>Start Over</Button>,
                    <Button variant="outlined" startIcon={<NavigateBeforeOutlinedIcon />} key="previousStep"
                        onClick={() => setStep(1)}>Previous Step</Button>,
                    <Button variant="outlined" startIcon={<NavigateNextOutlinedIcon />} key="nextStep"
                        onClick={() => { setStep(3); }}>Next Step</Button>
                ]}>
                <StepTwo groups={groups} />
            </StepSection>
            <StepSection step={step} number={3} description={`Increase or decrease the quantities of products in this configuration.`}
                buttons={[
                    <Button variant="outlined" startIcon={<UndoOutlinedIcon />} key="startOver"
                        onClick={async () => {
                            await clearState();
                            setStep(0);
                        }}>Start Over</Button>,
                    <Button variant="outlined" startIcon={<NavigateBeforeOutlinedIcon />} key="previousStep"
                        onClick={() => setStep(2)}>Previous Step</Button>,
                    <Button variant="outlined" startIcon={<NavigateNextOutlinedIcon />} key="nextStep"
                        onClick={() => {
                            fetchGroups();
                            setStep(4);
                        }}>Next Step</Button>
                ]}>
                <StepThree groups={groups} />
            </StepSection>
            <SkuSetter />
            <StepSection step={step} number={4} description={`View all products included in this configuration.`}
                buttons={[
                    <Button variant="outlined" startIcon={<NavigateBeforeOutlinedIcon />} key="previousStep"
                        onClick={() => setStep(3)}>Previous Step</Button>,
                    <Button variant="outlined" color="success" startIcon={<CheckIcon />} key="upload"
                        onClick={() => setNameDialogStatus("open")}>Upload</Button>
                ]}>
                <Dialog open={nameDialogStatus !== "closed"} onClose={() => setNameDialogStatus("closed")}>
                    {nameDialogStatus === "loading" ? <>
                        <DialogContent>
                            <DialogContentText>Uploading...</DialogContentText>
                            <LinearProgress />
                        </DialogContent>
                    </> : nameDialogStatus === "done" ? <>
                        <DialogContent>
                            <DialogContentText>Uploaded successfully!</DialogContentText>
                            <DialogActions>
                                <Button variant="outlined" onClick={() => setNameDialogStatus("closed")}>Continue Editing</Button>
                                <Button variant="outlined" onClick={() => setStep(0)}>Go Home</Button>
                            </DialogActions>
                        </DialogContent>
                    </> : <>
                        <DialogContent>
                            <DialogContentText>Enter the name for the entire group of configurations. This does not set a name for this specific version.</DialogContentText>
                            <TextField id="name" label="Name" variant="standard" margin="dense" autoFocus fullWidth required
                                defaultValue={name} onChange={(event) => setName(event.target.value)} />
                        </DialogContent>
                        <DialogActions>
                            <Button variant="outlined" onClick={() => setNameDialogStatus("closed")}>Cancel</Button>
                            <Button variant="outlined" color="success" onClick={async () => {
                                setNameDialogStatus("loading");
                                await handleUpload();
                                setNameDialogStatus("done");
                            }}>Upload</Button>
                        </DialogActions>
                    </>}
                </Dialog>
                <StepFour groups={groups} />
            </StepSection>
        </Box>
    );
}