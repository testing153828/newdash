/*
Step One Notes:
---

- `Select` Data center question:
    * On-premises
    * Choice Cloud
    * Azure
- `Checkbox` Is EUC required? If yes,
    - `Select`
        * Citrix
        * AVD
        * Citrix on AVD
    - `Integer` How many golden images?
    - `Integer` How many users?
- `Checkbox` If Citrix is required **AND** and data center is (on-premises OR Choice Cloud),
    - `Select`
        * Citrix Cloud
        * Traditional (on-premises)
        * Hybrid (both)
- Traditional MSP Question:
    - `Checkbox` Server management
        - `Checkbox` Patching only (as opposed to fully managed)?
        - If yes,
            - `Integer` Quantity
    - `Checkbox` Desktop patching
        - If yes,
            - `Integer` Quantity
    - `Checkbox` Security
        - `Checkbox` Endpoint protection; **DEFAULT**: yes (because we are managing)
            - `Integer` Servers; **DEFAULT**: `users // 8 + MSP.server_management.quantity + (citrix.traditional: 6, citrix.hybrid: 4, citrix.citrix_cloud: 2)`
            - `Integer` Desktops; **DEFAULT**: `MSP.desktop_patching.quantity`
        - `Checkbox` SIEM as a service
        - `Checkbox` Email security
            - `Integer` Quantity; **DEFAULT**: `users`
        - `Checkbox` Keeper
            - `Integer` Instances; **DEFAULT**: `users`
    - `Checkbox` Network management
        - `Integer` Switches quantity
        - `Integer` Firewalls quantity
        - `Integer` Load balancers
- Backup ADB (BCDR):
    - `Select` Tier
        * Tier 1
        * Tier 2
        * Tier 3
    - `Slider` Storage; **RANGE**: 0-48GB
*/

import { useState, cloneElement, useEffect } from "react";
import {
    Box, Card, Checkbox, FormControl, FormControlLabel, FormLabel, InputLabel, MenuItem,
    Radio, RadioGroup, Select, Slider, Stack, TextField, Tooltip, Typography
} from "@mui/material";
import { useData } from "./DataContext";
import GridViewOutlined from "@mui/icons-material/GridViewOutlined";
import HouseOutlined from "@mui/icons-material/HouseOutlined";
import CloudOutlined from "@mui/icons-material/CloudOutlined";
import StorageOutlinedIcon from "@mui/icons-material/StorageOutlined";
import DesktopWindowsOutlinedIcon from "@mui/icons-material/DesktopWindowsOutlined";
import DeviceHubOutlinedIcon from '@mui/icons-material/DeviceHubOutlined';
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
import PasswordOutlinedIcon from "@mui/icons-material/PasswordOutlined";
import EmailOutlinedIcon from "@mui/icons-material/EmailOutlined";
import EmergencyShareOutlinedIcon from '@mui/icons-material/EmergencyShareOutlined';
import ExpandOutlinedIcon from "@mui/icons-material/ExpandOutlined";
import LanguageOutlinedIcon from "@mui/icons-material/LanguageOutlined";
import JoinFullOutlinedIcon from "@mui/icons-material/JoinFullOutlined";
import RadarOutlinedIcon from "@mui/icons-material/RadarOutlined";
import BackupOutlinedIcon from '@mui/icons-material/BackupOutlined';
import SnippetFolderOutlinedIcon from '@mui/icons-material/SnippetFolderOutlined';
import SecurityOutlinedIcon from '@mui/icons-material/SecurityOutlined';
import TrackChangesOutlinedIcon from '@mui/icons-material/TrackChangesOutlined';
import CastOutlinedIcon from '@mui/icons-material/CastOutlined';
import ImportantDevicesOutlinedIcon from '@mui/icons-material/ImportantDevicesOutlined';
import FireplaceOutlinedIcon from '@mui/icons-material/FireplaceOutlined';
import AddLinkOutlinedIcon from '@mui/icons-material/AddLinkOutlined';
import WifiOutlinedIcon from '@mui/icons-material/WifiOutlined';
import AssignmentOutlinedIcon from '@mui/icons-material/AssignmentOutlined';
import AccountTreeOutlinedIcon from '@mui/icons-material/AccountTreeOutlined';
import SettingsSystemDaydreamOutlinedIcon from '@mui/icons-material/SettingsSystemDaydreamOutlined';
import Groups2OutlinedIcon from '@mui/icons-material/Groups2Outlined';
import NumbersOutlinedIcon from '@mui/icons-material/NumbersOutlined';
import ReceiptLongOutlinedIcon from '@mui/icons-material/ReceiptLongOutlined';
import FolderSharedOutlinedIcon from '@mui/icons-material/FolderSharedOutlined';
import ConnectedTvOutlinedIcon from '@mui/icons-material/ConnectedTvOutlined';
import LocalPrintshopOutlinedIcon from '@mui/icons-material/LocalPrintshopOutlined';
import GppMaybeOutlinedIcon from '@mui/icons-material/GppMaybeOutlined';
import WaterOutlinedIcon from '@mui/icons-material/WaterOutlined';
import AddOutlinedIcon from '@mui/icons-material/AddOutlined';
import "localforage";

/**
 * A container for multiple cards.
 * Has several cool features that appear based on the parameters!
 * @param {Object} parameters Any unknown parameters will be added
 *      directly to the root component.
 * @param {string | null} parameters.title An optional heading for the section.
 * @param {Array} parameters.children The cards. Auto-populated by JSX.
 * @param {boolean} parameters.expanded A state variable or hardcoded value
 *      indicating if the section children are shown. Note that the heading
 *      is always visible. If not present, the section is always expanded.
 * @param {CallableFunction | null} parameters.setExpanded An optional
 *      state setter callable to toggle if the section children are shown.
        If present, a checkbox will be added next to the heading that
 *      toggles if the section is expanded. If not present,
        the section expansion cannot be changed.
 * @param {boolean} parameters.visible An optional boolean indicating if the
 *      entire section, both heading and children, are visible.
 *      If not present, the section is always visible.
 * @param {("horizontal" | "vertical")} parameters.direction A string
 *      indicating the direction of flow from the heading to the children.
 *      `"horizontal"` is the default and should be used for top-level
 *      sections, while `"vertical"` should be used for any section inside
 *      another `CardSection`.
 */
function CardSection({ title, children, expanded, setExpanded, visible, direction = "horizontal", ...rest }) {
    return <Box display={(visible === undefined || visible) ? "grid" : "none"} gap={2} gridColumn="span 3"
        gridTemplateColumns={direction == "horizontal" ? "300px 1fr" : "1fr"} {...rest}>
        <Box sx={{ display: "flex", flexDirection: "column", rowGap: 1 }}>
            {title &&
                <Typography variant="h4">
                    {setExpanded !== undefined &&
                        <Checkbox sx={{ m: "-9px -5px -5px -9px" }}
                            defaultChecked={expanded}
                            onChange={() => setExpanded(!expanded)} />} {title}
                </Typography>}
        </Box>
        <Box display={(expanded === undefined || expanded) ? "grid" : "none"}
            gridTemplateColumns="repeat(3, 1fr)" gap={2}>{children}</Box>
    </Box>;
}

/**
 * A `TextField` that only accepts numbers and is designed to reflect and
 * update a numerical state value.
 * @param {Object} parameters Any unknown parameters will be added
 *      directly to the root component.
 * @param {string} parameters.label The name of the field.
 * @param {number} parameters.state A state variable or hardcoded value
 *      to fill the field with on render.
 * @param {CallableFunction} parameters.setState A state setter callable
 *      to call whenever the field is changed.
 */
function NumberField({ label, visible = true, state = 0, setState, ...rest }) {
    if (!visible) {
        return null;
    }
    return <TextField variant="standard" value={state} sx={{ width: "100%" }}
        type="number" InputProps={{ inputProps: { min: 0 } }} label={label}
        onChange={(event) => setState(parseFloat(event.target.value))} {...rest} />;
}

/**
 * A `Checkbox` and relevant elements designed to reflect and update a boolean
 * state value.
 * @param {Object} parameters Any unknown parameters will be added
 *      directly to the root component.
 * @param {string} parameters.label The name of the field.
 * @param {number} parameters.state A state variable or hardcoded value
 *      to fill the field with on render.
 * @param {CallableFunction} parameters.setState A state setter callable
 *      to call whenever the field is changed.
 */
function CheckboxField({ label, state = false, setState, ...rest }) {
    return (
        <FormControlLabel
            labelPlacement="end" // Change label placement to "end"
            control={<Checkbox defaultChecked={state} onChange={(event, value) => setState(value)} />}
            sx={{ width: "100%" }}
            label={label}
            {...rest}
        />
    );
}

// Combo Box Function
function ComboBox({ label, options, state, setState, ...rest }) {
    return (
        <FormControl sx={{ width: '100%' }}>
            <InputLabel>{label}</InputLabel>
            <Select
                label={label}
                value={state}
                onChange={(event) => setState(event.target.value)}
                {...rest}
            >
                {options.map((option) => (
                    <MenuItem key={option.value} value={option.value}>
                        {option.label}
                    </MenuItem>
                ))}
            </Select>
        </FormControl>
    );
}

/**
 * A base `Card` with several cool features that appear based on the parameters.
 * @param {Object} parameters Any unknown parameters will be added
 *      directly to the root component.
 * @param {Element} parameters.icon An optional MUI Icon for large display,
 *      and to be the click trigger for any activation behavior.
 * @param {string} parameters.color A CSS color for the icon.
 *      Defaults to grey (#888888).
 * @param {string} parameters.title An optional heading.
 * @param {CallableFunction} parameters.isActive An optional callable that
 *      when called should return a boolean specifying if this card is "active"
 *      or not. When "inactive", the card will appear greyed out. This is
 *      mainly for the subclasses to implement "radio" and "checkbox" functionality.
 *      Defaults to always returning true, making the card always active.
 * @param {CallableFunction} parameters.makeActive An optional callable to be
 *      called whenever the icon is clicked. It should do something to modify
 *      the active state of the card.
 * @param {Array} parameters.children Any contents. Auto-populated by JSX.
 * @param {number} parameters.width An optional integer specifying the grid
 *      span of the card. Default is 1, but larger values can be used to
 *      make the card take up the width of 2 cards, 3, etc.
 */
function ClickableCard({ icon, color = "grey", title, tooltip, isActive = () => true, makeActive, children, width = 1, ...rest }) {
    return (
        <Card variant="outlined" sx={{
            height: "100%", transition: "filter 0.25s ease", gridColumn: `span ${width}`,
            filter: isActive() ? "none" : "grayscale(1) opacity(0.33) brightness(0.85)"
        }} {...rest}>
            <Box sx={{
                display: "flex", p: 3, columnGap: 2,
                alignItems: children ? "start" : "center"
            }}>
                {icon && <Tooltip title={tooltip} placement="top">
                    {cloneElement(icon, {
                        sx: { fontSize: "67px", color: color },
                        onClick: makeActive,
                    })}
                </Tooltip>}
                <Box sx={{ display: "flex", flexDirection: width > 1 ? "row" : "column", columnGap: 3, rowGap: 1 }}>
                    {title && <Typography variant="h2">{title}</Typography>}
                    {children}
                </Box>
            </Box>
        </Card>
    );
}

/**
 * A `ClickableCard` designed to reflect a boolean state value, able to be
 * toggled on and off by clicking its icon.
 * @param {Object} parameters Any unknown parameters will be added
 *      directly to the root component.
 * @param {boolean} parameters.state A state variable to define if
 *      this card is active or not.
 * @param {CallableFunction} parameters.setState A state setter callable
 *      to call when the active state is changed.
 */
function CheckboxCard({ state, setState, ...rest }) {
    return <ClickableCard tooltip={`Click to ${state ? "disable" : "enable"} this product`}
        isActive={() => !!state} makeActive={() => setState(!state)} {...rest} />;
}

/**
 * A `ClickableCard` designed to share a state variable among a few other
 * `ClickableCard`s, able to set the state value to itself by clicking its icon.
 * @param {Object} parameters Any unknown parameters will be added
 *      directly to the root component.
 * @param {boolean} parameters.state A state variable to define if
 *      this card is active or not.
 * @param {CallableFunction} parameters.setState A state setter callable
 *      to call when the active state is changed.
 */
function RadioCard({ state, setState, value, ...rest }) {
    const makeActive = () => {
        // If the current state is already equal to the provided value, deselect it
        setState((prevState) => (prevState === value ? null : value));
    };

    return <ClickableCard tooltip="Click to select this product"
        isActive={() => state === value} makeActive={makeActive} {...rest} />;
}

/**
 * Step One contains user-friendly checkboxes, number fields, and toggleable
 * cards. These set React state variables, which in turn trigger effects that
 * update the IndexedDB offline cache. The cards do not necessarily correlate
 * one-to-one with Sku fields; rather, they set default values that can be
 * tweaked in Step Two. The effects can react to as many fields at once as
 * desired. This means that this step should only be accessible when starting a
 * new configuration, and should not be accessible once advancing to Step Two.
 */
export default function StepOne() {

    // Ideally these could be dynamic.
    const ADP_BANDS = [
        { value: 0, max: 8, label: "A" },
        { value: 9, max: 18, label: "B" },
        { value: 19, max: 28, label: "C" },
        { value: 29, max: 38, label: "D" },
        { value: 39, max: 48, label: "E" },
    ];
    const ADP_TIERS = [
        { value: 1, label: "Tier 1" },
        { value: 2, label: "Tier 2" },
        { value: 3, label: "Tier 3" },
    ];

    const {
        serverState, setServerState, ServerManagementEnabled, setServerManagementEnabled, DesktopPatchingEnabled, setDesktopPatchingEnabled,
        EucEnabled, setEucEnabled, BackupsEnabled, setBackupsEnabled, KeeperEnabled, setKeeperEnabled, EmailEnabled, setEmailEnabled,
        SiemEnabled, setSiemEnabled, EndpointProtectionEnabled, setEndpointProtectionEnabled, NetworkManagementEnabled, setNetworkManagementEnabled,
        DataCenterOnPremises, setDataCenterOnPremises, DataCenterChoiceCloud, setDataCenterChoiceCloud, DataCenterMicrosoftAzure, setDataCenterMicrosoftAzure,
        EucUsers, setEucUsers, HsdImages, setHsdImages, VdiImages, setVdiImages, EucType, setEucType, CitrixType, setCitrixType,
        ServerManagementQuantity, setServerManagementQuantity, DesktopPatchingQuantity, setDesktopPatchingQuantity, ServerPatchingOnly, setServerPatchingOnly,
        KeeperInstances, setKeeperInstances, EmailProtection, setEmailProtection, EndpointProtectionServers, setEndpointProtectionServers,
        EndpointProtectionDesktops, setEndpointProtectionDesktops, NetworkSwitches, setNetworkSwitches, NetworkFirewalls, setNetworkFirewalls,
        NetworkFirewallsQty, setNetworkFirewallsQty, PaloAltoLicenses, setPaloAltoLicenses, SecSeries, setSecSeries,
        NetworkLoadBalancers, setNetworkLoadBalancers, adpEnabled, setAdpEnabled, adpRubricState, setadpRubricState, rubricOptions,
        adpTier, setAdpTier, adpBand, setAdpBand, adpNodeCount, setadpNodeCount, adpStorage, setAdpStorage, CCColoSpace, setCCColoSpace,
        RackUSpace, setRackUSpace, NPlus1Required, setNPlus1Required, BackupAsAServiceUnitrends, setBackupAsAServiceUnitrends, BackupTotalStorageUnitrends, setBackupTotalStorageUnitrends, 
        BackupProtectedDevicesUnitrends, setBackupProtectedDevicesUnitrends, CustBackupOnsiteDevicesUnitrends, setCustBackupOnsiteDevicesUnitrends, BusinessContinuityDisasterRelief,setBusinessContinuityDisasterRelief,
        BackupPrimaryLocationUnitrends, setBackupPrimaryLocationUnitrends, BackupAlternateLocationUnitrends, setBackupAlternateLocationUnitrends, BCDRType, setBCDRType, BCDRTarget, setBCDRTarget, AssuredDP, setAssuredDP, AssuredDPData, setAssuredDPData,
        Adc, setAdc, AdcCitrix, setAdcCitrix, AdcCitrixCount, setAdcCitrixCount, AdcCitrixCpu, setAdcCitrixCpu, AdcCitrixMem, setAdcCitrixMem,
        LoadBalancerWoCitrix, setLoadBalancerWoCitrix, LoadBalancerWoCitrixCount, setLoadBalancerWoCitrixCount, LoadBalancerWoCitrixCpu, setLoadBalancerWoCitrixCpu,
        LoadBalancerWoCitrixMem, setLoadBalancerWoCitrixMem, CcTier1Storage, setCcTier1Storage, MsWindowsServerTotalQty, setMsWindowsServerTotalQty,
        HostedVdiServerTotalQty, setHostedVdiServerTotalQty, UnixLinuxServerTotalQty, setUnixLinuxServerTotalQty,
        NetworkAppServerTotalQty, setNetworkAppServerTotalQty, Tier1StorageServerTotalQty, setTier1StorageServerTotalQty,
        Tier2StorageServerTotalQty, setTier2StorageServerTotalQty, VcpuServerTotalQty, setVcpuServerTotalQty, VmemoryServerTotalQty, setVmemoryServerTotalQty,
        CitrixSqlServerTotalQty, setCitrixSqlServerTotalQty, SqlServerLicenseServerTotalQty, setSqlServerLicenseServerTotalQty,
        SqlServerEnterpriseLicenseServerTotalQty, setSqlServerEnterpriseLicenseServerTotalQty, CitrixInfrastructureServerTotalQty, setCitrixInfrastructureServerTotalQty,
        ServerTotalsVcpu, setServerTotalsVcpu, ServerTotalsMemory, setServerTotalsMemory, ServerTotalsTier1Storage, setServerTotalsTier1Storage, ServerTotalsTier2Storage, setServerTotalsTier2Storage, 
        M365Backups, setM365Backups, M365BackupsUsers, setM365BackupsUsers, M365BackupsType, setM365BackupsType, CloudBackup, setCloudBackup, CloudBackupData, setCloudBackupData,
        CloudBackupType, setCloudBackupType, SystemInfrastructure, setSystemInfrastructure, Hypervisor, setHypervisor, HypervisorTotal, setHypervisorTotal,
        HypervisorManagement, setHypervisorManagement, HypervisorManagementType, setHypervisorManagementType, AzureCost, setAzureCost, DevicesEnabled, setDevicesEnabled,
        AdditionalNetworkFirewalls, setAdditionalNetworkFirewalls, AdditionalNetworkFirewallsQty, setAdditionalNetworkFirewallsQty, ManagedNetworkSwitches, setManagedNetworkSwitches,
        ManagedNetworkSwitchesQty, setManagedNetworkSwitchesQty, NetworkWifi, setNetworkWifi, NetworkWifiQty, setNetworkWifiQty,
        AdcLicensing, setAdcLicensing, AdcLicensingQty, setAdcLicensingQty, AdcLicensingType, setAdcLicensingType, AdcLicensingVersion, setAdcLicensingVersion,
        AdcAdm, setAdcAdm, AdcAdmCount, setAdcAdmCount, AdcVserver, setAdcVserver, AdcVserverCount, setAdcVserverCount, CitrixLicense, setCitrixLicense,
        CitrixLicenseType, setCitrixLicenseType, ContentCollaberation, setContentCollaberation, ContentCollaberationCount, setContentCollaberationCount,
        EucType2, setEucType2, Nerdio, setNerdio, AvdUsers, setAvdUsers, Contract8x5or12x6, setContract8x5or12x6, Contract8x5or12x6Type, setContract8x5or12x6Type,
        ActiveDirectoryManagement, setActiveDirectoryManagement, ActiveDirectoryManagementOptions, setActiveDirectoryManagementOptions, ActiveDirectoryManagementCount, setActiveDirectoryManagementCount,
        RemoteDesktops, setRemoteDesktops, RemoteDesktopsQty, setRemoteDesktopsQty, ManagedPrintServices, setManagedPrintServices, ManagedPrintServicesLocations, setManagedPrintServicesLocations,
        ManagedPrintServicesUsers, setManagedPrintServicesUsers, EmailTraining, setEmailTraining, EmailLicense, setEmailLicense,
        RiskManagement, setRiskManagement, RiskManagementCount, setRiskManagementCount, SiemCount, setSiemCount, SiemSensorAppliance, setSiemSensorAppliance, SiemSensorApplianceCount, setSiemSensorApplianceCount,
        FirewallTP, setFirewallTP, FirewallWMA, setFirewallWMA, FirewallUrlFiltering, setFirewallUrlFiltering, FirewallDnsSecurity, setFirewallDnsSecurity,
        FirewallSdWan, setFirewallSdWan, FirewallGlobalProtect, setFirewallGlobalProtect, FirewallVPfM, setFirewallVPfM,
        Huntress, setHuntress, HuntressCount, setHuntressCount, HuntressM365Support, setHuntressM365Support, ZeroTrust, setZeroTrust, ZeroTrustCount, setZeroTrustCount,
        Microsoft365, Microsoft365BusBasic, setMicrosoft365BusBasic, Microsoft365BusBasicCount, setMicrosoft365BusBasicCount, Microsoft365BusBasicType, setMicrosoft365BusBasicType, 
        Microsoft365BusPremium, setMicrosoft365BusPremium, Microsoft365BusPremiumCount, setMicrosoft365BusPremiumCount, Microsoft365BusPremiumType, setMicrosoft365BusPremiumType, 
        Microsoft365BusStandard, setMicrosoft365BusStandard, Microsoft365BusStandardCount, setMicrosoft365BusStandardCount, Microsoft365BusStandardType, setMicrosoft365BusStandardType, 
        Microsoft365E3, setMicrosoft365E3, Microsoft365E3Count, setMicrosoft365E3Count, Microsoft365E3Type, setMicrosoft365E3Type, Microsoft365E5, setMicrosoft365E5, Microsoft365E5Count, setMicrosoft365E5Count, 
        Microsoft365E5Type, setMicrosoft365E5Type, StreamlineIT, setStreamlineIT, StreamlineITCount, setStreamlineITCount, TraditionalMspEnabled, setTraditionalMspEnabled,
        Microsoft365Enabled, setMicrosoft365Enabled, MiscellaneousEnabled, setMiscellaneousEnabled, NeedsOnboarding, setNeedsOnboarding, SowRequired, setSowRequired,
        newRows, setNewRows, resetServerState,}
        = useData()

    // Declares a state variable named serverState and its updater function setServerState 
    // initialServerState is an object containing the initial values for various server configurations.      

    // Setting values to Server Table Variables
    useEffect(() => {
        if ((CitrixType === "on_prem" || CitrixType === "hybrid") && NPlus1Required && EucType === "citrix") {
            setServerState(prevState => ({ ...prevState, CitrixWebQty: 2 }));
        } else if ((CitrixType === "on_prem" || CitrixType === "hybrid") && !NPlus1Required && EucType === "citrix") {
            setServerState(prevState => ({ ...prevState, CitrixWebQty: 1 }));
        } else {
            setServerState(prevState => ({ ...prevState, CitrixWebQty: 0 }));
        }
    }, [CitrixType, NPlus1Required, EucType]);
    console.log(serverState.CitrixWebQty);

    useEffect(() => {
        if (CitrixType === "on_prem" && NPlus1Required && EucType === "citrix") {
            setServerState(prevState => ({ ...prevState, CitrixDdcQty: 2 }));
        } else if (CitrixType === "on_prem" && !NPlus1Required && EucType === "citrix") {
            setServerState(prevState => ({ ...prevState, CitrixDdcQty: 1 }));
        } else {
            setServerState(prevState => ({ ...prevState, CitrixDdcQty: 0 }));
        }
    }, [CitrixType, NPlus1Required, EucType]);

    useEffect(() => {
        if ((CitrixType == "cloud" || CitrixType == "hybrid") && NPlus1Required && EucType === "citrix") {
            setServerState(prevState => ({ ...prevState, CitrixCloudConnect: 2 }));
        } else if ((CitrixType == "cloud" || CitrixType == "hybrid") && !NPlus1Required && EucType === "citrix") {
            setServerState(prevState => ({ ...prevState, CitrixCloudConnect: 1 }));
        } else {
            setServerState(prevState => ({ ...prevState, CitrixCloudConnect: 0 }));
        }
    }, [CitrixType, NPlus1Required, EucType]);

    useEffect(() => {
        if ((DataCenterChoiceCloud || (CitrixType == "cloud" && EucType === "citrix")) && NPlus1Required) {
            setServerState(prevState => ({ ...prevState, MsActiveDirectoryQty: 2 }));
        } else if ((DataCenterChoiceCloud || (CitrixType == "cloud" && EucType === "citrix")) && !NPlus1Required) {
            setServerState(prevState => ({ ...prevState, MsActiveDirectoryQty: 1 }));
        } else {
            setServerState(prevState => ({ ...prevState, MsActiveDirectoryQty: 0 }));
        }
    }, [DataCenterChoiceCloud, CitrixType, NPlus1Required, EucType]);

    useEffect(() => {
        if (DataCenterChoiceCloud || ((CitrixType == "cloud" || CitrixType == "on_prem" || CitrixType == "hybrid") && EucType === "citrix")) {
            setServerState(prevState => ({ ...prevState, FileServerProfilesQty: 1 }));
        } else {
            setServerState(prevState => ({ ...prevState, FileServerProfilesQty: 0 }));
        }
    }, [DataCenterChoiceCloud, CitrixType, EucType]);

    useEffect(() => {
        if (CitrixType == "on_prem" && EucUsers < 51 && EucType === "citrix") {
            setServerState(prevState => ({ ...prevState, SqlExpressQty: 1 }));
        } else {
            setServerState(prevState => ({ ...prevState, SqlExpressQty: 0 }));
        }
    }, [CitrixType, EucUsers, EucType]);

    useEffect(() => {
        if (CitrixType == "on_prem" && EucUsers > 50 && EucType === "citrix") {
            setServerState(prevState => ({ ...prevState, SqlServerQty: 1 }));
        } else {
            setServerState(prevState => ({ ...prevState, SqlServerQty: 0 }));
        }
    }, [CitrixType, EucUsers, EucType]);

    useEffect(() => {
        if (HsdImages > 0) {
            setServerState(prevState => ({ ...prevState, MasterHsdImagesQty: HsdImages }));
        } else {
            setServerState(prevState => ({ ...prevState, MasterHsdImagesQty: 0 }));
        }
    }, [HsdImages]);

    useEffect(() => {
        if (HsdImages > 0) {
            setServerState(prevState => ({ ...prevState, WorkerServersQty: Math.round(EucUsers / 8.0) }));
        } else {
            setServerState(prevState => ({ ...prevState, WorkerServersQty: 0 }));
        }
    }, [HsdImages, EucUsers]);

    useEffect(() => {
        if (VdiImages > 0) {
            setServerState(prevState => ({ ...prevState, MasterVdiImagesQty: VdiImages }));
        } else {
            setServerState(prevState => ({ ...prevState, MasterVdiImagesQty: 0 }));
        }
    }, [VdiImages]);

    useEffect(() => {
        if (VdiImages > 0) {
            setServerState(prevState => ({ ...prevState, VdiImagesQty: EucUsers }));
        } else {
            setServerState(prevState => ({ ...prevState, VdiImagesQty: 0 }));
        }
    }, [VdiImages, EucUsers]);

    useEffect(() => {
        if (AdcCitrix && NPlus1Required) {
            setServerState(prevState => ({ ...prevState, AdcCitrixCountQty: AdcCitrixCount * 2 }));
        } else if (AdcCitrix && !NPlus1Required) {
            setServerState(prevState => ({ ...prevState, AdcCitrixCountQty: AdcCitrixCount }));
        } else {
            setServerState(prevState => ({ ...prevState, AdcCitrixCountQty: 0 }));
        }
    }, [AdcCitrix, NPlus1Required, AdcCitrixCount])

    useEffect(() => {
        if (CitrixType == "on_prem" && EucType === "citrix") {
            setServerState(prevState => ({ ...prevState, LicenseServerQty: 1 }));
        } else {
            setServerState(prevState => ({ ...prevState, LicenseServerQty: 0 }));
        }
    }, [CitrixType, EucType])

    useEffect(() => {
        if (LoadBalancerWoCitrix && NPlus1Required) {
            setServerState(prevState => ({ ...prevState, AdcWoCitrixQty: LoadBalancerWoCitrixCount * 2 }));
        } else if (LoadBalancerWoCitrix && !NPlus1Required) {
            setServerState(prevState => ({ ...prevState, AdcWoCitrixQty: LoadBalancerWoCitrixCount }));
        } else {
            setServerState(prevState => ({ ...prevState, AdcWoCitrixQty: 0 }));
        }
    }, [LoadBalancerWoCitrix, NPlus1Required, LoadBalancerWoCitrixCount])

    useEffect(() => {
        if ((NetworkFirewalls && SecSeries != "NoneSelected") && NPlus1Required) {
            setServerState(prevState => ({ ...prevState, FirewallQty: 2 }));
        } else if ((NetworkFirewalls && SecSeries != "NoneSelected") && !NPlus1Required) {
            setServerState(prevState => ({ ...prevState, FirewallQty: 1 }));
        } else {
            setServerState(prevState => ({ ...prevState, FirewallQty: 0 }));
        }
    }, [NetworkFirewalls, SecSeries, NPlus1Required])

    useEffect(() => {
        if (AdcCitrix) {
            setServerState(prevState => ({ ...prevState, AdcVcpu: AdcCitrixCpu }));
        } else {
            setServerState(prevState => ({ ...prevState, AdcVcpu: 0 }));
        }
    }, [AdcCitrix, AdcCitrixCpu])

    useEffect(() => {
        if (LoadBalancerWoCitrix) {
            setServerState(prevState => ({ ...prevState, AdcWoCitrixVcpu: LoadBalancerWoCitrixCpu }));
        } else {
            setServerState(prevState => ({ ...prevState, AdcWoCitrixVcpu: 0 }));
        }
    }, [LoadBalancerWoCitrix, LoadBalancerWoCitrixCpu])

    useEffect(() => {
        if (NetworkFirewalls && (SecSeries == "SEC-50" || SecSeries == "SEC-100" || SecSeries == "SEC-200")) {
            setServerState(prevState => ({ ...prevState, FirewallVcpu: 2 }));
        } else if (NetworkFirewalls && (SecSeries == "SEC-300" || SecSeries == "SEC-1000-HV")) {
            setServerState(prevState => ({ ...prevState, FirewallVcpu: 4 }));
        } else if (NetworkFirewalls && SecSeries == "SEC-500") {
            setServerState(prevState => ({ ...prevState, FirewallVcpu: 8 }));
        } else if (NetworkFirewalls && SecSeries == "SEC-700") {
            setServerState(prevState => ({ ...prevState, FirewallVcpu: 16 }));
        } else {
            setServerState(prevState => ({ ...prevState, FirewallVcpu: 0 }));
        }
    }, [NetworkFirewalls, SecSeries])

    useEffect(() => {
        if (AdcCitrix) {
            setServerState(prevState => ({ ...prevState, AdcMem: AdcCitrixMem }));
        } else {
            setServerState(prevState => ({ ...prevState, AdcMem: 0 }));
        }
    }, [AdcCitrix, AdcCitrixMem])

    useEffect(() => {
        if (LoadBalancerWoCitrix) {
            setServerState(prevState => ({ ...prevState, AdcWoCitrixMem: LoadBalancerWoCitrixMem }));
        } else {
            setServerState(prevState => ({ ...prevState, AdcWoCitrixVcpu: 0 }));
        }
    }, [LoadBalancerWoCitrix, LoadBalancerWoCitrixMem])

    useEffect(() => {
        if (LoadBalancerWoCitrix && NPlus1Required) {
            setServerState(prevState => ({ ...prevState, SqlServerEnterpriseWoCitrixMem: LoadBalancerWoCitrixMem * 2 }));
        } else if (LoadBalancerWoCitrix && !NPlus1Required) {
            setServerState(prevState => ({ ...prevState, SqlServerEnterpriseWoCitrixMem: LoadBalancerWoCitrixMem }));
        } else {
            setServerState(prevState => ({ ...prevState, AdcWoCitrixVcpu: 0 }));
        }
    }, [LoadBalancerWoCitrix, NPlus1Required, LoadBalancerWoCitrixMem])

    useEffect(() => {
        if (NetworkFirewalls && SecSeries == "SEC-50") {
            setServerState(prevState => ({ ...prevState, FirewallMem: 5.5 }));
        } else if (NetworkFirewalls && (SecSeries == "SEC-100" || SecSeries == "SEC-200")) {
            setServerState(prevState => ({ ...prevState, FirewallMem: 6.5 }));
        } else if (NetworkFirewalls && (SecSeries == "SEC-300" || SecSeries == "SEC-1000-HV")) {
            setServerState(prevState => ({ ...prevState, FirewallMem: 9 }));
        } else if (NetworkFirewalls && SecSeries == "SEC-500") {
            setServerState(prevState => ({ ...prevState, FirewallMem: 16 }));
        } else if (NetworkFirewalls && SecSeries == "SEC-700") {
            setServerState(prevState => ({ ...prevState, FirewallMem: 56 }));
        } else {
            setServerState(prevState => ({ ...prevState, FirewallMem: 0 }));
        }
    }, [NetworkFirewalls, SecSeries])

    useEffect(() => {
        if (CitrixType == "cloud" && EucType === "citrix") {
            setServerState(prevState => ({ ...prevState, FileServerProfilesTier1Storage: (100 + (50 * EucUsers)) }));
        } else {
            setServerState(prevState => ({ ...prevState, FileServerProfilesTier1Storage: 100 }));
        }
    }, [CitrixType, EucUsers, EucType])

    useEffect(() => {
        if (NetworkFirewalls && SecSeries != "NoneSelected") {
            setServerState(prevState => ({ ...prevState, FirewallTier1Storage: 60 }));
        } else {
            setServerState(prevState => ({ ...prevState, FirewallTier1Storage: 0 }));
        }
    }, [NetworkFirewalls, SecSeries])

    useEffect(() => {
        if (DataCenterChoiceCloud) {
            setNetworkFirewalls(true)
        } else {
            setNetworkFirewalls(false)
        }
    }, [DataCenterChoiceCloud])

    return (
        <Stack spacing={8}>
            <CardSection title="Data Center Locations">
                <CheckboxCard color="cadetblue" title="On-Premises" icon={<HouseOutlined />} state={DataCenterOnPremises} setState={setDataCenterOnPremises} />
                <CheckboxCard color="steelblue" title="Choice Cloud" icon={<CloudOutlined />} state={DataCenterChoiceCloud} setState={setDataCenterChoiceCloud} />
                <CheckboxCard color="cadetblue" title="Microsoft Azure" icon={<GridViewOutlined />} state={DataCenterMicrosoftAzure} setState={setDataCenterMicrosoftAzure} />
            </CardSection>
            <CardSection title="Additional Options">
                <CheckboxCard color="steelblue" title="N+1 Required" icon={<AddOutlinedIcon />} state={NPlus1Required} setState={setNPlus1Required} />
                <CheckboxCard color="cadetblue" title="Needs Onboarding" icon={<AssignmentOutlinedIcon />} state={NeedsOnboarding} setState={setNeedsOnboarding} />
                <CheckboxCard color="steelblue" title="SOW Required" icon={<ReceiptLongOutlinedIcon />} state={SowRequired} setState={setSowRequired} />
            </CardSection>
            <CardSection title="End-User Computing" expanded={EucEnabled} setExpanded={setEucEnabled}>
                <ClickableCard width={3}>
                    <NumberField label="Citrix Users" state={EucUsers} setState={setEucUsers} />
                    <NumberField label="Master HSD Images" state={HsdImages} setState={setHsdImages} />
                    <NumberField label="Master VDI Images" state={VdiImages} setState={setVdiImages} />
                    <NumberField label="AVD Users" state={AvdUsers} setState={setAvdUsers} />
                    <CheckboxField label="Nerdio" state={Nerdio} setState={setNerdio} />
                </ClickableCard>
                <RadioCard color="steelblue" title="Citrix" icon={<RadarOutlinedIcon />} value="citrix" state={EucType} setState={setEucType} />
                <RadioCard color="cadetblue" title="Azure Virtual Desktop" icon={<GridViewOutlined />} value="avd" state={EucType2} setState={setEucType2} />
                <RadioCard color="steelblue" title="Citrix on AVD" icon={<JoinFullOutlinedIcon />} value="citrix_on_avd" state={EucType} setState={setEucType} />
                <CardSection title="Citrix Locations" direction="vertical" visible={EucType === "citrix" && (DataCenterOnPremises || DataCenterChoiceCloud)}>
                    <RadioCard color="steelblue" title="On-Premises" icon={<HouseOutlined />} value="on_prem" state={CitrixType} setState={setCitrixType} />
                    <RadioCard color="cadetblue" title="Citrix Cloud" icon={<CloudOutlined />} value="cloud" state={CitrixType} setState={setCitrixType} />
                    <RadioCard color="steelblue" title="Citrix Hybrid" icon={<JoinFullOutlinedIcon />} value="hybrid" state={CitrixType} setState={setCitrixType} />
                </CardSection>
                <CardSection title="Citrix License" direction="vertical" visible={EucType === "citrix" && (DataCenterOnPremises || DataCenterChoiceCloud)} >
                    <ClickableCard color="steelblue" title="Citrix License" icon={<AssignmentOutlinedIcon />} >
                        <ComboBox label="Type" options={[
                            { label: "Customer Provided", value: "Customer" },
                            { label: "MS - Citrix DaaS Premium", value: "Premium" },
                            { label: "MS - Citrix DaaS Premium +", value: "PremiumPlus" },
                            { label: "MS - Citrix DaaS Advanced", value: "Advanced" },
                            { label: "MS - Citrix DaaS Advanced +", value: "AdvancedPlus" },
                            { label: "MS - Citrix DaaS Standard for Azure", value: "Azure" },]}
                            state={CitrixLicenseType} setState={setCitrixLicenseType} />
                    </ClickableCard>
                </CardSection>
                {/* <CardSection title="Azure Virtual Desktop Options" direction="vertical" visible={EucType2 === "avd"} >
                    <ClickableCard color="steelblue" title="AVD Count" >
                        <NumberField label="Count" state={AvdUsers} setState={setAvdUsers} />
                    </ClickableCard>
                </CardSection> */}
            </CardSection>
            <CardSection title="ADC" expanded={Adc} setExpanded={setAdc}>
                <CheckboxCard color="steelblue" title="ADC With Citrix" icon={TrackChangesOutlinedIcon} state={AdcCitrix} setState={setAdcCitrix}>
                    <NumberField label="Count" state={AdcCitrixCount} setState={setAdcCitrixCount} />
                    <NumberField label="# of vCPU per ADC" state={AdcCitrixCpu} setState={setAdcCitrixCpu} />
                    <NumberField label="# of Mem per ADC" state={AdcCitrixMem} setState={setAdcCitrixMem} />
                </CheckboxCard>
                <CheckboxCard color="cadetblue" title="Load Balancers w/o Citrix" icon={DeviceHubOutlinedIcon} state={LoadBalancerWoCitrix} setState={setLoadBalancerWoCitrix}>
                    <NumberField label="Count" state={LoadBalancerWoCitrixCount} setState={setLoadBalancerWoCitrixCount} />
                    <NumberField label="# of vCPU per ADC" state={LoadBalancerWoCitrixCpu} setState={setLoadBalancerWoCitrixCpu} />
                    <NumberField label="# of Mem per ADC" state={LoadBalancerWoCitrixMem} setState={setLoadBalancerWoCitrixMem} />
                </CheckboxCard>
                <CheckboxCard color="steelblue" title="ADC Licensing" icon={<AssignmentOutlinedIcon />} state={AdcLicensing} setState={setAdcLicensing} >
                    <NumberField label="Count" state={AdcLicensingQty} setState={setAdcLicensingQty} />
                    <ComboBox label="License Type" options={[
                        { label: "Citrix ADC VPX 10 Mbps", value: "10Mbps" },
                        { label: "Citrix ADC VPX 50 Mbps", value: "50Mbps" },
                        { label: "Citrix ADC VPX 200 Mbps", value: "200Mbps" },
                        { label: "Citrix ADC VPX 1000 Mbps", value: "1000Mbps" },
                        { label: "Citrix ADC VPX 3000 Mbps", value: "3000Mbps" },
                        { label: "Citrix ADC VPX 5000 Mbps", value: "5000Mbps" },
                        { label: "Citrix ADC VPX 8000 Mbps", value: "8000Mbps" },]}
                        state={AdcLicensingType} setState={setAdcLicensingType} />
                    <ComboBox label="License Version" options={[
                        { label: "Standard", value: "Standard" },
                        { label: "Advanced", value: "Advanced" },
                        { label: "Premium", value: "Premium" },]}
                        state={AdcLicensingVersion} setState={setAdcLicensingVersion} />
                </CheckboxCard>
                <CheckboxCard color="steelblue" title="ADC ADM" icon={<AccountTreeOutlinedIcon />} state={AdcAdm} setState={setAdcAdm} >
                    <NumberField label="Count" state={AdcAdmCount} setState={setAdcAdmCount} />
                </CheckboxCard>
                <CheckboxCard color="cadetblue" title="ADC vServer" icon={<SettingsSystemDaydreamOutlinedIcon />} state={AdcVserver} setState={setAdcVserver} >
                    <NumberField label="Count" state={AdcVserverCount} setState={setAdcVserverCount} />
                </CheckboxCard>
            </CardSection>
            <CardSection title="Assured Data Protection" expanded={adpEnabled} setExpanded={setAdpEnabled}>
                <ClickableCard>
                    <FormControl>
                        <FormLabel id="rubric-group-label">Rubric</FormLabel>
                        <RadioGroup
                            aria-labelledby="rubric-group-label"
                            value={adpRubricState}
                            onChange={(event, data) => setadpRubricState(data)}
                        >
                            {rubricOptions.map((option) => (
                                <FormControlLabel
                                    key={option.value}
                                    value={option.value}
                                    control={<Radio />}
                                    label={option.label}
                                />
                            ))}
                        </RadioGroup>
                    </FormControl>
                </ClickableCard>
                <ClickableCard>
                    <FormControl>
                        <FormLabel id="tier-group-label">Tier</FormLabel>
                        <RadioGroup aria-labelledby="tier-group-label" value={adpTier} onChange={(event, data) => setAdpTier(data)}>
                            {ADP_TIERS.map((TIER) => (<FormControlLabel key={TIER.value} value={TIER.value} control={<Radio />} label={TIER.label} />))}
                        </RadioGroup>
                    </FormControl>
                </ClickableCard>
                {(adpRubricState === 'rent') && (
                    <ClickableCard>
                        <FormLabel id="storage-group-label">Storage</FormLabel>
                        <NumberField label="Amount of data BU (TB)" state={adpStorage} setState={setAdpStorage} />
                    </ClickableCard>
                )}
                {adpRubricState === 'own' && adpTier == 1 && (
                    <ClickableCard>
                        <FormLabel id="node-group-label">Storage</FormLabel>
                        <NumberField label="Amount of data BU (TB)" state={adpStorage} setState={setAdpStorage} />
                        <NumberField label="Enter Node Count (One Brik = 4 Nodes)" sx={{ width: '135%' }} state={adpNodeCount} setState={setadpNodeCount} />
                    </ClickableCard>
                )}
                {adpRubricState === 'own' && (adpTier == 2 || adpTier == 3) && (
                    <ClickableCard>
                        <FormLabel id="storage-group-label">Storage</FormLabel>
                        <NumberField label="Amount of data BU (TB)" state={adpStorage} setState={setAdpStorage} />
                    </ClickableCard>
                )}
            </CardSection>
            <CardSection title="Backups" expanded={BackupsEnabled} setExpanded={setBackupsEnabled}>
                <CheckboxCard color="cadetblue" title="Backup as a Service (Unitrends)" icon={<BackupOutlinedIcon />} state={BackupAsAServiceUnitrends} setState={setBackupAsAServiceUnitrends}>
                    <NumberField label="Total Storage" state={BackupTotalStorageUnitrends} setState={setBackupTotalStorageUnitrends} />
                    <NumberField label="Protected Deviced" state={BackupProtectedDevicesUnitrends} setState={setBackupProtectedDevicesUnitrends} />
                    <ComboBox label="Customer Onsite Appliance" options={[
                        { label: "Customer doesn't own onsite appliance", value: "CustOwnsNoAppliance" },
                        { label: "Customer owns onsite appliance", value: "CustOwnsAppliance" },]}
                        state={CustBackupOnsiteDevicesUnitrends} setState={setCustBackupOnsiteDevicesUnitrends} />
                    <ComboBox label="Primary Location" options={[
                        { label: "On Prem", value: "OnPrem" },
                        { label: "Dallas", value: "Dallas" },
                        { label: "Atlanta", value: "Atlanta" },]}
                        state={BackupPrimaryLocationUnitrends} setState={setBackupPrimaryLocationUnitrends} />
                    <ComboBox label="Alternate Location" options={[
                        { label: "On Prem", value: "OnPrem" },
                        { label: "Dallas", value: "Dallas" },
                        { label: "Atlanta", value: "Atlanta" },]}
                        state={BackupAlternateLocationUnitrends} setState={setBackupAlternateLocationUnitrends} />
                </CheckboxCard>
                <CheckboxCard color="steelblue" title="M365 Backups" icon={<BackupOutlinedIcon />} state={M365Backups} setState={setM365Backups}>
                    <NumberField label="# of Users" state={M365BackupsUsers} setState={setM365BackupsUsers} />
                    <ComboBox label="M365 Type" options={[
                        { label: "20 GB", value: "20GB" },
                        { label: "Unlimited", value: "Unlimited" },]}
                        state={M365BackupsType} setState={setM365BackupsType} />
                </CheckboxCard>
                <CheckboxCard color="cadetblue" title="Cloud Backup" icon={<BackupOutlinedIcon />} state={CloudBackup} setState={setCloudBackup} >
                    <NumberField label="Amount of Data (TB)" state={CloudBackupData} setState={setCloudBackupData} />
                    <ComboBox label="Cloud Backup Type" options={[
                        { label: "Foundation", value: "Foundation" },
                        { label: "Enterprise", value: "Enterprise" },]}
                        state={CloudBackupType} setState={setCloudBackupType} />
                </CheckboxCard>
                <CheckboxCard color="steelblue" title="Business Continuity / Disaster Relief" icon={<EmergencyShareOutlinedIcon />} state={BusinessContinuityDisasterRelief} setState={setBusinessContinuityDisasterRelief}>
                    <ComboBox label="BC/DR Type" options={[
                        { label: "Cold Storage", value: "ColdStorage" },
                        { label: "Active - Passive", value: "ActivePassive" },
                        { label: "Active - Active", value: 'ActiveActive' },]}
                        state={BCDRType} setState={setBCDRType} />
                    <ComboBox label="BC/DR Target" options={[
                        { label: "Choice - Atlanta", value: "ChoiceAtlanta" },
                        { label: "Choice - Dallas", value: "ChoiceDallas" },
                        { label: "Azure", value: "Azure" },
                        { label: "AWS", value: "AWS" },
                        { label: "Other", value: "Other" },]}
                        state={BCDRTarget} setState={setBCDRTarget} />
                </CheckboxCard>
            </CardSection>
            <CardSection title="System Infrastructure" expanded={SystemInfrastructure} setExpanded={setSystemInfrastructure}>
                <CheckboxCard color="steelblue" title="Hypervisor" icon={CastOutlinedIcon} state={Hypervisor} setState={setHypervisor} >
                    <NumberField label="Number of Nodes/Hosts" state={HypervisorTotal} setState={setHypervisorTotal} />
                </CheckboxCard>
                <CheckboxCard color="cadetblue" title="HypervisorManagement" icon={ImportantDevicesOutlinedIcon} state={HypervisorManagement} setState={setHypervisorManagement} >
                    <ComboBox label="Hypervisor Management Type" options={[
                        { label: "Nutanix Prism Central", value: "NutanixPrismCentral" },
                        { label: "VMware Vcenter", value: "VMwareVcenter" },]}
                        state={HypervisorManagementType} setState={setHypervisorManagementType} />
                </CheckboxCard>
                <CheckboxCard color="steelblue" title="Azure Infrastructure" icon={<GridViewOutlined />} state={DataCenterMicrosoftAzure} setState={DataCenterMicrosoftAzure} >
                    <NumberField label="Azure Cost" state={AzureCost} setState={setAzureCost} />
                </CheckboxCard>
            </CardSection>
            <CardSection title="Devices" expanded={DevicesEnabled} setExpanded={setDevicesEnabled} >
                <CheckboxCard color="steelblue" title="Additional Network Firewalls" icon={<FireplaceOutlinedIcon />} state={AdditionalNetworkFirewalls} setState={setAdditionalNetworkFirewalls} >
                    <NumberField label="Quantity" state={AdditionalNetworkFirewallsQty} setState={setAdditionalNetworkFirewallsQty} />
                </CheckboxCard>
                <CheckboxCard color="cadetblue" title="Managed Network Switches" icon={<AddLinkOutlinedIcon />} state={ManagedNetworkSwitches} setState={setManagedNetworkSwitches} >
                    <NumberField label="Quantity" state={ManagedNetworkSwitchesQty} setState={setManagedNetworkSwitchesQty} />
                </CheckboxCard>
                <CheckboxCard color="steelblue" title="Network Wifi" icon={<WifiOutlinedIcon />} state={NetworkWifi} setState={setNetworkWifi} >
                    <NumberField label="Quantity" state={NetworkWifiQty} setState={setNetworkWifiQty} />
                </CheckboxCard>
            </CardSection>
            <CardSection title="Traditional MSP" expanded={TraditionalMspEnabled} setExpanded={setTraditionalMspEnabled}>
                <CheckboxCard color="cadetblue" title="Server Management" icon={<StorageOutlinedIcon />} state={ServerManagementEnabled} setState={setServerManagementEnabled}>
                    <NumberField label="Quantity" state={ServerManagementQuantity} setState={setServerManagementQuantity} />
                    <CheckboxField label="Patching Only" state={ServerPatchingOnly} setState={setServerPatchingOnly} />
                </CheckboxCard>
                <CheckboxCard color="steelblue" title="Desktop Patching" icon={<DesktopWindowsOutlinedIcon />} state={DesktopPatchingEnabled} setState={setDesktopPatchingEnabled}>
                    <NumberField label="Quantity" state={DesktopPatchingQuantity} setState={setDesktopPatchingQuantity} />
                </CheckboxCard>
                <CheckboxCard color="cadetblue" title="Contract 8x5 or 12x6" icon={<ReceiptLongOutlinedIcon />} state={Contract8x5or12x6} setState={setContract8x5or12x6}>
                    <ComboBox label="Quantity" options={[
                        { label: "8x5", value: "8x5" },
                        { label: "12x6", value: "12x6" }
                    ]}
                        state={Contract8x5or12x6Type} setState={setContract8x5or12x6Type} />
                </CheckboxCard>
                <CheckboxCard color="cadetblue" title="Active Directory Management" icon={<FolderSharedOutlinedIcon />} state={ActiveDirectoryManagement} setState={setActiveDirectoryManagement}>
                    <ComboBox label="Options" options={[
                        { label: "MS - AD Standard", value: "Standard" },
                        { label: "MS - AD Advanced", value: "Advanced" }
                    ]}
                        state={ActiveDirectoryManagementOptions} setState={setActiveDirectoryManagementOptions} />
                    <NumberField label="User Count" state={ActiveDirectoryManagementCount} setState={setActiveDirectoryManagementCount} />
                </CheckboxCard>
                <CheckboxCard color="steelblue" title="Remote Desktop SAL" icon={<ConnectedTvOutlinedIcon />} state={RemoteDesktops} setState={setRemoteDesktops}>
                    <NumberField label="Licenses Required" state={RemoteDesktopsQty} setState={setRemoteDesktopsQty} />
                </CheckboxCard>
                <CheckboxCard color="cadetblue" title="Managed Print Services" icon={<LocalPrintshopOutlinedIcon />} state={ManagedPrintServices} setState={setManagedPrintServices} >
                    <NumberField label="# of Printers" state={ManagedPrintServicesLocations} setState={setManagedPrintServicesLocations} />
                    <NumberField label="# of Users" state={ManagedPrintServicesUsers} setState={setManagedPrintServicesUsers} />
                </CheckboxCard>
            </CardSection>
            <CardSection title="Microsoft 365" expanded={Microsoft365Enabled} setExpanded={setMicrosoft365Enabled}>
                <CheckboxCard color="cadetblue" title="Microsoft 365 Business Basic" icon={<GridViewOutlined />} state={Microsoft365BusBasic} setState={setMicrosoft365BusBasic} >
                    <NumberField label="# of Users" state={Microsoft365BusBasicCount} setState={setMicrosoft365BusBasicCount} />
                    <ComboBox label="Support Type" options={[
                        { label: "MS - O365 User Management", value: "UserManagement" },
                        { label: "MS - Adv O365 Support", value: "AdvSupport" },]}
                        state={Microsoft365BusBasicType} setState={setMicrosoft365BusBasicType} />
                </CheckboxCard>
                <CheckboxCard color="steelblue" title="Microsoft 365 Business Premium" icon={<GridViewOutlined />} state={Microsoft365BusPremium} setState={setMicrosoft365BusPremium}>
                    <NumberField label="# of Users" state={Microsoft365BusPremiumCount} setState={setMicrosoft365BusPremiumCount} />
                    <ComboBox label="Support Type" options={[
                        { label: "MS - O365 User Management", value: "UserManagement" },
                        { label: "MS - Adv O365 Support", value: "AdvSupport" },
                    ]} state={Microsoft365BusPremiumType} setState={setMicrosoft365BusPremiumType} />
                </CheckboxCard>
                <CheckboxCard color="cadetblue" title="Microsoft 365 Business Standard" icon={<GridViewOutlined />} state={Microsoft365BusStandard} setState={setMicrosoft365BusStandard}>
                    <NumberField label="# of Users" state={Microsoft365BusStandardCount} setState={setMicrosoft365BusStandardCount} />
                    <ComboBox label="Support Type" options={[
                        { label: "MS - O365 User Management", value: "UserManagement" },
                        { label: "MS - Adv O365 Support", value: "AdvSupport" },
                    ]} state={Microsoft365BusStandardType} setState={setMicrosoft365BusStandardType} />
                </CheckboxCard>
                <CheckboxCard color="steelblue" title="Microsoft 365 E3" icon={<GridViewOutlined />} state={Microsoft365E3} setState={setMicrosoft365E3}>
                    <NumberField label="# of Users" state={Microsoft365E3Count} setState={setMicrosoft365E3Count} />
                    <ComboBox label="Support Type" options={[
                        { label: "MS - O365 User Management", value: "UserManagement" },
                        { label: "MS - Adv O365 Support", value: "AdvSupport" },
                    ]} state={Microsoft365E3Type} setState={setMicrosoft365E3Type} />
                </CheckboxCard>
                <CheckboxCard color="cadetblue" title="Microsoft 365 E5" icon={<GridViewOutlined />} state={Microsoft365E5} setState={setMicrosoft365E5}>
                    <NumberField label="# of Users" state={Microsoft365E5Count} setState={setMicrosoft365E5Count} />
                    <ComboBox label="Support Type" options={[
                        { label: "MS - O365 User Management", value: "UserManagement" },
                        { label: "MS - Adv O365 Support", value: "AdvSupport" },
                    ]} state={Microsoft365E5Type} setState={setMicrosoft365E5Type} />
                </CheckboxCard>
            </CardSection>
            <CardSection title="Miscellaneous" expanded={MiscellaneousEnabled} setExpanded={setMiscellaneousEnabled}>
                <CheckboxCard color="cadetblue" title="Keeper" icon={<PasswordOutlinedIcon />} state={KeeperEnabled} setState={setKeeperEnabled}>
                    <NumberField label="Instances" state={KeeperInstances} setState={setKeeperInstances} />
                </CheckboxCard>
                <CheckboxCard color="steelblue" title="Email" icon={<EmailOutlinedIcon />} state={EmailEnabled} setState={setEmailEnabled}>
                    <NumberField label="Quantity" state={EmailProtection} setState={setEmailProtection} />
                    <ComboBox label="Training Required?" options={[
                        { label: "Yes", value: "yes" },
                        { label: "No", value: "no" },]}
                        state={EmailTraining} setState={setEmailTraining} />
                    <ComboBox label="Proofpoint License" options={[
                        { label: "Proofpoint Essentials Advanced SaaS Monthly Fee", value: "Advanced" },
                        { label: "Proofpoint Essentials Beginner SaaS Monthly Fee", value: "Beginner" },
                        { label: "Proofpoint Essentials Business SaaS Monthly Fee", value: "Business" },
                        { label: "Proofpoint Essentials Professional SaaS Monthly Fee", value: "Professional" },]}
                        state={EmailLicense} setState={setEmailLicense} />
                </CheckboxCard>
                <CheckboxCard color="steelblue" title="SIEM as a Service" icon={<LockOutlinedIcon />} state={SiemEnabled} setState={setSiemEnabled} >
                    <NumberField label="User Count" state={SiemCount} setState={setSiemCount} />
                    <ComboBox label="Sensor Appliance?" options={[
                        { label: "Yes", value: "yes" },
                        { label: "No", value: "no" },]}
                        state={SiemSensorAppliance} setState={setSiemSensorAppliance} />
                    <NumberField label="Sensor Appliance Count" visible={SiemSensorAppliance === "yes"} state={SiemSensorApplianceCount} setState={setSiemSensorApplianceCount} />
                </CheckboxCard>
                {/* <CheckboxCard color="steelblue" title="SIEM - Sensor Appliance" icon={<LockOutlinedIcon />} state={SiemSensorAppliance} setState={setSiemSensorAppliance} >
                    <NumberField label="Count" state={SiemSensorApplianceCount} setState={setSiemSensorApplianceCount} />
                </CheckboxCard> */}
                <CheckboxCard color="cadetblue" title="Endpoint Protection" defaultValue={true} icon={<ExpandOutlinedIcon />} state={EndpointProtectionEnabled} setState={setEndpointProtectionEnabled}>
                    <NumberField label="Servers" state={EndpointProtectionServers} setState={setEndpointProtectionServers} />
                    <NumberField label="Desktops" state={EndpointProtectionDesktops} setState={setEndpointProtectionDesktops} />
                </CheckboxCard>
                <CheckboxCard color="steelblue" title="Network Management" icon={<LanguageOutlinedIcon />} state={NetworkManagementEnabled} setState={setNetworkManagementEnabled}>
                    <NumberField label="Switches Quantity" state={NetworkSwitches} setState={setNetworkSwitches} />
                    <NumberField label="Load Balancers" state={NetworkLoadBalancers} setState={setNetworkLoadBalancers} />
                </CheckboxCard>
                <CheckboxCard color="cadetblue" title="Firewalls" icon={<SecurityOutlinedIcon />} state={NetworkFirewalls} setState={setNetworkFirewalls}>
                    <NumberField label="Firewalls Quantity" state={NetworkFirewallsQty} setState={setNetworkFirewallsQty} />
                    <NumberField label="# of Palo Alto Licenses" state={PaloAltoLicenses} setState={setPaloAltoLicenses} />
                    <ComboBox label="SEC Series" options={[
                        { label: "None Selected", value: "NoneSelected" },
                        { label: "SEC-50", value: "SEC-50" },
                        { label: "SEC-100", value: 'SEC-100' },
                        { label: "SEC-200", value: "SEC-200" },
                        { label: "SEC-300", value: "SEC-300" },
                        { label: "SEC-500", value: 'SEC-500' },
                        { label: "SEC-700", value: "SEC-700" },
                        { label: "SEC-1000-HV", value: 'SEC-1000-HV' },]}
                        state={SecSeries} setState={setSecSeries} />
                    <CheckboxField label="Threat Prevention" state={FirewallTP} setState={setFirewallTP} />
                    <CheckboxField label="Wildfire Malware Analysis" state={FirewallWMA} setState={setFirewallWMA} />
                    <CheckboxField label="URL Filtering" state={FirewallUrlFiltering} setState={setFirewallUrlFiltering} />
                    <CheckboxField label="DNS Security" state={FirewallDnsSecurity} setState={setFirewallDnsSecurity} />
                    <CheckboxField label="SD-WAN" state={FirewallSdWan} setState={setFirewallSdWan} />
                    <CheckboxField label="Global Protect" state={FirewallGlobalProtect} setState={setFirewallGlobalProtect} />
                    <CheckboxField label="Virtual Panorama for Management" state={FirewallVPfM} setState={setFirewallVPfM} />
                </CheckboxCard>
                <CheckboxCard color="steelblue" title="CC - Colo Space" icon={<SnippetFolderOutlinedIcon />} state={CCColoSpace} setState={setCCColoSpace}>
                    <NumberField label="Rack U Space" state={RackUSpace} setState={setRackUSpace} />
                </CheckboxCard>
                <CheckboxCard color="cadetblue" title="Content Collaboration (ShareFile)" icon={<Groups2OutlinedIcon />} state={ContentCollaberation} setState={setContentCollaberation}>
                    <NumberField label="Count" state={ContentCollaberationCount} setState={setContentCollaberationCount} />
                </CheckboxCard>
                <CheckboxCard color="cadetblue" title="Risk Management" icon={<GppMaybeOutlinedIcon />} state={RiskManagement} setState={setRiskManagement} >
                    <NumberField label="Count" state={RiskManagementCount} setState={setRiskManagementCount} />
                </CheckboxCard>
                <CheckboxCard color="steelblue" title="Huntress Labs MDR" icon={<SecurityOutlinedIcon />} state={Huntress} setState={setHuntress} >
                    <NumberField label="Count" state={HuntressCount} setState={setHuntressCount} />
                    <CheckboxField label="M365 Support" state={HuntressM365Support} setState={setHuntressM365Support} />
                </CheckboxCard>
                <CheckboxCard color="cadetblue" title="ZeroTrust (ThreatLocker)" icon={<LockOutlinedIcon />} state={ZeroTrust} setState={setZeroTrust} >
                    <NumberField label="Count" state={ZeroTrustCount} setState={setZeroTrustCount} />
                </CheckboxCard>
                <CheckboxCard color="steelblue" title="Streamline IT" icon={<WaterOutlinedIcon />} state={StreamlineIT} setState={setStreamlineIT} >
                    <NumberField label="# of Users" state={StreamlineITCount} setState={setStreamlineITCount} />
                </CheckboxCard>
            </CardSection>
        </Stack>
    );
}
