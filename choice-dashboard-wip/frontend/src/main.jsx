import React, { useEffect } from "react";
import ReactDOM from "react-dom/client";
import { BrowserRouter } from "react-router-dom";
import { CssBaseline, ThemeProvider, createTheme } from "@mui/material";
import App from "./App";
import "./index.css";
import localforage from "localforage";


const theme = createTheme({
  spacing: 8,
  palette: {
    primary: {
      main: "#bb1924",
    },
    secondary: {
      main: "#888888",
    },
    grey: {
      900: "#f5f5f5"
    }
  },
  typography: {
    allVariants: {
      fontFamily: "Source Sans Pro"
    },
    h1: {
      fontSize: "2rem"
    },
    h2: {
      fontSize: "1.67rem"
    },
    h3: {
      fontSize: "1.5rem"
    },
    h4: {
      fontSize: "1.33rem"
    },
    h5: {
      fontSize: "1.25rem"
    },
    h6: {
      fontSize: "0.8rem"
    },
  },
  shape: {
    borderRadius: 8,
  },
});

await localforage.config({
  name: "choiceDashboard",
  version: 3,
  storeName: "quantities",
  description: "Cache for configured quantities in sales wizard"
})

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <BrowserRouter>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <App />
      </ThemeProvider>
    </BrowserRouter>
  </React.StrictMode>
);
