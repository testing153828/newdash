# Stage 1: Build the Node.js backend
FROM node:18.18.1 as backend-builder

WORKDIR /app

# Copy backend package.json and package-lock.json
COPY package*.json ./

# Install backend dependencies
RUN npm install  --omit=dev


# Copy the rest of the backend source code
COPY . .

# Stage 2: Create the final Node.js backend image with ZeroTier
FROM node:18.18.1

WORKDIR /app

# Copy the built backend from the backend-builder stage
COPY --from=backend-builder /app /app

# Copy the .env file into the container
COPY .env .

EXPOSE 3001

CMD ["npm", "run", "start"]
